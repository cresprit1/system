#include <stdio.h>         // printf()
#include <string.h>        // strlen()
#include <fcntl.h>         // O_WRONLY
#include <unistd.h>        // write(), close()
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <math.h>
#include <termios.h>

#include "digital.h"
#include "usergpio.h"

static uint32_t mode = 1;
static uint8_t bits = 8;
static uint32_t speed = 50000; // 25*1000*1000; // 50000;
static uint16_t delay = 0;
static int verbose;

char tx[10]; 
char rx[10]; 

struct spi_ioc_transfer xfer[2]; 

static void pabort(const char *s)
{
	perror(s);
	abort();
}

/*
    spidevlib.c - A user-space program to comunicate using spidev.
                Gustavo Zamboni
*/
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
 
uint8_t buf[10];
uint8_t buf2[10];
 
struct spi_ioc_transfer xfer[2];
 
 
//////////
// Init SPIdev
//////////

int spi_init(char filename[40])
{
   int file;
    __u8    mode, lsb, bits;
    __u32 speed=16000000;
	
 
        if ((file = open(filename,O_RDWR)) < 0)
        {
            printf("Failed to open the bus.");
            /* ERROR HANDLING; you can check errno to see what went wrong */
            exit(1);
            }
 
        ///////////////
        // Verifications
        ///////////////
        //possible modes: mode |= SPI_LOOP; mode |= SPI_CPHA; mode |= SPI_CPOL; mode |= SPI_LSB_FIRST; mode |= SPI_CS_HIGH; mode |= SPI_3WIRE; mode |= SPI_NO_CS; mode |= SPI_READY;
        //multiple possibilities using |
         
	mode = SPI_CPHA; 
 
            if (ioctl(file, SPI_IOC_WR_MODE, &mode)<0)   {
                perror("can't set spi mode");
                return;
                }
        
            if (ioctl(file, SPI_IOC_RD_MODE, &mode) < 0)
                {
                perror("SPI rd_mode");
                return;
                }

            if (ioctl(file, SPI_IOC_RD_LSB_FIRST, &lsb) < 0)
                {
                perror("SPI rd_lsb_fist");
                return;
                }
        //sunxi supports only 8 bits
        
            if (ioctl(file, SPI_IOC_WR_BITS_PER_WORD, (__u8[1]){8})<0)   
                {
                perror("can't set bits per word");
                return;
                }
        
            if (ioctl(file, SPI_IOC_RD_BITS_PER_WORD, &bits) < 0) 
                {
                perror("SPI bits_per_word");
                return;
                }
        
            if (ioctl(file, SPI_IOC_WR_MAX_SPEED_HZ, &speed)<0)  
                {
                perror("can't set max speed hz");
                return;
                }
        
            if (ioctl(file, SPI_IOC_RD_MAX_SPEED_HZ, &speed) < 0) 
                {
                perror("SPI max_speed_hz");
                return;
                }
     
 
    printf("%s: spi mode %d, %d bits %sper word, %d Hz max\n",filename, mode, bits, lsb ? "(lsb first) " : "", speed);
 
    //xfer[0].tx_buf = (unsigned char)buf;
    xfer[0].len = 3; /* Length of  command to write*/
    xfer[0].cs_change = 0; /* Keep CS activated */
    xfer[0].delay_usecs = 0, //delay in us
    xfer[0].speed_hz = 16*1000*1000, //speed
    xfer[0].bits_per_word = 8, // bites per word 8
 
    //xfer[1].rx_buf = (unsigned char) buf2;
    xfer[1].len = 4; /* Length of Data to read */
    xfer[1].cs_change = 0; /* Keep CS activated */
    xfer[0].delay_usecs = 0;
    xfer[0].speed_hz = 16*1000*1000; //25000000;
    xfer[0].bits_per_word = 8;
 
    return file;
}
 
 
 
//////////
// Read n bytes from the 2 bytes add1 add2 address
//////////

int32_t spi_dataread(int file)
{
    int status, i;
    int32_t rawdata; 

    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);
    buf[0] = 0x12; 
    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 1; /* Length of  command to write*/
    xfer[1].rx_buf = (unsigned long) buf2;
    xfer[1].len = 3; /* Length of Data to read */

    status = ioctl(file, SPI_IOC_MESSAGE(2), xfer);

    if (status < 0)
        {
        perror("SPI_IOC_MESSAGE");
        return;
        }

	rawdata = (buf2[0]<<16)|(buf2[1]<<8)|buf2[2]; 

	if ( (rawdata & 0x00800000) > 0 )
		rawdata |= 0xFF000000; 
	else 
		rawdata &= 0x007fffff; 

        return rawdata;   
#if 0 
    printf("env: %02x %02x %02x %02x\n", buf2[0], buf2[1], buf2[2], buf2[3]);
//    printf("ret: ");

       //for ( i = 0 ; i < 3; i++)
       //         printf("%02x: ", buf2[i]); //, buf2[1], buf2[2], buf2[3]);
       //printf("\t");

    return buf2;
#endif 

}
 
char * spi_read(char add1, char add2, int nbytes,int file)
{
    int status, i;
 
    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);
    buf[0] = add1;
    buf[1] = add2;
    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 2+nbytes; /* Length of  command to write*/
    xfer[1].rx_buf = (unsigned long) buf2;
    xfer[1].len = nbytes; /* Length of Data to read */

    status = ioctl(file, SPI_IOC_MESSAGE(2), xfer); 
    if (status < 0)
        {
        perror("SPI_IOC_MESSAGE");
        return;
        }
    printf("env: %02x %02x %02x\n", buf[0], buf[1], buf[2]);

    printf("ret: "); 

	for ( i = 0 ; i < nbytes; i++)
		printf("%02x ", buf2[i]); //, buf2[1], buf2[2], buf2[3]);
	printf("\n");   
 
    return buf2;
} 
 
//////////
// Write n bytes int the 2 bytes address add1 add2
//////////

void spi_write(int add1,int add2,int nbytes,char value[10],int file)
{
    uint8_t   buf[32], buf2[32];
    int status;
 
    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);

    buf[0] = add1;
//    buf[1] = add2;
   
    if (nbytes>=1) buf[1] = value[0];
    if (nbytes>=2) buf[2] = value[1];
    if (nbytes>=3) buf[3] = value[2];
    if (nbytes>=4) buf[4] = value[3];

    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = nbytes+1; /* Length of  command to write*/
    status = ioctl(file, SPI_IOC_MESSAGE(1), xfer);

    if (status < 0)
    {
        perror("SPI_IOC_MESSAGE");
        return;
    }

    printf("env: %02x %02x %02x\n", buf[0], buf[1], buf[2]);
    printf("ret: %02x %02x %02x %02x\n", buf2[0], buf2[1], buf2[2], buf2[3]);
}

int openSPIdev(const char *devicename)
{
	int fd; 
	int ret; 

	fd = open(devicename, O_RDWR); 

        if (fd < 0)
                pabort("can't open device");

        /*
         * spi mode
         */

        ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
        if (ret == -1)
                pabort("can't set spi mode");

        ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
        if (ret == -1)
                pabort("can't get spi mode");

       /*
         * bits per word
         */

        ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
        if (ret == -1)
                pabort("can't set bits per word");

        ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
        if (ret == -1)
                pabort("can't get bits per word");

       /*
         * max speed hz
         */
        ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
        if (ret == -1)
                pabort("can't set max speed hz");

        ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
        if (ret == -1)
                pabort("can't get max speed hz");

        //if (verbose)
        {
                printf("SPI Info \n");
                printf("- spi device : %s\n", devicename);
                printf("- spi mode: %d\n", mode);
                printf("- bits per word: %d\n", bits);
                printf("- max speed: %d Hz (%d KHz)\n", speed, speed/1000);
        }

	return fd; 
}

void spi_srwrite(int fd, char data)
{
    unsigned char   buf[32], buf2[32];
    int status;
 
    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);

    buf[0] = data;
   // buf[1] = add1;
   // buf[2] = add2;

    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 1; // nbytes+3; /* Length of  command to write*/

    status = ioctl(fd, SPI_IOC_MESSAGE(1), xfer);

    if (status < 0)
    {
        perror("SPI_IOC_MESSAGE");
        return;
    }
    //printf("env: %02x %02x %02x\n", buf[0], buf[1], buf[2]);
    //printf("ret: %02x %02x %02x %02x\n", buf2[0], buf2[1], buf2[2], buf2[3]);
}

void transfer_cmd(int fd, uint8_t *tx, uint8_t *rx, size_t len)
{
	int ret;

	struct spi_ioc_transfer tr_cmd = {
	.tx_buf = (unsigned long)tx,
	.rx_buf = (unsigned long)rx,
	.len = len,
	.delay_usecs = delay,
	.speed_hz = speed,
	.bits_per_word = bits,
	};
#if 0
if (mode & SPI_TX_QUAD)
tr_cmd.tx_nbits = 4;
else if (mode & SPI_TX_DUAL)
tr_cmd.tx_nbits = 2;
if (mode & SPI_RX_QUAD)
tr_cmd.rx_nbits = 4;
else if (mode & SPI_RX_DUAL)
tr_cmd.rx_nbits = 2;
if (!(mode & SPI_LOOP)) {
if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
tr_cmd.rx_buf = 0;
else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
tr_cmd.tx_buf = 0;
}
#endif
	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr_cmd);

	if (ret < 1)
	{
		printf("can't send spi messageaaaaaaaaaaaaaa");
	}

	printf("%x:%x:%x:%x\n", (char)rx[0], (char)rx[1], (char)rx[2], (char)rx[3]); 
}

uint8_t send_rx[5]; 
//int file;
 
int readRegister(int fd, char addr1, char addr2)
{
	uint8_t CMD_tx[5]; 

	CMD_tx[0] = addr1; 
	CMD_tx[1] = addr2; 

	transfer_cmd(fd, CMD_tx, send_rx, 2); //sizeof(CMD_STATUS_tx)); 
}

int ctrlSPIdev(char *devname)
{
	char *buffer;
	char buf[10];
	int file; 

	file=spi_init(devname); //dev
	
	buffer=(char *)spi_read(0x20,0x08,10,file); //reading the address 0xE60E
	sleep(1); 

	buf[0] = 0x07; 
//	spi_write(0x07, 0x00, 0, buf, file); // reset
	spi_write(0x08, 0x00, 0, buf, file); // start 
//	spi_write(0x0a, 0x00, 0, buf, file); // stop
	
//	sleep(1); 	
	return file; 
}

int adcStop(int file)
{
	spi_write(0x0a, 0x00, 0, NULL, file); 

	return 0; 
}

int adcStart(int file)
{
	spi_write(0x08, 0x00, 0, NULL, file); 
	return 0; 
}

static int ibuf = 0; 
int32_t adcbuf[4096*3] = {0, }; 
int32_t sumdata = 0; 

void main(void)
{
	int fd, file; 
	int value, cur, i; 
	uint8_t* buffer; 
	int32_t data = 0; 

	/* shift reg enable */ 

	digitalPinMode(GPIO_ADC_SR_nOE, OUTPUT);
	digitalPinMode(GPIO_ADC_SR_nCLR, OUTPUT); 

	digitalPinMode(ADC_DRDY, INPUT); 

	digitalWrite(GPIO_ADC_SR_nCLR, 1); 

	digitalWrite(GPIO_ADC_SR_nOE, 1); 
	usleep(100); 
	digitalWrite(GPIO_ADC_SR_nOE, 0); 

	spi_srwrite(fd, 0x00);	//b00010000 RESET LoW, HR mode=0  
	usleep(1000); 

#if 1
   	fd = openSPIdev(DEVADCSR);

//	spi_srwrite(fd, 0xa8);  // OSR1:OSR0 01 RESET High, HR mode = 1
//	spi_srwrite(fd, 0x20); 	// 250k 
//	spi_srwrite(fd, 0xa0);	// 62.5K 
//	spi_srwrite(fd, 0x60);  // 128k 
//	spi_srwrite(fd, 0xe0);  // 32k

	spi_srwrite(fd, 0xf0); 
	usleep(1000); 

#endif 
	/* ADC */ 

#if 1
	speed = 25*1000*1000;  

	file = openSPIdev(DEVADC); 

	adcStart(file); 
	value = 1; 

	while(1)
	{
		cur = digitalRead(ADC_DRDY); 

		if (value != cur )
		{
			if ( cur == 0 ) 
			{
				data += spi_dataread(file); 
				
//				data = (uint32_t)((buffer[0]<<16)|(buffer[1]<<8)|buffer[2]);
				//printf("result : %08x-%d\n", data, data);   
//  spi_srwrite(fd, 0x20);  
				ibuf++; 	
				if ( ibuf >= 4096 )	
				{
					printf(" result : %d\n", data); 	

					ibuf = 0; 
					data = 0; 
				}
			}
			value = cur; 
		}
	}
	adcStop(file); 

#endif 

	close(file); 
	close(fd); 
}

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#include "nxgpio.h"

#define NXGPIO_MAJOR   501
#define NXGPIO_MINOR    100
#define NAME      "/dev/GPIO_DRV"

#define LED_ON      1
#define LED_OFF      0

int main(int argc, char **argv)
	{
	int iFd;
	int iRtn=0;
	dev_t led_dev;
	unsigned int led_state = 0;
	char * cpBuf = "";
	unsigned char cInput;
	unsigned int uiCnt;
	int iBuf = 0;
	int value = 0;
	
	if(argv[1] == NULL)
	{
		printf("No Argument\n");
		return;
	}

	
   //led_dev = makedev(NXGPIO_MAJOR, NXGPIO_MINOR);
   //mknod(NAME, S_IFCHR|0666, led_dev);

   iFd = open(NAME, O_RDWR);
   //printf("led fd: %d\n", iFd);

   sleep(1);

	if(strcmp(argv[1], "1") == 0)
	{
		ioctl(iFd, GPIOON, 78);
	}
	else if(strcmp(argv[1] , "0") == 0)
	{
		ioctl(iFd, GPIOOFF, 78);
	}
	else
	{
		printf("Wrong value : %s\n", argv[1]);		
	}
	sleep(1);


   return 0;
 }


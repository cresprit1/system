/*
1. 보드에서 i2c 드라이버의 Major, minor 번호를 찾습니다.
    cat /proc/devices

2. nodefile 을 만듭니다.
   mknod /dev/i2c-0   c  Major  minor

3. 프로그램을 작성합니다.  
     fd = open( "/dev/i2c-0", O_RDWR );  // 만들어진 노드파일을 엽니다.

4. 접근할 디바이스의 슬레이브주소를 설정합니다.
     ioctl( fd, I2C_SLAVE, ADDR_MY_DEV );

5.  write 할때
     unsigned char buf[32];

     buf[0] =  0x10;   // 접근할 디바이스의 레지스터 주소
     buf[1] =  0xaa;   // 0x10 번지에 쓸 데이타
     buf[2] =  0xbb;   // 0x11 번지에 쓸 데이타

     rtn = write( fd, buf, 3 );    // reg_addr + data + data  버퍼의 유효개수는 3개이다.
     rtn 값으로 성공했는지 확인한다.

6.  read 할때
     unsigned char buf[32];

     buf[0] =  0x10;   // 접근할 디바이스의 레지스터 주소

     rtn = write( fd, buf, 1 );    // 접근할 레지스터의 주소를 설정한다.
     rtn 값으로 성공했는지 확인한다.

     rtn = read( fd, buf, 2 );    //  2개의 데이타를 읽어온다.
     rtn 값으로 성공했는지 확인한다.

*/

#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>

#define TEMPERATURE
#define HUMIDITY

uint8_t htu21d_address = 0x40; //7 bit address of HTU21D

void delay_ms(int mseconds)
{
	clock_t start_time = clock(); 

	while(clock() < start_time + mseconds)
		; 
}

void HTU21D_Temperature1(int fd, float *r){

    uint8_t trigger_temperature_no_hold_master = 0xE3; //Address to temperature reading with no hold of twi lines
    uint8_t returned_over_I2C[3] = {0,0,0}; //Array to hold data
    uint8_t err_code; 

    returned_over_I2C[0] = trigger_temperature_no_hold_master; //접근할 디바이스의 레지스터 주소
    err_code = write( fd, returned_over_I2C, 1 ); // 접근할 레지스터의 주소를 설정한다.
    //printf("T_W:%d\n", err_code); 

	delay_ms(100); 
    //nrf_delay_ms(50);//Delay 50 ms, which is the maximum measurement time stated in datasheet
    
    err_code = read( fd, returned_over_I2C, 2 );
    //printf("T_R:%d, %d, %d\n", err_code, returned_over_I2C[0], returned_over_I2C[1]);     
//	printf("err:%x\n", err_code); 
	delay_ms(100); 
    // nrf_delay_ms(5);
    uint16_t x = (returned_over_I2C[0]<<8) | returned_over_I2C[1];
    //printf("%d\n", x); 
    float tempRH = x /(float) 65536.0; //2^16 = 65536
    //printf("%f\n", tempRH); 
    float rh = -46.85 + (175.72 * tempRH);
    //printf("%f\n", rh); 
    *r=rh;
}

//void HTU21D_Humidity(int fd, int *r)
void HTU21D_Humidity(int fd, float *r)
{
  uint8_t htu21d_address = 0x40; //7 bit address of HTU21D
  uint8_t trigger_humidity_no_hold_master = 0xE5; //Address to temperature reading with no hold of twi lines
  uint8_t returned_over_I2C[3] = {0,0,0}; //Array to hold data
  uint8_t err_code; 

  returned_over_I2C[0] = trigger_humidity_no_hold_master; //접근할 디바이스의 레지스터 주소
  
  err_code = write( fd, returned_over_I2C, 1 );// 접근할 레지스터의 주소를 설정한다.

  //printf("H_W:%d\n", err_code); 

  delay_ms(100); 
  //nrf_delay_ms(5);
  
  err_code = read( fd, returned_over_I2C, 2 );
  
  //printf("H_R:%d, %d, %d\n", err_code, returned_over_I2C[0], returned_over_I2C[1]);     
  delay_ms(100);
  
  //Calculate humidity
  uint16_t rawHumidity = ((unsigned int) returned_over_I2C[0] << 8) | (unsigned int) returned_over_I2C[1];
  float tempRH = rawHumidity / (float)65536.0; //2^16 = 65536
  float rh = -6 + (125 * tempRH); //From page 14 in datasheet
  //*r = (int)(rh); // this could be change to (int)(rh*10), then one decimal of the humidity data will be kept (but 34.5  = 345 ) 
  *r=rh; 
    
}

extern void SendResultMessage(int ch, char* serial, float *sdata); 

extern void getBlockInfo(char* filename);

char deviceSerial[128] = {0, }; 

void main(int argc, char** argv)
{
  int fd; 
  int ch; 
  int is_comm; 
  float s;
  float hu;
  float sdata[5]; 

  struct timeval val; 
  struct tm *ptm; 

  uint8_t returned_over_I2C[3] = {0,0,0}; //Array to hold data
  uint8_t err_code; 

  int shmid;
  char *buffer;
  char *string;

  memcpy(deviceSerial, "htu21d_test", strlen("htu21d_test"));

  fd = open("/dev/i2c-1", O_RDWR); 
 
  if ( fd == -1 )
  {
	printf(" Device Open Error\n"); 
 	exit(0); 
  } 

  err_code = ioctl(fd, I2C_SLAVE, htu21d_address); 
//  printf("err : %x\n", err_code);  
  returned_over_I2C[0] = 0xFE; 
  err_code = write( fd, returned_over_I2C, 1 );
//  printf("err : %x\n", err_code); 
  usleep(500); 

  getBlockInfo("../info.json");
 
  while(1)
  {
    HTU21D_Temperature1(fd, &s); 

    usleep(100); 
    HTU21D_Humidity(fd, &hu); 
    usleep(100);  

    sdata[0] = s; 
    sdata[1] = hu;  

    printf("htu21d,%2.1f,%s,%2.1f,%s,\n", sdata[0], "C", sdata[1], "\%");
    //SendResultMessage(1, deviceSerial, sdata); 

    //sleep(9); 
  }  // while(1) 
}



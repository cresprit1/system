
cp myservice.service /etc/systemd/system/.

- systemctl start/stop myservice.service
- systemctl enable/disable myservice.service 

aloohService 
 - for TTA aloohService (htu21d, co2)
 - cp alooh.service to /etc/systemd/system/.
 - cp usr.local.src.alooh.tar to /usr/local/arc/alooh
 - systemctl enable alooh.service
 - change hostname "EdgeMINI-II"
 - change macaddress (system/bin/start.sh)

/*
Copyright (c) <2017>, written by jaehoon sim <jayhoon.s@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <time.h>

#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //Used for UART
#include <fcntl.h> //Used for UART

#include <termios.h> //Used for UART
#include <errno.h>
#include <ctype.h>

#include "digital.h"

int uart0 = -1;
int t_flag; /* flag for thread creation */
int exit_flag;
int write_flag; 

pthread_t tid;
extern FILE *stdin;

#define NUM_PRINT_BYTES  16

enum {
    STRING = 1,
    BINARY,
};

enum {
    FALSE,
    TRUE,
};

void init(void)
{
        int i;

        /* LED Port */
        for ( i = LED203_RP00 ; i <= LED208_BP11; i++)
        {
	        digitalPinMode(i, OUTPUT);
		digitalWrite(i, 1); 
	}

        for ( i = SW200_BACK ; i <= SW206_6 ; i++)
                digitalPinMode(i, INPUT);

}

void fprint_bytes(FILE* fp, int type, int length, unsigned char *buffer)
{
	int i; 
	char temp[NUM_PRINT_BYTES] = {0, }; 
	
	for ( i = 0 ; i < length; i++ ) {
		//printf("%02x ", buffer[i]); 
		fprintf(fp, "%02x ", buffer[i]); 
		temp[i%NUM_PRINT_BYTES] = buffer[i]; 
		if (((i + 1) % NUM_PRINT_BYTES) == 0) {
			if (type == STRING) {
			//	printf("\t %s", temp); 
				fprintf(fp, "\t %s", temp); 
			}
			//printf("\n"); 	
			fprintf(fp, "\n"); 
			memset(temp, 0, NUM_PRINT_BYTES); 
		}
	}
	
	if (type == STRING) {
		if (i % NUM_PRINT_BYTES != 0) {
//			printf("\t%s", temp); 	
			fprintf(fp, "\t%s", temp); 
		}
	}
}

void print_bytes(int type, int length, unsigned char *buffer)
{
    int i;
    char temp[NUM_PRINT_BYTES] = {0,};

    for (i = 0; i < length; i++) {
        printf("%02x ", buffer[i]);
        temp[i%NUM_PRINT_BYTES] = buffer[i];
        if (((i + 1) % NUM_PRINT_BYTES) == 0) {
            if (type == STRING) {
                printf("\t%s", temp);
            }
            printf("\n");
            memset(temp, 0, NUM_PRINT_BYTES);
        }
    }
    if (type == STRING) {
        if (i % NUM_PRINT_BYTES != 0)
            printf("\t%s", temp);
    }
}

char buf[2048][6]; 
static int cnt = 0; 

unsigned char read_button(void)
{
	unsigned char value = 0; // 0xiF; 

	value |= digitalRead(SW200_BACK);
	value |= (digitalRead(SW201_HOME)&0x01) << 1; 
	value |= (digitalRead(SW202_UP)&0x01) <<2; 
	value |= (digitalRead(SW203_LEFT)&0x01) <<3; 
	value |= (digitalRead(SW204_RIGHT)&0x01) <<4; 
	value |= (digitalRead(SW205_DOWN)&0x01)<<5; 
 
	return value; 
}

unsigned char read_switch(void)
{
        unsigned char value = 0; // 0xiF;

        value |= (digitalRead(SW206_1)&0x01) << 5; ;
        value |= (digitalRead(SW206_2)&0x01) << 4;
        value |= (digitalRead(SW206_3)&0x01) <<3;
        value |= (digitalRead(SW206_4)&0x01) <<2;
        value |= (digitalRead(SW206_5)&0x01) <<1;
        value |= (digitalRead(SW206_6)&0x01)<<0;

//	printf("value = %x\n", value); 
        return value;
}


void *serial_rx(void *arg)
{
	unsigned char cbutton, pre_button = 0x3F;
	unsigned char cswitch, pre_switch = 0x3F; 

    	t_flag = 1;

	cbutton = pre_button; 
	cswitch = pre_switch; 

    do {
        // Read up to 255 characters from the port if they are there
	
	if ( exit_flag == FALSE) 
	{
		cbutton = read_button();  

        	if ( pre_button != cbutton) 
		{
			pre_button = cbutton;

			switch(pre_button)
			{
				case SW200_BACK_MASK : 
					printf("Push BACK button\n");  	
					break; 
                                case SW201_HOME_MASK :
                                        printf("Push HOME button\n");                                                   break;
                                case SW202_UP_MASK :
                                        printf("Push UP button\n");                                                   break;
                                case SW203_LEFT_MASK :
                                        printf("Push LEFT button\n");                                                   break;
                                case SW204_RIGHT_MASK :
                                        printf("Push RIGHT button\n");                                                   break;
                                case SW205_DOWN_MASK :
                                        printf("Push DOWN button\n");                                                   break;
			}
		}

		cswitch = read_switch(); 
		
		if ( pre_switch != cswitch )
		{
			pre_switch = cswitch; 
			printf(" switch : 0x%2x\n", pre_switch); 
		}
	}

        usleep(100*1000); // 100ms 
    } while (exit_flag == FALSE && write_flag == FALSE);

    pthread_exit(NULL); 
    return NULL;
}

int rx_thread_create(void)
{
    int ret = 0;
    pthread_attr_t attr;

    ret = pthread_attr_init(&attr);
    if (ret != 0) {
        perror("pthread_attr_init failed");
        return -1;
    }

    ret = pthread_create(&tid, &attr, &serial_rx, NULL);
    if (ret != 0) {
        perror("pthread_create failed");
        return -1;
    }

    ret = pthread_attr_destroy(&attr);
    if (ret != 0) {
        perror("pthread_attr_destroy failed");
        return -1;
    }

    return ret;
}
void onAll(void)
{
        int i;

        /* LED Port */
        for ( i = LED203_RP00 ; i <= LED208_BP11; i++)
                digitalWrite(i, 0); 

}

void offAll(void)
{
        int i;

        /* LED Port */
        for ( i = LED203_RP00 ; i <= LED208_BP11; i++)
                digitalWrite(i, 1); 


}

int tx_loop(void)
{
    unsigned char tx_hex[256] = {0,};
    unsigned char tx_bin[128] = {0,};

    unsigned char key;  

    int count = 0;

    do {
        fscanf(stdin, "%c", &key);
//        printf("TX BUFFER : #######\n");
        
//        printf("  hexa string : ");
//        print_bytes(STRING, strlen(tx_hex), tx_hex);
//        printf("  binary : ");
//        print_bytes(BINARY, strlen(tx_hex) / 2, tx_bin);

	switch(key)
	{
		case 'c' : 
			printf(" Off All LEDs\n"); 
			offAll(); 
			break; 

		case 's' : 
			printf(" On All LEDs\n"); 
			onAll(); 
			break; 

		case '1' : 
			printf("R1%s\n", tx_hex); 
		break; 

		case '2' : 
			printf("R2\n", tx_hex); 
			break; 

		default : 
			break; 
	}
#if 0 
        /* Filestrean, bytes to write, number of bytes to write */
        count = write(uart0, &tx_bin[0], strlen(tx_hex) / 2);
        if (count < 0) {
            printf("UART TX error\n");
            close(uart0);
            return -1;
        }
#endif 
        usleep(10000);
    } while (key != 'q'); // (strcmp(tx_hex, "FF") != 0);
    
    exit_flag = TRUE;
}

int main(void)
{
    struct termios options;
    
    int ret = 0;
    void *res;

    printf("Usage : type 'something', then see 'something'\n");
    printf("Usage : type 'q' to exit\n\n");

    init();  

//    printf(" rx_thread_create \n"); 
    
    /* RX */
    rx_thread_create();
    while (t_flag != 1) {
        usleep(10000);
    }

    /* TX */
    ret = tx_loop();
    if (ret < 0)
        return -1;

    pthread_join(tid, NULL); // (void**)&res);
    return 0;
}


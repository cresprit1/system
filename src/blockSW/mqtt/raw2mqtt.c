#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <curses.h>
#include <unistd.h>
#include <math.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>	
#include <sys/types.h>
#include <termios.h>
#include <fcntl.h>

#include <stdio.h> /* printf, sprintf */
#include <stdlib.h> /* exit, atoi, malloc, free */
#include <unistd.h> /* read, write, close */
#include <string.h> /* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> /* struct hostent, gethostbyname */

#include "MQTTClient.h"
#include "cJSON.h"

//int dataBuf[100000]={0,};

unsigned char gPayload[2048] = {0, }; 
int gPayloadlen = 0; 

#define ADDRESS     "tcp://iiot.pino.io:1883"
#define CLIENTID    "Gateway_1"
#define QOS         2
#define TIMEOUT     5000L // 10000L

#define PIG_ALL

//#define PIG_DEV2 

#ifdef PIG_ALL

const unsigned char topicList[15][128] = {
"data/main/SmartFarm/sub/SmartFarm/device/D1/sensor/TP1",
"data/main/SmartFarm/sub/SmartFarm/device/D1/sensor/TP2",
"data/main/SmartFarm/sub/SmartFarm/device/D1/sensor/TP3",
"data/main/SmartFarm/sub/SmartFarm/device/D1/sensor/TP4",
"data/main/SmartFarm/sub/SmartFarm/device/D1/sensor/TP5",
"data/main/SmartFarm/sub/SmartFarm/device/D1/sensor/TP6",
"data/main/SmartFarm/sub/SmartFarm/device/D2/sensor/TP1",
"data/main/SmartFarm/sub/SmartFarm/device/D2/sensor/TP2",
"data/main/SmartFarm/sub/SmartFarm/device/D2/sensor/TP3",
"data/main/SmartFarm/sub/SmartFarm/device/D2/sensor/TP4",
"data/main/SmartFarm/sub/SmartFarm/device/D2/sensor/TP5",
"data/main/SmartFarm/sub/SmartFarm/device/D2/sensor/TP6",
}; 

const unsigned char clientList[15][128] = {
"xpKDjqjzT-yFudKJdnYsqg", 
"R3AThEC-Si6oXDTVka9Ujw", 
"l0NXWUIcT3i6RqveA9ZaZg", 
"jadBLuCLTDqAFD0Lj-mbzA", 
"iKX_A8IkQ3qCR7jnAE7UuA", 
"SreRt8k9SpabhUIcniamzA", 
"UFu1DnkQQzWwW2aiLupxWQ",
"HKsSbNLRR76S0-3TxKaaiA",
"A4NEDxSqRsaGoycmNe_4_w",
"DK3jrHRUQ9CrgW_h-PyCoQ",
"PncJpQCoS7ujS9pPqsMxTw",
"bC130d1iRKGTr4JlzlddRg",
}; 
#endif 

#ifdef PIG_DEV1
unsigned char topicList[10][128] = {
"data/main/SmartFarm/sub/SmartFarm/device/D1/sensor/TP1",
"data/main/SmartFarm/sub/SmartFarm/device/D1/sensor/TP2",
"data/main/SmartFarm/sub/SmartFarm/device/D1/sensor/TP3",
"data/main/SmartFarm/sub/SmartFarm/device/D1/sensor/TP4",
"data/main/SmartFarm/sub/SmartFarm/device/D1/sensor/TP5",
"data/main/SmartFarm/sub/SmartFarm/device/D1/sensor/TP6",
}; 

unsigned char clientList[10][128] = {
"xpKDjqjzT-yFudKJdnYsqg", 
"R3AThEC-Si6oXDTVka9Ujw", 
"l0NXWUIcT3i6RqveA9ZaZg", 
"jadBLuCLTDqAFD0Lj-mbzA", 
"iKX_A8IkQ3qCR7jnAE7UuA", 
"SreRt8k9SpabhUIcniamzA", 
}; 
#endif 

#ifdef PIG_DEV2
unsigned char topicList[10][128] = {
"data/main/SmartFarm/sub/SmartFarm/device/D2/sensor/TP1",
"data/main/SmartFarm/sub/SmartFarm/device/D2/sensor/TP2",
"data/main/SmartFarm/sub/SmartFarm/device/D2/sensor/TP3",
"data/main/SmartFarm/sub/SmartFarm/device/D2/sensor/TP4",
"data/main/SmartFarm/sub/SmartFarm/device/D2/sensor/TP5",
"data/main/SmartFarm/sub/SmartFarm/device/D2/sensor/TP6",
}; 

unsigned char clientList[10][128] = {
"UFu1DnkQQzWwW2aiLupxWQ",
"HKsSbNLRR76S0-3TxKaaiA",
"A4NEDxSqRsaGoycmNe_4_w",
"DK3jrHRUQ9CrgW_h-PyCoQ",
"PncJpQCoS7ujS9pPqsMxTw",
"bC130d1iRKGTr4JlzlddRg",
}; 
#endif 

int makePublishDataOnMqtt2IIOT(float value, int index)
{
	int rc;
	
	cJSON *jsonResult;
	cJSON *jsonArray;

	cJSON *sensor;

	cJSON *data;
	cJSON *sensorArray;
	cJSON *dataPointArray1;
	cJSON *dataPointItem1;

 	char* pResult = NULL;
	struct timeval val;
	struct tm *ptm;

	int i=0;
	char timeBuf[128]={0,};
	char usec[7]={0,};
	char usec_use[4]={0,};
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;

	sensorArray = cJSON_CreateArray();

	/***********/ 
	/* 
	  {
      "sensor_id": "sensorID",
      "d": [{"v": v,
             "t": date_time
             }]
    }
	*/ 
	
	sensor = cJSON_CreateObject();

	dataPointArray1 = cJSON_CreateArray();
	dataPointItem1 =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor, "sensor_id", clientList[index]);
  cJSON_AddNumberToObject(dataPointItem1, "v", value);

	memset(timeBuf, 0, sizeof(timeBuf));
	
	gettimeofday(&val, NULL);
	
  ptm = gmtime(&val.tv_sec);        // UTC Time 
  
	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	
	cJSON_AddStringToObject(dataPointItem1, "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray1, dataPointItem1);
	cJSON_AddItemToObject(sensor, "d", dataPointArray1);
	cJSON_AddItemToArray(sensorArray, sensor);

	pResult = cJSON_Print(sensor);
	
  gPayloadlen = strlen(pResult); 
  
  memset(gPayload, 0, 2048); 
  memcpy(gPayload, pResult, gPayloadlen); 

	cJSON_Delete(sensor);

	return rc;
}

void publishData(int index)
{
    MQTTClient client;
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken token;
    
    int rc;

    rc = MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    printf(" create return %d\n",rc); 
     
    conn_opts.keepAliveInterval = 60;
    conn_opts.cleansession = 1; 
    conn_opts.connectTimeout = 3; 
    
    while(!MQTTClient_isConnected(client)) {
         
    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
    {
        printf("Failed to connect, return code %d\n", rc);
//        MQTTClient_destroy(&client);
    
 //       return; 
    }
    else
    {
  	sleep(2);   
    }
    
    }
     
    pubmsg.payload = gPayload;
    pubmsg.payloadlen = gPayloadlen;
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    
    MQTTClient_publishMessage(client, topicList[index], &pubmsg, &token);
    
    printf("Waiting for up to %d seconds for publication of %s\n"
            "on topic %s for client with ClientID: %s\n",
            (int)(TIMEOUT/1000), gPayload, topicList[index], CLIENTID);
            
    rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
    
    printf("Message with delivery token %d delivered\n", token);
    
    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
        
   printf("publish End\n"); 
}


void *mqtt_thread(void *arg)
{
    MQTTClient client;
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken token;
    int rc;

    while(1)
    {
      usleep(200*1000);   // 200ms 
    
      //printf("gPayloadlen : %d\n", gPayloadlen); 
 #if 0     
      if(gPayloadlen)
      {
    		pthread_mutex_lock(&mqttMutex);
    		
        MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
        conn_opts.keepAliveInterval = 20;
        conn_opts.cleansession = 1;
    
        if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
        {
            printf("Failed to connect, return code %d\n", rc);
            exit(-1);
        }
        
        pubmsg.payload = gPayload;
        pubmsg.payloadlen = gPayloadlen;
        pubmsg.qos = QOS;
        pubmsg.retained = 0;
        
        MQTTClient_publishMessage(client, TOPIC, &pubmsg, &token);
        
        printf("Waiting for up to %d seconds for publication of %s\n"
                "on topic %s for client with ClientID: %s\n",
                (int)(TIMEOUT/1000), gPayload, TOPIC, CLIENTID);
                
        rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
        
        printf("Message with delivery token %d delivered\n", token);
        
        MQTTClient_disconnect(client, 10000);
        MQTTClient_destroy(&client);
        
        gPayloadlen = 0; 
        
        pthread_mutex_unlock(&mqttMutex);		
      }
#endif       

    }; 

    
    return rc;
}

void main(int argc, char** argv)
{
  int i, index; 
  float value; 
  
/*  for ( i = 0 ; i < argc ; i++)
  {
    printf("%s\n", argv[i]);   
  } */
    
  if ( argc > 1 )
  {
    index = atoi(argv[2]); 
    value = atof(argv[1]); 
    
//    printf("%f\n", value); 
 
 	if (index < 12 ) 
 	{   
	    makePublishDataOnMqtt2IIOT(value, index); 
	    publishData(index); 
	}
  }
}


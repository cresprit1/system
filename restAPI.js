var http = require("http");
var fs = require('fs');

var HOSTIP = "218.147.182.24"; 
var PORT = "80"; 

var SERVICEHOSTIP = "218.147.182.24"; 
var SERVICEPORT = "3000"; 

var post2s = function(urlpath, method, data, callback) {

	var options = {
	 "method" : method, 
	 "hostname" : SERVICEHOSTIP, 
	 "port" : SERVICEPORT, 
	 "path" : urlpath, 
 	 "headers": {
		"Content-Type":"application/json"
	 }
	}

        var req = http.request(options, callback);

        req.write(data);
        req.end();
}

var post2o = function(urlpath, method, data, callback) {

	var options = {
	  "method": method,
	  "hostname": HOSTIP,
	  "port": PORT,
	  "path": urlpath,
	  "headers": {
		"Content-Type":"application/json"
	  }
	};
	
	var req = http.request(options, callback); 
	
	if(urlpath == "/deviceOntology/devConfiguration")
	{
		var file = "./blockInfo.json"; 

		fs.readFile(file,'utf8', function(error, data){
			console.log("** blockInfo **");
			console.log(JSON.stringify(data));
			console.log("\n\n");

//			console.log(data); 
 
			req.write(data);
			req.end();
		});
	}	
	else  
	{
		req.write(data); 
		req.end(); 
	}	
} 

module.exports.post2o = post2o;
module.exports.post2s = post2s; 

// post("/deviceOntology/devConfiguration", "POST"); 
// post("/deviceOntology/regiDeviceInfo", "POST"); 



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"
#include "ProcessDevCfgRes.h"


#define SENSOR_TYPE_TXT_WIFI 			"WiFi"
#define SENSOR_TYPE_TXT_COOMPOUND 	"CompoundSensor"
#define SENSOR_TYPE_TXT_CO2 			"CO2Sensor"
#define SENSOR_TYPE_TXT_VIBRATION		"VibrationSensor"
#define SENSOR_TYPE_TXT_RELAY_LAMP	"Relay-Lamp"
#define SENSOR_TYPE_TXT_RELAY_PAN		"Relay-Pan"
#define SENSOR_TYPE_TXT_LCD			"LCD"



char* sensorType[SENSOR_TYPE_MAX] ={
	SENSOR_TYPE_TXT_WIFI, 
	SENSOR_TYPE_TXT_COOMPOUND,
	SENSOR_TYPE_TXT_CO2,
	SENSOR_TYPE_TXT_VIBRATION,
	SENSOR_TYPE_TXT_RELAY_LAMP,
	SENSOR_TYPE_TXT_RELAY_PAN,
	SENSOR_TYPE_TXT_LCD,
};

int checkSensorType(char* _strSensorType)
{
	int type,i;
	
	for(i=0; i<SENSOR_TYPE_MAX; i++)
	{
		if(strcmp(sensorType[i], _strSensorType) == 0)
		{
			type = i;
			break;
		}
	}

	return type;
}

char* getSensorTypeTxt(int _sensorType)
{
	return sensorType[_sensorType];
}

int doProcessDevCfg(char* _jsonData, DevCfgRes* _devCfgRes)
{
	int result=0;
	int i =0;
	int arrayLength = 0;
	cJSON* devApp;
	cJSON* blockList;
	cJSON* typeList = NULL;
	char fileName[256];
	char query[256];
	char* startPos = NULL;
	char* out =  NULL;
	int blockIdLength = 0;
	cJSON *json = cJSON_Parse((const char *)_jsonData);
	if(cJSON_GetObjectItem(json, "devApplication") != NULL)
	{
		if ( cJSON_GetObjectItem(json, "devApplication")->valuestring != NULL )
		{
			printf("%s\n", cJSON_GetObjectItem(json, "devApplication")->valuestring); 
			strcpy(_devCfgRes->devAppUrl, cJSON_GetObjectItem(json, "devApplication")->valuestring);	
			memset(query, 0, sizeof(query));
			memset(fileName, 0, sizeof(fileName));

			startPos = strstr(_devCfgRes->devAppUrl, "smart");
			strcpy(fileName, startPos);
			strcpy(_devCfgRes->serviceName, fileName); 
		
			sprintf(query, "/root/project/OSLO/system/bin/test.sh %s %s %s", _devCfgRes->blockInfo[i].swDriverLink, fileName, "service");
			system(query);
		}
	}
	
	blockList = cJSON_GetObjectItem(json, "blockList");
	arrayLength = cJSON_GetArraySize(blockList);
	printf("BLOCK LIST COUNT %d\n", arrayLength);
	if(arrayLength < 1)
		return -1;
	
	_devCfgRes->blockCount = arrayLength;

	for(;i<arrayLength;i++)
	{
		cJSON* block = cJSON_GetArrayItem(blockList ,i);
		
		_devCfgRes->blockInfo[i].index = atoi(cJSON_GetObjectItem(block ,"index")->valuestring);
		strcpy(_devCfgRes->blockInfo[i].swDriverLink, cJSON_GetObjectItem(block ,"swDriverLink")->valuestring);
		_devCfgRes->blockInfo[i].sType = checkSensorType(cJSON_GetObjectItem(block ,"sensorType")->valuestring);
		strcpy(_devCfgRes->blockInfo[i].blockID, cJSON_GetObjectItem(block ,"blockID")->valuestring);
		_devCfgRes->blockInfo[i].mode =atoi(cJSON_GetObjectItem(block ,"mode")->valuestring);
		_devCfgRes->blockInfo[i].power = cJSON_GetObjectItem(block ,"power")->valueint;
		
		typeList =  cJSON_GetObjectItem(block ,"type");

		out = cJSON_GetArrayItem(typeList ,0)->valuestring;
		printf("type : %s (%d)\n", out, strlen(out)); 
		memset(_devCfgRes->blockInfo[i].type, 0, 64); // sizeof(_defCFgRes->blockInfo[i].type)); 	
		memcpy(_devCfgRes->blockInfo[i].type, &out[0], strlen(out)); 
		memset(fileName, 0, sizeof(fileName));

		startPos = strstr(_devCfgRes->blockInfo[i].swDriverLink, _devCfgRes->blockInfo[i].blockID);
		strcpy(fileName, startPos);

		memset(query, 0, sizeof(query));
		sprintf(query, "/root/project/OSLO/system/bin/test.sh %s %s %s", _devCfgRes->blockInfo[i].swDriverLink, fileName, "block");
		system(query);
	}
	

	return result;
}


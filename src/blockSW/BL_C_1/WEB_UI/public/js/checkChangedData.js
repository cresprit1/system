const NETWORK_SETUP_URL="index.html";
const DAQ_SETUP_URL="daq_setup.html";
const ANALOG_SENSOR_URL="analogsensor.html";
const DIGITAL_SENSOR_URL="digitalsensor.html";
const TIME_SETUP_URL="time_setup.html";
const USB_SENSOR_URL="usbsensor.html";

var arrChanged = new Array();
function add2Array(elname){
	arrChanged[arrChanged.length] = elname;
}

function cleanChanged()
{
	arrChanged.length = 0;
}

function disableNotChange(){
var f = document.frm;
	for (var i=0;i<f.elements.length;i++){
		if (f.elements[i].type=="button" || f.elements[i].type=="submit" || f.elements[i].type=="reset" || f.elements[i].type=="hidden")
			continue;
		if (hasChanged(f.elements[i].name) == true)
			return true;

	}

	return false;//disable submit for testing purposes
}

function hasChanged(elname){
	var found=false;
	for (var j=0;j<arrChanged.length;j++){
		if(arrChanged[j] == "optionsRadiosNType")
		{
			j++;
			continue;
		}
		if (elname==arrChanged[j])
		{
			found=true;
			break;
		}
	}
	return found;
}

function putOnChange(){
	var f = document.frm;
	for (var i=0;i<f.elements.length;i++) {
		f.elements[i].onchange=function(){
			add2Array(this.name)
		}
	}
}




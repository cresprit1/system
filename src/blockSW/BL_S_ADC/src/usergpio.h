#ifndef _USERGPIO_H 

/* OUTPUT */ 
#define GPIO_ADC_CS (32*0+23)
#define GPIO_ADC_SR_nOE  (32*0+25) // (A25)
#define GPIO_ADC_SR_nCLR  (32*0+27) // (A27)

#define SR_OUT_XTR_DIS 		0x001 //QA // CUR, IEPE, 
#define SR_OUT_GAIN_SEL2	0x002 //QB // ADC, CUR, IEPE
#define SR_OUT_GAIN_SEL1	0x004 //QC // ADC, CUR, IEPE
#define SR_OUT_HR_MODE		0x008 //QD // ADC, CUR, IEPE
#define SR_OUT_RESET		0x010 // QE // ADC, CUR, IEPE
#define SR_OUT_OSR0		0x020 // QF // ADC, CUR, IEPE
#define SR_OUT_OSR1		0x040 // QG // ADC, CUR, IEPE 

#define GPIO_CURSR_nOE		(32*0 + 20) // (A20)
#define GPIO_CURSR_nCLR 	(32*0 + 22) // (A22)

#define GPIO_IEPESR_nOE		(32*0 + 9) // (A9)
#define GPIO_IEPESR_nCLR  	(32*0 + 11) // (A11) 

/* INPUT */ 

#define ADC_DRDY		(32*0 + 0) // (A0)

#define IEPE_DRDY		(32*0 + 6) // (A6)
#define IEPE_XTR_ERR 		(32*2 + 26) // (C26) 
#define IEPE_SHORT_ERR		(32*1 + 13) // (B13)
#define IEPE_OPEN_ERR		(32*1 + 15) // (B15)

#define CUR_DRDY		(32*0 + 19) // (A19)
#define CUR_XTR_ERR 		(32*1 + 23) // (B23)
#define CUR_SENSOR_ERR1		(32*1 + 17) // (B17)
#define CUR_SENSOR_ERR2		(32*1 + 19) // (B19)

#define DEVADC		"/dev/spidev2.0" 
#define DEVADCSR 	"/dev/spidev2.1" 
#define DEVCUR 		"/dev/spidev2.2"
#define DEVCURSR 	"/dev/spidev2.3" 
#define DEVCURDAC 	"/dev/spidev2.4" 

#define DEVIEPE 	"/dev/spidev2.5"
#define DEVIEPESR 	"/dev/spidev2.6"
#define DEVIEPEDAC 	"/dev/spidev2.7"
#endif 

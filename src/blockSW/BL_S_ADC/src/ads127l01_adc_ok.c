#include <stdio.h>         // printf()
#include <string.h>        // strlen()
#include <fcntl.h>         // O_WRONLY
#include <unistd.h>        // write(), close()
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/shm.h>
#include <sys/time.h>
#include <math.h>
#include <termios.h>

#include "digital.h"
#include "usergpio.h"
#include "../../lib/Linked_queue.h"
#include <math.h>
#include <sys/time.h>
#include "../../lib/userlib.h"


#define IOCTL_MAGIC     'R'

#define IOCTL_ADC24_INIT        _IOW(IOCTL_MAGIC, 0, int)// IOCTL_TEST_MTV_POWER_ON_INFO)

#define IOCTL_ADC24_START       _IOW(IOCTL_MAGIC, 1, int)//, IOCTL_TEST_MTV_POWER_ON_INFO)
#define IOCTL_ADC24_STOP        _IOW(IOCTL_MAGIC, 2, int)//, IOCTL_TEST_MTV_POWER_ON_INFO)
#define IOCTL_ADC24_ALL         _IO(IOCTL_MAGIC, 3)

#define SAMPLING_RATE           256 // 64 //128

/* for relay statue message */
#define SHARED_MEMORY_ALRAM_KEY 2005
#define MESSAGE_QUEUE_ALRAM_KEY 4500

#define MEMORY_ALRAM_SIZE 32

#define READ_CLIENT_FLAG 0
#define READ_SERVER_FLAG 1
#define PRINT_CLIENT_FLAG 2

int is_comm = 0;

static int device_ch = 0;
static uint32_t mode = 1;
static uint8_t bits = 8;
static uint32_t speed = 8000000; // 50000; // 25*1000*1000; // 50000;
static uint16_t delay = 0;
static int verbose;
static int spi_df;

FILE *outf[3];

char tx[10];
char rx[10];

struct spi_ioc_transfer xfer[2];
//unsigned char gbuffer[SAMPLING_RATE*4] = {0,};
int gbuffer[SAMPLING_RATE*2] = {0,};
int gBufferIndex = 0;
LinkedQueue* gLinkedQueue = NULL;
float sdata[5];
char deviceSerial[128] = {0, };

static void pabort(const char *s)
{
        perror(s);
        abort();
}

int t_flag, wait_flag; /* flag for thread creation */
int exit_flag = 0;
pthread_t tid, tid2;
pthread_t rms_id;

#define NUM_PRINT_BYTES  16

enum {
    STRING = 1,
    BINARY,
};

enum {
    FALSE,
    TRUE,
};

void fprint_bytes(FILE* fp, int type, int length, unsigned char *buffer)
{
        int i;
        char temp[NUM_PRINT_BYTES] = {0, };

        for ( i = 0 ; i < length; i++ ) {
                //printf("%02x ", buffer[i]);
                fprintf(fp, "%02x ", buffer[i]);
                temp[i%NUM_PRINT_BYTES] = buffer[i];
                if (((i + 1) % NUM_PRINT_BYTES) == 0) {
                        if (type == STRING) {
                        //      printf("\t %s", temp);
                                fprintf(fp, "\t %s", temp);
                        }
                        //printf("\n");
                        fprintf(fp, "\n");
                        memset(temp, 0, NUM_PRINT_BYTES);
                }
        }

        if (type == STRING) {
                if (i % NUM_PRINT_BYTES != 0) {
//                      printf("\t%s", temp);
                        fprintf(fp, "\t%s", temp);
                }
        }
}

/*
    spidevlib.c - A user-space program to comunicate using spidev.
                Gustavo Zamboni
*/
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

uint8_t buf[10];
uint8_t buf2[10];

struct spi_ioc_transfer xfer[2];

//////////
// Init SPIdev
//////////

int spi_init(char filename[40])
{
   int file;
    __u8    mode, lsb, bits;
    __u32 speed=16000000;

        if ((file = open(filename,O_RDWR)) < 0)
        {
            printf("Failed to open the bus.");
            /* ERROR HANDLING; you can check errno to see what went wrong */
            exit(1);
            }

        ///////////////
        // Verifications
        ///////////////
        //possible modes: mode |= SPI_LOOP; mode |= SPI_CPHA; mode |= SPI_CPOL; mode |= SPI_LSB_FIRST; mode |= SPI_CS_HIGH; mode |= SPI_3WIRE; mode |= SPI_NO_CS; mode |= SPI_READY;
        //multiple possibilities using |

        mode = SPI_CPHA;

            if (ioctl(file, SPI_IOC_WR_MODE, &mode)<0)   {
                perror("can't set spi mode");
                return;
                }

            if (ioctl(file, SPI_IOC_RD_MODE, &mode) < 0)
                {
                perror("SPI rd_mode");
                return;
                }

            if (ioctl(file, SPI_IOC_RD_LSB_FIRST, &lsb) < 0)
                {
                perror("SPI rd_lsb_fist");
                return;
                }
        //sunxi supports only 8 bits

            if (ioctl(file, SPI_IOC_WR_BITS_PER_WORD, (__u8[1]){8})<0)
                {
                perror("can't set bits per word");
                return;
                }

            if (ioctl(file, SPI_IOC_RD_BITS_PER_WORD, &bits) < 0)
                {
                perror("SPI bits_per_word");
                return;
                }

            if (ioctl(file, SPI_IOC_WR_MAX_SPEED_HZ, &speed)<0)
                {
                perror("can't set max speed hz");
                return;
                }

            if (ioctl(file, SPI_IOC_RD_MAX_SPEED_HZ, &speed) < 0)
                {
                perror("SPI max_speed_hz");
                return;
                }


    printf("%s: spi mode %d, %d bits %sper word, %d Hz max\n",filename, mode, bits, lsb ? "(lsb first) " : "", speed);

    //xfer[0].tx_buf = (unsigned char)buf;
    xfer[0].len = 3; /* Length of  command to write*/
    xfer[0].cs_change = 0; /* Keep CS activated */
    xfer[0].delay_usecs = 0, //delay in us
    xfer[0].speed_hz = 16*1000*1000, //speed
    xfer[0].bits_per_word = 8, // bites per word 8

    //xfer[1].rx_buf = (unsigned char) buf2;
    xfer[1].len = 4; /* Length of Data to read */
    xfer[1].cs_change = 0; /* Keep CS activated */
    xfer[0].delay_usecs = 0;
    xfer[0].speed_hz = 16*1000*1000; //25000000;
    xfer[0].bits_per_word = 8;

    return file;
}



//////////
// Read n bytes from the 2 bytes add1 add2 address
//////////
int32_t spi_dataread(int file)
{
    int status, i;
    int32_t rawdata;

    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);
    buf[0] = 0x12;
    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 1; /* Length of  command to write*/
    xfer[1].rx_buf = (unsigned long) buf2;
    xfer[1].len = 3; /* Length of Data to read */

    status = ioctl(file, SPI_IOC_MESSAGE(2), xfer);
    if (status < 0)
        {
        perror("SPI_IOC_MESSAGE");
        return;
        }

        rawdata = (buf2[0]<<16)|(buf2[1]<<8)|buf2[2];

                if ( (rawdata & 0x00800000) > 0 )
                {
                        rawdata |= 0xFF000000;
                }
                else
                        rawdata &= 0x007fffff;

        return rawdata;
}

char * spi_read(char add1, char add2, int nbytes,int file)
{
    int status, i;

    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);
    buf[0] = add1;
    buf[1] = add2;
    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 2+nbytes; /* Length of  command to write*/
    xfer[1].rx_buf = (unsigned long) buf2;
    xfer[1].len = nbytes; /* Length of Data to read */

    status = ioctl(file, SPI_IOC_MESSAGE(2), xfer);
    if (status < 0)
        {
        perror("SPI_IOC_MESSAGE");
        return;
        }
    printf("env: %02x %02x %02x\n", buf[0], buf[1], buf[2]);

    printf("ret: ");

        for ( i = 0 ; i < nbytes; i++)
                printf("%02x ", buf2[i]); //, buf2[1], buf2[2], buf2[3]);
        printf("\n");

    return buf2;
}

//////////
// Write n bytes int the 2 bytes address add1 add2
//////////

void spi_write(int add1,int add2,int nbytes,char value[10],int file)
{
    uint8_t   buf[32], buf2[32];
    int status;

    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);

    buf[0] = add1;
    buf[1] = add2;

//    if (nbytes>=1) buf[1] = value[0];
//    if (nbytes>=2) buf[2] = value[1];
//    if (nbytes>=3) buf[3] = value[2];
//    if (nbytes>=4) buf[4] = value[3];

    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = nbytes+1; /* Length of  command to write*/
    status = ioctl(file, SPI_IOC_MESSAGE(1), xfer);

    if (status < 0)
    {
        perror("SPI_IOC_MESSAGE");
        return;
    }

//    printf("env: %02x %02x %02x\n", buf[0], buf[1], buf[2]);
//    printf("ret: %02x %02x %02x %02x\n", buf2[0], buf2[1], buf2[2], buf2[3]);
}

int openSPIdev(const char *devicename)
{
        int fd;
        int ret;

        fd = open(devicename, O_RDWR);

        if (fd < 0)
                pabort("can't open device");

        /*
         * spi mode
         */

        ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
        if (ret == -1)
                pabort("can't set spi mode");

        ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
        if (ret == -1)
                pabort("can't get spi mode");

       /*
         * bits per word
         */

        ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
        if (ret == -1)
                pabort("can't set bits per word");

        ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
        if (ret == -1)
                pabort("can't get bits per word");

       /*
         * max speed hz
         */
        ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
        if (ret == -1)
                pabort("can't set max speed hz");

        ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
        if (ret == -1)
                pabort("can't get max speed hz");

        //if (verbose)
        {
                printf("SPI Info \n");
                printf("- spi device : %s\n", devicename);
                printf("- spi mode: %d\n", mode);
                printf("- bits per word: %d\n", bits);
                printf("- max speed: %d Hz (%d KHz)\n", speed, speed/1000);
        }

        return fd;
}

void spi_srwrite(int fd, char data)
{
    unsigned char   buf[32], buf2[32];
    int status;

    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);

    buf[0] = data;
   // buf[1] = add1;
   // buf[2] = add2;

    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 1; // nbytes+3; /* Length of  command to write*/

    status = ioctl(fd, SPI_IOC_MESSAGE(1), xfer);

    if (status < 0)
    {
        perror("SPI_IOC_MESSAGE");
        return;
    }
    //printf("env: %02x %02x %02x\n", buf[0], buf[1], buf[2]);
    //printf("ret: %02x %02x %02x %02x\n", buf2[0], buf2[1], buf2[2], buf2[3]);
}

void transfer_cmd(int fd, uint8_t *tx, uint8_t *rx, size_t len)
{
        int ret;

        struct spi_ioc_transfer tr_cmd = {
        .tx_buf = (unsigned long)tx,
        .rx_buf = (unsigned long)rx,
        .len = len,
        .delay_usecs = delay,
        .speed_hz = speed,
        .bits_per_word = bits,
        };
#if 0
if (mode & SPI_TX_QUAD)
tr_cmd.tx_nbits = 4;
else if (mode & SPI_TX_DUAL)
tr_cmd.tx_nbits = 2;
if (mode & SPI_RX_QUAD)
tr_cmd.rx_nbits = 4;
else if (mode & SPI_RX_DUAL)
tr_cmd.rx_nbits = 2;
if (!(mode & SPI_LOOP)) {
if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
tr_cmd.rx_buf = 0;
else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
tr_cmd.tx_buf = 0;
}
#endif
        ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr_cmd);

        if (ret < 1)
        {
                printf("can't send spi messageaaaaaaaaaaaaaa");
        }

        printf("%x:%x:%x:%x\n", (char)rx[0], (char)rx[1], (char)rx[2], (char)rx[3]);
}

uint8_t send_rx[5];

int readRegister(int fd, char addr1, char addr2)
{
        uint8_t CMD_tx[5];

        CMD_tx[0] = addr1;
        CMD_tx[1] = addr2;

        transfer_cmd(fd, CMD_tx, send_rx, 2); //sizeof(CMD_STATUS_tx));
}

int ctrlSPIdev(char *devname)
{
        char *buffer;
        char buf[10];
        int file;

        file=spi_init(devname); //dev

        buffer=(char *)spi_read(0x20,0x08,10,file); //reading the address 0xE60E
        sleep(1);

        buf[0] = 0x07;
//      spi_write(0x07, 0x00, 0, buf, file); // reset
        spi_write(0x08, 0x00, 0, buf, file); // start
//      spi_write(0x0a, 0x00, 0, buf, file); // stop

//      sleep(1);
        return file;
}

int adcStop(int file)
{
        char buf[2] = {0, };

        buf[0] = 0x0a;
        write(file, buf, 2);
        return 0;
}

int adcStart(int file)
{
        char buf[2] = {0, };

        buf[0] = 0x08;  // start
        write(file, buf, 2);
        return 0;
}

#define MAXVOLTAGE 3.3f
#define BITS_TO_WRITE 8
#define BIT_SHIFT 6

//float singleVoltageStep = 5.0f / pow(2, BITS_TO_WRITE);

float singleVoltageStep = (float)(3.3f / pow(2, 8)); // BITS_TO_WRITE));

void setVoltage(float voltage, int file)
{
  // keep within limits
  if( voltage > MAXVOLTAGE ){ voltage = MAXVOLTAGE; }
  else if( voltage < 0 ){ voltage = 0; }

  // calc and set voltage
  unsigned short value = (unsigned short)(voltage/singleVoltageStep);
  value = value << BIT_SHIFT;
  unsigned char upper = value >> 8;
  unsigned char lower = value & 0xff;

  spi_write(upper, lower, 1, buf, file); // start
}

static int ibuf = 0;

void print_bytes(int type, int length, unsigned char *buffer)
{
    int i;
    char temp[NUM_PRINT_BYTES] = {0,};

    for (i = 0; i < length; i++) {
        printf("%02x ", buffer[i]);
        temp[i%NUM_PRINT_BYTES] = buffer[i];
        if (((i + 1) % NUM_PRINT_BYTES) == 0) {
            if (type == STRING) {
                printf("\t%s", temp);
            }
            printf("\n");
            memset(temp, 0, NUM_PRINT_BYTES);
        }
    }
    if (type == STRING) {
        if (i % NUM_PRINT_BYTES != 0)
            printf("\t%s", temp);
    }
}

//Queue rx_int_queue;

static int ridx = 0;

void *process(void *arg)
{
    int cnt;
    int i;

    do {


       usleep(500);
    } while (exit_flag == FALSE);

    pthread_exit(NULL);
    retur NULL;
}

static int value = 1;

void *serial_rx(void *arg)
{
    int rx_length = 0;
    int value, cur;
    int pBuf[4096] = {0, }; 

    unsigned char *dataAllocPtr = NULL;
    struct timeval timeValue;
    struct tm *ptmValue;

    int i;
    int32_t data;

    t_flag = 1;

    do {
        // Read up to 255 characters from the port if they are there
        if (exit_flag == FALSE)
        {
		pBuf[0] = device_ch;
		rx_length = read(spi_df, pBuf, SAMPLING_RATE);
	
//		printf("serial_rx : %d\n", rx_length); 		

		if ( rx_length )
		{
			memcpy(&gbuffer[gBufferIndex], pBuf, sizeof(int)*rx_length); 
			gBufferIndex += rx_length; 									
		}
			
		if(gBufferIndex >= SAMPLING_RATE)
		{
				CTime time;
				Element* element = (Element*)malloc(sizeof(Element));
				dataAllocPtr = (unsigned char*)malloc(gBufferIndex*sizeof(int));
				memcpy(dataAllocPtr, gbuffer, gBufferIndex*sizeof(int));

				gettimeofday(&timeValue, NULL);
				ptmValue = localtime(&timeValue.tv_sec);
				time.year = ptmValue->tm_year+1900;
				time.month = ptmValue->tm_mon+1;
				time.day = ptmValue->tm_mday;
				time.hour = ptmValue->tm_hour;
				time.min = ptmValue->tm_min;
				time.sec = ptmValue->tm_sec;
				time.usec = timeValue.tv_usec;
				element->time = time;

				element->data = dataAllocPtr;
				element->size = gBufferIndex;
				element->fileName = (unsigned char*)malloc(128);
				sprintf(element->fileName, "%04d_%02d_%02d-%02d_%02d_%02d_%06ld.dat"
, ptmValue->tm_year + 1900, ptmValue->tm_mon + 1, ptmValue->tm_mday
, ptmValue->tm_hour, ptmValue->tm_min, ptmValue->tm_sec, timeValue.tv_usec);
				enqueue(gLinkedQueue, element);
				gBufferIndex = 0;

//				printf(" enqueue\n"); 
			}
//			usleep(100);
        }
    } while (exit_flag == FALSE);

    pthread_exit(NULL);
    retur NULL;
}

double processRMS(int _sampleCnt, int* _pDataBuf, int channelNum)//, CTime _time)
{
#if 1
        int i;

        long double sum  = 0;
        long double rms = 0;

        for(i=0;i<_sampleCnt/*SAMPLING_RATE*/;i++)
        {
                fprintf(outf[channelNum], "%d\n", _pDataBuf[i]);

        //      sum += (long double)(_pDataBuf[i]*_pDataBuf[i]);
//              printf("%d - %Lf - %Lf\n", _pDataBuf[i], (long double)(_pDataBuf[i]*_pDataBuf[i]), sum);
        }

//      rms = sqrtl(sum/(long double)_sampleCnt/*SAMPLING_RATE*/);

//      printf("RMS : %Lf\n", rms);
        return rms;
#else

        long double rms = 0.0f;
        int peak = 0;
        int i;

        for ( i = 0 ; i <SAMPLING_RATE; i++)
        {
                if ( peak < _pDataBuf[i])
                {
                        peak = _pDataBuf[i];
                }
        }

        rms =(long double)( 0.7071*(long double)peak);

        printf("RMS : %Lf\n", rms);
        return rms;
#endif
}

void *rms_execute(void* arg)
{
	Element* element = NULL;
	int ret = 0;

	int queueLength = 0;
	int* pBuf = NULL;
	double rms = -1;
	int count = 0;

	while (1) {
			queueLength = getLength(gLinkedQueue);

			if(queueLength > 0)
			{
				int i, j, cnt = 1 ; 
              printf("start : %d\n", queueLength);

				if ( exit_flag == TRUE ) cnt = queueLength;
	
				for ( j = 0 ; j < cnt ; j++) 
				{
				element = dequeue(gLinkedQueue);
				pBuf = (int*)malloc(element->size*sizeof(int));

				memcpy(pBuf, element->data, element->size*sizeof(int)); 

#if 1 				
				for ( i = 0 ; i < element->size ; i++)
					fprintf(outf[device_ch], "%d\n", pBuf[i]); 
				fflush(outf[device_ch]); 
				//	printf("%d\n", pBuf[i]); 
#endif 				
				free(pBuf); 
				
				free(element->fileName);
				free(element->data);
				free(element);
				}
			}

			if ( exit_flag == TRUE )
				break; 

			usleep(4000); 

	} //while (exit_flag == FALSE);

    pthread_exit(NULL);
    retur NULL;
}

int rms_thread_create(void)
{
    int ret = 0;
    pthread_attr_t attr;

    ret = pthread_attr_init(&attr);
    if (ret != 0) {
        perror("pthread_attr_init failed");
        return -1;
    }

    ret = pthread_create(&rms_id, &attr, &rms_execute, NULL);
    if (ret != 0) {
        perror("pthread_create failed");
        return -1;
    }

    ret = pthread_attr_destroy(&attr);
    if (ret != 0) {
        perror("pthread_attr_destroy failed");
        return -1;
    }

    return ret;
}


int rx_thread_create(void)
{
    int ret = 0;
    pthread_attr_t attr;

    ret = pthread_attr_init(&attr);
    if (ret != 0) {
        perror("pthread_attr_init failed");
        return -1;
    }

    ret = pthread_create(&tid, &attr, &serial_rx, NULL);

    if (ret != 0) {
        perror("pthread_create failed");
        return -1;
    }
#if 0
    ret = pthread_create(&tid2, &attr, &process, NULL);

    if ( ret != 0) {
        perror("process pthread_create failed");
        return -1;
    }
#endif
    ret = pthread_attr_destroy(&attr);
    if (ret != 0) {
        perror("pthread_attr_destroy failed");
        return -1;
    }

    return ret;
}

int tx_loop(void)
{
    unsigned char tx_hex[10] = {0,};
    int count = 0;

        char *buffer_shm;
        char *string_shm;
        int shmid;
        int result;

        if ( is_comm == 3 )
        {
        shmid = shmget((key_t)SHARED_MEMORY_ALRAM_KEY, 0, 0);

        if ( shmid == -1 )
        {
                perror("shmat failed:");
                exit(0);
        }

        buffer_shm = (char*)shmat(shmid, NULL, 0);
        if ( buffer_shm == (char*)-1) {
                perror("shmat failed:");
                exit(0);
        }

        buffer_shm[0] = READ_CLIENT_FLAG;
        string_shm = buffer_shm + 1;

        }

    do {
#ifdef FOR_CERTI
        fscanf(stdin, "%s", tx_hex);
        printf("TX BUFFER : #######\n");

        printf("  hexa string : ");
        print_bytes(STRING, strlen(tx_hex), tx_hex);
#else
        if ( is_comm == 3 )
        {
    //          if ( sdata[0] < 5000.0 )
                {
                        result = (int)sdata[0]%2;

                        if(buffer_shm[0] == READ_CLIENT_FLAG)
                        {
                                printf("message:");
                                sprintf(string_shm, "%d", result);
                                printf("%s\n", string_shm);
                                string_shm[strlen(string_shm)]='\0';
                                buffer_shm[0] = READ_SERVER_FLAG;
                        }
                        else if (buffer_shm[0] == PRINT_CLIENT_FLAG)
                        {
                                puts(string_shm);
                                buffer_shm[0] = READ_CLIENT_FLAG;
                        }
                 }
        }
        sleep(1);
#endif
    } while (strcmp(tx_hex, "FF") != 0);

    exit_flag = TRUE;
}

int recBuf[2048]= {0, };
//Queue RX_int_queue;
int fd;

void adc_init(void)
{
        outf[0] = fopen("./adc_data.data", "w");

        /* shift reg enable */

        digitalPinMode(GPIO_ADC_SR_nOE, OUTPUT);
        digitalPinMode(GPIO_ADC_SR_nCLR, OUTPUT);
        digitalPinMode(GPIO_ADC_CS, OUTPUT);
        digitalPinMode(ADC_DRDY, INPUT);

        digitalWrite(GPIO_ADC_SR_nCLR, 1);
        usleep(100);
        digitalWrite(GPIO_ADC_SR_nOE, 1);
        usleep(100);
        digitalWrite(GPIO_ADC_SR_nOE, 0);
        usleep(100);
#if 1
        fd = openSPIdev(DEVADCSR);
        spi_srwrite(fd, 0xa8);  // 64k or 16k OSR1:OSR0 01 RESET High, HR mode = 1
 //     spi_srwrite(fd, 0x60); // 128x //250k
    //  spi_srwrite(fd, 0xe8); // 32k or 4k
 //       usleep(1000);

        sleep(1);
        close(fd);
        printf(" ADC SR ON\n");
#endif
}

void cur_init(void)
{
        outf[1] = fopen("./cur_data.data", "w");

        /* shift reg enable */

        digitalPinMode(GPIO_CURSR_nOE, OUTPUT);
        digitalPinMode(GPIO_CURSR_nCLR, OUTPUT);
        digitalPinMode(CUR_DRDY, INPUT);

        digitalWrite(GPIO_CURSR_nCLR, 1);
        usleep(100);
        digitalWrite(GPIO_CURSR_nOE, 1);
        usleep(100);
        digitalWrite(GPIO_CURSR_nOE, 0);
        usleep(100);

        fd = openSPIdev(DEVCURSR);
        spi_srwrite(fd, 0xa8);  // 64k or 16k OSR1:OSR0 01 RESET High, HR mode = 1
 //     spi_srwrite(fd, 0x60); // 128x //250k
    //  spi_srwrite(fd, 0xe8); // 32k or 4k

        sleep(1);
        close(fd);
        printf(" CUR SR ON\n");
}

void iepe_init(void)
{
        outf[2] = fopen("./iepe_data.data", "w");

        /* shift reg enable */

        digitalPinMode(GPIO_IEPESR_nOE, OUTPUT);
        digitalPinMode(GPIO_IEPESR_nCLR, OUTPUT);
        digitalPinMode(IEPE_DRDY, INPUT);

        digitalWrite(GPIO_IEPESR_nCLR, 1);
        usleep(100);
        digitalWrite(GPIO_IEPESR_nOE, 1);
        usleep(100);
        digitalWrite(GPIO_IEPESR_nOE, 0);
        usleep(100);
#if 1
        fd = openSPIdev(DEVIEPESR);
        spi_srwrite(fd, 0xa8);  // 64k or 16k OSR1:OSR0 01 RESET High, HR mode = 1
 //     spi_srwrite(fd, 0x60); // 128x //250k
    //  spi_srwrite(fd, 0xe8); // 32k or 4k

        sleep(1);

        printf(" IEPE SR On\n");
        close(fd);
#endif
}

void main(int argc, char** argv)
{
        int value, cur, i, ret;

        //uint8_t* buffer;
        int32_t data;
        int cnt = 0;

//      int is_comm = 0;

        exit_flag = FALSE;

#ifndef FOR_CERTI
        if ( argc > 1 )
        {
                  is_comm = atoi(argv[1]);
                  printf(" is_comm : %d\n", is_comm);
        }

        if ( argc > 2 )
                  ch = atoi(argv[2]);
        else
                  ch = 0;

        if ( argc > 3 )
                  memcpy(deviceSerial, argv[3], strlen(argv[3]));

        printf("deviceSerial : %s\n", deviceSerial);
        printf("ch : %d\n", ch);

        getBlockInfo("../info.json");
#endif

        if ( argc > 1 )
        {
                device_ch = atoi(argv[1]);
                printf(" device : %d\n", device_ch);
        }

        gLinkedQueue = createNode();

        if ( device_ch == 3 )
        {
                adc_init();
                cur_init();
                iepe_init();
        }
        else
        {
                switch(device_ch)
                {
                case 0 :
                        adc_init();
                break;

                case 1 :
                        cur_init();
                break;

                case 2 :
                        iepe_init();
                break;
                }
        }

        printf(" Completed Status Register\n");

        usleep(1000);

//	rx_thread_create();
//        rms_thread_create();
//        printf(" rms_thread_create\n");

        printf("open nxp-adc24-spi\n");
        spi_df = open("/dev/nxp-adc24-spi", O_RDWR);

        if ( spi_df < 0 )
        {
                printf("nxp-adc24-spi device open error : %d\n", spi_df);
                exit(0);
        }

        if (device_ch == 3 )
        {
                for ( i = 0 ; i < device_ch ; i++ )
                {
                        ioctl(spi_df, IOCTL_ADC24_INIT, i);
                        usleep(10000);
                }

                ioctl(spi_df, IOCTL_ADC24_START, device_ch);
                usleep(10000);

                printf(" ready\n");
        }
        else
        {
                ioctl(spi_df, IOCTL_ADC24_INIT, device_ch);
                usleep(10000);
                ioctl(spi_df, IOCTL_ADC24_START, device_ch);
               // usleep(10000);
        }

        printf("ADC module ready\n");

        value = 1;

	rx_thread_create(); 
	rms_thread_create(); 
 
        while (t_flag != 1) {
                usleep(100);
        }

        printf(" thread ok\n");
#if 1

        ret = tx_loop();

        if ( ret < 0 )
                return -1;

        printf("exit tx_loop\n");

        pthread_join(rms_id, NULL);

        ioctl(spi_df, IOCTL_ADC24_STOP, device_ch);

        if ( device_ch < 3 )
        {
//                ioctl(spi_df, IOCTL_ADC24_STOP, device_ch);
                fclose(outf[device_ch]);
        }
        else
        {
 //               ioctl(spi_df, IOCTL_ADC24_STOP, device_ch);
                fclose(outf[0]);
                fclose(outf[1]);
                fclose(outf[2]);
        }

        close(spi_df);
        printf("A\n");

        while (wait_flag) {
                usleep(500);
                printf(".");
        }

        printf("B\n");

//      pthread_join(tid, NULL);
//      pthread_join(tid2, NULL);
//      pthread_join(rms_id, NULL);

#endif

        printf("D\n");
//      close(fd);
        return;
}

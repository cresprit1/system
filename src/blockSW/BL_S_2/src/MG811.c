/*******************Demo for MG-811 Gas Sensor Module V1.1*****************************
Author:  Tiequan Shao: tiequan.shao@sandboxelectronics.com
         Peng Wei:     peng.wei@sandboxelectronics.com
         
Lisence: Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0)

Note:    This piece of source code is supposed to be used as a demostration ONLY. More
         sophisticated calibration is required for industrial field application. 
         
                                                    Sandbox Electronics    2012-05-31
************************************************************************************/

#include <stdio.h>
#include <math.h> 
#include <sys/shm.h>
#include "../../lib/userlib.h"

#include "analog.h"
#include "digital.h"
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#define SHARED_MEMORY_ALRAM_KEY 2005
#define MESSAGE_QUEUE_ALARM_KEY 4500

#define MEMORY_ALRAM_SIZE 32

#define SHARED_MEMORY_KEY 1005
#define MEMORY_SIZE 200

#define READ_CLIENT_FLAG 0
#define READ_SERVER_FLAG 1
#define PRINT_CLIENT_FLAG 2

int is_comm = 0; 

float sdata[5]; 
char deviceSerial[128] = {0, }; 
int ch = 0; 

/************************Hardware Related Macros************************************/
#define         MG_PIN                       (0)     //define which analog input channel you are going to use
#define         BOOL_PIN                     (10)
#define         DC_GAIN                      (8.5) // 3 : 0.6 -> 1.8 , 8.5 0.6 -> 5 (8.5)   //define the DC gain of amplifier

/***********************Software Related Macros************************************/
#define         READ_SAMPLE_INTERVAL         (50)    //define how many samples you are going to take in normal operation
#define         READ_SAMPLE_TIMES            (5)     //define the time interval(in milisecond) between each samples in 
                                                     //normal operation

/**********************Application Related Macros**********************************/
//These two values differ from sensor to sensor. user should derermine this value.
//#define         ZERO_POINT_VOLTAGE           (0.220) //define the output of the sensor in volts when the concentration of CO2 is 400PPM
#define ZERO_POINT_VOLTAGE (0.85/8.5) // (0.260)//(0.39) //(0.324) 
#define         REACTION_VOLTGAE       (0.01) //      (0.010) //define the voltage drop of the sensor when move the sensor from air into 1000ppm CO2

/*****************************Globals***********************************************/
float           CO2Curve[3]  =  {2.602,ZERO_POINT_VOLTAGE,(REACTION_VOLTGAE/(2.602-3))};   
                                                     //two points are taken from the curve. 
                                                     //with these two points, a line is formed which is
                                                     //"approximately equivalent" to the original curve.
                                                     //data format:{ x, y, slope}; point1: (lg400, 0.324), point2: (lg4000, 0.280) 
                                                     //slope = ( reaction voltage ) / (log400 –log1000) 

// C function showing how to do time delay

#include <time.h>
 
void delay(int milli_seconds) // number_of_seconds)
{
    // Converting time into milli_seconds
   // int milli_seconds = 1000 * number_of_seconds;
 
    // Stroing start time
    clock_t start_time = clock(); 
 
    // looping till required time is not acheived
    while (clock() < start_time + milli_seconds)
        ;
}
 
void setup()
{
    digitalPinMode(BOOL_PIN, INPUT);                        //set pin to input
    digitalWrite(BOOL_PIN, HIGH);                    //turn on pullup resistors

 //   printf("MG-811 Demostration\n");                
}



/*****************************  MGRead *********************************************
Input:   mg_pin - analog channel
Output:  output of SEN-000007
Remarks: This function reads the output of SEN-000007
************************************************************************************/ 
float MGRead(int mg_pin)
{
    int i;
    float v=0;
    int value; 

    for (i=0;i<READ_SAMPLE_TIMES;i++) {

	v += analogRawRead(mg_pin); 
        delay(READ_SAMPLE_INTERVAL);
    }

    v = (v/READ_SAMPLE_TIMES)* (1.8/4096); 

    return v;  
}

/*****************************  MQGetPercentage **********************************
Input:   volts   - SEN-000007 output measured in volts
         pcurve  - pointer to the curve of the target gas
Output:  ppm of the target gas
Remarks: By using the slope and a point of the line. The x(logarithmic value of ppm) 
         of the line could be derived if y(MG-811 output) is provided. As it is a 
         logarithmic coordinate, power of 10 is used to convert the result to non-logarithmic 
         value.
************************************************************************************/ 
int  MGGetPercentage(float volts, float *pcurve)
{
   if ((volts/DC_GAIN )>=ZERO_POINT_VOLTAGE) {
      return -1;
   } else { 
      return pow(10, ((volts/DC_GAIN)-pcurve[1])/pcurve[2]+pcurve[0]);
   }
}

void main(int argc, char** argv)
{
    int percentage;
    float volts;
    int i; 

    int shmid; 
    int result; 
    char *buffer; 
    char *string; 

  if ( argc > 1 )
  {
        is_comm = atoi(argv[1]);
        printf(" is_comm : %d\n", is_comm);
  }

  if ( argc > 2 )
        ch = atoi(argv[2]);
  else
        ch = 0;

  if ( argc > 3 )
        memcpy(deviceSerial, argv[3], strlen(argv[3]));

if (ch > 0 && is_comm > 2 )  // 3 (display) or 6  (relay) 
{

	if ( is_comm == 3 )  
	shmid = shmget((key_t) SHARED_MEMORY_KEY, 0, 0); // display 
	else if ( is_comm == 6 )  
	shmid = shmget((key_t) SHARED_MEMORY_ALRAM_KEY, 0, 0);  // relay 
	
	if (shmid == -1)
	{
		perror("shmat failed: "); 
		exit(0); 
	}

	buffer = (char*)shmat(shmid, NULL, 0); 
	if(buffer == (char*)-1) {
		perror("shmat failed:"); 
		exit(0); 
	}

	buffer[0] = READ_CLIENT_FLAG; 
	string = buffer + 1; 
}

  printf("deviceSerial : %s\n", deviceSerial); 
  printf("ch : %d\n", ch); 

  getBlockInfo("../info.json");
	
#if 1 // def USER_LOOP  
//while(1) 
//for ( i = 0 ; i < 30 ; i++) 
while(1) 
#endif 
{
	   
//  volts = MGRead(MG_PIN)*0.4;
    printf("%d\n", i++);  
    volts = MGRead(MG_PIN); 
    
    printf("  - SEN-00007: %f V / before_amp : %f V ", volts, volts/DC_GAIN);
    
    percentage = MGGetPercentage(volts,CO2Curve);

    sdata[0] = percentage;  
    
    if (percentage == -1) {
		printf( "  - CO2: < 400 \n");    
		sdata[0] = 400; // min value (ppm) ZERO_POINT_VOLTAGE; 
		percentage = 400; 
    } else {
        printf("  - CO2: %d ppm\n", percentage); 
    }
#if 0 	
   if (digitalRead(BOOL_PIN) ){
        printf("=====BOOL is HIGH======\n");
    } else {
        printf("=====BOOL is LOW ======\n");
    }
#endif 

//    sleep(1); 
    //} 

  //  fprintf(fp, "%d %d\n", percentage, digitalRead(BOOL_PIN)); 
 //   fclose(fp); 
 
	SendResultMessage(ch, deviceSerial, sdata); 
       
if ( is_comm == 3 || is_comm == 6 )
{
	int i; 

{ 
       if(buffer[0] == READ_CLIENT_FLAG)
        {
		if ( is_comm == 3 ) 
		{	
		        sprintf(string, "mg811,%2.1f,%s,", sdata[0], "PPM");
       	     		string[strlen(string)-1]='\0';
		}
		else if ( is_comm == 6 ) 
		{
			if ( percentage > 1000 )
				result = 1; 
			else 
				result = 0; 

			sprintf(string, "%d", result); 
			string[strlen(string)]='\0';
		}

                buffer[0] = READ_SERVER_FLAG;
        }
        else if (buffer[0] == PRINT_CLIENT_FLAG)
        {
                puts(string);
                buffer[0] = READ_CLIENT_FLAG;
        }

	sleep(9); 
}
}
else 
//    SendResultMessage(ch, deviceSerial, sdata);
	sleep(9); 
   } 	
}



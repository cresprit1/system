/*
  Copyright (c) 2009 Dave Gamble
 
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
 
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
 
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include "cJSON.h"

#include <stdio.h> /* printf, sprintf */
#include <stdlib.h> /* exit, atoi, malloc, free */
#include <unistd.h> /* read, write, close */
#include <string.h> /* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> /* struct hostent, gethostbyname */


#define _DEBUG

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>

//#include "config.h"
#include "cJSON.h"

static int sock = -1;

typedef struct _MYIF
{
  struct ifreq ifr_eth0;
  struct ifreq ifr_wlan0;
  uint16_t port;
};

struct _MYIF myif;

extern char gSERVER_NAME[16];

/**************************************
/*
/*      Setting Network Server Info
/*
/**************************************/

int gDATA_PORT = 9999;
int gALIVE_PORT = 9999;
int gPeriodSec = 1;		// default 1s  

char gSERVER_NAME[16] = {0, };

void getServerInfo(char* filename)
{
  int count=0, i, k, len;
  char temp[10] = {0, };

  FILE *rf;

  char *fstr; // [1024] = {0, };
  char *str;

  rf = fopen(filename, "r");

  if(rf == NULL)
        {
          printf(" list file open error\n");
          return;
        }

        fseek(rf, 0, SEEK_END);
        int length = ftell(rf);

        fstr = (char *)malloc(length+1);
        memset(fstr, length+1, 0);

        if ( fstr == NULL )
        {
          printf(" malloc error\n");
          return;
        }

        fseek(rf, 0, SEEK_SET);
  fread(fstr, length+1, 1, rf);

#ifdef _DEBUG
  printf("length = %d\n", length);
#endif

        cJSON *json = cJSON_Parse((const char*)fstr);

        cJSON *hostinfo = cJSON_GetObjectItem(json, "hostInfo");

        cJSON *hostInfo_ip = cJSON_GetObjectItem(hostinfo, "ip");
        cJSON *hostInfo_dport = cJSON_GetObjectItem(hostinfo, "dport");
//        cJSON *hostInfo_aport = cJSON_GetObjectItem(hostinfo, "aport");
	cJSON *hostInfo_period = cJSON_GetObjectItem(hostinfo, "period"); 

                str = cJSON_Print(hostInfo_ip);
                len = strlen(str); //strlen(cJSON_Print(data_mac));
          memcpy(gSERVER_NAME, &str[1], len-2);

                str = cJSON_Print(hostInfo_dport);
                len = strlen(str);
	  memset(temp, 0, 10); 
          memcpy(temp, &str[1], len-2);

          gDATA_PORT = atoi(temp);

/*                 str = cJSON_Print(hostInfo_aport);
                len = strlen(str);
	  memset(temp, 0, 10); 
          memcpy(temp, &str[1], len-2);
          gALIVE_PORT = atoi(temp);
*/ 
	 str = cJSON_Print(hostInfo_period); 
	 len = strlen(str); 
	printf("%s\n", str); 
	 memset(temp, 0, 10); 
	 memcpy(temp, &str[1], len-2); 
	 gPeriodSec = atoi(temp); 

#ifdef _DEBUG
          printf(" %s : %d : %d\n", gSERVER_NAME, gDATA_PORT, gPeriodSec);
#endif

        free(fstr);

  return;
}

void getMyNetworkInfo(void)
{
   int fd;

   fd = socket(AF_INET, SOCK_DGRAM, 0);

    /* I want to get an IPv4 IP address */
    myif.ifr_eth0.ifr_addr.sa_family = AF_INET;
    myif.ifr_wlan0.ifr_addr.sa_family = AF_INET;

    /* I want IP address attached to "eth0" */
    strncpy( myif.ifr_eth0.ifr_name, "eth0", IFNAMSIZ-1);
    ioctl(fd, SIOCGIFADDR, &myif.ifr_eth0);

    /* I want IP address attached to "wlan0" */
    strncpy( myif.ifr_wlan0.ifr_name, "wlan0", IFNAMSIZ-1);
    ioctl(fd, SIOCGIFADDR, &myif.ifr_wlan0);

    close(fd);

   /* display result */
   printf("%x - %s\n", ((struct sockaddr_in *)&myif.ifr_eth0.ifr_addr)->sin_addr, inet_ntoa(((struct sockaddr_in *)&myif.ifr_eth0.ifr_addr)->sin_addr));

   /* display result */
   printf("%x - %s\n", ((struct sockaddr_in *)&myif.ifr_wlan0.ifr_addr)->sin_addr, inet_ntoa(((struct sockaddr_in *)&myif.ifr_wlan0.ifr_addr)->sin_addr));

  return 0;
}

uint32_t getWLANIP(void)
{
  return (uint32_t)(((struct sockaddr_in *)&myif.ifr_wlan0.ifr_addr)->sin_addr.s_addr);
}

uint32_t getETHIP(void)
{
  return (uint32_t)(((struct sockaddr_in *)&myif.ifr_eth0.ifr_addr)->sin_addr.s_addr);
}

int openNetwork(void)
{
    getMyNetworkInfo();
}

// Parse text to JSON, then render back to text, and print!
void doit(char *text)
{
	char *out;cJSON *json;
	
	json=cJSON_Parse(text);
	out=cJSON_Print(json);
	cJSON_Delete(json);
	printf("%s\n",out);
	free(out);	
}

// Read a file, parse, render back, etc.
void dofile(char *filename)
{
	FILE *f=fopen(filename,"rb");fseek(f,0,SEEK_END);long len=ftell(f);fseek(f,0,SEEK_SET);
	char *data=malloc(len+1);fread(data,1,len,f);fclose(f);
	doit(data);
	free(data);
}

//static const char *host= "218.147.182.24"; 
//static char ServiceServerIP[] = "218.147.182.24:8003/api/v1/devices/%s/info";

//static const char *host= "192.168.20.202"; // 0.16";
//static char ServiceServerIP[] = "192.168.20.202/api/v1/insert/data";
//static char ServiceServerPort[] = "5000";

int sendPostMessage(char* devname, int ch, char* mesg) 
{
    /* first what are we going to send and where are we going to send it? */
    int portno =        5000;
    char *message_fmt = "POST /api/v1/insert/data HTTP/1.0\r\nContent-Type: application/json\r\nContent-Length: %d\r\n\r\n%s";

    struct hostent *server;
    struct sockaddr_in serv_addr;
    int sockfd, bytes, sent, received, total;

    char message[4096],response[4096];

    sprintf(message, message_fmt, strlen(mesg), mesg); 
    
#if 1//def _DEBUG 
    printf("=======================\nRequest:\n%s\n",message);
#endif 

    /* create the socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) error("ERROR opening socket");

    /* lookup the ip address */
  //  server = gethostbyname(host);
    server = gethostbyname(gSERVER_NAME); 
    if (server == NULL) error("ERROR, no such host");

    /* fill in the structure */
    memset(&serv_addr,0,sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno);
    memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);

    /* connect the socket */
    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
        error("ERROR connecting");

   /* send the request */
    total = strlen(message);
    sent = 0;
    do {
        bytes = write(sockfd,message+sent,total-sent);
        if (bytes < 0)
            error("ERROR writing message to socket");
        if (bytes == 0)
            break;
        sent+=bytes;
    } while (sent < total);

#if 0 
    /* receive the response */
    memset(response,0,sizeof(response));
    total = sizeof(response)-1;
    received = 0;
   do 
   {
        bytes = read(sockfd,response+received,total-received);
        if (bytes < 0)
            error("ERROR reading response from socket");
   //     if (bytes == 0)
   //          break;
        received+=bytes;
    } while (received < total);

    if (received == total)
        error("ERROR storing complete response from socket");
#endif 

    /* close the socket */
    close(sockfd);

    /* process response */
    printf("Send OK\n"); 

    return 0;
}

static int sensor_cnt = 0; 

char timeprebuf[10][30];   

typedef struct block 
{
	char kindOf[128]; 
	char desc[256]; 
	char blockID[10]; 
	char type[5][10]; 
	int cnt; 
}; 

struct block blockInfo; 

void getBlockInfo(char* filename)
{

#if 1 
  int count=0, i;
  FILE *rf;
  
  char *fstr; // [1024] = {0, }; 
  char *tempstr; 
  
  rf = fopen(filename, "r");
                                                        
  if(rf == NULL)
  {
  	printf(" list file open error\n"); 
	return -1;
  }
	
  fseek(rf, 0, SEEK_END); 

  int length = ftell(rf);
	
	fstr = (char *)malloc(length+1); 
	memset(fstr, length+1, 0); 
	
	if ( fstr == NULL )
	{
	  printf(" malloc error\n"); 
	  return; 
	} 
	
	fseek(rf, 0, SEEK_SET); 
  	fread(fstr, length+1, 1, rf);
  
#ifdef _DEBUG
  printf("length = %d\n", length); 
#endif 

	cJSON *json = cJSON_Parse((const char*)fstr); 
	
	cJSON *type = cJSON_GetObjectItem(json, "type"); 
	cJSON *kindof = cJSON_GetObjectItem(json, "sensorType"); 
	cJSON *desc = cJSON_GetObjectItem(json, "description"); 
	cJSON *bid = cJSON_GetObjectItem(json, "blockID"); 
	
	blockInfo.cnt = cJSON_GetArraySize(type); 
	
	printf("cnt : %d\n", blockInfo.cnt); 

	if (blockInfo.cnt) 
	{
		for ( i = 0 ; i < blockInfo.cnt; i++) 
	  	{	
		  cJSON *cJSONtype = cJSON_GetArrayItem(type, i); 
		  tempstr = cJSON_Print(cJSONtype); 		  
		  memcpy(blockInfo.type[i], &tempstr[1], strlen(tempstr)-2); 
#ifdef _DEBUG	
  		printf(" %s %s\n", cJSON_Print(cJSONtype), blockInfo.type[i]); 
#endif 	
		}
	}

	tempstr = cJSON_Print(kindof); 
  	memcpy(blockInfo.kindOf, &tempstr[1], strlen(tempstr)-2); 
	tempstr = cJSON_Print(desc);  
	memcpy(blockInfo.desc, &tempstr[1], strlen(tempstr)-2); 
	tempstr = cJSON_Print(bid); 	
	memcpy(blockInfo.blockID, &tempstr[1], strlen(tempstr)-2); 
		
#ifdef _DEBUG	
  		printf(" %s : %s, %s\n", blockInfo.kindOf, blockInfo.desc, blockInfo.blockID); 
#endif 	

	free(fstr); 
#endif 

}

void SendResultMessage(int ch, char *serial, float *sdata)//float temp, float hu)
{
        char* pResult = NULL;
        int i;

        cJSON *jsonResult;
        cJSON *jsonArray;

        cJSON *jsonSensorValuesArray;

        cJSON *sensorData[5];

        /* current time */

        struct timeval val;
        struct tm *ptm;

        char timeBuf[128]={0,};
        char usec[7]={0,};
        char usec_use[4]={0,};

        memset(timeBuf, 0, sizeof(timeBuf));
        gettimeofday(&val, NULL);
        ptm = localtime(&val.tv_sec);

        sprintf(usec,"%03ld", val.tv_usec);
        strncpy(usec_use, usec, 3);
        sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s"
                , ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
                , ptm->tm_hour, ptm->tm_min, ptm->tm_sec
                , usec_use);

//        jsonResult = cJSON_CreateObject();
        jsonSensorValuesArray = cJSON_CreateArray();

#if 0 
        for ( i = 0 ; i < blockInfo.cnt ; i++)
        {
                sensorData[i] = cJSON_CreateObject();

                cJSON_AddStringToObject(sensorData[i], "type", blockInfo.type[i]);
                cJSON_AddNumberToObject(sensorData[i], "id", 1);
                cJSON_AddNumberToObject(sensorData[i], "v", sdata[i]);
                cJSON_AddItemToArray(jsonSensorValuesArray, sensorData[i]);
        }

        cJSON_AddStringToObject(jsonResult, "B_ID", blockInfo.blockID);
        cJSON_AddNumberToObject(jsonResult, "channel", ch);
        cJSON_AddStringToObject(jsonResult, "serial", serial);
        cJSON_AddItemToObject(jsonResult, "values", jsonSensorValuesArray);
        cJSON_AddStringToObject(jsonResult, "time", timeBuf);
#else

	printf("blockInfo.cnt : %d\n", blockInfo.cnt); 

        for ( i = 0 ; i < blockInfo.cnt ; i++)
        {
                sensorData[i] = cJSON_CreateObject();

		cJSON_AddStringToObject(sensorData[i], "b_type", blockInfo.blockID);
        	cJSON_AddNumberToObject(sensorData[i], "ch", ch);
        	cJSON_AddStringToObject(sensorData[i], "serial", serial);

                cJSON_AddStringToObject(sensorData[i], "s_type", blockInfo.type[i]);
                cJSON_AddNumberToObject(sensorData[i], "i", 1);
                cJSON_AddNumberToObject(sensorData[i], "v", sdata[i]);
 		cJSON_AddStringToObject(sensorData[i], "t", timeBuf);

                cJSON_AddItemToArray(jsonSensorValuesArray, sensorData[i]);
        }
#endif 
       // pResult = cJSON_Print(jsonResult);
	pResult = cJSON_Print(jsonSensorValuesArray); 
        printf("Result(%d) : %s\n", (ch+1), pResult);
        //cJSON_Delete(jsonResult);
	cJSON_Delete(jsonSensorValuesArray); 

	sendPostMessage(serial, ch, pResult); 
}


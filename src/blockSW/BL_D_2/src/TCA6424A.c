#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>

#include "TCA6424A.h"

extern unsigned char NACK;

void delay_ms(int mseconds)
{
        clock_t start_time = clock();

        while(clock() < start_time + mseconds)
                ;
}

/**************************
//   i2c 
***************************/

// Transmit size pointer
int PtrTransmit;
// I2C transmit buffer array
unsigned char I2CBufferArray[100];
// I2C receive buffer
unsigned char I2CBuffer;
// fail gets set by USCIAB0RX_VECTOR if a NACK is detected
unsigned char NACK=0;

int fd; 

void finishI2C(void)
{
	close(fd); 
}

void setupI2C(unsigned char slaveaddr)
{
	uint8_t err_code; 

	fd = open("/dev/i2c-6", O_RDWR); 

	if ( fd == -1 )
	{
		printf(" Device Open Error\n"); 
		exit(0); 
	}
	
	err_code = ioctl(fd, I2C_SLAVE, (0x22<<1)); // slaveaddr); 

 	printf("I2C_Setup : %x\n", err_code);
}

void I2C_Write(unsigned char byte_Count, unsigned char Slave_Address, unsigned char Register_Address, unsigned char Register_data[], unsigned char offset)
{
	// If word mode, write 2 bytes
	uint8_t err_code; 
	
	int x = byte_Count-1;
	unsigned char y=0;

/*	I2CBufferArray[byte_Count] = Register_Address;

	for (;x>=0;x--)
	{
		I2CBufferArray[x] = Register_data[y+offset];
		y++;
	}
*/ 	
	I2CBufferArray[0] = Register_Address; 
	I2CBufferArray[1] = 0xFF; 
	I2CBufferArray[2] = 0xFF; 
	I2CBufferArray[3] = 0xFF; 

	printf("byte_Count : %d - %x\n", byte_Count, I2CBufferArray[0]); 

	err_code = write(fd, I2CBufferArray, byte_Count); 
 	printf("I2C_Write(err_code) : %x\n", err_code);
}

void I2C_Read(unsigned char byte_Count, unsigned char Slave_Address, unsigned char Register_Address, unsigned char* read_buffer)
{
	uint8_t err_code; 

        I2CBufferArray[0] = Register_Address;
	err_code = write(fd, I2CBufferArray, 1); 
 	printf("I2C_Read1(err_code) : %x\n", err_code);

	delay_ms(100);  

        err_code = read(fd, read_buffer, 3); // byte_Count);
	printf("I2C_Read2(err_code) : %x\n", err_code);

	delay_ms(100); 
}

void I2C_Write_Byte(unsigned char Slave_Address, unsigned char Register_Address, unsigned char Register_Data)
{
	uint8_t err_code; 

	// Set register address.
	I2CBufferArray[0] = Register_Address;
	// Data to be written
	I2CBufferArray[1] = Register_Data;
	// set I2CBufferArray size

	err_code = write(fd, I2CBufferArray, 2); 

	

	printf("I2C_Write_Byte(err_code) : %x\n", err_code); 
}

uint8_t I2C_Read_Byte ( unsigned char Slave_Address, unsigned char Register_Address)
{       
	uint8_t err_code;
	uint8_t read_buffer[2]={0,}; 

        I2CBufferArray[0] = Register_Address;
        err_code = write(fd, I2CBufferArray, 1);
        printf("I2C_Read1(err_code) : %x\n", err_code);

        delay_ms(100);

        err_code = read(fd, read_buffer, 1);
        printf("I2C_Read2(err_code) : %x\n", err_code);

        delay_ms(100);

	printf("%x %x\n", read_buffer[0], read_buffer[1]); 

	return read_buffer[0]; 	
}

// ****************************************************************************
//! @fn          void TCA6424AInitDefault(TCA6424ARegs* Regs)
//! @brief
//!				 Initializes the confRegs structure which is
//!              a local mirror of the TPL7200 config registers
//!              additional TPL7200writeConfigRegisters(confRegs) function call
//! 			 is needed in order to get the TPL7200 registers updated
//!
//! TODO 		 Implement I2C return (success/failure)
// ****************************************************************************

void TCA6424AInitDefault(TCA6424ARegs* Regs){
	printf("Default Set\n"); 
	Regs->Output.all = 0x00FFFFFF;
//	Regs->Output.all = 0x00000000; 
	Regs->PolarityInversion.all = 0x00FFFFFF;
	Regs->Config.all = 0x00000000;
}

// ****************************************************************************
//! @fn          void TCA6424AInitI2CReg(TCA6424ARegs* Regs)
//! @brief
//!
//! TODO 		 Implement I2C return (success/failure)
// ****************************************************************************
void TCA6424AInitI2CReg(TCA6424ARegs* Regs){
	printf("InitI2CReg\n"); 
	I2C_Write(3, TCA6424A_ADDRESS, TCA6424A_POLARITY_REG0 ,  (unsigned char*)&Regs->PolarityInversion, 0);
	I2C_Write(3, TCA6424A_ADDRESS, TCA6424A_CONFIG_REG0 | 0x80, (unsigned char*)&Regs->Config, 0);
	I2C_Write(3, TCA6424A_ADDRESS, TCA6424A_OUTPUT_REG0 | 0x80, (unsigned char*)&Regs->Output, 0);
}

// ****************************************************************************
//! @fn          void TCA6424AReadInputReg(TCA6424ARegs* Regs)
//! @brief
//!
//! TODO 		 Implement I2C return (success/failure)
// ****************************************************************************
void TCA6424AReadInputReg(TCA6424ARegs* Regs)
{
	I2C_Read(3,TCA6424A_ADDRESS, TCA6424A_INPUT_REG0 | 0x80, (unsigned char *)&Regs->Input);
}


// ****************************************************************************
//! @fn          void TCA6424AInitI2CReg(TCA6424ARegs* Regs)
//! @brief
//!
//! TODO 		 Implement I2C return (success/failure)
// ****************************************************************************
void TCA6424AWriteReg(unsigned char regaddress, unsigned char regVal)
{
	I2C_Write_Byte(TCA6424A_ADDRESS, regaddress, regVal);
	if(NACK)
	{
		//handle I2C failure later

	}
}

unsigned char TCA6424AReadReg(unsigned char address, unsigned char regaddress)
{
	return (unsigned char) I2C_Read_Byte(address,regaddress);
}

void TCA6424AWriteConfig(TCA6424ARegs * Regs)
{
	I2C_Write(3, TCA6424A_ADDRESS, TCA6424A_CONFIG_REG0 | 0x80, (unsigned char*)&Regs->Config, 0);
}

void TCA6424AWriteOutput(TCA6424ARegs * Regs)
{
	I2C_Write(3, TCA6424A_ADDRESS, TCA6424A_OUTPUT_REG0 | 0x80, (unsigned char*)&Regs->Output, 0);
}

void TCA6424AWritePolarity(TCA6424ARegs * Regs)
{
	I2C_Write(3, TCA6424A_ADDRESS, TCA6424A_POLARITY_REG0 | 0x80, (unsigned char*)&Regs->PolarityInversion, 0);
}


var http = require("http");
var fs = require('fs');

var HOSTIP = "192.168.0.201"; 
var PORT = "8000"; 

var post2o = function(urlpath, method, data, callback) {

	var options = {
	  "method": method,
	  "hostname": HOSTIP,
	  "port": PORT,
	  "path": urlpath,
	  "headers": {
		"Content-Type":"application/json"
	  }
	};
	
	var req = http.request(options, callback); 
	
	if(urlpath == "/deviceOntology/devConfiguration")
	{
		var file = "./blockInfo.json"; 

		fs.readFile(file,'utf8', function(error, data){
			console.log("** blockInfo **");
			console.log(JSON.stringify(data));
			console.log("\n\n"); 
			req.write(data);
			req.end();
		});
	}	
	else  
	{
		req.write(data); 
		req.end(); 
	}	
} 

module.exports.post2o = post2o;

// post("/deviceOntology/devConfiguration", "POST"); 

// post("/deviceOntology/regiDeviceInfo", "POST"); 


#include <stdio.h>
#include <stdbool.h>

#define HIGH 1
#define LOW 0 
#define INPUT 1
#define OUTPUT 0 

bool digitalPinMode(int pin, int dir){
  FILE * fd;
  char fName[128];
 
  //Exporting the pin to be used
  if(( fd = fopen("/sys/class/gpio/export", "w")) == NULL) {
    printf("Error: unable to export pin\n");  
    return false;
  }
 
  fprintf(fd, "%d\n", pin);
  fclose(fd);	// Setting direction of the pin
  sprintf(fName, "/sys/class/gpio/gpio%d/direction", pin); 
  if((fd = fopen(fName, "w")) == NULL) { 
    printf("Error: can't open pin direction\n");
    return false;
  }
 
  if(dir == OUTPUT) {
    fprintf(fd, "out\n");
  } else {
    fprintf(fd, "in\n");
  }
 
  fclose(fd);
  return true;
}

 
int digitalRead(int pin) {
  FILE * fd;
  char fName[128];
  char val[2];
 
  //Open pin value file
  sprintf(fName, "/sys/class/gpio/gpio%d/value", pin);
  if((fd = fopen(fName, "r")) == NULL) {
     printf("Error: can't open pin value\n");
     return false;
  }
 
  fgets(val, 2, fd);
  fclose(fd);
 
  return atoi(val);
}

 
bool digitalWrite(int pin, int val) { 
  FILE * fd;
  char fName[128];
 
  // Open pin value file
  sprintf(fName, "/sys/class/gpio/gpio%d/value", pin);
  if((fd = fopen(fName, "w")) == NULL) {
    printf("Error: can't open pin value\n");
    return false;
  }
 
  if(val == HIGH) { 
    fprintf(fd, "1\n");
  } else { 
    fprintf(fd, "0\n");
  }
 
  fclose(fd);
  return true;
}

float sdata[5]; 
char deviceSerial[128] = {0, }; 
int ch = 0; 

#define LAMP_PIN 24

void main(int argc, char *argv[])
{
	int onoff; 

  int is_comm = 0; 
  
  FILE *fp;

  fp = fopen("../result.txt", "w+");

  if ( fp == NULL ) {
        printf("File Open Error\n");
        exit(1);
  }

  if ( argc > 1 )
  {
        is_comm = atoi(argv[1]);
        printf(" is_comm : %d\n", is_comm);
  }
        if (argc > 2)
                ch = atoi(argv[1]);
        else
                ch = 0;

        if ( argc > 3 )
                memcpy(deviceSerial, argv[3], strlen(argv[3])-1);

	if ( argc > 4 ) 
	{
		onoff = atoi(argv[4]); 
		printf("onoff : %d\n", onoff); 
	}

        printf("LAMP Ctl - %s %s %s\n", argv[2], argv[3], argv[4]);

        getBlockInfo("../info.json");
	
	digitalPinMode(LAMP_PIN, OUTPUT); 
	digitalWrite(LAMP_PIN, onoff);

	if ( onoff ) 
		digitalWrite(LAMP_PIN, 0); // ON 
	else
		digitalWrite(LAMP_PIN, 1); // OFF 
 
	sdata[1] = (float)onoff; 

	fprintf(fp, "%d\n", onoff);
  	fclose(fp);

	if ( is_comm )
	{
		SendResultMessage(ch, deviceSerial, sdata); 
		sleep(1); 	
	}
}


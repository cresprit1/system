#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include "cJSON.h"
#include "digital.h" 
#include "blockctl.h"

//#define _DEBUG 

#if 0 
#define MAX_SLOT 6
#define MAX_BIDIO 6 
#define MAX_RECORD 10 

#define BOARD_IO 	6 

#define BOARD_ID1 	7
#define BOARD_ID2	8
#define BOARD_ID3	9
#define BOARD_ID4	10
#define BOARD_ID5	11
#define BOARD_ID6	12
#define BOARD_ID7 	13

/* 3.3v */ 
#define BOARD_SD1	(32*3+28) // D28 or E2 
#define BOARD_SD2	(32*3+29) // D29 or E3 
#define BOARD_SD3	(32*3+30) // D30 or E4 
#define BOARD_SD4	(32*3+31) // D31 or C14 
#define BOARD_SD5	(32*4+0) // E0  or C23 
#define BOARD_SD6	(32*4+1) // E1 or B26 

/* 5.0v (module power) */ 
#define BOARD_MD1	(32*4+2)  // E2
#define BOARD_MD2	(32*4+3)  // E3 
#define BOARD_MD3	(32*4+4)  // E4
#define BOARD_MD4	(32*2+14) // C14
#define BOARD_MD5	(32*2+23) // C23
#define BOARD_MD6	(32*1+26) // B26


int slotPowerIO[MAX_SLOT] = { BOARD_SD1, BOARD_SD2, BOARD_SD3, BOARD_SD4, BOARD_SD5, BOARD_SD6}; 

int blockIO[MAX_BIDIO] = { BOARD_ID1, BOARD_ID2, BOARD_ID3, BOARD_ID4, BOARD_ID5, BOARD_ID6}; 
#endif 

static const char *filename = "./blockInfo.json";

struct record_blockInfo {

	int level; 

        char sensorType[20];
        char blockID[10];
};

//int inputslot[MAX_SLOT]={0, 255, 2, 1, 255, 255};

int detectBlock[MAX_SLOT] = {-1, }; 

struct record_blockInfo btable[MAX_RECORD]; 

int btable_num = -1; 

int doit(char *text)
{
	int i, j = 0, index; 

	char *out; 
	cJSON *json, *subjson, *item, *value; 

	json = cJSON_Parse(text); 
        subjson = cJSON_GetObjectItem(json, "blockList"); 

	btable_num = cJSON_GetArraySize(subjson); 

	for ( j = 0 ; j < btable_num ; j++) 
	{
		item = cJSON_GetArrayItem(subjson, j); 
		out = cJSON_Print(item); 
#ifdef _DEBUG 
		printf("%s\n", out); 
#endif 
		free(out); 

		value = cJSON_GetObjectItem(item, "channel"); 	
		out = cJSON_Print(value); 
		btable[j].level = atoi(out); 
#ifdef _DEBUG
		printf("%s\n", out); 
#endif 
		free(out); 	

		value = cJSON_GetObjectItem(item, "sensorType");  
		out = cJSON_Print(value); 
		memset(btable[j].sensorType, 0, 20); 
		memcpy(btable[j].sensorType, &out[1], strlen(out)-2); 
#ifdef _DEBUG
		printf("%s\n", out); 
#endif 
		free(out); 

		value = cJSON_GetObjectItem(item, "blockID"); 
		out = cJSON_Print(value);
		memset(btable[j].blockID, 0, 10); 
		memcpy(btable[j].blockID, &out[1], strlen(out)-2);  
#ifdef _DEBUG
		printf("%s\n", out); 
#endif 
		free(out); 
	}

#ifdef _DEBUG
	for ( i = 0 ; i < 6 ; i++) 
	{
		printf("%d, %s, %s\n", btable[i].level, btable[i].sensorType, btable[i].blockID); 
	}
#endif 
	
	cJSON_Delete(json); 
	return btable_num; 
}

int doreadfile(char *filename)
{
	int ret; 

	FILE *f=fopen(filename,"rb"); 

	fseek(f, 0, SEEK_END); 

	long len=ftell(f); 
	fseek(f, 0, SEEK_SET); 

	char *data = malloc(len+1); 
	fread(data, 1, len, f); 
	fclose(f); 

 	ret = doit(data); 

	free(data); 

	return ret; 
}

#if 0 
void checkBlockID(void)
{
        int port;
	int i=0; 

	
        for ( i = 0 ; i < MAX_SLOT ; i++ )
        {
		detectBlock[i] = readBlockID(i); 
#ifdef _DEBUG
		printf("detectBlock = %d\n", detectBlock[i]); 
#endif 
        }
}
#endif 

void execute(char **argv)
{
	pid_t pid; 
	int status; 

	if((pid = fork()) <0) { /* fork a child process */
		printf("*** ERROR : forking child process failed\n"); 
		exit(1); 
	}
	else if (pid == 0) {
		if (execvp(*argv, argv) < 0) { /* execute the command */
			printf("*** ERROR : exec failed\n"); 
			exit(1); 
		}
	}
	else {				      /* for the parent: */ 
		while( wait(&status) != pid)  /* wait for completion */ 
			; 
	}
}

void main(void)
{
	int i, j, bcnt, index = -1; 
	char buf[256] = { 0, }; 
	
	bcnt = doreadfile("../blockInfo.json"); 

 	while(1)
{
	for ( i = 0 ; i < bcnt ; i++ )
	{
		index = btable[i].level; 

		if ( index == -1 ) 
			continue; 

		printf("\n == CH : %d (%s-%s)\n", btable[i].level, btable[i].blockID, btable[i].sensorType); 

                powerOnModule(index);
//                powerOnBlock(index);

//	 	printf("%d, %s, %s\n", btable[i].level, btable[i].sensorType, btable[i].blockID);
		sprintf(buf, "./../blockSW/%s/app.sh %d", btable[i].blockID, btable[i].level); 
//		printf("%s\n", buf); 
		j = system(buf);  	

                powerOffModule(index);
 //               powerOffBlock(index);

	}
}
}


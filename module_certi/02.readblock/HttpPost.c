#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <curl/curl.h>

#ifdef _DEBUG
char* timeToString(struct tm *t);

char* timeToString(struct tm *t) {
	static char s[20];

	sprintf_s(s, "%04d-%02d-%02d %02d:%02d:%02d",
		t->tm_year + 1900, t->tm_mon + 1, t->tm_mday,
		t->tm_hour, t->tm_min, t->tm_sec
	);

	return s;
}
#endif


struct string {
	char *ptr;
	size_t len;
};

void init_string(struct string *s) {
	s->len = 0;
	s->ptr = (char *)malloc(s->len + 1);
	if (s->ptr == NULL) {
		fprintf(stderr, "malloc() failed\n");
		exit(EXIT_FAILURE);
	}
	s->ptr[0] = '\0';
}

size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s)
{
	size_t new_len = s->len + size * nmemb;
	s->ptr = (char *)realloc(s->ptr, new_len + 1);
	if (s->ptr == NULL) {
		fprintf(stderr, "realloc() failed\n");
		exit(EXIT_FAILURE);
	}
	memcpy(s->ptr + s->len, ptr, size*nmemb);
	s->ptr[new_len] = '\0';
	s->len = new_len;

	return size * nmemb;
}




int HttpPost(const char * url, long port, const char *postthis, char *result)
{

#ifdef _DEBUG
	struct tm t;
	time_t timer;
#endif

	CURL *curl;
	CURLcode res;

#ifdef _DEBUG
	timer = time(NULL);
	localtime_s(&t, &timer);
	printf("1: %s\n", timeToString(&t));
#endif

	curl = curl_easy_init();

#ifdef _DEBUG
	timer = time(NULL);
	localtime_s(&t, &timer);
	printf("2: %s\n", timeToString(&t));
#endif

	if (curl) {


		struct string s;
		init_string(&s);

		struct curl_slist *headerlist = NULL;
		headerlist = curl_slist_append(headerlist, "Content-Type: application/json; charset=UTF-8");


		curl_easy_setopt(curl, CURLOPT_URL, url);
		//curl_easy_setopt(curl, CURLOPT_PORT, port);

		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);


		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postthis);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postthis);

		/* if we don't provide POSTFIELDSIZE, libcurl will strlen() by
		itself */
		curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long)strlen(postthis));
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
		//curl_easy_setopt(curl, CURLOPT_RETURNTRANSFER, 1);

		/* Perform the request, res will get the return code */

#ifdef _DEBUG
		timer = time(NULL);
		localtime_s(&t, &timer);
		printf("3: %s\n", timeToString(&t));
#endif

		res = curl_easy_perform(curl);

#ifdef _DEBUG
		timer = time(NULL);
		localtime_s(&t, &timer);
		printf("4: %s\n", timeToString(&t));
#endif

		/* Check for errors */
		if (res != CURLE_OK)
		{
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
		}
		else
		{
			//printf("HTTP result = %s\n",res);
			if (result == NULL)
			{
				printf("result : %s\n", s.ptr);
				free(s.ptr);
			}
			else
			{
				printf("result : %s\n", s.ptr);
				strncpy(result, s.ptr, strlen(s.ptr));
			}
		}


		/* always cleanup */
		curl_easy_cleanup(curl);

#ifdef _DEBUG
		timer = time(NULL);
		localtime_s(&t, &timer);
		printf("5: %s\n", timeToString(&t));
#endif

	}
	return 0;
}


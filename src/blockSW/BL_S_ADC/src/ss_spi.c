#include <stdio.h>         // printf()
#include <string.h>        // strlen()
#include <fcntl.h>         // O_WRONLY
#include <unistd.h>        // write(), close()
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <time.h>
#include <pthread.h>	
#include <signal.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <math.h>
#include "analogSensor.h"
#include "Linked_queue.h"
 
//##include "fft_api.h"
#include "sigio.h"
#include "sqlite3.h"
#include <MQTTClient.h>
#include "cJSON.h"
#include <termios.h>
#include "fftw3.h"
//#include "calculateParam.h"

static unsigned int midx; 
//chcohi #define FEATURE_SINGLE_CHANNEL
#if 1
#define DBG(...)  fprintf(stderr, " DBG(%s, %s(), %d): ", __FILE__, __FUNCTION__, __LINE__); fprintf(stderr, __VA_ARGS__)
#define DBG_A(...) fprintf(stderr, " DBG(%s, %s(), %d): ", __FILE__, __FUNCTION__, __LINE__); fprintf(stderr, __VA_ARGS__)
#else
#define DBG(...)
#define DBG_A(...)
#endif
#define FEATURE_PREQUENCY
#define FEATURE_CLEANED_SRC_CODE

//#define FEATURE_MQTT
#define FEATURE_SERIAL
#define FEATURE_PUB_THREAD
#define FEATURE_SQLITE3
//#define FEATURE_RAWDATA
//#define PARAM_LOG
//#define FEATURE_TACHO
//jonsama
//#define DEBUG
//#define SAMPLECNT_DEBUG
#define DEBUG_LPF_NONE	0
#define DEBUG_LPF_AD1		1
#define DEBUG_LPF_AD2		2
#define DEBUG_LPF_AD3		4
#define DEBUG_LPF_AD4		8


//#define DEBUG_LPF	(DEBUG_LPF_AD1|DEBUG_LPF_AD2|DEBUG_LPF_AD3|DEBUG_FFT_TIME_AD4)
#define DEBUG_LPF	(DEBUG_LPF_NONE)

#define DEBUG_FFT_TIME_NONE	0
#define DEBUG_FFT_TIME_AD1	1
#define DEBUG_FFT_TIME_AD2	2
#define DEBUG_FFT_TIME_AD3	4
#define DEBUG_FFT_TIME_AD4	8


//#ifdef DEBUG_FFT_ON
//#define DEBUG_FFT	(DEBUG_FFT_TIME_AD1|DEBUG_FFT_TIME_AD2|DEBUG_FFT_TIME_AD3|DEBUG_FFT_TIME_AD4)
//#else
#define DEBUG_FFT	(DEBUG_FFT_TIME_AD1)
//#endif
typedef enum{
	INDEX_COLUMN_1 = 0,
	INDEX_COLUMN_2,
	INDEX_COLUMN_3,	
	INDEX_COLUMN_4,
	INDEX_COLUMN_5, 
	INDEX_COLUMN_6,
	INDEX_COLUMN_7, 
	INDEX_COLUMN_8,
	INDEX_COLUMN_9, 
	INDEX_COLUMN_10, 	
	INDEX_COLUMN_11,	
	INDEX_COLUMN_12,	
	INDEX_COLUMN_13,	
	INDEX_COLUMN_14,	
	INDEX_COLUMN_15,	
	INDEX_COLUMN_MAX,
}ColumnIndex;

typedef enum{
	INDEX_VEL_OVERALL = 0,
	INDEX_VEL_V1,
	INDEX_VEL_V2,
	INDEX_VEL_V3,
	INDEX_VEL_V4,
	INDEX_VEL_V5,
	INDEX_VEL_V6,
	INDEX_VEL_V7,
	INDEX_VEL_V8,
	INDEX_VEL_VP2P,	
	INDEX_ACC_OVERALL,//10
	INDEX_ACC_A1,
	INDEX_ACC_A2,
	INDEX_ACC_A3,
	INDEX_ACC_A4,
	INDEX_ACC_A5,
	INDEX_ACC_A6,
	INDEX_ACC_A7,
	INDEX_ACC_A8,
	INDEX_ACC_AP2P,	
	INDEX_ALARM_VALUE,
	INDEX_ALARM_RATE1,
	INDEX_ALARM_RATE2,
	INDEX_BASELINE,
	INDEX_SENSOR_VLT,
	INDEX_MAX,
	
}ParamIndex;

typedef enum{
	INDEX_MIC_OVERALL = 0,
	INDEX_MIC_M1,
	INDEX_MIC_M2,
	INDEX_MIC_M3,		
	INDEX_MIC_M4,		
	INDEX_MIC_M5,		
	INDEX_MIC_M6,		
	INDEX_MIC_M7,		
	INDEX_MIC_M8,		
	INDEX_MIC_M9,
	INDEX_MIC_ALARM_VALUE,
	INDEX_MIC_ALARM_RATE1,
	INDEX_MIC_ALARM_RATE2,
	INDEX_MIC_SENSOR_VLT,
  	INDEX_MIC_MAX,
	
}ParamIndexMIC;

//[[180423]chchoi-mod:changed 2 alarm  => a alarm
//#define INDEX_ALARM1	20
//#define INDEX_ALARM2	21
//#define INDEX_ALARM_VALUE		20
//#define INDEX_ALARM_RATE1		21
//#define INDEX_ALARM_RATE2		22
//#define INDEX_BASELINE			23
//#define INDEX_SENSOR_VLT		24


//[[180706]]chchoi-add:alarmType(check value/rate)
typedef enum
{
	TYPE_ALARM_NONE = -1,
	TYPE_ALARM_VALUE = 0,
	TYPE_ALARM_RATE = 1,
}AlarmType;


typedef struct 
{
	unsigned int hasParam;
	unsigned int paramF1;
	unsigned int paramF2;
	unsigned int paramType;
	
}RequiredParam;

typedef struct 
{
	//[[180423]chchoi-mod:changed 2 alarm  => a alarm
	//[[180706]chchoi-add:reAdd alarm1_f1, ...
	float alarm1_f1;
	float alarm1_f2;
	float alarm2_f1;
	float alarm2_f2;
	float alarm1;	
	float alarm2;
	AlarmType alarmCheckType;
	int sensorVoltage;
}AlarmCondition;

RequiredParam requiredParamCH1[INDEX_COLUMN_MAX];
RequiredParam requiredParamCH2[INDEX_COLUMN_MAX];
RequiredParam requiredParamCH3[INDEX_COLUMN_MAX];
RequiredParam requiredParamCH4[INDEX_MIC_MAX];

#ifdef FEATURE_CLEANED_SRC_CODE
#define VEL_OVERALL 	0
#define VEL_V1 			1
#define VEL_V2 			2
#define VEL_V3 			3
#define VEL_V4 			4
#define VEL_V5 			5
#define VEL_V6 			6
#define VEL_V7 			7
#define VEL_V8 			8
#define VEL_V9 			9


#define ACC_OVERALL 	10
#define ACC_A1 			11
#define ACC_A2 			12
#define ACC_A3 			13
#define ACC_A4 			14
#define ACC_A5 			15
#define ACC_A6 			16
#define ACC_A7 			17
#define ACC_A8 			18
#define ACC_A9 			19

#define ACCVEL_MAX		20

#define MIC_OVERALL 	0
#define MIC_M1 			1
#define MIC_M2 			2
#define MIC_M3 			3
#define MIC_M4 			4
#define MIC_MAX		5

typedef struct{
	double vel_overall;
	double vel_v1;
	double vel_v2;
	double vel_v3;
	double vel_v4;
	double vel_v5;
	double vel_v6;
	double vel_v7;
	double vel_v8;
	double vel_v9;
	
	double acc_overall;
	double acc_a1;
	double acc_a2;
	double acc_a3;
	double acc_a4;
	double acc_a5;
	double acc_a6;
	double acc_a7;
	double acc_a8;
	double acc_a9;
	
	double mic_overall;
	double mic_m1;
	double mic_m2;
	double mic_m3;
	double mic_m4;
	
	double acc_peak;
	double crest_factor;
	double tacho1x;
	double tacho2x;
	double Frq120Hz;
	double tacho3xTo5x;
	double vain1x;
	double vain2x;
	double vel_partial;
}PARAM;

#endif/*FEATURE_CLEANED_SRC_CODE*/


#ifdef FEATURE_SERIAL
#define DEV_SERIAL	"/dev/ttyAMA1"
#endif



typedef struct{
	double x_value;
	double y_value;
}SPKData;



#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#define setClock() (clock() + 500000)
#define FALSE 0
#define TRUE 1

#define AD_SAMPLING_RATE	51200
#define TIME_PER_SAMPLINGRATE (1/AD_SAMPLING_RATE)
#define TAU1				0.0001
#define TAU2				0.001
#define TAU3				0.01


int bNeedForcedUpdate = FALSE;
int readyToDBClosing = 0;


clock_t clk_start;

int ch1CountTest= 0;
int ch2CountTest= 0;
int ch3CountTest= 0;
int ch4CountTest= 0;



#ifdef FEATURE_PREQUENCY
#define DEVICE_FILENAME "/dev/sigiodev"
#define MILLION 1000000L
#define DATA_COUNT 100
#define MAX_PATH 260
#define CHECK_COUNT 1000

int dev;  // sigio_handler()
struct timeval pre, now; 
long pre_usec = 0;
//int count=0;
int gpioPin = 0;
int equipmentNumber = -1;


#ifdef FEATURE_MQTT
#define ADDRESS     "tcp://192.168.0.44:1883"
#define CLIENTID    "Gateway_1"
#define TOPIC       "test/A"
#define PAYLOAD     "Hello World!"
#define QOS         1
#define TIMEOUT     10000L
#define STR_ID	    "id"
#define STR_DBM	    "dbm"
#define STR_MACH1	"machine1"
#define STR_MACH2	"machine2"
#define STR_MACH3	"machine3"





//char* address = "tcp://192.168.20.23:1883";
char* address = "tcp://159.89.196.191:1883";

typedef struct node 
{
	MQTTClient client;

	char* macaddr; 
	char* topic; 
	char* clientId; 
//	char* akey; 
}; 

struct node bleNode[4]; 



  
/* BLE node 1 */
//char* topic = "59a7e2fa553ec9078f328fdd";
//char* clientId = "BLEnode1";
//char* akey = "addd97bd649d40049fb6a4e994086853";

/* BLE node 2 */
char* topic = "59a7e3dc553ec9078f328fe2"; 
char* clientId = "BLEnode2";
char* akey = "81e80232b4c643a5b92ce2f16a468a02"; 

char* clientIdArray[2][4]={
		{"SSEMVIT_EQUIP0_CH1", "SSEMVIT_EQUIP0_CH2","SSEMVIT_EQUIP0_CH3", "SSEMVIT_EQUIP0_CH4" 	},
		{"SSEMVIT_EQUIP1_CH1", "SSEMVIT_EQUIP1_CH2","SSEMVIT_EQUIP1_CH3", "SSEMVIT_EQUIP1_CH4" 	}
};
void node_init(void)
{
	/* CHANNEL 1 */
	//bleNode[0].macaddr = "C5:2B:3C:5F:52:BE"; 
	//bleNode[0].topic = "59a7e2fa553ec9078f328fdd"; 
	bleNode[0].topic = TOPIC; 
	bleNode[0].clientId = clientIdArray[equipmentNumber][0]; 
	//bleNode[0].akey = "addd97bd649d40049fb6a4e994086853";

	/* CHANNEL 2 */
	//bleNode[1].macaddr = "F0:DD:61:2C:93:35"; 
	//bleNode[1].topic = "59a7e3dc553ec9078f328fe2"; 
	bleNode[1].topic = TOPIC;   
	bleNode[1].clientId = clientIdArray[equipmentNumber][1]; 
	//bleNode[1].akey = "81e80232b4c643a5b92ce2f16a468a02"; 

	/* CHANNEL 3 */
	//bleNode[0].macaddr = "C5:2B:3C:5F:52:BE"; 
	//bleNode[0].topic = "59a7e2fa553ec9078f328fdd"; 
	bleNode[2].topic = TOPIC; 
	bleNode[2].clientId = clientIdArray[equipmentNumber][2]; 
	//bleNode[0].akey = "addd97bd649d40049fb6a4e994086853";

	/* CHANNEL 4 */
	//bleNode[1].macaddr = "F0:DD:61:2C:93:35"; 
	bleNode[3].topic = TOPIC;	
	bleNode[3].clientId = clientIdArray[equipmentNumber][3];  

	//bleNode[1].akey = "81e80232b4c643a5b92ce2f16a468a02"; 
	
}
#endif/*FEATURE_MQTT*/

//Interruption about the Start Button
int bInterruptStartButton = FALSE;
struct gpio_info {
        unsigned int io;
        unsigned int mode;
        unsigned int value;
};
                        
struct gpio_info gInfo; 
FILE *fp; 
unsigned long timediff; 
char buff2[8*DATA_COUNT]={0,};
float timediffArray[1000]={0,};
int temp=0;
long preUsec = 0;
double total_energy = 0;
double hfd = 0;
double rsh1x = 0;
double rsh2x = 0;
double rsh3x = 0;
int rpm = 20;
int vain = 3;

#ifdef FEATURE_SERIAL
int fdSerial = 0;
struct termios newtio;
int bFlushSerialCh1 = 0;
int bFlushSerialCh2 = 0;
int bFlushSerialCh3 = 0;
int bFlushSerialCh4 = 0;
char pSerialDataCh1[1024] = {0,};
char pSerialDataCh2[1024] = {0,};
char pSerialDataCh3[1024] = {0,};
char pSerialDataCh4[1024] = {0,};

char pCH2SqlQueryBuf[1024] = {0,};
char pCH3SqlQueryBuf[1024] = {0,};
char pCH4SqlQueryBuf[1024] = {0,};

int nInitDataCountCh1 = 0;
int nInitDataCountCh2 = 0;
int nInitDataCountCh3 = 0;
int nInitDataCountCh4 = 0;

int nCompDataCountCh1 = 0;
int nCompDataCountCh2 = 0;
int nCompDataCountCh3 = 0;
int nCompDataCountCh4 = 0;

int nCountCheck1hCh1 = 0;
int nCountCheck1hCh2 = 0;
int nCountCheck1hCh3 = 0;
int nCountCheck1hCh4 = 0;



#endif

#define TABLE_NAME_CHANNEL1_INIT_DATA	"CHANNEL1_INIT_DATA_TB"
#define TABLE_NAME_CHANNEL2_INIT_DATA	"CHANNEL2_INIT_DATA_TB"
#define TABLE_NAME_CHANNEL3_INIT_DATA	"CHANNEL3_INIT_DATA_TB"
#define TABLE_NAME_CHANNEL4_INIT_DATA	"CHANNEL4_INIT_DATA_TB"

#define TABLE_NAME_CHANNEL1_BASELINE	"CHANNEL1_BASELINE_TB"
#define TABLE_NAME_CHANNEL2_BASELINE	"CHANNEL2_BASELINE_TB"
#define TABLE_NAME_CHANNEL3_BASELINE	"CHANNEL3_BASELINE_TB"
#define TABLE_NAME_CHANNEL4_BASELINE	"CHANNEL4_BASELINE_TB"

//[[180823]]chchoi-add:realtime, premaint data db table
#define TABLE_NAME_CHANNEL1_REALTIME_DATA	"CHANNEL1_REALTIME_DATA_TB"
#define TABLE_NAME_CHANNEL2_REALTIME_DATA	"CHANNEL2_REALTIME_DATA_TB"
#define TABLE_NAME_CHANNEL3_REALTIME_DATA	"CHANNEL3_REALTIME_DATA_TB"
#define TABLE_NAME_CHANNEL4_REALTIME_DATA	"CHANNEL4_REALTIME_DATA_TB"

#define TABLE_NAME_CHANNEL1_PRE_MAINT_DATA	"CHANNEL1_PRE_MAINT_DATA_TB"
#define TABLE_NAME_CHANNEL2_PRE_MAINT_DATA	"CHANNEL2_PRE_MAINT_DATA_TB"
#define TABLE_NAME_CHANNEL3_PRE_MAINT_DATA	"CHANNEL3_PRE_MAINT_DATA_TB"
#define TABLE_NAME_CHANNEL4_PRE_MAINT_DATA	"CHANNEL4_PRE_MAINT_DATA_TB"

#define COLUMN_NAME_ACC_OVERALL	"ACC_OVERALL"
#define COLUMN_NAME_ACC_A1	"ACC_A1"
#define COLUMN_NAME_ACC_A2	"ACC_A2"
#define COLUMN_NAME_ACC_A3	"ACC_A3"
#define COLUMN_NAME_ACC_A4	"ACC_A4"
#define COLUMN_NAME_ACC_A5	"ACC_A5"
#define COLUMN_NAME_ACC_A6	"ACC_A6"
#define COLUMN_NAME_ACC_A7	"ACC_A7"
#define COLUMN_NAME_ACC_A8	"ACC_A8"
#define COLUMN_NAME_ACC_A9	"ACC_A9"

#define COLUMN_NAME_VEL_OVERALL	"VEL_OVERALL"
#define COLUMN_NAME_VEL_V1	"VEL_V1"
#define COLUMN_NAME_VEL_V2	"VEL_V2"
#define COLUMN_NAME_VEL_V3	"VEL_V3"
#define COLUMN_NAME_VEL_V4	"VEL_V4"
#define COLUMN_NAME_VEL_V5	"VEL_V5"
#define COLUMN_NAME_VEL_V6	"VEL_V6"
#define COLUMN_NAME_VEL_V7	"VEL_V7"
#define COLUMN_NAME_VEL_V8	"VEL_V8"
#define COLUMN_NAME_VEL_V9	"VEL_V9"



#define COLUMN_NAME_MIC_OVERALL	"MIC_OVERALL"
#define COLUMN_NAME_MIC_M1		"MIC_M1"
#define COLUMN_NAME_MIC_M2		"MIC_M2"
#define COLUMN_NAME_MIC_M3		"MIC_M3"
#define COLUMN_NAME_MIC_M4		"MIC_M4"
#define COLUMN_NAME_MIC_M5		"MIC_M5"
#define COLUMN_NAME_MIC_M6		"MIC_M6"
#define COLUMN_NAME_MIC_M7		"MIC_M7"
#define COLUMN_NAME_MIC_M8		"MIC_M8"
#define COLUMN_NAME_MIC_M9		"MIC_M9"
 







#define PARAM_TYPE_VEL_OVERALL	"V0"
#define PARAM_TYPE_VEL_V1			"V1"
#define PARAM_TYPE_VEL_V2			"V2"
#define PARAM_TYPE_VEL_V3			"V3"
#define PARAM_TYPE_VEL_V4			"V4"
#define PARAM_TYPE_VEL_V5			"V5"
#define PARAM_TYPE_VEL_V6			"V6"
#define PARAM_TYPE_VEL_V7			"V7"
#define PARAM_TYPE_VEL_V8			"V8"
#define PARAM_TYPE_VEL_V9			"VP2P"

#define PARAM_TYPE_ACC_OVERALL	"A0"
#define PARAM_TYPE_ACC_A1			"A1"
#define PARAM_TYPE_ACC_A2			"A2"
#define PARAM_TYPE_ACC_A3			"A3"
#define PARAM_TYPE_ACC_A4			"A4"
#define PARAM_TYPE_ACC_A5			"A5"
#define PARAM_TYPE_ACC_A6			"A6"
#define PARAM_TYPE_ACC_A7			"A7"
#define PARAM_TYPE_ACC_A8			"A8"
#define PARAM_TYPE_ACC_A9			"AP2P"

#define PARAM_TYPE_MIC_OVERALL	"M0"
#define PARAM_TYPE_MIC_M1			"M1"
#define PARAM_TYPE_MIC_M2			"M2"
#define PARAM_TYPE_MIC_M3			"M3"
#define PARAM_TYPE_MIC_M4			"M4"
#define PARAM_TYPE_MIC_M5			"M5"
#define PARAM_TYPE_MIC_M6			"M6"
#define PARAM_TYPE_MIC_M7			"M7"
#define PARAM_TYPE_MIC_M8			"M8"
#define PARAM_TYPE_MIC_M9			"M9"


#define PARAM_TYPE_ALARM_V		"AL_V"
#define PARAM_TYPE_ALARM_R1		"AL_R1"
#define PARAM_TYPE_ALARM_R2		"AL_R2"


#define PARAM_TYPE_BASELINE		"BL"
#define PARAM_TYPE_SENSOR_VOLTAGE	"VLT"

#define COUNT_REALTIME_DATA_COLUMN		15

#define ALERT1					1
#define ALERT2					2

#define INIT_DATA_COUNT_MAX	(12*60*60)
#define COUNT_PER_AHOUR (60*60)
#define CHECK_DATA_COUNT	2
#define PERCENT2(x)	(x*2/100)
#define PERCENT(x, y)	(x*y/100)

#define PARAM_COUNT	23


#define DATA_COUNT_PREMAINT	(2*60)

#define QUERY_BUF_LENGTH	512



static void sigio_handler(int signo)
{
	
	//char buff[128]; 

	unsigned long result;
	//read(dev, buff, 1); 
}
#endif/*FEATURE_PREQUENCY*/


static void pabort(const char *s)
{
	perror(s);
	abort();
}

static const char *device = "/dev/spidev2.0";
static const char *filename = "data.dat";
static const char *cfgFilename = "config/a.cfg";

static uint32_t mode;
static uint8_t bits;
static uint32_t speed = 500000;
static uint16_t delay = 0;
static int verbose;
static int8_t runf; 
static uint8_t printOption; 
static int sampleNumber = 0; 
//static int gTimeout = 3600*24; 
static int rnumber = -1; 
static uint32_t rdata; 
static int flag = 1;  // all 
unsigned char* pADDataBuf= NULL;
unsigned char* pADCh1DataBuf = NULL;
unsigned char* pADCh2DataBuf = NULL;
unsigned char* pADCh3DataBuf = NULL;
unsigned char* pADCh4DataBuf = NULL;

unsigned int nADCh1BufOffset = 0;
unsigned int nADCh2BufOffset = 0;
unsigned int nADCh3BufOffset = 0;
unsigned int nADCh4BufOffset = 0;
int bCleanTable = 0;

pthread_t ad1_fft_pthread;
pthread_t ad2_fft_pthread;
pthread_t ad3_fft_pthread;
pthread_t ad4_fft_pthread;
#ifdef FEATURE_SERIAL
pthread_t serial_pthread;
#endif

pthread_t ad1_lpf_pthread;
pthread_t ad2_lpf_pthread;
pthread_t ad3_lpf_pthread;
pthread_t ad4_lpf_pthread;


pthread_t ad1_pub_pthread;
pthread_t ad2_pub_pthread;
pthread_t ad3_pub_pthread;
pthread_t ad4_pub_pthread;




pthread_t tacho_pthread;

int gAD1FlagLowPassFilterEnable= FALSE;
int gAD2FlagLowPassFilterEnable= FALSE;
int gAD3FlagLowPassFilterEnable= FALSE;
int gAD4FlagLowPassFilterEnable= FALSE;

float gTauValue = TAU2;
float time_per_samples=0.00001953125;/*1/51200*/


static char* pShortFileBuf = NULL;
int gChannel = -1;
FILE* fp_ch4 = NULL;
static const char *device1 = "/dev/spidev1.0"; 

static const char *device2 = "/dev/spidev1.1"; 
static const char *device3 = "/dev/spidev1.2"; 
static const char *device4 = "/dev/spidev1.3"; 
#ifdef FEATURE_SINGLE_CHANNEL
pthread_mutex_t singleQueueMutex;
pthread_cond_t gSignal;

#else
pthread_mutex_t ch1QueueMutex;
pthread_mutex_t ch2QueueMutex;
pthread_mutex_t ch3QueueMutex;
pthread_mutex_t ch4QueueMutex;
#endif/*FEATURE_SINGLE_CHANNEL*/
static int dataFlag = 0;
pthread_mutex_t writeFileMutex;

float lf1 = 0;
float lf2 = 0;
float ltacho = 0;


/* time */
#define MILLION 1000000L

/* regster */ 

#define REGISTER_ADMODE_ID 99 

#define MODE_TIME 1
#define MODE_FFT 2 
#define MODE_FIR 3 
#define MODE_RMS 4 

int gADMode = MODE_TIME; 

//[[180605]]chchoi-mod:fixed data count per 1Min
#define NB_REGISTER 16
static uint32_t REGDATA_inner[NB_REGISTER] = {MODE_TIME, };

int deviceflag = 0; 	

#define FLAG_AD1 1
#define FLAG_AD2 2
#define FLAG_AD3 4
#define FLAG_AD4 8
#define FLAG_AD_ALL 15

#define DATA_UNIT_SIZE 2*1024
/* processing */

#define PROCESS_RECODING        1
#define PROCESS_SETREG          2
#define PROCESS_READREG         3
#define PROCESS_MODESET 		4
#define PROCESS_AUTOSAVE 		5 
#define PROCESS_STREAM			6
#define PROCESS_WEB_STREAM	7

/* For SPI */
#define DATA_HEADER_SIZE 4
#define BUFFER_SIZE (DATA_HEADER_SIZE + 2048) 
#define MAX_SAMPLE 32 
//#define BUFFER_SIZE (DATA_HEADER_SIZE + 1024) 
//#define MAX_SAMPLE 32 

#define CMD_TEST            0x10101010
#define CMD_DATA            0x29380000

#define CMD_SET_MODE 	    0x68390284 
#define CMD_GET_MODE 	    0x68380284 

#define CMD_STATUS          0x68390384
#define CMD_END             0x68390484

#define CMD_READ_REG	    0x30380000
#define CMD_READ			0x30380100

#define CMD_SET_REG			0x30390000
#define CMD_SET_REGA        0x30390100

#define CMD_CHANGE_TRMODE   0x40400000
#define CMD_REQ_CNT 	    0x28380000


#define RC_SYN              0x55AA55AA
#define RC_RDY              0x12345678

#define VEL_OVERALL_F1	10
#define VEL_OVERALL_F2	1000

#define VEL_V1_F1			5
#define VEL_V1_F2			200
#define VEL_V2_F1			300
#define VEL_V2_F2			1000
/*

#define VEL_OVERALL_F1	10
#define VEL_OVERALL_F2	1000

#define VEL_V1_F1			30
#define VEL_V1_F2			110
#define VEL_V2_F1			100
#define VEL_V2_F2			200
#define VEL_V3_F1			5
#define VEL_V3_F2			50
#define VEL_V4_F1			200
#define VEL_V4_F2			500
#define VEL_V5_F1			500
#define VEL_V5_F2			1000

*/
#define ACC_OVERALL_F1	500
#define ACC_OVERALL_F2	10000

#define ACC_A1_F1			500
#define ACC_A1_F2			2500
#define ACC_A2_F1			2500
#define ACC_A2_F2			10000
#define ACC_A3_F1			500
#define ACC_A3_F2			3000
/*
#define ACC_OVERALL_F1	500
#define ACC_OVERALL_F2	10000

#define ACC_A1_F1			2000
#define ACC_A1_F2			10000
#define ACC_A2_F1			500
#define ACC_A2_F2			1500
#define ACC_A3_F1			500
#define ACC_A3_F2			3000
#define ACC_A4_F1			1000
#define ACC_A4_F2			2500
#define ACC_A5_F1			2500
#define ACC_A5_F2			5000
#define ACC_A6_F1			5000
#define ACC_A6_F2			10000
#define ACC_A7_F1			10000
#define ACC_A7_F2			25600
*/
#define MIC_OVERALL_F1	1000
#define MIC_OVERALL_F2	10000

#define MIC_M1_F1			10
#define MIC_M1_F2			1000

#define MIC_M2_F1			1000
#define MIC_M2_F2			5000

#define MIC_M3_F1			5000
#define MIC_M3_F2			10000

#define MIC_M4_F1			10000
#define MIC_M4_F2			16000

#define COLUMN_MAX			15

struct request {
  uint32_t cmd; 
  uint32_t data[NB_REGISTER]; 
};

static struct request reg; 

uint8_t default_rx[BUFFER_SIZE] = { 0x00, }; 
uint32_t send_rx[2];  

uint32_t CMD_READREGDATA[NB_REGISTER] = {0x00, }; 
uint8_t CMD_READDATA_tx[BUFFER_SIZE] = {0x84, 0x03, 0x39, 0x68}; 

uint32_t CMD_SETREG_rx[2]; 
uint32_t CMD_REQ_CNT_tx[2] = {CMD_REQ_CNT, };
uint32_t CMD_TEST_tx[2] = { CMD_TEST, };
uint32_t CMD_DATA_tx[2] = { CMD_DATA, }; 
uint32_t CMD_STATUS_tx[2] = { CMD_STATUS, }; 
uint32_t CMD_END_tx[2] = { CMD_END, }; 
uint32_t CMD_READREG_tx[2] = { CMD_READ_REG, }; 
uint32_t CMD_READREGA_tx[2] = { CMD_READ, }; 
uint32_t CMD_SETREG_tx[2] = { CMD_SET_REG, };
uint32_t CMD_SETREGA_tx[2] = { CMD_SET_REGA, }; 

//uint8_t BUF_AD1[DATA_UNIT_SIZE * 20] = {0,};
//uint8_t BUF_AD2[DATA_UNIT_SIZE * 20] = {0,};
//uint8_t BUF_AD3[DATA_UNIT_SIZE * 20] = {0,};
//uint8_t BUF_AD4[DATA_UNIT_SIZE * 20] = {0,};

#ifdef FEATURE_SINGLE_CHANNEL
LinkedQueue* gLinkedQueue = NULL;
#else 
LinkedQueue* gLinkedQueueAD1 = NULL;
LinkedQueue* gLinkedQueueAD2 = NULL;
LinkedQueue* gLinkedQueueAD3 = NULL;
LinkedQueue* gLinkedQueueAD4 = NULL;
#endif/*FEATURE_SINGLE_CHANNEL*/

LinkedQueue* gAD1WillSendQ = NULL;
LinkedQueue* gAD2WillSendQ = NULL;
LinkedQueue* gAD3WillSendQ = NULL;
LinkedQueue* gAD4WillSendQ = NULL;





float convertDataBuf1[AD_SAMPLING_RATE]={0,};
float convertDataBuf2[AD_SAMPLING_RATE]={0,};
float convertDataBuf3[AD_SAMPLING_RATE]={0,};
float convertDataBuf4[AD_SAMPLING_RATE]={0,};

float lowPassFilterBuf1[AD_SAMPLING_RATE]={0,};
float lowPassFilterBuf2[AD_SAMPLING_RATE]={0,};
float lowPassFilterBuf3[AD_SAMPLING_RATE]={0,};
float lowPassFilterBuf4[AD_SAMPLING_RATE]={0,};

int sample_rate = 1;
int sample_count  = AD_SAMPLING_RATE;


char *input_tx;
int stream_state = -1;
int isStartAll = 0;
int isNeedSetADMode = 0;
unsigned char value;
char p[4];
FILE *fpx, *fp1, *fp2, *fp3, *fp4;
FILE *fpLogCh1 = NULL;
FILE *fpLogCh2 = NULL;
FILE *fpLogCh3 = NULL;
FILE *fpLogCh4 = NULL;

FILE *fpPreMainPer5minCh1 = NULL;
FILE *fpPreMainPer5minCh2 = NULL;
FILE *fpPreMainPer5minCh3 = NULL;
FILE *fpPreMainPer5minCh4 = NULL;


int fdx, fd1, fd2, fd3, fd4; 
int ch1LogMinValue = -1;
int ch2LogMinValue = -1;
int ch3LogMinValue = -1;
int ch4LogMinValue = -1;
int testcount = 0;
int ch1FlushCount = 0;
int ch2FlushCount = 0;
int ch3FlushCount = 0;
int ch4FlushCount = 0;

double gParamCh1[COLUMN_MAX];
double gParamCh2[COLUMN_MAX];
double gParamCh3[COLUMN_MAX];
double gParamCh4[COLUMN_MAX];

//param sum to compare with Baseline Data
//We will sum params to be the data count for 5 mins.
double gParamCompCh1[DATA_COUNT_PREMAINT][COLUMN_MAX];
double gParamCompCh2[DATA_COUNT_PREMAINT][COLUMN_MAX];
double gParamCompCh3[DATA_COUNT_PREMAINT][COLUMN_MAX];
double gParamCompCh4[DATA_COUNT_PREMAINT][COLUMN_MAX];

/*
PARAM gParamCompMeanCh1;
PARAM gParamCompMeanCh2;
PARAM gParamCompMeanCh3;
PARAM gParamCompMeanCh4;

PARAM gParamCompStdCh1;
PARAM gParamCompStdCh2;
PARAM gParamCompStdCh3;
PARAM gParamCompStdCh4;
*/

#if 1
double gMeanBaselineParamCh1[COLUMN_MAX];
double gStdevBaselineParamCh1[COLUMN_MAX];
double gMeanBaselineParamCh2[COLUMN_MAX];
double gStdevBaselineParamCh2[COLUMN_MAX];
double gMeanBaselineParamCh3[COLUMN_MAX];
double gStdevBaselineParamCh3[COLUMN_MAX];
double gMeanBaselineParamCh4[COLUMN_MAX];
double gStdevBaselineParamCh4[COLUMN_MAX];

#else
PARAM gMeanBaselineParamCh1;
PARAM gStdevBaselineParamCh1;
PARAM gMeanBaselineParamCh2;
PARAM gStdevBaselineParamCh2;
PARAM gMeanBaselineParamCh3;
PARAM gStdevBaselineParamCh3;
PARAM gMeanBaselineParamCh4;
PARAM gStdevBaselineParamCh4;
#endif

double gParamCompMeanCh1[COLUMN_MAX];
double gParamCompDevPer5MinCh1[COLUMN_MAX];

double gParamCompMeanCh2[COLUMN_MAX];
double gParamCompDevPer5MinCh2[COLUMN_MAX];

double gParamCompMeanCh3[COLUMN_MAX];
double gParamCompDevPer5MinCh3[COLUMN_MAX];

double gParamCompMeanCh4[COLUMN_MAX];
double gParamCompDevPer5MinCh4[COLUMN_MAX];

AlarmCondition alarmContionCH1;

AlarmCondition alarmContionCH2;
AlarmCondition alarmContionCH3;
AlarmCondition alarmContionCH4;




char* gParamTypeArray[INDEX_MAX] = {
		PARAM_TYPE_VEL_OVERALL,
		PARAM_TYPE_VEL_V1,
		PARAM_TYPE_VEL_V2,
		PARAM_TYPE_VEL_V3,
		PARAM_TYPE_VEL_V4,
		PARAM_TYPE_VEL_V5,
		PARAM_TYPE_VEL_V6,
		PARAM_TYPE_VEL_V7,
		PARAM_TYPE_VEL_V8,
		PARAM_TYPE_VEL_V9,		
		PARAM_TYPE_ACC_OVERALL,
		PARAM_TYPE_ACC_A1,
		PARAM_TYPE_ACC_A2,
		PARAM_TYPE_ACC_A3,
		PARAM_TYPE_ACC_A4,
		PARAM_TYPE_ACC_A5,
		PARAM_TYPE_ACC_A6,
		PARAM_TYPE_ACC_A7,
		PARAM_TYPE_ACC_A8,
		PARAM_TYPE_ACC_A9,
		PARAM_TYPE_ALARM_V,
		PARAM_TYPE_ALARM_R1,	
		PARAM_TYPE_ALARM_R2,			
		PARAM_TYPE_BASELINE,
		PARAM_TYPE_SENSOR_VOLTAGE,
	};

char* gParamTypeArrayMIC[INDEX_MIC_MAX] = {
		PARAM_TYPE_MIC_OVERALL,
		PARAM_TYPE_MIC_M1,
		PARAM_TYPE_MIC_M2,
		PARAM_TYPE_MIC_M3,
		PARAM_TYPE_MIC_M4,
		PARAM_TYPE_MIC_M5,
		PARAM_TYPE_MIC_M6,
		PARAM_TYPE_MIC_M7,
		PARAM_TYPE_MIC_M8,
		PARAM_TYPE_MIC_M9,		
		PARAM_TYPE_ALARM_V,
		PARAM_TYPE_ALARM_R1,	
		PARAM_TYPE_ALARM_R2,			
		PARAM_TYPE_SENSOR_VOLTAGE,
		
	};

char* gColumnNameArray[20] = {
		COLUMN_NAME_VEL_OVERALL,
		COLUMN_NAME_VEL_V1,
		COLUMN_NAME_VEL_V2,
		COLUMN_NAME_VEL_V3,
		COLUMN_NAME_VEL_V4,
		COLUMN_NAME_VEL_V5,
		COLUMN_NAME_VEL_V6,
		COLUMN_NAME_VEL_V7,
		COLUMN_NAME_VEL_V8,
		COLUMN_NAME_VEL_V9,
		COLUMN_NAME_ACC_OVERALL,
		COLUMN_NAME_ACC_A1,
		COLUMN_NAME_ACC_A2,
		COLUMN_NAME_ACC_A3,
		COLUMN_NAME_ACC_A4,
		COLUMN_NAME_ACC_A5,
		COLUMN_NAME_ACC_A6,
		COLUMN_NAME_ACC_A7,
		COLUMN_NAME_ACC_A8,
		COLUMN_NAME_ACC_A9, 	
	};

char* gColumnNameArrayMIC[INDEX_MIC_MAX] = {
		COLUMN_NAME_MIC_OVERALL,
		COLUMN_NAME_MIC_M1,
		COLUMN_NAME_MIC_M2,
		COLUMN_NAME_MIC_M3,
		COLUMN_NAME_MIC_M4,
		COLUMN_NAME_MIC_M5,
		COLUMN_NAME_MIC_M6,
		COLUMN_NAME_MIC_M7,
		COLUMN_NAME_MIC_M8,
		COLUMN_NAME_MIC_M9,
 	};


	
//double gCh1Baseline[ACCVEL_MAX]={0.0,};
double gCh2Baseline[ACCVEL_MAX]={0.0,};
double gCh3Baseline[ACCVEL_MAX]={0.0,};

char gQueryCH1[1024];
char gQueryCH2[1024];
char gQueryCH3[1024];
char gQueryCH4[1024];




int bHasBaselineCh1 = FALSE;
int bHasBaselineCh2 = FALSE;
int bHasBaselineCh3 = FALSE;
int bHasBaselineCh4 = FALSE;

int nBaselineCount = 0;
int nSensorVoltage = 0;
int gParamCountCH1 = -1;
int gParamCountCH2 = -1;
int gParamCountCH3 = -1;
int gParamCountCH4 = -1;



sqlite3 *db;

int gInitFilename = 0;
char* Channel1SensorId[2][ACCVEL_MAX]={
	{
		"81e355af-7e47-4322-a5a2-deca94bfc1aa",//ACC_Overall
		"fda45954-6e6b-4bd9-b483-90c59accf28f",//ACC_A1
		"f4f92a16-20c6-4ddb-a6d7-bab7836a5627",//ACC_A2
		"d4448edc-3c83-4caf-bdec-8f0acdc7e4c6",//ACC_A3
		"5958d313-ca89-4d46-b0c0-5550fb5ef261",//ACC_A4
		"8abd8cc5-e294-4df8-9238-803df314db57",//ACC_A5
		"5c0c9f3f-9cd2-4f58-9aa5-328cb9ac913b",//ACC_A6
		"264977cd-afbf-4921-8735-ba8b669a8968",//ACC_A7
		"de0f4dce-3280-4040-9dbf-0cafb82707c2",//VEL_Overall
		"8b50e6c0-0dd8-46f8-8d28-2efe3cb26cf0",//VEL_V1
		"c04e145d-a8ea-47ba-9bd6-80640db3491e",//VEL_V2
		"a462375f-14fd-437d-9a56-0dece9f6e0a3",//VEL_V3
		"d3489298-0148-4e22-9868-68bfa7a84fb5",//VEL_V4
		"5337cdce-eeaf-4efe-befd-f8fde50b4208",//VEL_V5
	},
	{
		"62a49b22-5503-4186-8d5a-99b80d351a86",//ACC_Overall
		"7e8c8b5d-bcbd-48be-829f-06cbdf9e6818",//ACC_A1
		"1c6758b7-7aef-4ae4-b29d-4f76a8ef3074",//ACC_A2
		"a8d2e626-1c1d-43eb-bddb-5ad4a38b3e0f",//ACC_A3
		"cd3e56cc-f98a-4f24-943b-d1f66555d53b",//ACC_A4
		"4b1975c0-48f9-436a-b17d-6e83db8e2399",//ACC_A5
		"578dd848-579a-4895-98a1-be0b7175d8cb",//ACC_A6
		"bc0707c0-3d15-48b9-b9f0-7dbdb8209357",//ACC_A7
		"e7c5f927-31ea-4843-bd64-d7f0bf3b3b3a",//VEL_Overall
		"6e4d1744-60cc-40d6-b3ea-be8c09be5751",//VEL_V1
		"92d18092-7a93-4e0b-ad9d-c9c7bf3c29b5",//VEL_V2
		"be709fdb-f65b-48ce-be6d-f18567517811",//VEL_V3
		"04cb91db-3ac2-47d9-bc9d-5b13f7363218",//VEL_V4
		"5586e651-a0bd-42dd-bd60-debfdee8a4dc",//VEL_V5
	}
};


char* Channel2SensorId[2][ACCVEL_MAX]={
	{
		"16f595a6-c8b6-4a93-bc43-0ba4b0474485",//ACC_Overall
		"d336e13f-5ff2-4e10-a178-84d09bd056b4",//ACC_A1
		"ea0ee0af-a067-45de-a427-db50baa2a870",//ACC_A2
		"e5e67df9-dbae-4ce1-aeae-d7a30ded72e4",//ACC_A3
		"b23d27b3-9bb2-42ff-882d-b607aa3dde1d",//ACC_A4
		"3ec1335e-367e-4efa-a919-6311a961f00d",//ACC_A5
		"fde540ad-d3fd-480d-a006-d10f0188a382",//ACC_A6
		"c22c977a-1afd-4ef9-9fa5-96e6922a7308",//ACC_A7
		"a93d5352-6171-4ecb-a378-82ddb384bbbf",//VEL_Overall
		"9537b852-cdf9-4565-8296-eb926b82d8ae",//VEL_V1
		"5ee98536-9e9b-4e36-a1bb-642f48604ccf",//VEL_V2
		"304992e5-0749-42d1-b390-f1745404d9a6",//VEL_V3
		"c22a937d-11cb-44e3-94c1-b4511995ee5d",//VEL_V4
		"11a90854-813f-4418-9d7c-47ddb4fa5fdd",//VEL_V5
	},
	{
		"dee4b70a-ea9b-4e3f-b0ac-bf8f2eb92350",//ACC_Overall
		"d4d7f5e3-fa15-4986-b22d-0ac81100375e",//ACC_A1
		"4b319a4d-5696-48b5-992e-1a1d29453652",//ACC_A2
		"63e5ea3b-5fd8-4988-b644-8966b6bd9827",//ACC_A3
		"e8c777a3-656b-45d4-ad20-f2f04c3acb12",//ACC_A4
		"96b83cab-f60f-40c9-a070-37c982acb627",//ACC_A5
		"31848d1f-8d27-4298-9f4e-d3e6306b8e19",//ACC_A6
		"1cc9a811-acff-4eb8-80c8-633f1acebe9e",//ACC_A7
		"8db51bd1-3cb9-45c0-8b40-a4f32e8301b1",//VEL_Overall
		"439886c5-f633-41e0-b972-f8e70059813c",//VEL_V1
		"e4397afa-faf0-47d9-a13e-884c0f7cf5f4",//VEL_V2
		"e111e29a-85df-4af6-a6a3-eff79bd7774d",//VEL_V3
		"04a8486b-7e69-45d5-99ee-06244bd1937f",//VEL_V4
		"4005352c-dd12-4b6e-a150-c56c0fc51d98",//VEL_V5	
	}
};
char* Channel3SensorId[2][ACCVEL_MAX]={
	{
		"d2f1281f-ec81-427b-9a29-b0b579074dd5",//ACC_Overall
		"5e3bf3c2-1a2f-4b33-91ff-4fc0ad5cedd5",//ACC_A1
		"a16caf08-36fa-4e46-8603-e09bdfe8d799",//ACC_A2
		"92cef7fb-64a3-4046-a7f2-73da76913d14",//ACC_A3
		"17cb7e34-8acf-4d4f-bb87-9987cf228a42",//ACC_A4
		"6389695a-5d6a-44e3-801d-fd65a3596dd4",//ACC_A5
		"7edaa669-7c25-46f7-a149-e205b882f931",//ACC_A6
		"52d94882-d9ab-4dc7-b167-3a82ef5f7ff4",//ACC_A7
		"1a167d11-09a9-4f80-b434-a880ffeb6255",//VEL_Overall
		"27f896b6-4b41-4c18-8152-6c31e66aad54",//VEL_V1
		"c2639f1a-eb9d-4ebd-9f3e-fb5be8cdf8bc",//VEL_V2
		"35a62a28-d752-4e25-aad5-f34a52b980ae",//VEL_V3
		"fd694a37-d563-419d-8b1e-98e00a25fd5d",//VEL_V4
		"5a418a75-ef7a-43ed-ade4-20eed8792abb",//VEL_V5
	},
	{
		"e7a488f5-19c6-4167-9c25-7d35fafb5ac1",//ACC_Overall
		"b493c80e-cbe1-4e61-b6b6-370d4e69d4ed",//ACC_A1
		"42eb9f5e-16e5-4205-8764-9dc4ce57b76b",//ACC_A2
		"e29c0b41-d92c-4d43-9644-49a1f05258c3",//ACC_A3
		"2ce1862f-53b3-4560-a48c-178b37690b5f",//ACC_A4
		"029f9486-c2d9-4566-9cc5-2093b7561deb",//ACC_A5
		"cb7c7269-328e-4fb9-b14b-f6b4a71ad648",//ACC_A6
		"031e7d58-8652-4111-8523-1b539eec1f7f",//ACC_A7
		"e1d43e46-1e0e-43db-9f27-5cea907dae40",//VEL_Overall
		"ba3e6842-f961-4fcb-985d-a0836294718c",//VEL_V1
		"32052c77-1663-4981-b372-c3fc00b2924f",//VEL_V2
		"22dcbed3-aa32-49b6-9711-0af3568b27e2",//VEL_V3
		"9b5e8e2e-9fe0-477b-9544-32ec9a3c982a",//VEL_V4
		"22dd60af-05fa-498d-9eaf-89d32bd0f35a",//VEL_V5
	}
};

char* Channel4SensorId[2][MIC_MAX]={
	{
		"34b07920-3464-46d2-9767-f0bc35774fe1",//MIC_Overall
		"411ec4d7-0024-4141-9049-b55f084ee417",//MIC_M1
		"2de58838-3d22-41a0-9cfb-76df4c33bc80",//MIC_M2
		"b18dfa95-01ae-488e-b008-61b820ceff6c",//MIC_M3
		"cb373c9b-9af1-4254-baa2-b73cca11a0fc",//MIC_M4
	},
	{
		"e508245a-185c-41ae-a187-af05def9ffc9",//MIC_Overall
		"7c0f6c7f-1fff-4b72-ac72-1a86d951f0cb",//MIC_M1
		"c89eec4d-dbef-478b-b4b9-aec5f8ee02a2",//MIC_M2
		"e3c8a76f-c7e4-4777-8f66-1d0481099016",//MIC_M3
		"86066174-11e9-4b3a-b1d2-fd5c35a5c4cc",//MIC_M4
	},
};


fftw_complex *gpCH1FFT_Input = NULL;
fftw_complex *gpCH1FFT_Output = NULL;
fftw_complex *gpCH1InvFFT_Input = NULL;
fftw_complex *gpCH1InvFFT_Output = NULL;
fftw_plan gCH1Plan = NULL;
fftw_plan gCH1PlanInverse = NULL;

fftw_complex *gpCH2FFT_Input = NULL;
fftw_complex *gpCH2FFT_Output = NULL;
fftw_complex *gpCH2InvFFT_Input = NULL;
fftw_complex *gpCH2InvFFT_Output = NULL;
fftw_plan gCH2Plan = NULL;
fftw_plan gCH2PlanInverse = NULL;


fftw_complex *gpCH3FFT_Input = NULL;
fftw_complex *gpCH3FFT_Output = NULL;
fftw_complex *gpCH3InvFFT_Input = NULL;
fftw_complex *gpCH3InvFFT_Output = NULL;
fftw_plan gCH3Plan = NULL;
fftw_plan gCH3PlanInverse = NULL;

fftw_complex *gpCH4FFT_Input = NULL;
fftw_complex *gpCH4FFT_Output = NULL;
fftw_complex *gpCH4InvFFT_Input = NULL;
fftw_complex *gpCH4InvFFT_Output = NULL;
fftw_plan gCH4Plan = NULL;
fftw_plan gCH4PlanInverse = NULL;



static void hex_dump_p(const void *src, size_t length, size_t line_size, char *prefix)
{
	int i = 0;
	const unsigned char *address = src;
	const unsigned char *line = address;
	unsigned char c;
	printf("%s | ", prefix);
	
	while (length-- > 0) {
		printf("%02X ", *address++);
		if (!(++i % line_size) || (length == 0 && i % line_size)) {
			if (length == 0) {
				while (i++ % line_size)
					printf("__ ");
			}
			printf(" | ");  /* right close */
			while (line < address) {
				c = *line++;
				printf("%c", (c < 33 || c == 255) ? 0x2E : c);
			}
			printf("\n");
			if (length > 0)
				printf("%s | ", prefix);
		}
	}
}

static void hex_dump(int f_p,FILE* f_p2, const void *src, size_t length, size_t line_size, char *prefix, uint8_t flag)
{
	int i = 0;
	const unsigned char *address = src;
	const unsigned char *line = address;
	unsigned char c;

  switch (flag) 
  {
    case 0 :  // binary
 //    fwrite(f_p2, src, length);
	 
	
      //fwrite(src, length, 1, f_p2);
	   
    break; 
    #if 0
    case 2 :  // float
    {
      uint32_t data32; 
      
      while (length > 0)
      {
        data32 = *address;
        data32 = data32 | *(address+1) << 8; 
        data32 = data32 | *(address+2) << 16; 
        data32 = data32 | *(address+3) << 24; 
        address = address +4; 
        
        //fwrite((float)data32, sizeof(data32), 4, f_p);       
        fprintf(f_p, "%x : %f\n", data32, (float)data32); 
        length = length - 4; 
        
      }
    }
    
    break; 
    
    case 1 :    // hex 
    default:
    {  
    	fprintf(f_p, "%s | ", prefix);
    	
    	while (length-- > 0) {
    		fprintf(f_p, "%02X ", *address++);
    		if (!(++i % line_size) || (length == 0 && i % line_size)) {
    			if (length == 0) {
    				while (i++ % line_size)
    					fprintf(f_p, "__ ");
    			}
    		#if 0	
    			fprintf(f_p, " | ");  /* right close */
    			while (line < address) {
    				c = *line++;
    				fprintf(f_p, "%c", (c < 33 || c == 255) ? 0x2E : c);
    			}
        #endif 
    			fprintf(f_p, "\n");
    			if (length > 0)
    				fprintf(f_p, "%s | ", prefix);
    		}
    	}
    }
    break; 
	#endif
  }
}

/*
 *  Unescape - process hexadecimal escape character
 *      converts shell input "\x23" -> 0x23
 */
static int unescape(char *_dst, char *_src, size_t len)
{
	int ret = 0;
	char *src = _src;
	char *dst = _dst;
	unsigned int ch;

	while (*src) {
		if (*src == '\\' && *(src+1) == 'x') {
			sscanf(src + 2, "%2x", &ch);
			src += 4;
			*dst++ = (unsigned char)ch;
		} else {
			*dst++ = *src++;
		}
		ret++;
	}
	return ret;
}

struct spi_ioc_transfer tr_cmd[2]; 
struct spi_ioc_transfer tr_data; 

static int transfer(int fd, FILE* fp, uint8_t const *tx, uint8_t const *rx, size_t len, uint8_t flag)
{
	int ret;
	
	struct spi_ioc_transfer tr_data = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = len, 
		.delay_usecs = delay,
		.speed_hz = speed,
		.bits_per_word = bits,
	};
#if 0
	if (mode & SPI_TX_QUAD)
		tr_data.tx_nbits = 4;
	else if (mode & SPI_TX_DUAL)
		tr_data.tx_nbits = 2;
	if (mode & SPI_RX_QUAD)
		tr_data.rx_nbits = 4;
	else if (mode & SPI_RX_DUAL)
		tr_data.rx_nbits = 2;
	if (!(mode & SPI_LOOP)) {
		if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
			tr_data.rx_buf = 0;
		else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
			tr_data.tx_buf = 0;
	}
#endif
	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr_data);
	
	if (ret < 1)
		pabort("can't send spi message");

//  if (verbose)
//		  hex_dump(tx, len, len, "TX", flag);

#if 1 

	if ( rx[0] == 0x00 && rx[1] == 0x00 && rx[2] == 0x38 & rx[3] == 0x29)
  	{
  		//hex_dump(fp,fp2, &rx[4], (len-DATA_HEADER_SIZE), (len-DATA_HEADER_SIZE), "RX", flag);
  	}
  	else 
  	{
  		printf(" spi data error\n"); 

#ifdef DEBUG
  		printf(" %d spi data error\n", fd); 
  		printf("%x:%x:%x:%x\n", rx[0], rx[1], rx[2], rx[3]); 
#endif 
  		return -1; 
  	}
#else  		
  hex_dump(rx, len, len, "RX", flag);
#endif 
	return 0; 
}
 
 static int transferBuffer(int fd, LinkedQueue* queue, uint8_t const *tx, uint8_t const *rx, size_t len, uint8_t flag, int totalCnt, int index, int channel_num)
 {
	int ret, i, k;
	unsigned char *dataAllocPtr = NULL;
	unsigned char *dataTempPtr = NULL;	
	unsigned int nDataOffset = 0;
	
	char byteBuffer=0;
	char testbuf[4] = {0,};
	unsigned char value2 = 0;
	short value=0;
	int retVal = 0;
	unsigned char fileName = 0;
	
	 struct timeval timeValue;
	 struct tm *ptmValue;
	
	 struct spi_ioc_transfer tr_data = {
		 .tx_buf = (unsigned long)tx,
		 .rx_buf = (unsigned long)rx,
		 .len = len, 
		 .delay_usecs = delay,
		 .speed_hz = speed,
		 .bits_per_word = bits,
	 };
#if 0	 
	 if (mode & SPI_TX_QUAD)
		 tr_data.tx_nbits = 4;
	 else if (mode & SPI_TX_DUAL)
		 tr_data.tx_nbits = 2;
	 if (mode & SPI_RX_QUAD)
		 tr_data.rx_nbits = 4;
	 else if (mode & SPI_RX_DUAL)
		 tr_data.rx_nbits = 2;
	 if (!(mode & SPI_LOOP)) {
		 if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
			 tr_data.rx_buf = 0;
		 else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
			 tr_data.tx_buf = 0;
	 }
 #endif
	 ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr_data);
	 
	 if (ret < 1)
		 pabort("can't send spi message");
 
 //  if (verbose)
 // 	   hex_dump(tx, len, len, "TX", flag);
 
#if 1 
		switch(channel_num)
		{
			case 1:
				nDataOffset = nADCh1BufOffset;		
				dataTempPtr = pADCh1DataBuf;
				break;

			case 2:
				nDataOffset = nADCh2BufOffset;				
				dataTempPtr = pADCh2DataBuf;				
				break;

			case 3:
				nDataOffset = nADCh3BufOffset;				
				dataTempPtr = pADCh3DataBuf;				
				break;

			case 4:
				nDataOffset = nADCh4BufOffset;				
				dataTempPtr = pADCh4DataBuf;								
				break;
				
		}
		
		if ( rx[0] == 0x00 && rx[1] == 0x00 && rx[2] == 0x38 & rx[3] == 0x29)
		{
			if(dataTempPtr == NULL)
			{
				dataAllocPtr = (unsigned char*)malloc(10240*2);	
				memset(dataAllocPtr, 0, 10240*2);
				dataTempPtr = dataAllocPtr;
			}
			
			memcpy(dataTempPtr+(nDataOffset*2048), &rx[4], len-DATA_HEADER_SIZE);

			nDataOffset++;

			if(nDataOffset%5 == 0)
			{
				switch(channel_num)
				{
					case 1:
						bFlushSerialCh1 = 1;
						break;
					
					case 2:
						bFlushSerialCh2 = 1;
						break;

					case 3:
						bFlushSerialCh3 = 1;
						break;

					case 4:
						bFlushSerialCh4 = 1;
						break;
				}
			}

			
			if(nDataOffset == 10)
			{
				CTime time;
				Element* element = (Element*)malloc(sizeof(Element));
				
				gettimeofday(&timeValue, NULL);
				ptmValue = localtime(&timeValue.tv_sec);
				time.year = ptmValue->tm_year+1900;
				time.month = ptmValue->tm_mon+1;
				time.day = ptmValue->tm_mday;
				time.hour = ptmValue->tm_hour;
				time.min = ptmValue->tm_min;
				time.sec = ptmValue->tm_sec;
				time.usec = timeValue.tv_usec;
				element->time = time;

				element->data = dataTempPtr;
				element->size = 102400;
				element->fileName = (unsigned char*)malloc(128);
				sprintf(element->fileName, "%04d_%02d_%02d-%02d_%02d_%02d_%06ld_CH%d.dat"
      , ptmValue->tm_year + 1900, ptmValue->tm_mon + 1, ptmValue->tm_mday
      , ptmValue->tm_hour, ptmValue->tm_min, ptmValue->tm_sec, timeValue.tv_usec, channel_num);
				dataTempPtr = NULL;
				nDataOffset =0;
				enqueue(queue, element);
			}
		}
		else 
		{
 
#ifdef DEBUG
		 printf(" %d spi data error\n", fd); 
		 printf("%x:%x:%x:%x\n", rx[0], rx[1], rx[2], rx[3]); 
#endif 
		 return -1; 
	 }

	switch(channel_num)
	{
		case 1:
			nADCh1BufOffset = nDataOffset;		
			pADCh1DataBuf = dataTempPtr;
			break;

		case 2:
			nADCh2BufOffset = nDataOffset;				
			pADCh2DataBuf = dataTempPtr; 			
			break;

		case 3:
			nADCh3BufOffset = nDataOffset;				
			pADCh3DataBuf = dataTempPtr; 			
			break;

		case 4:
			nADCh4BufOffset = nDataOffset;				
			pADCh4DataBuf = dataTempPtr; 							
			break;
			
	}


#else  		
   hex_dump(rx, len, len, "RX", flag);
#endif 
	 return retVal; 
 }

 
  static int transferBuffer_withSaving(int fd, FILE* fp,  LinkedQueue* queue, uint8_t const *tx, uint8_t const *rx, size_t len, uint8_t flag, int totalCnt, int index)
  {
	  int ret, i;
	  
	  struct spi_ioc_transfer tr_data = {
		  .tx_buf = (unsigned long)tx,
		  .rx_buf = (unsigned long)rx,
		  .len = len, 
		  .delay_usecs = delay,
		  .speed_hz = speed,
		  .bits_per_word = bits,
	  };
#if 0	  
	  if (mode & SPI_TX_QUAD)
		  tr_data.tx_nbits = 4;
	  else if (mode & SPI_TX_DUAL)
		  tr_data.tx_nbits = 2;
	  if (mode & SPI_RX_QUAD)
		  tr_data.rx_nbits = 4;
	  else if (mode & SPI_RX_DUAL)
		  tr_data.rx_nbits = 2;
	  if (!(mode & SPI_LOOP)) {
		  if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
			  tr_data.rx_buf = 0;
		  else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
			  tr_data.tx_buf = 0;
	  }
  #endif
	  ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr_data);
	  
	  if (ret < 1)
		  pabort("can't send spi message");
  
  //  if (verbose)
  //		hex_dump(tx, len, len, "TX", flag);
  
#if 1 
  
	  if ( rx[0] == 0x00 && rx[1] == 0x00 && rx[2] == 0x38 & rx[3] == 0x29)
	  {
		if(index == 0)
		{
			//pADDataBuf = (unsigned char*)malloc(totalCnt*2048);
			memset(pADDataBuf, 0, totalCnt*2048);
		}
		
		 //hex_dump(fp,fp2, &rx[4], (len-DATA_HEADER_SIZE), (len-DATA_HEADER_SIZE), "RX", flag);
		 memcpy(pADDataBuf+(index*2048), &rx[4], len-DATA_HEADER_SIZE);
		if((totalCnt -1) == index)
		{
			Element* element = (Element*)malloc(sizeof(Element));
			element->data = pADDataBuf;
			element->size = totalCnt*2048;
			enqueue(queue, element);
		}
		
	 }
	  else 
	  {
		  DBG(" spi data error\n"); 
  
#ifdef DEBUG
		  printf(" %d spi data error\n", fd); 
		  printf("%x:%x:%x:%x\n", rx[0], rx[1], rx[2], rx[3]); 
#endif 
		  return -1; 
	  }
#else  		
	hex_dump(rx, len, len, "RX", flag);
#endif 
	  return 0; 
  }
static int transferNet(int fd, int fp,  FILE* fp2,uint8_t const *tx, uint8_t const *rx, size_t len, uint8_t flag)
{
	int ret;
	
	struct spi_ioc_transfer tr_data = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = len, 
		.delay_usecs = delay,
		.speed_hz = speed,
		.bits_per_word = bits,
	};
#if 0	
	if (mode & SPI_TX_QUAD)
		tr_data.tx_nbits = 4;
	else if (mode & SPI_TX_DUAL)
		tr_data.tx_nbits = 2;
	if (mode & SPI_RX_QUAD)
		tr_data.rx_nbits = 4;
	else if (mode & SPI_RX_DUAL)
		tr_data.rx_nbits = 2;
	if (!(mode & SPI_LOOP)) {
		if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
			tr_data.rx_buf = 0;
		else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
			tr_data.tx_buf = 0;
	}
#endif
	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr_data);
	
	if (ret < 1)
		pabort("can't send spi message");

//  if (verbose)
//		  hex_dump(tx, len, len, "TX", flag);

#if 1 
	//DBG("data\n"); 

	if ( rx[0] == 0x00 && rx[1] == 0x00 && rx[2] == 0x38 & rx[3] == 0x29)
  	{
  		hex_dump(fp, fp2, &rx[4], (len-DATA_HEADER_SIZE), (len-DATA_HEADER_SIZE), "RX", flag);
		
  	}
  	else 
  	{
		DBG("spi data erro\n"); 

#ifdef DEBUG
  		printf(" %d spi data error\n", fd); 
  		printf("%x:%x:%x:%x\n", rx[0], rx[1], rx[2], rx[3]); 
#endif 
  		return -1; 
  	}
#else  		
  hex_dump(rx, len, len, "RX", flag);
#endif 
	return 0; 
}

static void transfer8(int fd, unsigned char *tx, unsigned char *rx, size_t len)
{
        int ret;

        struct spi_ioc_transfer tr_cmd = {
                .tx_buf = (unsigned char)tx,
                .rx_buf = (unsigned char)rx,
                .len = len,
                .delay_usecs = delay,
                .speed_hz = speed,
                .bits_per_word = bits,
        };

        ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr_cmd);

        if (ret < 1)
        {
                pabort("can't send spi messageaaaaaaaaaaaaaa");
                DBG("++++++++++++++++++++++++++++++++++++++++++++");
        }

#ifdef DEBUG
        if (verbose)
                hex_dump_p(tx, len, len, "TX");

        hex_dump_p(rx, len, len, "RX");
#endif


}

static void transfer_cmd(int fd, void *tx, void *rx, size_t len)
{
	int ret;
	
	struct spi_ioc_transfer tr_cmd = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = len,
		.delay_usecs = delay,
		.speed_hz = speed,
		.bits_per_word = bits,
	};
#if 0	
	if (mode & SPI_TX_QUAD)
		tr_cmd.tx_nbits = 4;
	else if (mode & SPI_TX_DUAL)
		tr_cmd.tx_nbits = 2;
	if (mode & SPI_RX_QUAD)
		tr_cmd.rx_nbits = 4;
	else if (mode & SPI_RX_DUAL)
		tr_cmd.rx_nbits = 2;
	if (!(mode & SPI_LOOP)) {
		if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
			tr_cmd.rx_buf = 0;
		else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
			tr_cmd.tx_buf = 0;
	}
#endif
	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr_cmd);
	
	if (ret < 1)
		{
		pabort("can't send spi messageaaaaaaaaaaaaaa");
		DBG("++++++++++++++++++++++++++++++++++++++++++++");
		}

#ifdef DEBUG
	if (verbose)
		hex_dump_p(tx, len, len, "TX");
		
	hex_dump_p(rx, len, len, "RX");
#endif 


}

static void print_usage(const char *prog)
{
	printf("Usage: %s [-DSIRfcspovnd]\n", prog);
	puts("  -D --device       device to use (default /dev/spidev1.1)\n"
	     "  -I                Initialize device's registers and set value to a register \n"
	     "  -S                Save sensor data to File.\n"       
	     "  -R                Read device's All registers \n"
	     "  -s --speed        max speed (Hz)\n"
	     "\n"
//	     "  -p              Send data (e.g. \"1234\\xde\\xad\")\n"
	     "  -c --count        Numbfer of Recoding Sample\n"
	     "  -f --filename     File for Recoding or Initializing device\n" 
	     "  -p --printOption  Option of Recoding file (0:binary, 1:hex, 2:float)\n"
	     "  -n --rnumber      module register number\n" 
	     "  -d --data         register data\n"
	     "  -t --timeout      timeout\n"
	     "  -v --verbose      Verbose (show tx buffer)\n"
	     "  -A                autosave AD1, AD2, AD3, AD4 \n"
		 "  -M --mode		  Set AD data mode"
	);
	exit(1);
}


static void parse_opts(int argc, char *argv[])
{
	int nhour = 0;
	while (1) {
		static const struct option lopts[] = {
			// value name, option, 
			{ "device",  1, 0, 'D' },
			{ "speed",   1, 0, 's' },
			//{ "count",   1, 0, 'c' }, 
			{ "cleanTable",   1, 0, 'c' }, 
			{ "filename", 1, 0, 'f'}, 
//#ifdef SELECTED_CHANNEL
			{ "cfgFilename", 1, 0, 'x'}, 			
//#endif			
			{ "printOption", 1, 0, 'p'},
			{ "rnumber", 1, 0, 'n'},
			{ "initCount", 1, 0, 'i'}, 
			{ "gMode", 1, 0, 'M'}, 
			{ "start", 1, 0, 'X'},
			{ "end", 1, 0, 'Y'},
			{ "Tacho", 1, 0, 'T'},
#ifdef FEATURE_PREQUENCY
			{ "gpio", 1, 0, 'g'},			
#endif
			{"equipment", 1, 0, 'q'},
			{ NULL, 0, 0, 0 },
		};
		
		int c;

	//	c = getopt_long(argc, argv, "D:SIARLf:X:Y:T:r:q:x:c:s:g:p:vn:d:t:M:m", lopts, NULL);
		c = getopt_long(argc, argv, "D:SIARLf:X:Y:T:r:q:x:s:g:p:vcn:d:i:M:m", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'A' : 
		  runf = PROCESS_AUTOSAVE; 
		  break; 		
		case 'S': 
		  runf = PROCESS_RECODING; 
		  break; 
		case 'I':
		  runf = PROCESS_SETREG; 
		  flag = 1; 
		  break; 
		case 'R' :
		  runf = PROCESS_READREG;
	    	  flag = 1; 
		  break; 
		case 'L' :
			dataFlag = 1;
			break;
		case 'q' :
			equipmentNumber = atoi(optarg);
			printf("Equipment Number:%d\n", equipmentNumber);
			break;
		case 'X':
		  lf1 = atof(optarg); 
			printf("f1:%f\n", lf1);						  
		  break; 
		case 'Y':
		  lf2 = atof(optarg); 
			printf("f2:%f\n", lf2);								  
		  break; 
		case 'T': 
		  ltacho= atof(optarg); 
			printf("tacho:%f\n", ltacho);										  
		  break; 
#ifdef FEATURE_PREQUENCY
		  case 'g':
			gpioPin = atoi(optarg);
			printf("selected GPIO :%d\n", gpioPin);
			break;
#endif
			
 		case 'f': 
		  filename = optarg; 
		  break; 
//#ifdef SELECTED_CHANNEL
 		case 'x': 
		  cfgFilename = optarg; 
		  printf("config FilePath:%s\n",cfgFilename);
		  break; 		  
//#endif		  
//#ifdef FEATURE_SAMPLING_RATE
		case 'r' : 
			sample_rate = atoi(optarg);
			if(sample_rate == 0)
				sample_rate = 1;
			sample_count = DATA_UNIT_SIZE/sample_rate;
			printf("sample rate : %d, sample count : %d\n", sample_rate, sample_count);
		  break; 

//#endif
		case 'c' : 
		  //sampleNumber = atoi(optarg); 
		  bCleanTable = TRUE;
		  break; 
		case 'p' : 
		  printOption = atoi(optarg); 
		  break; 
		case 'D':
		  device = optarg;
		  break; 
		case 's':
		  speed = atoi(optarg);
			break;
		case 'n' : 
		  rnumber = atoi(optarg); 
		  if(rnumber >= 0) 
		    flag = 0; 
		  else 
		    flag = 1; 
		  break; 
		case 'v':
			verbose = 1;
			break;		  
		case 'i':
			nhour = atoi(optarg); 
			nBaselineCount = nhour*COUNT_PER_AHOUR;
			printf("************* BaseLine : %d\n", nBaselineCount);
			break; 
		case 'd':
		  rdata = atoi(optarg); 
		  break; 
		case 'M' : 
			runf = PROCESS_MODESET; 
			flag = 1; 
			gADMode = atoi(optarg); 
			break; 
		case 'm':
			gADMode = atoi(optarg); 
			break; 
		default:
			print_usage(argv[0]);
			break;
		}
	}
}


int AnalogModule_Error()
{
//	execl("$SPATH/LED_error.sh", "$SPATH/LED_error.sh", "on", NULL); 
//	printf("Error AnalogModule!!\n"); 	
	AD_OnoffLed(1); 
}

int AnalogModule_Stop(int fd)
{
    // SPI Disable
        
    //AD_DisableModule(midx);     
    close(fd); 
}

int openADC(const char *devicename)
{
	int fd; 
	int ret; 

	fd = open(devicename, O_RDWR); 

        if (fd < 0)
                pabort("can't open device");

        /*
         * spi mode
         */

        ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
        if (ret == -1)
                pabort("can't set spi mode");

        ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
        if (ret == -1)
                pabort("can't get spi mode");

       /*
         * bits per word
         */

        ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
        if (ret == -1)
                pabort("can't set bits per word");

        ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
        if (ret == -1)
                pabort("can't get bits per word");

       /*
         * max speed hz
         */
        ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
        if (ret == -1)
                pabort("can't set max speed hz");

        ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
        if (ret == -1)
                pabort("can't get max speed hz");

        //if (verbose)
        {
                printf("SPI Info \n");
                printf("- spi device : %s\n", devicename);
                printf("- spi mode: %d\n", mode);
                printf("- bits per word: %d\n", bits);
                printf("- max speed: %d Hz (%d KHz)\n", speed, speed/1000);
        }

	uint8_t CMD_tx[2];  
	uint8_t CMD_rx[2]; 

	CMD_tx[0] = 0x07; 
	
	transfer_cmd(fd, CMD_tx, CMD_rx, 1);

	CMD_tx[0] = 0x

	return fd; 
}

	


int AnalogModule_Start(const char *devicename)
{
	int fd; 
	int ret; 
	
#ifdef DEBUG	
  printf("- Device : %s\n", devicename); 
#endif 

	if(!strncmp(devicename, device1, 14) ) {
		midx = MODULE_AD1;
	}
	else if (!strncmp(devicename, device2, 14)) {
		midx = MODULE_AD2;  
	}
	else if (!strncmp(devicename, device3, 14)) {
		midx = MODULE_AD3; 
	} else if (!strncmp(devicename, device4, 14)) {
		midx = MODULE_AD4; 
	}

	AD_EnableModule(midx);	
	
	fd = open(devicename, O_RDWR); 
	
	if (fd < 0) 
		pabort("can't open device"); 

	/*
	 * spi mode
	 */
	 
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		pabort("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		pabort("can't get spi mode");

	/*
	 * bits per word
	 */
	 
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		pabort("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		pabort("can't get bits per word");

	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		pabort("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		pabort("can't get max speed hz");

	if (verbose)
	{
	  	printf("SPI Info \n"); 
	  	printf("- spi device : %s\n", devicename); 
		printf("- spi mode: %d\n", mode);
		printf("- bits per word: %d\n", bits);
		printf("- max speed: %d Hz (%d KHz)\n", speed, speed/1000);
	}

	AD_DisableModule(midx);	
	return fd; 
}

void Start(int indexM) 
{
  // module power on 
}

int GetStatus(int indexM)
{
  int onoff = 0; 
  onoff = 1; 
  
  return onoff; 
}


void setADModeClient(int nMode)
{
	gADMode = nMode;
	printf("ADMode was set as %d\n", nMode);
}


void setChannelClient(int nChannel)
{
	gChannel = nChannel;
	printf("Channel was set as %d\n", nChannel);
}

int setADMode(int fd, int value)
{
	uint32_t CMD_tx[2]; 
	
	if ( value < MODE_TIME || value > MODE_RMS )
		value = MODE_TIME; 
		
	CMD_tx[0] = CMD_SET_MODE; 
	CMD_tx[1] = value; 

	transfer_cmd(fd, CMD_tx, send_rx, sizeof(CMD_STATUS_tx));
	printf("\n Send Mode Data : %d\n", value); 
	transfer_cmd(fd, CMD_STATUS_tx, send_rx, sizeof(CMD_STATUS_tx)); 
}

int getADMode(int fd)
{
	uint32_t CMD_tx[2]; 
	
	CMD_tx[0] = CMD_GET_MODE; 
	
	transfer_cmd(fd, CMD_tx, send_rx, sizeof(CMD_STATUS_tx));
	transfer_cmd(fd, CMD_STATUS_tx, send_rx, sizeof(CMD_STATUS_tx)); 

//	printf("getADMode : %d\n", (int)send_rx[1]); 	
	return send_rx[1]; 
}

void Stop(int indexM) 
{
  // module power off 
}


int32_t requestCnt(int fd)
{
	int32_t cnt; 
	//[[180605]]chchoi-mod:fixed data count per 1Min
	transfer_cmd(fd, CMD_REQ_CNT_tx, send_rx, sizeof(CMD_REQ_CNT_tx)); 
	transfer_cmd(fd, CMD_STATUS_tx, send_rx, sizeof(CMD_STATUS_tx)); 

	cnt = (int32_t)send_rx[1]; 
 
	return cnt;  
}

int32_t requestState_Read(int fd)
{
  int32_t status; 
  
  transfer_cmd(fd, CMD_STATUS_tx, send_rx, sizeof(CMD_STATUS_tx)); 
  transfer_cmd(fd, CMD_STATUS_tx, send_rx, sizeof(CMD_STATUS_tx));
    
  status = (int32_t) send_rx[1];
#ifdef DEBUG
  printf(" requestState_Read : %x(%d)\n", status, status);
#endif        

/*  if ( (int32_t) send_rx[0] == 0 )
 {
	return -1;  
 }
 else 
 {

  	status = (int32_t) send_rx[1];
  	#ifdef DEBUG
    	printf(" requestState_Read : %x(%d)\n", status, status);
    	#endif
 } */ 
                                                                                                                       
  return status; 
}

void request_ready(fd)
{
  transfer_cmd(fd, CMD_TEST_tx, send_rx, sizeof(CMD_TEST_tx)); 
}

void requestData_Start(int fd, int samplecount)
{
	//DBG("transfer_cmd start\n");
	//[[180605]]chchoi-mod:fixed data count per 1Min
	CMD_DATA_tx[0] = CMD_DATA | samplecount ; 
	transfer_cmd(fd, CMD_DATA_tx, send_rx, sizeof(CMD_DATA_tx)); 
}

int requestData_ReadNet(int fd, int fp, FILE* fp2, uint8_t option)
{
  int ret; 
   
  ret = transferNet(fd, fp, fp2, CMD_READDATA_tx, default_rx, sizeof(CMD_READDATA_tx), option);
  
  return ret; 
}

int requestData_ReadBuffer(int fd, LinkedQueue* queue, int channel_num,  uint8_t option, int totalCnt, int index)
{
  int ret; 
   
  ret = transferBuffer(fd, queue, CMD_READDATA_tx, default_rx, sizeof(CMD_READDATA_tx), option, totalCnt, index, channel_num);
  
  return ret; 
}


int requestData_ReadBuffer_withSaving(int fd, FILE* fp,  LinkedQueue* queue, uint8_t option, int totalCnt, int index)
{
  int ret; 
   
  ret = transferBuffer_withSaving(fd, fp, queue, CMD_READDATA_tx, default_rx, sizeof(CMD_READDATA_tx), option, totalCnt, index);
  
  return ret; 
}



int requestData_Read(int fd, FILE* fp, uint8_t option)
{
  int ret; 
   
  ret = transfer(fd, fp, CMD_READDATA_tx, default_rx, sizeof(CMD_READDATA_tx), option);
  
  return ret; 
}


void requestData_End(int fd)
{
//DBG("transfer_cmd end\n");

  transfer_cmd(fd, CMD_END_tx, send_rx, sizeof(CMD_END_tx)); 
}

void requestReg_Read(int fd, uint8_t number)
{
#ifdef DEBUG	
	printf(" Start to read R%d. \n", number); 
#endif 
	CMD_READREG_tx[0] = CMD_READREG_tx[0] | number; 
	transfer_cmd(fd, CMD_READREG_tx, send_rx, sizeof(send_rx));
	transfer_cmd(fd, CMD_READREG_tx, send_rx, sizeof(send_rx));
	REGDATA_inner[number] = send_rx[1];

#ifdef DEBUG	  
	printf(" Finish to read R%d. \n", number); 
#endif   
}

void requestReg_ReadAll(int fd)
{
  int i; 
  
#ifdef DEBUG	
  printf(" Start to read registers. \n"); 
#endif 

  transfer_cmd(fd, CMD_READREGA_tx, send_rx, sizeof(send_rx));
  
  reg.cmd = CMD_CHANGE_TRMODE; 
  transfer_cmd(fd, &reg, default_rx, sizeof(reg));
  
  for(i=0;i<NB_REGISTER;i++)
  {
    REGDATA_inner[i] = (*(default_rx+i*4+0));
    REGDATA_inner[i] = REGDATA_inner[i] | (*(default_rx+i*4+1)) << 8;
    REGDATA_inner[i] = REGDATA_inner[i] | (*(default_rx+i*4+2)) << 16;
    REGDATA_inner[i] = REGDATA_inner[i] | (*(default_rx+i*4+3)) << 24;
  } 
  
#ifdef DEBUG	
  printf(" Finish to read registers. \n"); 
#endif   
}

void requestReg_Set(int fd, uint8_t number, uint32_t data)
{
#ifdef DEBUG	
  printf(" Start to set Register. \n"); 
#endif 

  CMD_SETREG_tx[0] = CMD_SETREG_tx[0] | number; 
  CMD_SETREG_tx[1] = data; 
  
  transfer_cmd(fd, CMD_SETREG_tx, CMD_SETREG_rx, sizeof(CMD_SETREG_tx));
  transfer_cmd(fd, CMD_READREG_tx, send_rx, sizeof(send_rx));

#ifdef DEBUG	  
  printf(" Finish to set Register. \n"); 
#endif 
}


double fnc_std(double var){
    return sqrt(var);
}

void getMean_N_Std(double* mean, double* std, int num, int paramType, PARAM data[]){
	double sum = 0;
    double sumvar=0, var;
    int i;
	
	for(i=0;i<num;i++)
	{
		switch(paramType)
		{
			case ACC_OVERALL:
				sum += data[i].acc_overall;
				break;

			case ACC_A1:
				sum += data[i].acc_a1;
				break;

			case ACC_A2:
				sum += data[i].acc_a2;
				break;
				
			case ACC_A3:
				sum += data[i].acc_a3;
				break;
					
			case VEL_OVERALL:
				sum += data[i].vel_overall;				
				break;
				
			case VEL_V1:
				sum += data[i].vel_v1;				
				break;
					
			case VEL_V2:
				sum += data[i].vel_v2;				
				break;
				
		}

	}
	*mean = (double) sum/num;
	printf("Sum = %lf\n", sum);
	printf("Mean = %lf\n", *mean);
	
	
    for(i=0;i<num;i=i+1){
		switch(paramType)
		{
			case ACC_OVERALL:
		        sumvar+=(data[i].acc_overall - (*mean))*(data[i].acc_overall -(*mean));
				break;
				
			case ACC_A1:
				sumvar+=(data[i].acc_a1 - (*mean))*(data[i].acc_a1 -(*mean));
				break;
			
			case ACC_A2:
		        sumvar+=(data[i].acc_a2 - (*mean))*(data[i].acc_a2 -(*mean));
				break;

			case ACC_A3:
				sumvar+=(data[i].acc_a3 - (*mean))*(data[i].acc_a3 -(*mean));
				break;

		
			case VEL_OVERALL:
				sumvar+=(data[i].vel_overall - (*mean))*(data[i].vel_overall -(*mean));
				break;
				
			case VEL_V1:
				sumvar+=(data[i].vel_v1 - (*mean))*(data[i].vel_v1 -(*mean));
				break;
			
			case VEL_V2:
		        sumvar+=(data[i].vel_v2 - (*mean))*(data[i].vel_v2 -(*mean));
				break;

		}

    }
    var = sumvar/(num);

    *std = sqrt(var);
}

int compare(const void *a, const void *b)   
{
    double num1 = *(double *)a;    // void 포인터를 int 포인터로 변환한 뒤 역참조하여 값을 가져옴
    double num2 = *(double *)b;    // void 포인터를 int 포인터로 변환한 뒤 역참조하여 값을 가져옴

    if (num1 < num2)    // a가 b보다 작을 때는
        return 1;      // 1 반환
    
    if (num1 > num2)    // a가 b보다 클 때는
        return -1;       // -1 반환
    
    return 0;    // a와 b가 같을 때는 0 반환
}

double getMeanSortedCh1(int num, int _percent, int paramType, double (*_data)[COLUMN_MAX]){
	double sum = 0;
    double sumvar=0, var;
	double mean = 0.0;
    int i,j;
	int count = PERCENT(num, _percent);
	double data[DATA_COUNT_PREMAINT] = {0.0,};

	for(i=0 ; i<num; i++)
	{
		data[i] = _data[i][paramType];
	}
	
    // 정렬할 배열, 요소 개수, 요소 크기, 비교 함수를 넣어줌
    qsort(data, sizeof(data) / sizeof(double), sizeof(double), compare);


	for(i=0;i< count;i++)
	{
		sum += data[i];
	}
	mean = (double) sum/count;
	//printf("Count = %d  Mean = %lf\n",count, mean);
	
	return mean;	

}

double getMean(int num, int paramType, double (*_data)[COLUMN_MAX]){
	double sum = 0;
    double sumvar=0, var;
	double mean = 0.0;
    int i,j;

	for(i=0 ; i<num; i++)
	{
		sum += _data[i][paramType];
	}
	
	mean = (double) sum/num;
	printf("Mean = %lf, TYPE:%d\n",mean, paramType);
	
	return mean;	
}


double getMeanSortedCh2(int num, int _percent, int paramType, double (*_data)[COLUMN_MAX]){
	double sum = 0;
    double sumvar=0, var;
	double mean = 0.0;
    int i,j;
	int count = PERCENT(num, _percent);
	double data[INIT_DATA_COUNT_MAX] = {0.0,};

	for(i=0 ; i<num; i++)
	{
		data[i] = _data[i][paramType];
	}
	
    // 정렬할 배열, 요소 개수, 요소 크기, 비교 함수를 넣어줌
    qsort(data, sizeof(data) / sizeof(double), sizeof(double), compare);


	for(i=0;i< count;i++)
	{
		sum += data[i];
	}
	mean = (double) sum/count;
	//printf("Count = %d  Mean = %lf\n",count, mean);
	
	return mean;	
}
double getMeanSortedCh3(int num, int _percent, int paramType, double (*_data)[COLUMN_MAX]){
	double sum = 0;
    double sumvar=0, var;
	double mean = 0.0;
    int i,j;
	int count = PERCENT(num, _percent);
	double data[INIT_DATA_COUNT_MAX] = {0.0,};

	for(i=0 ; i<num; i++)
	{
		data[i] = _data[i][paramType];
	}
	
    // 정렬할 배열, 요소 개수, 요소 크기, 비교 함수를 넣어줌
    qsort(data, sizeof(data) / sizeof(double), sizeof(double), compare);


	for(i=0;i< count;i++)
	{
		sum += data[i];
	}
	mean = (double) sum/count;
	//printf("Count = %d  Mean = %lf\n",count, mean);
	
	return mean;	

}

double getMeanSortedMIC(int num, int _percent, int paramType, double (*_data)[COLUMN_MAX]){
	double sum = 0;
    double sumvar=0, var;
	double mean = 0.0;
    int i;
	int count = PERCENT(num, _percent);
	double data[INIT_DATA_COUNT_MAX] = {0.0,};

	for(i=0 ; i<num; i++)
	{
		data[i] = _data[i][paramType];
	}

    // 정렬할 배열, 요소 개수, 요소 크기, 비교 함수를 넣어줌
    qsort(data, sizeof(data) / sizeof(double), sizeof(double), compare);


	for(i=0;i< count;i++)
	{
		sum += data[i];
	}
	mean = (double) sum/count;
	//printf("Count = %d  Mean = %lf\n",count, mean);
	
	return mean;	

}


double getStdev(int num, double data[]){
	double sum = 0;
    double sumvar=0, var;
	double mean = 0.0;
	double stdev =0.0;
    int i;
	
	for(i=0;i<num;i++)
		sum += data[i];				
	
	mean = (double) sum/num;
	
    for(i=0;i<num;i=i+1)
	    sumvar+= ((data[i]-mean)*(data[i]-mean));

    var = sumvar/(num);

    stdev = sqrt(var);

	return stdev;
}



double getParamRawData(PARAM* param,int _sampleCount, short* convDataBuf)
{
	double acc_peak = 0;

}
#if 1
double getParamFFTData(double* _param,int _sampleCount, fftw_complex *_out, RequiredParam* _requiredParam, int _paramCount, int chNum)
{
	/* velocity overall */
	
	int i,j;
	int tacho = 0;
	int count = 0;
	double ewr = 0;
	float frequency = 0.0;
	double value = 0;
	double acc_value = 0;
	double vel_value = 0;

	double sum[COLUMN_MAX] = {0.0,};
	//double rms[COLUMN_MAX] = {0.0,};

printf("ParamCount:%d\n", _paramCount);
	double vel_partial = 0;
	double vel_partial_rms = 0;
	int vel_partial_count = 0;
	int sampleCnt = 0;
	double acc_temp = 0.0;
	double pow0 = 0.0;
	double pow1 = 0.0;	
	double out_pow_sqrt = 0.0;
	double out_temp_sqrt2 = 0.0;	
	double sqrt_51200 = sqrt(10240);
	double FREQUENCY = 0.0;


	printf("sqrt_51200:%lf\n", sqrt_51200);
	
	frequency = (float)51200/_sampleCount;
	FREQUENCY = 2*3.14*frequency;
	
	for(i =0;  i<= _sampleCount/2; i++)
	{
		pow0 = _out[i][0]*_out[i][0];
		pow1 = _out[i][1]*_out[i][1];
			
		out_pow_sqrt = sqrt(pow0+pow1)/sqrt_51200;
		
		acc_value = (double)(out_pow_sqrt)* (out_pow_sqrt);
		
		for(j=0;j<_paramCount;j++)
		{
			/******************************************** accellorate *******************************************/		
			if(_requiredParam[j].paramType >= INDEX_ACC_OVERALL && _requiredParam[j].paramType < INDEX_ACC_AP2P)
			{
				if(i*frequency >= _requiredParam[j].paramF1 && i*frequency <= _requiredParam[j].paramF2)
				{
					sum[j] += acc_value;			
				}
			}	
			/********************************************* velocity *********************************************/
			else
				if(i > 0)
				{
					vel_value = (double)acc_value/((FREQUENCY*i)*(FREQUENCY*i));
					sampleCnt++;
					if(_requiredParam[j].paramType >= INDEX_VEL_OVERALL && _requiredParam[j].paramType < INDEX_VEL_VP2P)
						if(i*frequency >= _requiredParam[j].paramF1 && i*frequency <= _requiredParam[j].paramF2)
						{
							sum[j] += vel_value;
						}
				}
		}
	}

	for(j=0;j<_paramCount;j++)
	{
		//[[180604]chchoi-mod:changed denominator range to 51200, changed the unit to g
		//_param[j] = sqrt(sum[j]/(_requiredParam[j].paramF2-_requiredParam[j].paramF1+1))*1000;	
		if(_requiredParam[j].paramType >= INDEX_ACC_OVERALL && _requiredParam[j].paramType < INDEX_ACC_AP2P)
			_param[j] = sqrt(sum[j]/(10240/2))/9.8;	
		else
			_param[j] = sqrt(sum[j]/(10240/2))*1000;	
		//printf("CH%d param[%d]:%lf\n",chNum, j, _param[j]);
	}
}
#else
double getParamFFTData(double* _param,int _sampleCount, fftw_complex *_out, RequiredParam* _requiredParam, int _paramCount)
{
	/* velocity overall */
	
	int i,j;
	int tacho = 0;
	int count = 0;
	double ewr = 0;
	float frequency = 0.0;
	double value = 0;
	double acc_value = 0;
	double vel_value = 0;

	double sum[COLUMN_MAX] = {0.0,};
	//double rms[COLUMN_MAX] = {0.0,};

printf("ParamCount:%d\n", _paramCount);
	double vel_partial = 0;
	double vel_partial_rms = 0;
	int vel_partial_count = 0;
	int sampleCnt = 0;
	frequency = (float)51200/_sampleCount;
	
	for(i = _sampleCount/2; i >= 0; i--)
	{
		acc_value = (double)(sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))/sqrt(51200)* sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))/sqrt(51200));

		for(j=0;j<_paramCount;j++)
		{
			/******************************************** accellorate *******************************************/		
			if(_requiredParam[j].paramType >= INDEX_ACC_OVERALL && _requiredParam[j].paramType < INDEX_ACC_AP2P)
			{
				if(i*frequency >= _requiredParam[j].paramF1 && i*frequency <= _requiredParam[j].paramF2)
				{
					sum[j] += acc_value;			
				}
			}	
			/********************************************* velocity *********************************************/
			else
				if(i > 0)
				{
					vel_value = (double)sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))/(2*3.14*frequency*i)/sqrt(51200)* sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))/(2*3.14*frequency*i)/sqrt(51200);
					sampleCnt++;
					if(_requiredParam[j].paramType >= INDEX_VEL_OVERALL && _requiredParam[j].paramType < INDEX_VEL_VP2P)
						if(i*frequency >= _requiredParam[j].paramF1 && i*frequency <= _requiredParam[j].paramF2)
						{
							sum[j] += vel_value;
						}
				}
		}
	}

	for(j=0;j<_paramCount;j++)
	{
		_param[j] = sqrt(sum[j]/(_requiredParam[j].paramF2-_requiredParam[j].paramF1+1))*1000;	
		printf("param[%d]:%lf\n", j, _param[j]);
	}
}
#endif


double getParamMICFFTData(double* _param,int _sampleCount, fftw_complex *_out, RequiredParam* _requiredParam, int _paramCount)
	{
		/* velocity overall */
		
		int i,j;
		int tacho = 0;
		int count = 0;
		double ewr = 0;
		float frequency = 0.0;
		double value = 0;
		double mic_value = 0;
	
		double sum[10] = {0.0,};
		//double rms[COLUMN_MAX] = {0.0,};
	
	printf("ParamCount:%d\n", _paramCount);
		double vel_partial = 0;
		double vel_partial_rms = 0;
		int vel_partial_count = 0;
		int sampleCnt = 0;
		double acc_temp = 0.0;
		double pow0 = 0.0;
		double pow1 = 0.0;	
		double out_pow_sqrt = 0.0;
		double out_temp_sqrt2 = 0.0;	
		double sqrt_51200 = sqrt(10240);
		double FREQUENCY = 0.0;
	
	
		printf("sqrt_51200:%lf\n", sqrt_51200);
		
		frequency = (float)51200/_sampleCount;
		FREQUENCY = 2*3.14*frequency;
		
		for(i =0;  i<= _sampleCount/2; i++)
		{
			pow0 = _out[i][0]*_out[i][0];
			pow1 = _out[i][1]*_out[i][1];
				
			out_pow_sqrt = sqrt(pow0+pow1)/sqrt_51200;
			
			value = (double)(out_pow_sqrt)* (out_pow_sqrt);
			
			for(j=0;j<_paramCount;j++)
			{
				mic_value = (double)value/((FREQUENCY*i)*(FREQUENCY*i));
				sampleCnt++;
				if(i*frequency >= _requiredParam[j].paramF1 && i*frequency <= _requiredParam[j].paramF2)
				{
					sum[j] += mic_value;
				}
			}
		}
	
		for(j=0;j<_paramCount;j++)
		{
			//[[180604]chchoi-mod:changed denominator range to 51200, changed the unit to g
			//_param[j] = sqrt(sum[j]/(_requiredParam[j].paramF2-_requiredParam[j].paramF1+1))*1000;	
				_param[j] = sqrt(sum[j]/(AD_SAMPLING_RATE/2))/9.8*1000;

			//printf("CH%d param[%d]:%lf\n",chNum, j, _param[j]);
		}
	}




/**********************************************************************************************
*						velocity_overall
*						
*						Sum all
***********************************************************************************************/
double getParam_velocity_overall(int _sampleCount ,fftw_complex *_out)
{
	int i;
	double ewr=0;
	float frequency = 0.0;
	double value = 0;
	double overall = 0;
	
	frequency = (float)51200/_sampleCount;

	//for(i = 0; i< _sampleCount; i++)
	for(i = _sampleCount/2; i != 0; i--)
	{
		value = (double)sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/(2*3.14*frequency*i)/sqrt(51200)* sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/(2*3.14*frequency*i)/sqrt(51200);


		ewr += value;
		
	}
	
	overall = sqrt(ewr/(_sampleCount/2));
	
	return overall;
}




/**********************************************************************************************
*																											
*									Acceleration Overall
*						
***********************************************************************************************/
double getParam_acceleration_overall(int _sampleCount ,fftw_complex *_out)
{
	int i;
	double ewr=0;
	float frequency = 0.0;
	double value = 0;
	double overall = 0;
	
	//for(i = 1; i<= _sampleCount; i++)
	for(i = _sampleCount/2; i != 0; i--)
	{
		value = (double)(sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/sqrt(51200)* sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/sqrt(51200));
		
		ewr += value;
	}

	overall = sqrt(ewr/(_sampleCount/2));
	
	return overall;
}



/**********************************************************************************************
*																											
*									Acceleration True Peak
*						
***********************************************************************************************/
double getParam_acceleration_peak(int _sampleCount ,fftw_complex *_out)
{
	int i;

	double value = 0;
	double peak = 0;
	//for(i = 1; i<= _sampleCount; i++)
	for(i = _sampleCount; i != 0; i--)
	{
		value = (double)sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/sqrt(51200);
		if(peak < value)
			peak = value;
	}

	
	return peak;
}



double getParam_crest_factor(int _sampleCount ,fftw_complex *_out){
    double sum=0, cf;
	double rms = 0;
	double value = 0;
	double value4peak = 0;
	double peak =0;
	
    int i;
	for(i = _sampleCount; i != 0; i--)
	{
        value = (double)sqrt((pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/sqrt(51200))* (sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/sqrt(51200));
		value4peak = (double)sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/sqrt(51200);
		if(peak < value4peak)
			peak = value4peak;
		sum += value;
    }

    rms = sqrt(sum/(_sampleCount));

	cf = peak/rms;
		
    return cf;
}


/**********************************************************************************************
*						
*											1x Peak
*						
***********************************************************************************************/

double getParam_1x(int _tacho, int _sampleCount, fftw_complex *_out)
{
	int i;
	int count=0;
	double value;
	double total;
	float frequency = 0.0;

	frequency = (float)(51200/_sampleCount);

	i = (int)_tacho;
	printf("tacho 1x: %d\n", i);
#ifdef FEATURE_PARAM_DEBUG	
	printf("1X tacho : %d\n", _tacho);
#endif
	if(i > 2)
	{
		total = (double)sqrt(pow(_out[i-2][0],2) + pow(_out[i-2][1],2))/sqrt(51200);
		count++;
		total += (double)sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/sqrt(51200);		
		count++;		
	}
	
	total += (double)sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))/sqrt(51200);
	count++;
	total +=  (double)sqrt(pow(_out[i+1][0],2) + pow(_out[i+1][1],2))/sqrt(51200);
	count++;	
	total +=  (double)sqrt(pow(_out[i+2][0],2) + pow(_out[i+2][1],2))/sqrt(51200);	
	count++;
	value = total/count;		
	
#ifdef FEATURE_PARAM_DEBUG	
	printf("1x result: %lf\n", sqrt(pow(_out[i][0],2) + pow(_out[i][1],2)));
#endif

	return value;
}


/**********************************************************************************************
*						
*											2x Peak
*						
***********************************************************************************************/

double getParam_2x(int _tacho, int _sampleCount, fftw_complex *_out)
{
	int i;
	int count=0;
	double value;
	double total;
	float frequency = 0.0;
	
	frequency = (float)(51200/_sampleCount);

	i = (int)_tacho*2;
	
	printf("tacho 2x: %d\n", i);
#ifdef FEATURE_PARAM_DEBUG	
	printf("2X tacho : %d\n", _tacho);
#endif

	if(i > 2)
	{
		total = (double)sqrt(pow(_out[i-2][0],2) + pow(_out[i-2][1],2))/sqrt(51200);
		count++;
		total += (double)sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/sqrt(51200);		
		count++;		
	}
	
	total += (double)sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))/sqrt(51200);
	count++;
	total +=  (double)sqrt(pow(_out[i+1][0],2) + pow(_out[i+1][1],2))/sqrt(51200);
	count++;	
	total +=  (double)sqrt(pow(_out[i+2][0],2) + pow(_out[i+2][1],2))/sqrt(51200);	
	count++;
	value = total/count;		
#ifdef FEATURE_PARAM_DEBUG	
	printf("2x result : %lf\n", (sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))));
#endif

	return value;
}


/**********************************************************************************************
*						
*											120Hz Peak
*						
***********************************************************************************************/

double getParam_120Hz(int _sampleCount, fftw_complex *_out)
{
	int i;
	double value;
	float frequency = 0.0;
	
	frequency = (float)51200/_sampleCount;

	i = 120;

	value = (double)sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))/sqrt(51200);
#ifdef FEATURE_PARAM_DEBUG	
	printf("120Hz : %lf\n", sqrt(pow(_out[i][0],2) + pow(_out[i][1],2)));
#endif

	return value;
}


/**********************************************************************************************
*						
*											3x~5x Peak
*						
***********************************************************************************************/
double getParam_From3xTo5x(int _tacho, int _sampleCount, fftw_complex *_out)
{
	int i;
	double ewr=0;
	float frequency = 0.0;
	double value = 0;
	frequency = (float)51200/_sampleCount;
#ifdef FEATURE_PARAM_DEBUG	
	printf("3xTo5x tacho : %d\n", _tacho);
#endif
	for(i = _sampleCount/2; i > 0; i--)
	{
		if(i*frequency>= _tacho*3 && i*frequency <= _tacho*5)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr += (double)sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/sqrt(51200);
			
		}
	}

	return ewr;
}


/**********************************************************************************************
*						
*											1xVPF
*						
***********************************************************************************************/

double getParam_1xVPF(int _tacho, int _vain, int _sampleCount, fftw_complex *_out)
{

	int i;
	int count=0;
	double value;
	double total;
	float frequency = 0.0;

	frequency = (float)51200/_sampleCount;
#ifdef FEATURE_PARAM_DEBUG	
	printf("tacho : %d, _vain : %d\n", _tacho, _vain);
#endif
	i = (int)_tacho*_vain;

	if(i > 2)
	{
		total = (double)sqrt(pow(_out[i-2][0],2) + pow(_out[i-2][1],2))/sqrt(51200);
		count++;
		total += (double)sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/sqrt(51200);		
		count++;		
	}
	
	total += (double)sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))/sqrt(51200);
	count++;
	total +=  (double)sqrt(pow(_out[i+1][0],2) + pow(_out[i+1][1],2))/sqrt(51200);
	count++;	
	total +=  (double)sqrt(pow(_out[i+2][0],2) + pow(_out[i+2][1],2))/sqrt(51200);	
	count++;
	value = total/count;		

#ifdef FEATURE_PARAM_DEBUG	
	printf("1xVPF : %lf\n", (double)(sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))));
#endif

	return value;

}

/**********************************************************************************************
*						
*											2xVPF
*						
***********************************************************************************************/

double getParam_2xVPF(int _tacho, int _vain, int _sampleCount, fftw_complex *_out)
{

	int i;
	int count=0;
	double value;
	double total;
	float frequency = 0.0;
	
	frequency = (float)51200/_sampleCount;
#ifdef FEATURE_PARAM_DEBUG	
	printf("tacho : %d, _vain : %d\n", _tacho, _vain);
#endif
	i = (int)_tacho*_vain*2;

	if(i > 2)
	{
		total = (double)sqrt(pow(_out[i-2][0],2) + pow(_out[i-2][1],2))/sqrt(51200);
		count++;
		total += (double)sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/sqrt(51200);		
		count++;		
	}
	
	total += (double)sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))/sqrt(51200);
	count++;
	total +=  (double)sqrt(pow(_out[i+1][0],2) + pow(_out[i+1][1],2))/sqrt(51200);
	count++;	
	total +=  (double)sqrt(pow(_out[i+2][0],2) + pow(_out[i+2][1],2))/sqrt(51200);	
	count++;
	value = total/count;		
#ifdef FEATURE_PARAM_DEBUG	
	printf("2xVPF : %lf\n", sqrt(pow(_out[i][0],2) + pow(_out[i][1],2)));
#endif

	return value;

}


double   getParam_partial_overall(int _f1, int _f2, int _sampleCount, fftw_complex *out)
{
	int i;
	double ewr=0;
	float frequency = 0.0;
	
	frequency = (float)51200/_sampleCount;
	
	for(i = _f1; i < _f2; i++)
	{
		//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
		ewr +=(double)(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/sqrt(51200)/(2*3.14*frequency*i))* (sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/sqrt(51200)/(2*3.14*frequency*i));
	}

	return ewr;
}

void SQLITE_CleanTables()
{
	char pQuery[QUERY_BUF_LENGTH];
	int rc;
	sqlite3_stmt *res;
	char *err_msg = 0;

	memset(pQuery, 0, QUERY_BUF_LENGTH);


		sprintf(pQuery, "DROP TABLE IF EXISTS %s;"
						"DROP TABLE IF EXISTS %s;"
						"DROP TABLE IF EXISTS %s;"
						"DROP TABLE IF EXISTS %s;"
						"DROP TABLE IF EXISTS %s;"
						"DROP TABLE IF EXISTS %s;"
						"DROP TABLE IF EXISTS %s;"
						"DROP TABLE IF EXISTS %s;"
						, TABLE_NAME_CHANNEL1_BASELINE, TABLE_NAME_CHANNEL2_BASELINE, TABLE_NAME_CHANNEL3_BASELINE, TABLE_NAME_CHANNEL4_BASELINE \
						, TABLE_NAME_CHANNEL1_INIT_DATA, TABLE_NAME_CHANNEL2_INIT_DATA, TABLE_NAME_CHANNEL3_INIT_DATA, TABLE_NAME_CHANNEL4_INIT_DATA \
						//[[180823]]chchoi-add:insert realtimeData DB
						, TABLE_NAME_CHANNEL1_REALTIME_DATA, TABLE_NAME_CHANNEL2_REALTIME_DATA, TABLE_NAME_CHANNEL3_REALTIME_DATA, TABLE_NAME_CHANNEL4_REALTIME_DATA \
						, TABLE_NAME_CHANNEL1_PRE_MAINT_DATA, TABLE_NAME_CHANNEL2_PRE_MAINT_DATA, TABLE_NAME_CHANNEL3_PRE_MAINT_DATA, TABLE_NAME_CHANNEL4_PRE_MAINT_DATA \						
						);
	
		rc = sqlite3_exec(db, pQuery, 0, 0, &err_msg);
		
		printf("SQLITE REMOVE DATA in all tables\n");
}



int SQLITE_CheckBaselineDataTables(const char* tableName, double* _meanParam, double* _devParam, int _count, RequiredParam* _requiredParam)
{
	char *err_msg = 0;
	char *szErrMsg = NULL;
	char pQuery[QUERY_BUF_LENGTH];
	int count = -1;
	int rc;
	int hasBaselineData = 0;
	int i = 0;
	sqlite3_stmt *res;
	
	memset(pQuery, 0, QUERY_BUF_LENGTH);

	strcpy(pQuery, "SELECT ");
	
	for(;i<_count;i++)
	{
		if(_requiredParam[i].hasParam == TRUE)
		{
			if(i > 0)
				strcat(pQuery, ",");		
			strcat(pQuery,gColumnNameArray[_requiredParam[i].paramType]);
			strcat(pQuery, " ");
		}
	}
	strcat(pQuery, "from ");	
	strcat(pQuery, tableName);	
	strcat(pQuery, ";");	

	printf("Query : %s\n", pQuery);


	rc = sqlite3_prepare_v2(db, pQuery, -1, &res, 0);
	printf("prepare\n");
	if(rc != SQLITE_OK)
	{
		//if No CHANNEL1_BASELINE_TB table, create CHANNEL1_BASELINE_TB table.
		printf("szErrMsg : %s\n", szErrMsg);
//		if(strstr(szErrMsg, "no such table") != NULL)
		{
			printf("compare no such table\n");
			memset(pQuery, 0, QUERY_BUF_LENGTH);
			sprintf(pQuery, "CREATE TABLE %s ( ", tableName);
			for(i=0;i<_count;i++)
			{
				if(_requiredParam[i].hasParam == TRUE)
				{
					if(i > 0)
						strcat(pQuery, ", ");		
					strcat(pQuery,gColumnNameArray[_requiredParam[i].paramType]);
					strcat(pQuery, " DOUBLE");
				}
			}
			
			strcat(pQuery, ");");	

			printf("Query: %s\n", pQuery);		
			
			rc = sqlite3_exec(db, pQuery, NULL, 0, &szErrMsg);
			printf("Can't create the %s table\n", tableName);
		}
	}
	else
	{
		if(SQLITE_ROW == sqlite3_step(res))
		{
			for(i = 0; i< _count; i++)
				_meanParam[i] = sqlite3_column_double(res, i);
			
			if(SQLITE_ROW == sqlite3_step(res))
			{
				for(i = 0; i< _count; i++)			
					_devParam[i] = sqlite3_column_double(res, i);
 				
				printf("%s Has Baseline...................\n", tableName);
				//printf("%lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf\n",gParamBaselineCh1.acc_overall, 
				hasBaselineData = TRUE;
			}
		}
		sqlite3_finalize(res);
	}

	return hasBaselineData;
}

int SQLITE_CheckBaselineDataTablesMIC(const char* tableName, double* _meanParam, double* _devParam, int _count, RequiredParam* _requiredParam)
{
	char *err_msg = 0;
	char *szErrMsg = NULL;
	char pQuery[QUERY_BUF_LENGTH];
	int count = -1;
	int rc;
	int hasBaselineData = 0;
	int i = 0;
	sqlite3_stmt *res;
	
	memset(pQuery, 0, QUERY_BUF_LENGTH);

	strcpy(pQuery, "SELECT ");
	
	for(;i<_count;i++)
	{
		if(_requiredParam[i].hasParam == TRUE)
		{
			if(i > 0)
				strcat(pQuery, ",");		
			strcat(pQuery,gColumnNameArrayMIC[_requiredParam[i].paramType]);
			strcat(pQuery, " ");
		}
	}
	strcat(pQuery, "from ");	
	strcat(pQuery, tableName);	
	strcat(pQuery, ";");	

	printf("Query : %s\n", pQuery);


	rc = sqlite3_prepare_v2(db, pQuery, -1, &res, 0);
	printf("prepare\n");
	if(rc != SQLITE_OK)
	{
		//if No CHANNEL1_BASELINE_TB table, create CHANNEL1_BASELINE_TB table.
		printf("szErrMsg : %s\n", szErrMsg);
//		if(strstr(szErrMsg, "no such table") != NULL)
		{
			printf("compare no such table\n");
			memset(pQuery, 0, QUERY_BUF_LENGTH);
			sprintf(pQuery, "CREATE TABLE %s ( ", tableName);
			for(i=0;i<_count;i++)
			{
				if(_requiredParam[i].hasParam == TRUE)
				{
					if(i > 0)
						strcat(pQuery, ", ");		
					strcat(pQuery,gColumnNameArrayMIC[_requiredParam[i].paramType]);
					strcat(pQuery, " DOUBLE");
				}
			}
			
			strcat(pQuery, ");");	

			printf("Query: %s\n", pQuery);		
			
			rc = sqlite3_exec(db, pQuery, NULL, 0, &szErrMsg);
			printf("Can't create the %s table\n", tableName);
		}
	}
	else
	{
		if(SQLITE_ROW == sqlite3_step(res))
		{
			for(i = 0; i< _count; i++)
				_meanParam[i] = sqlite3_column_double(res, i);
			
			if(SQLITE_ROW == sqlite3_step(res))
			{
				for(i = 0; i< _count; i++)			
					_devParam[i] = sqlite3_column_double(res, i);
				
				printf("%s Has Baseline...................\n", tableName);
				//printf("%lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf\n",gParamBaselineCh1.acc_overall, 
				hasBaselineData = TRUE;
			}
		}
		sqlite3_finalize(res);
	}

	return hasBaselineData;
}



int SQLITE_CheckInitDataTables(const char* channelName, int _count, RequiredParam* _requiredParam)
{
	char *err_msg = 0;
	char *szErrMsg = NULL;
	char pQuery[QUERY_BUF_LENGTH];
	int count = -1;
	int rc;
	int i;
	sqlite3_stmt *res;
	//Initialize the query buffer
	memset(pQuery, 0, QUERY_BUF_LENGTH);
	sprintf(pQuery, "SELECT COUNT(*) from %s", channelName);
	
	printf("Query: %s\n", pQuery);
	
	rc = sqlite3_prepare_v2(db, pQuery, -1, &res, 0);
	if(rc != SQLITE_OK)
	{
		
	//	if(strstr(szErrMsg, "no such table") == NULL)
		{
		
			//Initialize the query buffer
			memset(pQuery, 0, QUERY_BUF_LENGTH);
			sprintf(pQuery, "CREATE TABLE %s ( ", channelName);
			for(i=0;i<_count;i++)
			{
				if(_requiredParam[i].hasParam == TRUE)
				{
					if(i > 0)
						strcat(pQuery, ", ");		
					strcat(pQuery,gColumnNameArray[_requiredParam[i].paramType]);
					strcat(pQuery, " DOUBLE");
				}
			}
			
			strcat(pQuery, ");");	

			printf("Query: %s\n", pQuery);		

//			sprintf(pQuery, "CREATE TABLE %s ( VEL_OVERALL DOUBLE, VEL_V1 DOUBLE, VEL_V2 DOUBLE, ACC_OVERALL DOUBLE, ACC_A1 DOUBLE, ACC_A2 DOUBLE, ACC_A3 DOUBLE );", channelName);
			
			rc = sqlite3_exec(db, pQuery, NULL, 0, &szErrMsg);
			count = 0;
		}
	}
	else
	{
		if(SQLITE_ROW == sqlite3_step(res))
		{
				count = sqlite3_column_int(res, 0);
			//	printf("nInitDataCountCh1 : %d\n", nInitDataCountCh1);
		}
		sqlite3_finalize(res);
	}

	return count;
}

int SQLITE_CheckInitDataTablesMIC(const char* channelName, int _count, RequiredParam* _requiredParam)
{
	char *err_msg = 0;
	char *szErrMsg = NULL;
	char pQuery[QUERY_BUF_LENGTH];
	int count = -1;
	int rc;
	int i;
	sqlite3_stmt *res;
	//Initialize the query buffer
	memset(pQuery, 0, QUERY_BUF_LENGTH);
	sprintf(pQuery, "SELECT COUNT(*) from %s", channelName);
	
	printf("Query: %s\n", pQuery);
	
	rc = sqlite3_prepare_v2(db, pQuery, -1, &res, 0);
	if(rc != SQLITE_OK)
	{
		
	//	if(strstr(szErrMsg, "no such table") == NULL)
		{
		
			//Initialize the query buffer
			memset(pQuery, 0, QUERY_BUF_LENGTH);
			sprintf(pQuery, "CREATE TABLE %s ( ", channelName);
			for(i=0;i<_count;i++)
			{
				if(_requiredParam[i].hasParam == TRUE)
				{
					if(i > 0)
						strcat(pQuery, ", ");		
					strcat(pQuery,gColumnNameArrayMIC[_requiredParam[i].paramType]);
					strcat(pQuery, " DOUBLE");
				}
			}
			
			strcat(pQuery, ");");	

			printf("Query: %s\n", pQuery);		

//			sprintf(pQuery, "CREATE TABLE %s ( VEL_OVERALL DOUBLE, VEL_V1 DOUBLE, VEL_V2 DOUBLE, ACC_OVERALL DOUBLE, ACC_A1 DOUBLE, ACC_A2 DOUBLE, ACC_A3 DOUBLE );", channelName);
			
			rc = sqlite3_exec(db, pQuery, NULL, 0, &szErrMsg);
			count = 0;
		}
	}
	else
	{
		if(SQLITE_ROW == sqlite3_step(res))
		{
				count = sqlite3_column_int(res, 0);
			//	printf("nInitDataCountCh1 : %d\n", nInitDataCountCh1);
		}
		sqlite3_finalize(res);
	}

	return count;
}

//[[180823]]chchoi-add:insert realtimeData DB
int SQLITE_CheckDataTables(const char* channelName, int index)
{
	
	char *err_msg = 0;
	char *szErrMsg = NULL;
	char TXT_P = 'P';
	char TXT_R = 'R';
	char TXT;
	
	char pQuery[QUERY_BUF_LENGTH];
	char columnName[3];
	int rc = -1;
	int i;
	sqlite3_stmt *res;
	//Initialize the query buffer
	memset(pQuery, 0, QUERY_BUF_LENGTH);
	memset(columnName, 0, 3);
	
	if(strstr(channelName, "PRE_MAINT_DATA_TB"))
		TXT = TXT_P;
	else if(strstr(channelName, "REALTIME_DATA_TB"))
		TXT = TXT_R;
	else
	{
		printf("There is not acceptable Table Name\n");
		return -1;
	}
	
	//Initialize the query buffer
	memset(pQuery, 0, QUERY_BUF_LENGTH);
	sprintf(pQuery, "CREATE TABLE IF NOT EXISTS %s ( ", channelName);
	
	strcat(pQuery, "DATE TEXT, ");		
	strcat(pQuery, "MSEC TEXT");					
	for(i=1;i<= COUNT_REALTIME_DATA_COLUMN;i++)
	{
		strcat(pQuery, " , ");
		memset(columnName, 0, 3);
		sprintf(columnName, "%c%d",TXT, i);
		
		strcat(pQuery,	columnName);
		strcat(pQuery, " DOUBLE ");
	}
	
	strcat(pQuery, ");");	

	rc = sqlite3_exec(db, pQuery, NULL, 0, &szErrMsg);
	printf("Result: %d	Query: %s\n",rc, pQuery);		

	memset(pQuery, 0, QUERY_BUF_LENGTH);
	sprintf(pQuery, "CREATE INDEX \"index_DATE%d\" ON \"%s\"(\"DATE\");", index, channelName);
	rc = sqlite3_exec(db, pQuery, NULL, 0, &szErrMsg);

	printf("Result: %d	Query: %s\nErrMsg:%s\n",rc, pQuery, szErrMsg);		

	return rc;
}



int SQLITE_SetInitParamFFT(double* _param, char* _tableName, int _count, char* _query)
{
	char *szErrMsg = NULL;
	int rc = SQLITE_OK;
	char pQuery[1024] = {0,};
	char pValue[20] = {0,};
	int i;

	if(1)//_query[0] == NULL)
	{
		memset(pQuery, 0, QUERY_BUF_LENGTH);
		sprintf(pQuery, "INSERT INTO %s VALUES( ", _tableName);
		for(i=0;i<_count;i++)
		{
			if(i > 0)
				strcat(pQuery, ", ");		
			memset(pValue, 0, sizeof(pValue));
			sprintf(pValue, "%lf",_param[i]);
			strcat(pQuery, pValue);
			strcat(pQuery, " ");
		}
		
		strcat(pQuery, ");");	
		strcpy(_query, pQuery);
		printf("Query gen: %s\n", _query);
	}
	else
		printf("Query Const: %s\n", _query);		

		rc = sqlite3_exec(db, _query, NULL, 0, &szErrMsg);				


	
	return rc;
}


int SQLITE_InsertDataTable(double* _param, char* _tableName, int _count, CTime _time)
{
	char *szErrMsg = NULL;
	int rc = SQLITE_OK;
	char pQuery[1024] = {0,};
	char pValue[20] = {0,};
	char pDate[30];
	char pMsec[6];
	sqlite3_stmt *res;
	int i;

	memset(pQuery, 0, QUERY_BUF_LENGTH);
	memset(pDate, 0, 30);
	memset(pMsec, 0, 6);
	sprintf(pQuery, "INSERT INTO %s VALUES( ", _tableName);
	sprintf(pDate, " \"%04d/%02d/%02d %02d:%02d:%02d\",",_time.year, _time.month, _time.day, _time.hour, _time.min, _time.sec);
	strcat(pQuery, pDate);
	sprintf(pMsec, "\"%06ld\",", _time.usec);
	strcat(pQuery, pMsec);


	for(i=0;i<15;i++)
	{
		if(i > 0)
			strcat(pQuery, ", ");		
		
		memset(pValue, 0, sizeof(pValue));
		if(i < _count)
		{
			sprintf(pValue, "%lf",_param[i]);
			strcat(pQuery, pValue);
		}
		else
			strcat(pQuery, "0");

		strcat(pQuery, " ");
	}
	
	strcat(pQuery, ");");	
	//strcpy(_query, pQuery);

	rc = sqlite3_prepare_v2(db, pQuery, -1, &res, 0);				
	
	if (rc != SQLITE_OK) {
			
			fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
			sqlite3_close(db);
			
			return 1;
	}	 
	sqlite3_step(res);

	sqlite3_finalize(res);
	printf("Query gen: %s %s\n", szErrMsg, pQuery);
	return rc;
}


#if 0
int SQLITE_SetInitParamMIC(double* _param, char* _tableName, int _count, char* _query)
{
	char *szErrMsg = NULL;
	int rc = SQLITE_OK;
	char pSqlQueryBuf[1024] = {0,};
	
	memset(pSqlQueryBuf, 0, sizeof(pSqlQueryBuf));
#if 0			
sprintf(pCH1SqlQueryBuf, "INSERT INTO %s VALUES('%s', %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf,%lf, %lf);",\
	TABLE_NAME_CHANNEL1_INIT_DATA, _fileName, gParamCh1.acc_overall, gParamCh1.acc_a1,gParamCh1.acc_a2, gParamCh1.acc_a3, gParamCh1.acc_a4, gParamCh1.acc_a5,\
	gParamCh1.acc_a6, gParamCh1.acc_a7, gParamCh1.vel_overall, gParamCh1.vel_v1, gParamCh1.vel_v2, gParamCh1.vel_v3,\
	gParamCh1.vel_v4, gParamCh1.vel_v5);
#endif			
	sprintf(pSqlQueryBuf, "INSERT INTO %s VALUES(%lf, %lf, %lf, %lf, %lf);",\
		 TABLE_NAME_CHANNEL4_INIT_DATA, _param.mic_overall, _param.mic_m1, _param.mic_m2, _param.mic_m3, _param.mic_m4 );

	rc = sqlite3_exec(db, pSqlQueryBuf, NULL, 0, &szErrMsg);
	
	return rc;
}
#endif

int SQLITE_GenBaselineParamFromInitParam(char* columnName, char* tableName, double* _mean, double* _stdev)
{
	char pQuery[1024] = {0,};
	sqlite3_stmt *res;
	double baselineValue = 0.0;
	int rc;
	int i = 0;
	double data[INIT_DATA_COUNT_MAX]={0.0,};
	
	memset(pQuery, 0, sizeof(pQuery));
	
	sprintf(pQuery, "select avg(%s) from (select %s from %s order by %s desc limit %d);",\
		columnName, columnName, tableName, columnName, PERCENT(nBaselineCount, CHECK_DATA_COUNT));
	
	rc = sqlite3_prepare_v2(db, pQuery, -1, &res, 0);
	
	if(rc == SQLITE_OK)
	{
		if(SQLITE_ROW == sqlite3_step(res))
		{
			baselineValue = sqlite3_column_double(res, 0);
			*_mean =sqlite3_column_double(res, 0);
			printf("Column %s Baseline Value: %f\n", columnName, baselineValue);
		}
	}
	else
	{
		printf("getBaselineCh1Value Error\n");
		sqlite3_finalize(res);
	}

	memset(pQuery, 0, sizeof(pQuery));
		
	sprintf(pQuery, "select %s from %s order by %s desc limit %d;",\
		columnName, tableName, columnName, PERCENT(nBaselineCount, CHECK_DATA_COUNT));

	rc = sqlite3_prepare_v2(db, pQuery, -1, &res, 0);

	if(rc == SQLITE_OK)
	{
		while(sqlite3_step(res) == SQLITE_ROW)
		{
			data[i++] = sqlite3_column_double(res, 0);		
		}

	
		sqlite3_finalize(res);
		*_stdev = getStdev(i, data);
	}

	return rc;
}


int SQLITE_SetBaselineParamFFT(double* _initMeanParam, double* _initStdevParam, char* _tableName, int _count)
{
	char pQuery[1024] = {0,};
	char pValue[20] = {0,};
	
	int rc = 0;
	char* szErrMsg = NULL;
	int i;

	memset(pQuery, 0, QUERY_BUF_LENGTH);
	sprintf(pQuery, "INSERT INTO %s VALUES( ", _tableName);
	for(i=0;i<_count;i++)
	{
		if(i > 0)
			strcat(pQuery, ", ");		
		memset(pValue, 0, sizeof(pValue));
		sprintf(pValue, "%lf",_initMeanParam[i]);
		strcat(pQuery, pValue);
		strcat(pQuery, " ");
	}
	
	strcat(pQuery, ");");	
	
	printf("Query1: %s\n", pQuery);		
	
	rc = sqlite3_exec(db, pQuery, NULL, 0, &szErrMsg);
	if(rc != SQLITE_OK)
		printf("Failed to insert initMeanParam into the BASELINE table\n");

	memset(pQuery, 0, QUERY_BUF_LENGTH);
	sprintf(pQuery, "INSERT INTO %s VALUES( ", _tableName);
	for(i=0;i<_count;i++)
	{
		if(i > 0)
			strcat(pQuery, ", ");		
		memset(pValue, 0, sizeof(pValue));
		sprintf(pValue, "%lf",_initStdevParam[i]);
		strcat(pQuery,pValue);
		strcat(pQuery, " ");
	}
	
	strcat(pQuery, ");");	
	printf("Query2: %s\n", pQuery);		
	
	rc = sqlite3_exec(db, pQuery, NULL, 0, &szErrMsg);
	if(rc != SQLITE_OK)
		printf("Failed to insert initStdevParam into the BASELINE table\n");

	return rc;
}


int SQLITE_SetBaselineParamMIC(PARAM _initMeanParam, PARAM _initStdevParam)
{
	char pQuery[1024] = {0,};
	int rc = 0;
	char* szErrMsg = NULL;
	
	sprintf(pQuery, "INSERT INTO %s VALUES( %lf, %lf, %lf, %lf, %lf);",\
				   TABLE_NAME_CHANNEL4_BASELINE,  _initMeanParam.mic_overall, _initMeanParam.mic_m1, _initMeanParam.mic_m2, _initMeanParam.mic_m3, _initMeanParam.mic_m4);

	rc = sqlite3_exec(db, pQuery, NULL, 0, &szErrMsg);
	if(rc != SQLITE_OK)
		printf("Failed to insert initMeanParam into the BASELINE table\n");

	memset(pQuery, 0, sizeof(pQuery));

	sprintf(pQuery, "INSERT INTO %s VALUES( %lf, %lf, %lf, %lf, %lf);",\
		TABLE_NAME_CHANNEL4_BASELINE,  _initStdevParam.mic_overall, _initStdevParam.mic_m1, _initStdevParam.mic_m2, _initStdevParam.mic_m3, _initStdevParam.mic_m4);

	rc = sqlite3_exec(db, pQuery, NULL, 0, &szErrMsg);
	if(rc != SQLITE_OK)
		printf("Failed to insert initStdevParam into the BASELINE table\n");


	
	return rc;
}


void checkAlertWithBaselineFFTper5Min(double* _paramMeanBaseline, double* _paramMeanComp, double* _paramDevComp, int _count)
{
	int i;
	
	for(i=0;i<_count;i++)
	{
		_paramDevComp[i] = _paramMeanComp[i] - _paramMeanBaseline[i];
		//_paramDevComp[i] = _paramMeanComp[i] / _paramMeanBaseline[i];
	}
}
#if 0
int checkDataWithBaselineFFTperSecCH4(double* _paramMeanBaseline,double* _paramStdevBaseline, double* _paramCompData, char* _pAlarmValue, int _count, AlarmCondition _cond)
{
	int ret =  0;
	double value = 0.0;
	int i=0;
	int alarmValue[10]={0,};
	int isAlarmed = 0;
	
	printf(":%f, %f, %f, %f\n", _cond.alarm1_f1,  _cond.alarm1_f2,  _cond.alarm2_f1,  _cond.alarm2_f2);
	for(;i<_count;i++)
	{
	
		printf("=>%lf, %lf\n", _paramCompData[i],  (_paramMeanBaseline[i]*_cond.alarm1_f1 + _paramStdevBaseline[i]*_cond.alarm1_f2));
		if(_paramCompData[i] - (_paramMeanBaseline[i]*_cond.alarm1_f1 + _paramStdevBaseline[i]*_cond.alarm1_f2) >= 0)
		{
			isAlarmed = TRUE;		
			alarmValue[i] = ALERT1;		
		}
		printf("=>%lf, %lf\n", _paramCompData[i],  (_paramMeanBaseline[i]*_cond.alarm2_f1 + _paramStdevBaseline[i]*_cond.alarm2_f2));
		if(_paramCompData[i] - (_paramMeanBaseline[i]*_cond.alarm2_f1 + _paramStdevBaseline[i]*_cond.alarm2_f2) >= 0)
		{
			isAlarmed = TRUE;
			alarmValue[i] = ALERT2;		
		}
	}

	sprintf(_pAlarmValue, "%d%d%d%d%d%d%d%d%d%d", alarmValue[0],alarmValue[1],alarmValue[2],alarmValue[3],alarmValue[4],alarmValue[5],alarmValue[6],alarmValue[7],alarmValue[8],alarmValue[9]);
	printf("------------------------%s\n", _pAlarmValue);
	return isAlarmed;
}
#endif

int checkDataWithBaselineFFTperSec(double* _paramMeanBaseline,double* _paramStdevBaseline, double* _paramCompData,int _count, AlarmCondition _cond)
{
	int ret =  0;
	double value = 0.0;
	int i=0;
	
	for(;i<_count;i++)
	{
		if(_cond.alarmCheckType == TYPE_ALARM_VALUE)
		{
			if(_paramCompData[i] - _cond.alarm1 >= 0)
			{
				ret = ALERT1;		
			}
			if(_paramCompData[i] - _cond.alarm2 >= 0)
			{
				ret = ALERT2;		
			}
		}
		else//TYPE_ALARM_RATE
		{
			if(_paramCompData[i] - (_paramMeanBaseline[i]*_cond.alarm1_f1 + _paramStdevBaseline[i]*_cond.alarm1_f2) >= 0)
			{
				ret = ALERT1;		
			}
			if(_paramCompData[i] - (_paramMeanBaseline[i]*_cond.alarm2_f1 + _paramStdevBaseline[i]*_cond.alarm2_f2) >= 0)
			{
				ret = ALERT2;		
			}
		}
	}

	return ret;
}


 


void checkAlertWithBaselineMICper5Min(PARAM _paramMeanBaseline, PARAM _paramMeanComp, PARAM* _paramDevComp)
{
	//MIC
	_paramDevComp->mic_overall = _paramMeanComp.mic_overall - _paramMeanBaseline.mic_overall;
	_paramDevComp->mic_m1 = _paramMeanComp.mic_m1 - _paramMeanBaseline.mic_m1;
	_paramDevComp->mic_m2 = _paramMeanComp.mic_m2 - _paramMeanBaseline.mic_m2;
	_paramDevComp->mic_m3 = _paramMeanComp.mic_m3 - _paramMeanBaseline.mic_m3;
}


int checkDataWithBaselineMICperSec(PARAM _paramMeanBaseline,PARAM _paramStdevBaseline, PARAM _paramCompData, PARAM* _paramDevComp)
{
	int ret =  0;
	double value = 0.0;
	
	//MIC OVERALL
	if(_paramCompData.mic_overall - (_paramMeanBaseline.mic_overall*1.7 + _paramStdevBaseline.mic_overall*2) >= 0)
		ret = ALERT1;		
	
	value = _paramCompData.mic_overall - _paramMeanBaseline.mic_overall;
	if(value > 0 && _paramMeanBaseline.mic_overall > 0)
		_paramDevComp->mic_overall = value;
	
	//MIC M1
	if(_paramCompData.mic_m1 - (_paramMeanBaseline.mic_m1*1.7 + _paramStdevBaseline.mic_m1*2) >= 0)
		ret = ALERT1;		
	value = _paramCompData.mic_m1 - _paramMeanBaseline.mic_m1;
	if(value > 0 && _paramMeanBaseline.mic_m1 )
		_paramDevComp->mic_m1 = value;
	
	//MIC M2
	if(_paramCompData.mic_m2 - (_paramMeanBaseline.mic_m2*1.7 + _paramStdevBaseline.mic_m2*2) >= 0)
		ret = ALERT1;		
	value = _paramCompData.mic_m2 - _paramMeanBaseline.mic_m2;
	if(value > 0 && _paramMeanBaseline.mic_m2 )
		_paramDevComp->mic_m2 = value;
	
	//MIC M3
	if(_paramCompData.mic_m3 - (_paramMeanBaseline.mic_m3*1.7 + _paramStdevBaseline.mic_m3*2) >= 0)
		ret = ALERT1;		
	value = _paramCompData.mic_m3 - _paramMeanBaseline.mic_m3;
	if(value > 0 && _paramMeanBaseline.mic_m3 )
		_paramDevComp->mic_m3= value;

	//MIC M4
	if(_paramCompData.mic_m4 - (_paramMeanBaseline.mic_m4*1.7 + _paramStdevBaseline.mic_m4*2) >= 0)
		ret = ALERT1;		
	value = _paramCompData.mic_m4 - _paramMeanBaseline.mic_m4;
	if(value > 0 && _paramMeanBaseline.mic_m4 )
		_paramDevComp->mic_m4 = value;

	

	//MIC OVERALL
	if(_paramCompData.mic_overall - (_paramMeanBaseline.mic_overall*2.5 + _paramStdevBaseline.mic_overall*2) >= 0)
		ret = ALERT2;		
	
	//MIC M1
	if(_paramCompData.mic_m1 - (_paramMeanBaseline.mic_m1*2.5 + _paramStdevBaseline.mic_m1*2) >= 0)
		ret = ALERT2;		
	
	//MIC M2
	if(_paramCompData.mic_m2 - (_paramMeanBaseline.mic_m2*2.5 + _paramStdevBaseline.mic_m2*2) >= 0)
		ret = ALERT2;		
	
	//MIC M3
	if(_paramCompData.mic_m3 - (_paramMeanBaseline.mic_m3*2.5 + _paramStdevBaseline.mic_m3*2) >= 0)
		ret = ALERT2;		

	//MIC M4
	if(_paramCompData.mic_m4 - (_paramMeanBaseline.mic_m4*2.5 + _paramStdevBaseline.mic_m4*2) >= 0)
		ret = ALERT2;		

	return ret;


}


void requestReg_SetAll(int fd, uint32_t *regbuf)
{
  int i; 
  
#ifdef DEBUG	
  printf(" Start to initialiaze Registers. \n"); 
#endif 

  transfer_cmd(fd, CMD_SETREGA_tx, send_rx, sizeof(send_rx));
  
  reg.cmd = CMD_SET_REGA; 
  
  for ( i = 0 ; i < NB_REGISTER; i++ )
  {
    reg.data[i] = regbuf[i]; 
  }
  
  transfer_cmd(fd, &reg, default_rx, sizeof(reg));
#ifdef DEBUG	  
  printf(" Finished to initialiazie Registers. \n"); 
#endif   
} 




int checkRPM2(void* arg)
{
#ifdef FEATURE_PREQUENCY
	struct sigaction sigact, oldact; 
	int oflag; 
	int iRtn = 0; 
	char* iBuf;	
	char cInput; 
	long sec = 0;
	long usec = 0;
	//long preUsec = 0;
	float prequency = 0;
	int dataCount = 0;
	int i = 0;
	char cwdPath[MAX_PATH]={0,};
	char filePath[MAX_PATH]={0,};

	float totalPrequency = 0;
	int count = 100;
#endif/*FEATURE_PREQUENCY*/


	//printf("starttime: %ld s %ld us\n", start.tv_sec, start.tv_usec); 

	sigact.sa_handler = sigio_handler; 
	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = SA_INTERRUPT; 
	
	if(sigaction(SIGIO, &sigact, &oldact) < 0)
	{
		printf("sigaction error\n"); 
		//perror("sigaction error : "); 
		//exit(0); 
	}
/* 
	GPIO interrupt setting 
*/ 	
	gInfo.io = gpioPin; 
	gInfo.mode = 2; 

	/* 0 : low 
	   1 : high 
	   2 : falling 
	   3 : rising 
	*/ 
	
	dev = open(DEVICE_FILENAME, O_RDWR); 

	if(dev >= 0)
	{
		ioctl(dev,SIGIO_SET, &gInfo); 
		 
		fcntl(dev, F_SETOWN, getpid()); 
		oflag = fcntl(dev, F_GETFL); 
		fcntl(dev, F_SETFL, oflag|FASYNC); 
		
		printf("open OK_------------------------------------------------\n"); 
 		while(1)
		{
     		if(iRtn == 100)
                		break;
			usleep(100);
			dataCount = read(dev, buff2, DATA_COUNT);

			for(i=0;i<dataCount ;i++)
			{
				memcpy(&sec, &buff2[i*8], 4);
				memcpy(&usec, &buff2[i*8+4], 4);

				printf("%ld.%ld\n", sec, usec);
				
				if(usec != 0)
				if(preUsec > usec)
					prequency =(float) 1000000/(1000000+usec-preUsec);
				else
					prequency =(float) 1000000/(usec-preUsec);
				count--;
				totalPrequency+= prequency;
				preUsec = usec;
				
			}

			if(count <= 0)
			{
				rpm = (int)(totalPrequency/100);
				totalPrequency = 0;
				count = 100;
				printf("***********************Updated Tacho***********************\n");
			}
			memset(buff2, 0, 8*DATA_COUNT);
			//if(preUsec != 0 || preUsec < usec)	
				//usleep(10);
			//else
			//	usleep(preUsec+1000000 - usec);
			
        }
                                                         		
		close(dev); 
	}
	else 
		printf("open error\n"); 
	
	//printf("%lf totalPrequency/CHECK_COUNT:%d\n", totalPrequency,(int)totalPrequency/CHECK_COUNT); 
}

#ifdef FEATURE_MQTT

int connectMqttServer(int node_index)
{
	int rc;
	MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;

	MQTTClient_create(&(bleNode[node_index].client), address, bleNode[node_index].clientId, MQTTCLIENT_PERSISTENCE_NONE, NULL);
	
	conn_opts.keepAliveInterval = 20;
	conn_opts.cleansession = 1;

	printf("MQTT Server address:%s\n", address);
	printf("topic:%s\n", bleNode[node_index].topic);
	
	if ((rc = MQTTClient_connect(bleNode[node_index].client, &conn_opts)) != MQTTCLIENT_SUCCESS)
	{
		printf("Failed to connect for CH%d, return code %d\n", node_index+1, rc);
		printf("Retry to connect Server after 10 sec\n");
		sleep(10);
	}

	printf("Successed to connect Server for CH%d\n", node_index+1);
	return rc;
}
#endif



int checkRPM(void* arg)
{
#ifdef FEATURE_PREQUENCY
	struct sigaction sigact, oldact; 
	int oflag; 
	int iRtn = 0; 
	char* iBuf;	
	char cInput; 
	long sec = 0;
	long usec = 0;
	//long preUsec = 0;
	float prequency = 0;
	int dataCount = 0;
	int i = 0;
	char cwdPath[MAX_PATH]={0,};
	char filePath[MAX_PATH]={0,};
	float testTacho=0;
	float totalPrequency = 0;
	int count = 1000;
#endif/*FEATURE_PREQUENCY*/


	//printf("starttime: %ld s %ld us\n", start.tv_sec, start.tv_usec); 

	sigact.sa_handler = sigio_handler; 
	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = SA_INTERRUPT; 
	
	if(sigaction(SIGIO, &sigact, &oldact) < 0)
	{
		printf("sigaction error\n"); 
		//perror("sigaction error : "); 
		//exit(0); 
	}
/* 
	GPIO interrupt setting 
*/ 	
	gInfo.io = gpioPin; 
	gInfo.mode = 3; 

	/* 0 : low 
	   1 : high 
	   2 : falling 
	   3 : rising 
	*/ 
	
	dev = open(DEVICE_FILENAME, O_RDWR); 

	if(dev >= 0)
	{
		ioctl(dev,SIGIO_SET, &gInfo); 
		 
		fcntl(dev, F_SETOWN, getpid()); 
		oflag = fcntl(dev, F_GETFL); 
		fcntl(dev, F_SETFL, oflag|FASYNC); 
		
		printf("open OK_------------------------------------------------\n"); 
 		while(1)
		{
		float a = 1.0;
		usleep(1000000);
     		if(iRtn == 100)
                		break;
			dataCount = read(dev, buff2, DATA_COUNT);
			printf("-----------------tacho:%d--------------------------\n", dataCount);
			
			if(dataCount != 0)
			{
				//totalPrequency = (float)1/dataCount*1000000;
				testTacho = (float)(1.0/dataCount);
				testTacho *= 1000000;
	//			testTacho = (int)((float)a/dataCount)*1000000;
				rpm = (int)floor(testTacho+0.5);
				printf("-------------%d----tacho:%f--------------------------\n",rpm, testTacho);
	
			}
			/*
			for(i=0;i<dataCount ;i++)
			{
				memcpy(&sec, &buff2[i*8], 4);
				memcpy(&usec, &buff2[i*8+4], 4);
				
				//printf("%ld.%ld\n", sec, usec);
				if(usec != 0)
				if(preUsec > usec)
					prequency =(float) 1000000/(1000000+usec-preUsec);
				else
					prequency =(float) 1000000/(usec-preUsec);
				count--;
				totalPrequency+= prequency;
				preUsec = usec;
				
			}
			*/
				//rpm = (int)( dataCount/1000000);
//
			//memset(buff2, 0, 8*DATA_COUNT);
			//if(preUsec != 0 || preUsec < usec)	
				//usleep(10);
			//else
			//	usleep(preUsec+1000000 - usec);
			
        }
                                                         		
		close(dev); 
	}
	else 
		printf("open error\n"); 

	//printf("%lf totalPrequency/CHECK_COUNT:%d\n", totalPrequency,(int)totalPrequency/CHECK_COUNT); 
}

void getdoubledt(void)
{
  struct timeval val;
  struct tm *ptm;

  gettimeofday(&val, NULL);
  ptm = localtime(&val.tv_sec);

  printf(" %04d %02d %02d %02d %02d %02d %06ld\n"
      , ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
      , ptm->tm_hour, ptm->tm_min, ptm->tm_sec
      , val.tv_usec);
}

void getdoubledt_(int _index)
{
  struct timeval val;
  struct tm *ptm;
/*
  gettimeofday(&val, NULL);
  ptm = localtime(&val.tv_sec);

  printf("%d %04d %02d %02d %02d %02d %02d %06ld\n"
      , _index, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
      , ptm->tm_hour, ptm->tm_min, ptm->tm_sec
      , val.tv_usec);
      */
}




/*	 Return Peak Value*/
double convertRawToTimeChannel1(unsigned int _sampleCnt, unsigned char* _rawData, FILE* _fp)
{
        unsigned char i,j;
        int index;
        short data;
        unsigned char *dp;
		double peak = 0;
		double db_data = 0;
		
        dp = (unsigned char *)&data;
		
#ifdef FEATURE_LPF		
 		if(gAD1FlagLowPassFilterEnable == TRUE)
			printf("doConvert1 error\n\n\n");
#endif

        for(index=0;index<_sampleCnt;index++)
        {
                *dp = _rawData[index*2];
                *(dp+1) = _rawData[index*2+1];
                //*(dp+2) = fgetc(f);
                //*(dp+3) = fgetc(f);
                //convertDataBuf[index] = data*0.0359;
                db_data = data;
               	
				db_data =  db_data*(1000/alarmContionCH1.sensorVoltage)/32767*98;
                convertDataBuf1[index] = db_data;
				//fprintf(fp1, "%lf\n", db_data);
				if(convertDataBuf1[index] > peak)
					peak = convertDataBuf1[index];
				
        }

#ifdef	FEATURE_RAWDATA		
		pthread_mutex_lock(&writeFileMutex); 
		fwrite(convertDataBuf1, AD_SAMPLING_RATE, 4, _fp);
		fclose(_fp);
		pthread_mutex_unlock(&writeFileMutex); 
#endif				

#ifdef FEATURE_LPF		
		gAD1FlagLowPassFilterEnable = TRUE;
#endif

		return peak;
}

double convertRawToTimeChannel2(unsigned int _sampleCnt, unsigned char* _rawData, FILE* _fp)
{
        unsigned char i,j;
        int index;
        short data;
        unsigned char *dp;
		
		double peak = 0;
		double db_data = 0;

 
		 dp = (unsigned char *)&data;
#ifdef FEATURE_LPF		
		 if(gAD2FlagLowPassFilterEnable == TRUE)
			 printf("doConvert2 error\n\n\n");
#endif
 
        for(index=0;index<_sampleCnt;index++)
        {
                *dp = _rawData[index*2];
                *(dp+1) = _rawData[index*2+1];
                //*(dp+2) = fgetc(f);
                //*(dp+3) = fgetc(f);
                //convertDataBuf[index] = data*0.0359;
                
				 db_data = data;
				db_data =  db_data*(1000/alarmContionCH2.sensorVoltage)/32767*98;
                convertDataBuf2[index] = db_data;
				
				if(convertDataBuf2[index] > peak)
					peak = convertDataBuf2[index];
        }
  		gAD2FlagLowPassFilterEnable = TRUE;
#ifdef	FEATURE_RAWDATA		
		pthread_mutex_lock(&writeFileMutex); 
		fwrite(convertDataBuf2, AD_SAMPLING_RATE, 4, _fp);
		
		fclose(_fp);
		pthread_mutex_unlock(&writeFileMutex); 
#endif
		return peak;
 }

double convertRawToTimeChannel3(unsigned int _sampleCnt, unsigned char* _rawData, FILE* _fp)
{
        unsigned char i,j;
        int index;
        short data;
        unsigned char *dp;
	
		double peak = 0;
		double db_data = 0;
		
		dp = (unsigned char *)&data;
		
#ifdef FEATURE_LPF		
		if(gAD3FlagLowPassFilterEnable == TRUE)
			printf("doConvert3 error\n\n\n");
#endif	
 
        for(index=0;index<_sampleCnt;index++)
        {
                *dp = _rawData[index*2];
                *(dp+1) = _rawData[index*2+1];
                //*(dp+2) = fgetc(f);
                //*(dp+3) = fgetc(f);
                //convertDataBuf[index] = data*0.0359;
				 db_data = data;
				db_data =  db_data*(1000/alarmContionCH3.sensorVoltage)/32767*98;
                convertDataBuf3[index] = db_data;
				
				if(convertDataBuf3[index] > peak)
					peak = convertDataBuf3[index];
        }
#ifdef	FEATURE_RAWDATA		
		pthread_mutex_lock(&writeFileMutex); 
		fwrite(convertDataBuf3, AD_SAMPLING_RATE, 4, _fp);

		fclose(_fp);
		pthread_mutex_unlock(&writeFileMutex); 		 
#endif
		gAD3FlagLowPassFilterEnable = TRUE;

 	return peak;
	
}

double convertRawToTimeChannel4(unsigned int _sampleCnt, unsigned char* _rawData, FILE* _fp)
{
        unsigned char i,j;
        int index;
        short data;
        unsigned char *dp;
		
		double peak = 0;
		double db_data = 0;

		dp = (unsigned char *)&data;
		
#ifdef FEATURE_LPF		
		if(gAD4FlagLowPassFilterEnable == TRUE)
			printf("doConvert4 error\n\n\n");
#endif	
        for(index=0;index<_sampleCnt;index++)
        {
                *dp = _rawData[index*2];
                *(dp+1) = _rawData[index*2+1];
                //*(dp+2) = fgetc(f);
                //*(dp+3) = fgetc(f);
                //convertDataBuf[index] = data*0.0359;
				 db_data = data;
				db_data =  db_data*(1000/alarmContionCH4.sensorVoltage)/32767*98;
                convertDataBuf4[index] = db_data;
				
				if(convertDataBuf4[index] > peak)
					peak = convertDataBuf4[index];
        }

#ifdef	FEATURE_RAWDATA		
		pthread_mutex_lock(&writeFileMutex); 
		fwrite(convertDataBuf4, AD_SAMPLING_RATE, 4, _fp);

		fclose(_fp);
		pthread_mutex_unlock(&writeFileMutex); 
#endif
		gAD4FlagLowPassFilterEnable = TRUE;

		return peak;
}



int convert_N_ExtractionParamsChannel1(int _sampleCnt, unsigned char* _pDataBuf, int channelNum, FILE* _fp, CTime _time)
{
		int i,j;
		int N;
		int flag= 0;
		double value = 0;
		double acc_peak = 0;
		double ifft_acc_min = 0xFFFFFFFF;
		double ifft_acc_max = 0;		
		
		double ifft_vel_min = 0xFFFFFFFF;
		double ifft_vel_max = 0;		
		
		double acc_value = 0;
		int alertStatus = 0;
		struct timeval val;
		struct tm *ptm;
		unsigned char* A_DATA_PATH = NULL;
		unsigned char LogPath[256];
		int isAlarmed = 0;
		unsigned int AlarmValue = 0;
		char *szErrMsg = NULL;
		int rc;
		sqlite3_stmt *res;
		double paramCompDevPerSecCh1[COLUMN_MAX]={0.0,};
		FILE * alarmFFTData = NULL;
		FILE * per1hFFTData = NULL;

		fftw_complex *alarmFFTDataBuf = NULL;
		gettimeofday(&val, NULL);
		ptm = localtime(&val.tv_sec);

		N = _sampleCnt;
#if DEBUG_FFT_
		printf("# N = %d\n",_sampleCnt);
#endif
		A_DATA_PATH = getenv("A_PATH");
		//[[180604]chchoi-mod:change to save Collecting Data Time
		if(ch1LogMinValue == -1 || ch1LogMinValue != _time.hour)
		{
			if(ch1LogMinValue != -1)
				fclose(fpLogCh1);
			memset(LogPath, 0 , sizeof(LogPath));
			sprintf(LogPath, "%s/Log/Log_%04d_%02d_%02d_%02d_CH1.CSV", A_DATA_PATH,_time.year, _time.month, _time.day, _time.hour);
			ch1LogMinValue = _time.hour;
			fpLogCh1 = fopen(LogPath, "ab+");
}
		

#if DEBUG_FFT_
		printf("# FFT CH:%d Start Time : ",channelNum );
		getdoubledt_();
#endif
				getdoubledt_(1);
		acc_peak = convertRawToTimeChannel1(_sampleCnt, _pDataBuf, _fp);
		
		getdoubledt_(2);
		if(gpCH1FFT_Input == NULL)
		{
			gpCH1FFT_Input = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
			gpCH1FFT_Output = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
			gpCH1InvFFT_Input = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
			gpCH1InvFFT_Output = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
		}
		
		alarmFFTDataBuf = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
		memset(gpCH1FFT_Input, 0, sizeof(fftw_complex) * N);
		memset(gpCH1FFT_Output, 0, sizeof(fftw_complex) * N);
		memset(gpCH1InvFFT_Input, 0, sizeof(fftw_complex) * N);
		memset(gpCH1InvFFT_Output, 0, sizeof(fftw_complex) * N);
		memset(alarmFFTDataBuf, 0, sizeof(fftw_complex) * N);

		
		getdoubledt_(3);
		for(i=0;i<N;i++){
				gpCH1FFT_Input[i][0] =(float)convertDataBuf1[i];
				//printf("ori, %f\n", (float)convertDataBuf1[i]);
		}

		if(gCH1Plan == NULL)
			gCH1Plan = fftw_plan_dft_1d(N, gpCH1FFT_Input, gpCH1FFT_Output, FFTW_FORWARD, FFTW_ESTIMATE);
		
		fftw_execute(gCH1Plan); 
		getdoubledt_(4);

#ifdef DEBUG_FFT_
		getdoubledt();
#endif

#if 1
		memcpy(alarmFFTDataBuf, gpCH1FFT_Output, sizeof(fftw_complex) * N);
		getParamFFTData(gParamCh1,_sampleCnt,gpCH1FFT_Output, requiredParamCH1, gParamCountCH1, 1);
#if DEBUG_FFT_
		printf("# FFT CH:%d End Time : ", channelNum);
		getdoubledt();
#endif
		getdoubledt_(5);

		for(i = 0; i< gParamCountCH1;i++)
		{
			if(requiredParamCH1[i].paramType == INDEX_ACC_AP2P)
			{
				for(j=1;j<N;j++){
					if(j < requiredParamCH1[i].paramF1 || (j > requiredParamCH1[i].paramF2 && j < (N-requiredParamCH1[i].paramF2)) || j>(N-requiredParamCH1[i].paramF1))									
					{
						gpCH1FFT_Output[j][0] = 0;
						//[[180604]]chchoi-add:insert 0
						gpCH1FFT_Output[j][1] = 0;						
					}
				}
				
				break;
			}
		}

			 //fftw_free(out);
		//out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
		if(gCH1PlanInverse == NULL)
			gCH1PlanInverse = fftw_plan_dft_1d(N, gpCH1FFT_Output, gpCH1InvFFT_Output, FFTW_BACKWARD, FFTW_ESTIMATE);
		
		fftw_execute(gCH1PlanInverse);		
		printf("%d\n", testcount++);
		for(i=1;i<N;i++){
			gpCH1InvFFT_Output[i][0] *=1./N;
			
			value = gpCH1InvFFT_Output[i][0];
			//value = (float)outIfft[i][0]/(2*3.14*i);			
			
			if(value > ifft_acc_max)
				ifft_acc_max = value;
			
			if(value < ifft_acc_min)
				ifft_acc_min = value;
		}
		pthread_mutex_lock(&writeFileMutex); 

		//fprintf(fpLogCh1, "CH%d, %04d/%02d/%02d %02d:%02d:%02d,%06ld ", channelNum,ptm->tm_year + 1900, ptm->tm_mon+1, ptm->tm_mday, 
		//	ptm->tm_hour, ptm->tm_min,ptm->tm_sec, val.tv_usec);
	//	printf("CH%d  Velocity V4 Max:%lf Min: %lf\n",channelNum, ifft_vel_max, ifft_vel_min);
	
		//[[0180604]chchoi-mod:changed the unit to g
		for(i=0;i<gParamCountCH1;i++)
			if(requiredParamCH1[i].paramType == INDEX_ACC_AP2P)
				gParamCh1[i] = (ifft_acc_max - ifft_acc_min)/9.8;
#if DEBUG_FFT_
				printf("# IFFT CH:%d End Time : ", channelNum);
				getdoubledt();
#endif
		
#if 1
		getdoubledt_(6);
		pthread_mutex_unlock(&writeFileMutex); 

		nCountCheck1hCh1++;


#ifdef FEATURE_SQLITE3

		
		if(SQLITE_OK != SQLITE_InsertDataTable(gParamCh1,TABLE_NAME_CHANNEL1_REALTIME_DATA, gParamCountCH1, _time ))
			printf("Realtime Data Insert Error\n");
		//No BaseLine Data in DB Table
		if(bHasBaselineCh1 == FALSE)
		{
			if(nInitDataCountCh1 < nBaselineCount)//6*60*60
			{
				memset(gQueryCH1, 0, sizeof(gQueryCH1));
				if(SQLITE_OK == SQLITE_SetInitParamFFT(gParamCh1,TABLE_NAME_CHANNEL1_INIT_DATA, gParamCountCH1, gQueryCH1 ))
					nInitDataCountCh1++;

				//If the init data is 6*60*60(6Hour), calculate the Baseline and insert Baseline table.
				if(nInitDataCountCh1 == nBaselineCount)//6*60*60
				{
					for(i=0;i<gParamCountCH1;i++)
					{
						SQLITE_GenBaselineParamFromInitParam(gColumnNameArray[requiredParamCH1[i].paramType], TABLE_NAME_CHANNEL1_INIT_DATA, &gMeanBaselineParamCh1[i], &gStdevBaselineParamCh1[i]);
					}

					if(SQLITE_SetBaselineParamFFT(gMeanBaselineParamCh1, gStdevBaselineParamCh1, TABLE_NAME_CHANNEL1_BASELINE, gParamCountCH1) != SQLITE_OK)
						printf("SetBaselineParamCh1 Error\n");
					else
						bHasBaselineCh1 = TRUE; 					
					//Get init datas from the CHANNEL1_INIT_DATA_TB table.				
				}
			}
		}
		else//Add params to calculate the Mean value
		{
			if(nCompDataCountCh1 < DATA_COUNT_PREMAINT)
			{
				for(i=0;i<gParamCountCH1;i++)
				{
					gParamCompCh1[nCompDataCountCh1][i] = gParamCh1[i];
				}
				nCompDataCountCh1++;
			}
			
			if(nCompDataCountCh1 == DATA_COUNT_PREMAINT)
			{
				for(i=0;i<gParamCountCH1;i++)
				{
					//gParamCompMeanCh1[i] = getMean(DATA_COUNT_PREMAINT, requiredParamCH1[i].paramType, gParamCompCh1);
					gParamCompMeanCh1[i] = getMeanSortedCh1(DATA_COUNT_PREMAINT, CHECK_DATA_COUNT, i, gParamCompCh1);
					printf("CH1 Type:%d		%lf\n",requiredParamCH1[i].paramType, gParamCompMeanCh1[i]);
					
				}				
				printf("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
				if(SQLITE_OK != SQLITE_InsertDataTable(gParamCompMeanCh1,TABLE_NAME_CHANNEL1_PRE_MAINT_DATA, gParamCountCH1, _time ))
					printf("Realtime Data Insert Error\n");
				
				checkAlertWithBaselineFFTper5Min(gMeanBaselineParamCh1, gParamCompMeanCh1, gParamCompDevPer5MinCh1, gParamCountCH1);
			}
		}

		getdoubledt_(9);
		if(bHasBaselineCh1 == TRUE)
			alertStatus = checkDataWithBaselineFFTperSec(gMeanBaselineParamCh1, gStdevBaselineParamCh1, gParamCh1, gParamCountCH1, alarmContionCH1);
		
		if(alertStatus == ALERT1 || alertStatus == ALERT2)
		{
			memset(LogPath, 0 , sizeof(LogPath));
			sprintf(LogPath, "%s/Alarm/Alarm_%04d_%02d_%02d_%02d_%02d_%06ld_CH1.CSV", A_DATA_PATH, _time.year, _time.month, _time.day
			, _time.hour, _time.min, _time.sec, _time.usec);
			alarmFFTData = fopen(LogPath, "at+");

			for(i = 0; i <= _sampleCnt/2; i++)
			{
				//[[180604]]chchoi-mod:fixed fft data value(not RMS value)
				//acc_value = (double)(sqrt(pow(alarmFFTDataBuf[i][0],2) + pow(alarmFFTDataBuf[i][1],2))/sqrt(51200)* sqrt(pow(alarmFFTDataBuf[i][0],2) + pow(alarmFFTDataBuf[i][1],2))/sqrt(51200));
				acc_value = (double)(sqrt(pow(alarmFFTDataBuf[i][0],2) + pow(alarmFFTDataBuf[i][1],2))/sqrt(10240))/9.8;				
				fprintf(alarmFFTData, "%d, %lf\n", i, acc_value);
			}
			fclose(alarmFFTData);
			
			printf("XXXXXXXXXXXXXXXXXXXXXXXXXXX   EMERGENCY XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
			printf("XXXXXXXXXXXXXXXXXXXXXXXXXXX   EMERGENCY XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
			printf("XXXXXXXXXXXXXXXXXXXXXXXXXXX   EMERGENCY XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
		}
		//1H 3600 == (60*60)
		if(nCountCheck1hCh1%(60*60) == 0)
		{
			memset(LogPath, 0 , sizeof(LogPath));
			sprintf(LogPath, "%s/Periodic/Periodic1H_%04d_%02d_%02d_%02d_%02d_CH1.CSV", A_DATA_PATH,_time.year, _time.month, _time.day
			, _time.hour, _time.min);
			printf("Periodic:%s\n", LogPath);
			per1hFFTData = fopen(LogPath, "at+");
			
			for(i = 0; i <= _sampleCnt/2; i++)
			{
				acc_value = (double)(sqrt(pow(alarmFFTDataBuf[i][0],2) + pow(alarmFFTDataBuf[i][1],2))/sqrt(10240))/9.8;
				fprintf(per1hFFTData, "%d, %lf\n", i, acc_value);
			}
			fclose(per1hFFTData);
		}
		free(alarmFFTDataBuf);
#endif/*FEATURE_SQLITE3*/	
#ifdef DEBUG_FFT_ 
		getdoubledt();
#endif
getdoubledt_(10);

#ifdef FEATURE_SERIAL		
		memset(pSerialDataCh1, 0, sizeof(pSerialDataCh1));
		sprintf(pSerialDataCh1, "CH%d, %04d/%02d/%02d %02d:%02d:%02d,%06ld,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,0,%d\r\n",\
						channelNum,_time.year, _time.month, _time.day, _time.hour, _time.min, _time.sec, _time.usec, \
gParamCh1[INDEX_COLUMN_1], gParamCh1[INDEX_COLUMN_2], gParamCh1[INDEX_COLUMN_3], gParamCh1[INDEX_COLUMN_4], gParamCh1[INDEX_COLUMN_5],\
gParamCh1[INDEX_COLUMN_6], gParamCh1[INDEX_COLUMN_7], gParamCh1[INDEX_COLUMN_8], gParamCh1[INDEX_COLUMN_9], gParamCh1[INDEX_COLUMN_10],\
gParamCh1[INDEX_COLUMN_11], gParamCh1[INDEX_COLUMN_12], gParamCh1[INDEX_COLUMN_13], gParamCh1[INDEX_COLUMN_14], gParamCh1[INDEX_COLUMN_15],\
gParamCompDevPer5MinCh1[INDEX_COLUMN_1], gParamCompDevPer5MinCh1[INDEX_COLUMN_2], gParamCompDevPer5MinCh1[INDEX_COLUMN_3],gParamCompDevPer5MinCh1[INDEX_COLUMN_4], gParamCompDevPer5MinCh1[INDEX_COLUMN_5], \
gParamCompDevPer5MinCh1[INDEX_COLUMN_6], gParamCompDevPer5MinCh1[INDEX_COLUMN_7], gParamCompDevPer5MinCh1[INDEX_COLUMN_8],gParamCompDevPer5MinCh1[INDEX_COLUMN_9], gParamCompDevPer5MinCh1[INDEX_COLUMN_10], \
gParamCompDevPer5MinCh1[INDEX_COLUMN_11], gParamCompDevPer5MinCh1[INDEX_COLUMN_12], gParamCompDevPer5MinCh1[INDEX_COLUMN_13],gParamCompDevPer5MinCh1[INDEX_COLUMN_14], gParamCompDevPer5MinCh1[INDEX_COLUMN_15], \
			alertStatus);		
		
		fwrite(pSerialDataCh1, strlen(pSerialDataCh1)+2, 1, fpLogCh1);
		getdoubledt_(11);

		if(nCompDataCountCh1 == DATA_COUNT_PREMAINT)
		{
			memset(LogPath, 0 , sizeof(LogPath));

			sprintf(LogPath, "%s/PreMain/PreMain_%04d_%02d_%02d_%02d_%02d_%02d_CH1.CSV", A_DATA_PATH,_time.year, _time.month, _time.day, _time.hour, _time.min, _time.sec);
			printf("PREMAIN:%s\n", LogPath);			
			fpPreMainPer5minCh1 = fopen(LogPath, "wt+");
			
			fwrite(pSerialDataCh1, strlen(pSerialDataCh1)+2, 1, fpPreMainPer5minCh1);
			fclose(fpPreMainPer5minCh1);
			nCompDataCountCh1 = 0;
		}
		getdoubledt_(12);

		bFlushSerialCh1 = 1;
#endif/*FEATURE_SERIAL*/


		//if(ch1FlushCount % 10 == 0)
		//	fflush(fpLogCh1);
		ch1FlushCount++;
		printf("[[Count]]:[%d]\n",ch1FlushCount++);
		if(ch1FlushCount == 3000)
		{
			ch1FlushCount = 0;
			readyToDBClosing = FLAG_AD1;
		}
//		fprintf(fpLogCh1, "CH%d  Accellerate Peak : %lf\n", channelNum, gParamCh1.acc_peak);		



#endif
#endif
//fclose(fpLogCh1);
#ifdef FEATURE_MQTT
	genPublishDataAD1();
#endif
}



int convert_N_ExtractionParamsChannel2(int _sampleCnt, unsigned char* _pDataBuf, int channelNum, FILE* _fp, CTime _time)
{
		int i,j;
		int N;
		int flag= 0;
		double value = 0;
		double acc_peak = 0;
		double ifft_acc_min = 0xFFFFFFFF;
		double ifft_acc_max = 0;		
		
		double ifft_vel_min = 0xFFFFFFFF;
		double ifft_vel_max = 0;		
		
		double acc_value = 0;
		int isAlarmed = 0;
		unsigned int AlarmValue = 0;
		int alertStatus = 0;

		struct timeval val;
		struct tm *ptm;
		unsigned char* A_DATA_PATH = NULL;
		unsigned char LogPath[256] ;
		char *szErrMsg = NULL;
		int rc;
		sqlite3_stmt *res;
		double paramCompDevPerSecCh2[COLUMN_MAX]={0.0,};
		FILE * alarmFFTData = NULL;
		FILE * per1hFFTData = NULL;
		
		fftw_complex *alarmFFTDataBuf = NULL;
		gettimeofday(&val, NULL);
		ptm = localtime(&val.tv_sec);

		N = _sampleCnt;
#if DEBUG_FFT_
		printf("# N = %d\n",_sampleCnt);
#endif
		A_DATA_PATH = getenv("A_PATH");

		if(ch2LogMinValue == -1 || ch2LogMinValue != _time.hour)
		{
			if(ch2LogMinValue != -1)
				fclose(fpLogCh2);
			memset(LogPath, 0 , sizeof(LogPath));
			sprintf(LogPath, "%s/Log/Log_%04d_%02d_%02d_%02d_CH2.CSV", A_DATA_PATH,_time.year, _time.month, _time.day, _time.hour);
			ch2LogMinValue = _time.hour;
			fpLogCh2 = fopen(LogPath, "ab+");
		}


#if DEBUG_FFT_
		printf("# FFT CH:%d Start Time : ",channelNum );
		getdoubledt();
#endif
		
		acc_peak = convertRawToTimeChannel2(_sampleCnt, _pDataBuf, _fp);
		if(gpCH2FFT_Input == NULL)
		{
			gpCH2FFT_Input = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
			gpCH2FFT_Output = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
			gpCH2InvFFT_Input = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
			gpCH2InvFFT_Output = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
		}
		
		alarmFFTDataBuf = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
		memset(gpCH2FFT_Input, 0, sizeof(fftw_complex) * N);
		memset(gpCH2FFT_Output, 0, sizeof(fftw_complex) * N);
		memset(gpCH2InvFFT_Input, 0, sizeof(fftw_complex) * N);
		memset(gpCH2InvFFT_Output, 0, sizeof(fftw_complex) * N);
		memset(alarmFFTDataBuf, 0, sizeof(fftw_complex) * N);

		
		for(i=0;i<N;i++){
				gpCH2FFT_Input[i][0] =(float)convertDataBuf2[i];
				//printf("ori, %f\n", (float)convertDataBuf1[i]);
		}

		if(gCH2Plan == NULL)
			gCH2Plan = fftw_plan_dft_1d(N, gpCH2FFT_Input, gpCH2FFT_Output, FFTW_FORWARD, FFTW_ESTIMATE);
		
		fftw_execute(gCH2Plan); 

#ifdef DEBUG_FFT_
		getdoubledt();
#endif

#if 1
		memcpy(alarmFFTDataBuf, gpCH2FFT_Output, sizeof(fftw_complex) * N);
		getParamFFTData(gParamCh2,_sampleCnt,gpCH2FFT_Output, requiredParamCH2, gParamCountCH2, 2);
#if DEBUG_FFT_
		printf("# FFT CH:%d End Time : ", channelNum);
		getdoubledt();
#endif

		for(i = 0; i< gParamCountCH2;i++)
		{
			if(requiredParamCH2[i].paramType == INDEX_ACC_AP2P)
			{
				for(j=1;j<N;j++){
					if(j < requiredParamCH2[i].paramF1 || (j > requiredParamCH2[i].paramF2 && j < (N-requiredParamCH2[i].paramF2)) || j>(N-requiredParamCH2[i].paramF1))									
					{
						gpCH2FFT_Output[j][0] = 0;
						//[[180604]]chchoi-add:insert 0
						gpCH2FFT_Output[j][1] = 0;						
					}
				}
				
				break;
			}
		}

		//fftw_free(out);
		//out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
		if(gCH2PlanInverse == NULL)
			gCH2PlanInverse = fftw_plan_dft_1d(N, gpCH2FFT_Output, gpCH2InvFFT_Output, FFTW_BACKWARD, FFTW_ESTIMATE);
		
		fftw_execute(gCH2PlanInverse);		
		printf("%d\n", testcount++);
		for(i=1;i<N;i++){
			gpCH2InvFFT_Output[i][0] *=1./N;
			
			value = gpCH2InvFFT_Output[i][0];
			//value = (float)outIfft[i][0]/(2*3.14*i);			
			
			if(value > ifft_acc_max)
				ifft_acc_max = value;
			
			if(value < ifft_acc_min)
				ifft_acc_min = value;
		}
		pthread_mutex_lock(&writeFileMutex); 

		//fprintf(fpLogCh1, "CH%d, %04d/%02d/%02d %02d:%02d:%02d,%06ld ", channelNum,ptm->tm_year + 1900, ptm->tm_mon+1, ptm->tm_mday, 
		//	ptm->tm_hour, ptm->tm_min,ptm->tm_sec, val.tv_usec);
	//	printf("CH%d  Velocity V4 Max:%lf Min: %lf\n",channelNum, ifft_vel_max, ifft_vel_min);
	
		//[[0180604]chchoi-mod:changed the unit to g
		for(i=0;i<gParamCountCH2;i++)
			if(requiredParamCH2[i].paramType == INDEX_ACC_AP2P)
				gParamCh2[i] = (ifft_acc_max - ifft_acc_min)/9.8;
#if DEBUG_FFT_
		printf("# IFFT CH:%d End Time : ", channelNum);
		getdoubledt();
#endif

#if 1
//		printf("CH%d  Accellerate Peak : %lf\n", channelNum, gParamCh1.acc_peak);		

		//fprintf(fpLogCh1, ",%lf,%lf,%lf,%lf,%lf,%lf",  gParamCh1.vel_overall, gParamCh1.vel_v1, gParamCh1.vel_v2, gParamCh1.vel_v3, gParamCh1.vel_v4,gParamCh1.vel_v5);
		//fprintf(fpLogCh1, ",%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,0,0,0,0,0,0,0,0\n", gParamCh1.acc_overall, gParamCh1.acc_a1, gParamCh1.acc_a2, gParamCh1.acc_a3, gParamCh1.acc_a4, gParamCh1.acc_a5, gParamCh1.acc_a6, gParamCh1.acc_a7);
		pthread_mutex_unlock(&writeFileMutex); 
		nCountCheck1hCh2++;


#ifdef FEATURE_SQLITE3

		
		if(SQLITE_OK != SQLITE_InsertDataTable(gParamCh2,TABLE_NAME_CHANNEL2_REALTIME_DATA, gParamCountCH2, _time ))
			printf("Realtime Data Insert Error\n");
		//No BaseLine Data in DB Table
		if(bHasBaselineCh2 == FALSE)
		{
			if(nInitDataCountCh2 < nBaselineCount)//6*60*60
			{
				memset(gQueryCH2, 0, sizeof(gQueryCH2));
				if(SQLITE_OK == SQLITE_SetInitParamFFT(gParamCh2,TABLE_NAME_CHANNEL2_INIT_DATA, gParamCountCH2,gQueryCH2 ))
					nInitDataCountCh2++;
		
				//If the init data is 6*60*60(6Hour), calculate the Baseline and insert Baseline table.
				if(nInitDataCountCh2 == nBaselineCount)//6*60*60
				{
				printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");	

				for(i=0;i<gParamCountCH2;i++)
				{
					SQLITE_GenBaselineParamFromInitParam(gColumnNameArray[requiredParamCH2[i].paramType], TABLE_NAME_CHANNEL2_INIT_DATA, &gMeanBaselineParamCh2[i], &gStdevBaselineParamCh2[i]);
				}

					if(SQLITE_SetBaselineParamFFT(gMeanBaselineParamCh2, gStdevBaselineParamCh2, TABLE_NAME_CHANNEL2_BASELINE, gParamCountCH2) != SQLITE_OK)
					{
						//Insert ERROR
						printf("SetBaselineParamCh2 Error\n");
					}
					else
						bHasBaselineCh2 = TRUE; 					
					//Get init datas from the CHANNEL1_INIT_DATA_TB table.				
					}
				}
			}
		else//Add params to calculate the Mean value
		{
			if(nCompDataCountCh2 < DATA_COUNT_PREMAINT)
			{
				for(i=0;i<gParamCountCH2;i++)
				{
					gParamCompCh2[nCompDataCountCh2][i] = gParamCh2[i];
				}
				nCompDataCountCh2++;
				printf("CompDataCount: %d\n", nCompDataCountCh2);
			}
			
			if(nCompDataCountCh2 == DATA_COUNT_PREMAINT)
			{
				int index = ACC_OVERALL;

				for(i=0;i<gParamCountCH2;i++)
				{
					gParamCompMeanCh2[i] = getMeanSortedCh2(DATA_COUNT_PREMAINT, CHECK_DATA_COUNT, i, gParamCompCh2);
					printf("CH2 Type:%d		%lf\n",requiredParamCH2[i].paramType, gParamCompMeanCh2[i]);
				}				
				
				if(SQLITE_OK != SQLITE_InsertDataTable(gParamCompMeanCh2,TABLE_NAME_CHANNEL2_PRE_MAINT_DATA, gParamCountCH2, _time ))
					printf("Realtime Data Insert Error\n");
					
				checkAlertWithBaselineFFTper5Min(gMeanBaselineParamCh2, gParamCompMeanCh2, gParamCompDevPer5MinCh2, gParamCountCH2);
			}
		}

		
		if(bHasBaselineCh2 == TRUE)
			alertStatus= checkDataWithBaselineFFTperSec(gMeanBaselineParamCh2, gStdevBaselineParamCh2, gParamCh2, gParamCountCH2, alarmContionCH2);
		
		if(alertStatus == ALERT1 || alertStatus == ALERT2)
		{
			memset(LogPath, 0 , sizeof(LogPath));
			sprintf(LogPath, "%s/Alarm/Alarm_%04d_%02d_%02d_%02d_%02d_CH2.CSV", A_DATA_PATH,_time.year, _time.month, _time.day
			, _time.hour, _time.min);
			alarmFFTData = fopen(LogPath, "at+");

			for(i = 0; i <= _sampleCnt/2; i++)
			{
				acc_value = (double)(sqrt(pow(alarmFFTDataBuf[i][0],2) + pow(alarmFFTDataBuf[i][1],2))/sqrt(10240))/9.8;
				fprintf(alarmFFTData, "%d, %lf\n", i, acc_value);
			}
			fclose(alarmFFTData);
			
			printf("XXXXXXXXXXXXXXXXXXXXXXXXXXX   EMERGENCY XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
			printf("XXXXXXXXXXXXXXXXXXXXXXXXXXX   EMERGENCY XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
			printf("XXXXXXXXXXXXXXXXXXXXXXXXXXX   EMERGENCY XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
		}
		//1H 3600 == (60*60)
		if(nCountCheck1hCh2%(60*60) == 0)
		{
			memset(LogPath, 0 , sizeof(LogPath));
			sprintf(LogPath, "%s/Periodic/Periodic1H_%04d_%02d_%02d_%02d_%02d_CH2.CSV", A_DATA_PATH,_time.year, _time.month, _time.day
			, _time.hour, _time.min);
			printf("Periodic:%s\n", LogPath);
			per1hFFTData = fopen(LogPath, "at+");
			
			for(i = 0; i <= _sampleCnt/2; i++)
			{
				acc_value = (double)(sqrt(pow(alarmFFTDataBuf[i][0],2) + pow(alarmFFTDataBuf[i][1],2))/sqrt(10240))/9.8;
				fprintf(per1hFFTData, "%d, %lf\n", i, acc_value);
			}
			fclose(per1hFFTData);
		}
		free(alarmFFTDataBuf);
#endif/*FEATURE_SQLITE3*/	
#ifdef DEBUG_FFT_ 
		getdoubledt();
#endif
#ifdef FEATURE_SERIAL		
		memset(pSerialDataCh2, 0, sizeof(pSerialDataCh2));
		sprintf(pSerialDataCh2, "CH%d, %04d/%02d/%02d %02d:%02d:%02d,%06ld,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,0,%d\r\n",\
			channelNum,_time.year, _time.month, _time.day, _time.hour, _time.min, _time.sec, _time.usec, \
gParamCh2[INDEX_COLUMN_1], gParamCh2[INDEX_COLUMN_2], gParamCh2[INDEX_COLUMN_3], gParamCh2[INDEX_COLUMN_4], gParamCh2[INDEX_COLUMN_5],\
gParamCh2[INDEX_COLUMN_6], gParamCh2[INDEX_COLUMN_7], gParamCh2[INDEX_COLUMN_8], gParamCh2[INDEX_COLUMN_9], gParamCh2[INDEX_COLUMN_10],\
gParamCh2[INDEX_COLUMN_11], gParamCh2[INDEX_COLUMN_12], gParamCh2[INDEX_COLUMN_13], gParamCh2[INDEX_COLUMN_14], gParamCh2[INDEX_COLUMN_15],\
gParamCompDevPer5MinCh2[INDEX_COLUMN_1], gParamCompDevPer5MinCh2[INDEX_COLUMN_2], gParamCompDevPer5MinCh2[INDEX_COLUMN_3],gParamCompDevPer5MinCh2[INDEX_COLUMN_4], gParamCompDevPer5MinCh2[INDEX_COLUMN_5], \
gParamCompDevPer5MinCh2[INDEX_COLUMN_6], gParamCompDevPer5MinCh2[INDEX_COLUMN_7], gParamCompDevPer5MinCh2[INDEX_COLUMN_8],gParamCompDevPer5MinCh2[INDEX_COLUMN_9], gParamCompDevPer5MinCh2[INDEX_COLUMN_10], \
gParamCompDevPer5MinCh2[INDEX_COLUMN_11], gParamCompDevPer5MinCh2[INDEX_COLUMN_12], gParamCompDevPer5MinCh2[INDEX_COLUMN_13],gParamCompDevPer5MinCh2[INDEX_COLUMN_14], gParamCompDevPer5MinCh2[INDEX_COLUMN_15], \
			alertStatus);		
		
		fwrite(pSerialDataCh2, strlen(pSerialDataCh2)+2, 1, fpLogCh2);

		if(nCompDataCountCh2 == DATA_COUNT_PREMAINT)
		{
			memset(LogPath, 0 , sizeof(LogPath));

			sprintf(LogPath, "%s/PreMain/PreMain_%04d_%02d_%02d_%02d_%02d_%02d_CH2.CSV", A_DATA_PATH,_time.year, _time.month, _time.day
			, _time.hour, _time.min, _time.sec);
			printf("PREMAIN:%s\n", LogPath);			
			fpPreMainPer5minCh2 = fopen(LogPath, "wt+");
			
			fwrite(pSerialDataCh2, strlen(pSerialDataCh2)+2, 1, fpPreMainPer5minCh2);
			fclose(fpPreMainPer5minCh2);
			nCompDataCountCh2 = 0;
		}

		bFlushSerialCh2 = 1;
#endif/*FEATURE_SERIAL*/


		//if(ch1FlushCount % 10 == 0)
		//	fflush(fpLogCh1);
		if(readyToDBClosing & FLAG_AD1)
			readyToDBClosing |= FLAG_AD2;
//		fprintf(fpLogCh1, "CH%d  Accellerate Peak : %lf\n", channelNum, gParamCh1.acc_peak);		



#endif
#endif
//fclose(fpLogCh2);
#ifdef FEATURE_MQTT
	genPublishDataAD2();
#endif
}





int convert_N_ExtractionParamsChannel3(int _sampleCnt, unsigned char* _pDataBuf, int channelNum, FILE* _fp, CTime _time)
{
		int i,j;
		int N;
		int flag= 0;
		double value = 0;
		double acc_peak = 0;
		double ifft_acc_min = 0xFFFFFFFF;
		double ifft_acc_max = 0;		
		
		double ifft_vel_min = 0xFFFFFFFF;
		double ifft_vel_max = 0;		
		
		double acc_value = 0;
		int isAlarmed = 0;
		unsigned int AlarmValue = 0;
		int alertStatus = 0;

		struct timeval val;
		struct tm *ptm;
		unsigned char* A_DATA_PATH = NULL;
		unsigned char LogPath[256] ;
		char *szErrMsg = NULL;
		int rc;
		sqlite3_stmt *res;
		double paramCompDevPerSecCh3[COLUMN_MAX]={0.0,};
		FILE * alarmFFTData = NULL;
		FILE * per1hFFTData = NULL;
		
		fftw_complex *alarmFFTDataBuf = NULL;

		N = _sampleCnt;
#if DEBUG_FFT_
		printf("# N = %d\n",_sampleCnt);
#endif
		A_DATA_PATH = getenv("A_PATH");

		if(ch3LogMinValue == -1 || ch3LogMinValue != _time.hour)
		{
			if(ch3LogMinValue != -1)
				fclose(fpLogCh3);
			memset(LogPath, 0 , sizeof(LogPath));
			sprintf(LogPath, "%s/Log/Log_%04d_%02d_%02d_%02d_CH3.CSV", A_DATA_PATH, _time.year, _time.month, _time.day
			, _time.hour);
			ch3LogMinValue = _time.hour;
			fpLogCh3 = fopen(LogPath, "ab+");
		}


#if DEBUG_FFT_
		printf("# FFT CH:%d Start Time : ",channelNum );
		getdoubledt();
#endif
		
		acc_peak = convertRawToTimeChannel3(_sampleCnt, _pDataBuf, _fp);
		if(gpCH3FFT_Input == NULL)
		{
			gpCH3FFT_Input = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
			gpCH3FFT_Output = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
			gpCH3InvFFT_Input = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
			gpCH3InvFFT_Output = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
		}
		
		alarmFFTDataBuf = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
		memset(gpCH3FFT_Input, 0, sizeof(fftw_complex) * N);
		memset(gpCH3FFT_Output, 0, sizeof(fftw_complex) * N);
		memset(gpCH3InvFFT_Input, 0, sizeof(fftw_complex) * N);
		memset(gpCH3InvFFT_Output, 0, sizeof(fftw_complex) * N);
		memset(alarmFFTDataBuf, 0, sizeof(fftw_complex) * N);

		
		for(i=0;i<N;i++){
				gpCH3FFT_Input[i][0] =(float)convertDataBuf3[i];
				//printf("ori, %f\n", (float)convertDataBuf1[i]);
		}

		if(gCH3Plan == NULL)
			gCH3Plan = fftw_plan_dft_1d(N, gpCH3FFT_Input, gpCH3FFT_Output, FFTW_FORWARD, FFTW_ESTIMATE);
		
		fftw_execute(gCH3Plan); 

#ifdef DEBUG_FFT_
		getdoubledt();
#endif

#if 1
		memcpy(alarmFFTDataBuf, gpCH3FFT_Output, sizeof(fftw_complex) * N);
		getParamFFTData(gParamCh3,_sampleCnt,gpCH3FFT_Output, requiredParamCH3, gParamCountCH3, 3);
#if DEBUG_FFT_
		printf("# FFT CH:%d End Time : ", channelNum);
		getdoubledt();
#endif

		for(i = 0; i< gParamCountCH3;i++)
		{
			if(requiredParamCH3[i].paramType == INDEX_ACC_AP2P)
			{
				for(j=1;j<N;j++){
					if(j < requiredParamCH3[i].paramF1 || (j > requiredParamCH3[i].paramF2 && j < (N-requiredParamCH3[i].paramF2)) || j>(N-requiredParamCH3[i].paramF1))									
					{
						gpCH3FFT_Output[j][0] = 0;
						gpCH3FFT_Output[j][1] = 0;						
					}
				}
				
				break;
			}
		}

		//fftw_free(out);
		//out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
		if(gCH3PlanInverse == NULL)
			gCH3PlanInverse = fftw_plan_dft_1d(N, gpCH3FFT_Output, gpCH3InvFFT_Output, FFTW_BACKWARD, FFTW_ESTIMATE);
		
		fftw_execute(gCH3PlanInverse);		
		printf("%d\n", testcount++);
		for(i=1;i<N;i++){
			gpCH3InvFFT_Output[i][0] *=1./N;
			
			value = gpCH3InvFFT_Output[i][0];
			//value = (float)outIfft[i][0]/(2*3.14*i);			
			
			if(value > ifft_acc_max)
				ifft_acc_max = value;
			
			if(value < ifft_acc_min)
				ifft_acc_min = value;
		}
#ifdef DEBUG_FFT_		
		printf("%s\n",	_fileName);
#endif
		pthread_mutex_lock(&writeFileMutex); 

		//fprintf(fpLogCh1, "CH%d, %04d/%02d/%02d %02d:%02d:%02d,%06ld ", channelNum,ptm->tm_year + 1900, ptm->tm_mon+1, ptm->tm_mday, 
		//	ptm->tm_hour, ptm->tm_min,ptm->tm_sec, val.tv_usec);
	//	printf("CH%d  Velocity V4 Max:%lf Min: %lf\n",channelNum, ifft_vel_max, ifft_vel_min);
		for(i=0;i<gParamCountCH3;i++)
			if(requiredParamCH3[i].paramType == INDEX_ACC_AP2P)
				gParamCh3[i] = (ifft_acc_max - ifft_acc_min)/9.8;
#if DEBUG_FFT_
		printf("# IFFT CH:%d End Time : ", channelNum);
		getdoubledt();
#endif

#if 1
		pthread_mutex_unlock(&writeFileMutex); 
		nCountCheck1hCh3++;


#ifdef FEATURE_SQLITE3

		
		if(SQLITE_OK != SQLITE_InsertDataTable(gParamCh3,TABLE_NAME_CHANNEL3_REALTIME_DATA, gParamCountCH3, _time ))
			printf("Realtime Data Insert Error\n");
		//No BaseLine Data in DB Table
		if(bHasBaselineCh3 == FALSE)
		{
			if(nInitDataCountCh3 < nBaselineCount)//6*60*60
			{
				memset(gQueryCH3, 0, sizeof(gQueryCH3));
			
				if(SQLITE_OK == SQLITE_SetInitParamFFT(gParamCh3,TABLE_NAME_CHANNEL3_INIT_DATA, gParamCountCH3, gQueryCH3 ))
					nInitDataCountCh3++;
		
				//If the init data is 6*60*60(6Hour), calculate the Baseline and insert Baseline table.
				if(nInitDataCountCh3 == nBaselineCount)//6*60*60
				{
				printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");	

				for(i=0;i<gParamCountCH3;i++)
				{
					SQLITE_GenBaselineParamFromInitParam(gColumnNameArray[requiredParamCH3[i].paramType], TABLE_NAME_CHANNEL3_INIT_DATA, &gMeanBaselineParamCh3[i], &gStdevBaselineParamCh3[i]);
				}

					if(SQLITE_SetBaselineParamFFT(gMeanBaselineParamCh3, gStdevBaselineParamCh3, TABLE_NAME_CHANNEL3_BASELINE, gParamCountCH3) != SQLITE_OK)
					{
						//Insert ERROR
						printf("SetBaselineParamCh3 Error\n");
					}
					else
						bHasBaselineCh3 = TRUE; 					
					//Get init datas from the CHANNEL1_INIT_DATA_TB table.				
					}
				}
			}
		else//Add params to calculate the Mean value
		{
			if(nCompDataCountCh3 < DATA_COUNT_PREMAINT)
			{
				for(i=0;i<gParamCountCH3;i++)
				{
					gParamCompCh3[nCompDataCountCh3][i] = gParamCh3[i];
				}
				nCompDataCountCh3++;
				printf("CompDataCount: %d\n", nCompDataCountCh3);
			}
			
			if(nCompDataCountCh3 == DATA_COUNT_PREMAINT)
			{
				int index = ACC_OVERALL;

				for(i=0;i<gParamCountCH3;i++)
				{
					//gParamCompMeanCh3[i] = getMean(DATA_COUNT_PREMAINT, requiredParamCH3[i].paramType, gParamCompCh3);
					gParamCompMeanCh3[i] = getMeanSortedCh3(DATA_COUNT_PREMAINT, CHECK_DATA_COUNT, i, gParamCompCh3);
					printf("CH3 Type:%d		%lf\n",requiredParamCH3[i].paramType, gParamCompMeanCh3[i]);
				}				
			
				if(SQLITE_OK != SQLITE_InsertDataTable(gParamCompMeanCh3,TABLE_NAME_CHANNEL3_PRE_MAINT_DATA, gParamCountCH3, _time ))
					printf("Realtime Data Insert Error\n");
				
				checkAlertWithBaselineFFTper5Min(gMeanBaselineParamCh3, gParamCompMeanCh3, gParamCompDevPer5MinCh3, gParamCountCH3);
			}
		}

		
		if(bHasBaselineCh3 == TRUE)
			alertStatus= checkDataWithBaselineFFTperSec(gMeanBaselineParamCh3, gStdevBaselineParamCh3, gParamCh3, gParamCountCH3,alarmContionCH3);
		
		if(alertStatus == ALERT1 || alertStatus == ALERT2)
		{
			memset(LogPath, 0 , sizeof(LogPath));
			sprintf(LogPath, "%s/Alarm/Alarm_%04d_%02d_%02d_%02d_%02d_CH3.CSV", A_DATA_PATH, _time.year, _time.month, _time.day
			, _time.hour, _time.min);
			alarmFFTData = fopen(LogPath, "at+");

			for(i = 0; i <= _sampleCnt/2; i++)
			{
				acc_value = (double)(sqrt(pow(alarmFFTDataBuf[i][0],2) + pow(alarmFFTDataBuf[i][1],2))/sqrt(10240))/9.8;
				fprintf(alarmFFTData, "%d, %lf\n", i, acc_value);
			}
			fclose(alarmFFTData);
			
			printf("XXXXXXXXXXXXXXXXXXXXXXXXXXX   EMERGENCY XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
			printf("XXXXXXXXXXXXXXXXXXXXXXXXXXX   EMERGENCY XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
			printf("XXXXXXXXXXXXXXXXXXXXXXXXXXX   EMERGENCY XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
		}
		//1H 3600 == (60*60)
		if(nCountCheck1hCh3%(60*60) == 0)
		{
			memset(LogPath, 0 , sizeof(LogPath));
			sprintf(LogPath, "%s/Periodic/Periodic1H_%04d_%02d_%02d_%02d_%02d_CH3.CSV", A_DATA_PATH, _time.year, _time.month, _time.day
			, _time.hour, _time.min);
			printf("Periodic:%s\n", LogPath);
			per1hFFTData = fopen(LogPath, "at+");
			
			for(i = 0; i <= _sampleCnt/2; i++)
			{
				acc_value = (double)(sqrt(pow(alarmFFTDataBuf[i][0],2) + pow(alarmFFTDataBuf[i][1],2))/sqrt(10240))/9.8;
				fprintf(per1hFFTData, "%d, %lf\n", i, acc_value);
			}
			fclose(per1hFFTData);
		}
		free(alarmFFTDataBuf);
#endif/*FEATURE_SQLITE3*/	
#ifdef DEBUG_FFT_ 
		getdoubledt();
#endif
#ifdef FEATURE_SERIAL		
		memset(pSerialDataCh3, 0, sizeof(pSerialDataCh3));
		sprintf(pSerialDataCh3, "CH%d, %04d/%02d/%02d %02d:%02d:%02d,%06ld,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,0,%d\r\n",\
			channelNum,_time.year, _time.month, _time.day, _time.hour, _time.min, _time.sec, _time.usec, \
gParamCh3[INDEX_COLUMN_1], gParamCh3[INDEX_COLUMN_2], gParamCh3[INDEX_COLUMN_3], gParamCh3[INDEX_COLUMN_4], gParamCh3[INDEX_COLUMN_5],\
gParamCh3[INDEX_COLUMN_6], gParamCh3[INDEX_COLUMN_7], gParamCh3[INDEX_COLUMN_8], gParamCh3[INDEX_COLUMN_9], gParamCh3[INDEX_COLUMN_10],\
gParamCh3[INDEX_COLUMN_11], gParamCh3[INDEX_COLUMN_12], gParamCh3[INDEX_COLUMN_13], gParamCh3[INDEX_COLUMN_14], gParamCh3[INDEX_COLUMN_15],\
gParamCompDevPer5MinCh3[INDEX_COLUMN_1], gParamCompDevPer5MinCh3[INDEX_COLUMN_2], gParamCompDevPer5MinCh3[INDEX_COLUMN_3],gParamCompDevPer5MinCh3[INDEX_COLUMN_4], gParamCompDevPer5MinCh3[INDEX_COLUMN_5], \
gParamCompDevPer5MinCh3[INDEX_COLUMN_6], gParamCompDevPer5MinCh3[INDEX_COLUMN_7], gParamCompDevPer5MinCh3[INDEX_COLUMN_8],gParamCompDevPer5MinCh3[INDEX_COLUMN_9], gParamCompDevPer5MinCh3[INDEX_COLUMN_10], \
gParamCompDevPer5MinCh3[INDEX_COLUMN_11], gParamCompDevPer5MinCh3[INDEX_COLUMN_12], gParamCompDevPer5MinCh3[INDEX_COLUMN_13],gParamCompDevPer5MinCh3[INDEX_COLUMN_14], gParamCompDevPer5MinCh1[INDEX_COLUMN_15], \
			alertStatus);		
		
		fwrite(pSerialDataCh3, strlen(pSerialDataCh3)+2, 1, fpLogCh3);

		if(nCompDataCountCh3 == DATA_COUNT_PREMAINT)
		{
			memset(LogPath, 0 , sizeof(LogPath));

			sprintf(LogPath, "%s/PreMain/PreMain_%04d_%02d_%02d_%02d_%02d_%02d_CH3.CSV", A_DATA_PATH, _time.year, _time.month, _time.day
			, _time.hour, _time.min, _time.sec);
			printf("PREMAIN:%s\n", LogPath);			
			fpPreMainPer5minCh3 = fopen(LogPath, "wt+");
			
			fwrite(pSerialDataCh3, strlen(pSerialDataCh3)+2, 1, fpPreMainPer5minCh3);
			fclose(fpPreMainPer5minCh3);
			nCompDataCountCh3 = 0;
		}

		bFlushSerialCh3 = 1;
#endif/*FEATURE_SERIAL*/


		//if(ch1FlushCount % 10 == 0)
		//	fflush(fpLogCh1);
		ch3FlushCount++;
//		fprintf(fpLogCh1, "CH%d  Accellerate Peak : %lf\n", channelNum, gParamCh1.acc_peak);		
		if(readyToDBClosing & FLAG_AD1)
			readyToDBClosing |= FLAG_AD3;



#endif
#endif
//fclose(fpLogCh3);
#ifdef FEATURE_MQTT
	genPublishDataAD3();
#endif
}




int convert_N_ExtractionParamsChannel4(int _sampleCnt, unsigned char* _pDataBuf, int channelNum, FILE* _fp, CTime _time)
{
		int i,j;
		int N;
		int flag= 0;
		double value = 0;
		double acc_peak = 0;
		double ifft_acc_min = 0xFFFFFFFF;
		double ifft_acc_max = 0;		
		
		double ifft_vel_min = 0xFFFFFFFF;
		double ifft_vel_max = 0;		
		
		double acc_value = 0;
		int isAlarmed = 0;
		unsigned int AlarmValue = 0;
		int alertStatus = 0;

		struct timeval val;
		struct tm *ptm;
		unsigned char* A_DATA_PATH = NULL;
		unsigned char LogPath[256];
		char *szErrMsg = NULL;
		int rc;
		sqlite3_stmt *res;
		double paramCompDevPerSecCh4[COLUMN_MAX]={0.0,};
		FILE * alarmFFTData = NULL;
		FILE * per1hFFTData = NULL;
		
		fftw_complex *alarmFFTDataBuf = NULL;

		N = _sampleCnt;
#if DEBUG_FFT_
		printf("# N = %d\n",_sampleCnt);
#endif
		A_DATA_PATH = getenv("A_PATH");

		if(ch4LogMinValue == -1 || ch4LogMinValue != _time.hour)
		{
			if(ch4LogMinValue != -1)
				fclose(fpLogCh4);
			memset(LogPath, 0 , sizeof(LogPath));
			sprintf(LogPath, "%s/Log/Log_%04d_%02d_%02d_%02d_CH4.CSV", A_DATA_PATH, _time.year, _time.month, _time.day
			, _time.hour);
			ch4LogMinValue = _time.hour;
			fpLogCh4 = fopen(LogPath, "ab+");
		}


#if DEBUG_FFT_
		printf("# FFT CH:%d Start Time : ",channelNum );
		getdoubledt();
#endif
		
		acc_peak = convertRawToTimeChannel4(_sampleCnt, _pDataBuf, _fp);
		if(gpCH4FFT_Input == NULL)
		{
			gpCH4FFT_Input = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
			gpCH4FFT_Output = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
			gpCH4InvFFT_Input = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
			gpCH4InvFFT_Output = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
		}
		
		alarmFFTDataBuf = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
		memset(gpCH4FFT_Input, 0, sizeof(fftw_complex) * N);
		memset(gpCH4FFT_Output, 0, sizeof(fftw_complex) * N);
		memset(gpCH4InvFFT_Input, 0, sizeof(fftw_complex) * N);
		memset(gpCH4InvFFT_Output, 0, sizeof(fftw_complex) * N);
		memset(alarmFFTDataBuf, 0, sizeof(fftw_complex) * N);

		
		for(i=0;i<N;i++){
				gpCH4FFT_Input[i][0] =(float)convertDataBuf4[i];
				//printf("ori, %f\n", (float)convertDataBuf4[i]);
		}

		if(gCH4Plan == NULL)
			gCH4Plan = fftw_plan_dft_1d(N, gpCH4FFT_Input, gpCH4FFT_Output, FFTW_FORWARD, FFTW_ESTIMATE);
		
		fftw_execute(gCH4Plan); 

#ifdef DEBUG_FFT_
		getdoubledt();
#endif

#if 1
		memcpy(alarmFFTDataBuf, gpCH4FFT_Output, sizeof(fftw_complex) * N);
		getParamMICFFTData(gParamCh4,_sampleCnt,gpCH4FFT_Output, requiredParamCH4, gParamCountCH4);
#if DEBUG_FFT_
		printf("# FFT CH:%d End Time : ", channelNum);
		getdoubledt();
#endif
		
#ifdef DEBUG_FFT_		
		printf("%s\n",	_fileName);
#endif
		pthread_mutex_lock(&writeFileMutex); 

		//fprintf(fpLogCh4, "CH%d, %04d/%02d/%02d %02d:%02d:%02d,%06ld ", channelNum,ptm->tm_year + 1900, ptm->tm_mon+1, ptm->tm_mday, 
		//	ptm->tm_hour, ptm->tm_min,ptm->tm_sec, val.tv_usec);
	//	printf("CH%d  Velocity V4 Max:%lf Min: %lf\n",channelNum, ifft_vel_max, ifft_vel_min);
#if DEBUG_FFT_
		printf("# IFFT CH:%d End Time : ", channelNum);
		getdoubledt();
#endif

#if 1

		pthread_mutex_unlock(&writeFileMutex); 
		nCountCheck1hCh4++;


#ifdef FEATURE_SQLITE3
		if(SQLITE_OK != SQLITE_InsertDataTable(gParamCh4,TABLE_NAME_CHANNEL4_REALTIME_DATA, gParamCountCH4, _time ))
			printf("Realtime Data Insert Error\n");

		
		//No BaseLine Data in DB Table
		if(bHasBaselineCh4 == FALSE)
		{
			if(nInitDataCountCh4 < nBaselineCount)//6*60*60
			{
			
				memset(gQueryCH4, 0, sizeof(gQueryCH4));
				if(SQLITE_OK == SQLITE_SetInitParamFFT(gParamCh4,TABLE_NAME_CHANNEL4_INIT_DATA, gParamCountCH4, gQueryCH4 ))
					nInitDataCountCh4++;
		
				//If the init data is 6*60*60(6Hour), calculate the Baseline and insert Baseline table.
				if(nInitDataCountCh4 == nBaselineCount)//6*60*60
				{
				printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");	

				for(i=0;i<gParamCountCH4;i++)
				{
					SQLITE_GenBaselineParamFromInitParam(gColumnNameArrayMIC[requiredParamCH4[i].paramType], TABLE_NAME_CHANNEL4_INIT_DATA, &gMeanBaselineParamCh4[i], &gStdevBaselineParamCh4[i]);
				}

					if(SQLITE_SetBaselineParamFFT(gMeanBaselineParamCh4, gStdevBaselineParamCh4, TABLE_NAME_CHANNEL4_BASELINE, gParamCountCH4) != SQLITE_OK)
					{
						//Insert ERROR
						printf("SetBaselineParamCh4 Error\n");
					}
					else
						bHasBaselineCh4 = TRUE; 					
					//Get init datas from the CHANNEL4_INIT_DATA_TB table.				
					}
				}
			}
		else//Add params to calculate the Mean value
		{
			if(nCompDataCountCh4 < DATA_COUNT_PREMAINT)
			{
				for(i=0;i<gParamCountCH4;i++)
				{
					gParamCompCh4[nCompDataCountCh4][i] = gParamCh4[i];
				}
				nCompDataCountCh4++;
				printf("CompDataCount: %d\n", nCompDataCountCh4);
			}
			
			if(nCompDataCountCh4 == DATA_COUNT_PREMAINT)
			{
				int index = ACC_OVERALL;

				for(i=0;i<gParamCountCH4;i++)
				{
					//gParamCompMeanCh4[i] = getMean(DATA_COUNT_PREMAINT, requiredParamCH4[i].paramType, gParamCompCh4);
					gParamCompMeanCh4[i] = getMeanSortedMIC(DATA_COUNT_PREMAINT, CHECK_DATA_COUNT, i, gParamCompCh4);
					printf("CH4 Type:%d		%lf\n",requiredParamCH4[i].paramType, gParamCompMeanCh4[i]);					
				}				
				
				if(SQLITE_OK != SQLITE_InsertDataTable(gParamCompMeanCh4,TABLE_NAME_CHANNEL4_PRE_MAINT_DATA, gParamCountCH4, _time ))
					printf("Realtime Data Insert Error\n");
				
				checkAlertWithBaselineFFTper5Min(gMeanBaselineParamCh4, gParamCompMeanCh4, gParamCompDevPer5MinCh4, gParamCountCH4);
			}
		}
		

		if(bHasBaselineCh4 == TRUE)
			alertStatus= checkDataWithBaselineFFTperSec(gMeanBaselineParamCh4, gStdevBaselineParamCh4, gParamCh4,  gParamCountCH4, alarmContionCH4);
		
		if(alertStatus == ALERT1 || alertStatus == ALERT2)
		{
			memset(LogPath, 0 , sizeof(LogPath));
			sprintf(LogPath, "%s/Alarm/Alarm_%04d_%02d_%02d_%02d_%02d_CH4.CSV", A_DATA_PATH,_time.year, _time.month, _time.day
			, _time.hour, _time.min);
			alarmFFTData = fopen(LogPath, "at+");

			for(i = 0; i <= _sampleCnt/2; i++)
			{
				acc_value = (double)(sqrt(pow(alarmFFTDataBuf[i][0],2) + pow(alarmFFTDataBuf[i][1],2))/sqrt(10240))/9.8;
				fprintf(alarmFFTData, "%d, %lf\n", i, acc_value);
			}
			fclose(alarmFFTData);
			
			printf("XXXXXXXXXXXXXXXXXXXXXXXXXXX   EMERGENCY XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
			printf("XXXXXXXXXXXXXXXXXXXXXXXXXXX   EMERGENCY XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
			printf("XXXXXXXXXXXXXXXXXXXXXXXXXXX   EMERGENCY XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
		}
		//1H 3600 == (60*60)
		if(nCountCheck1hCh4%(60*60) == 0)
		{
			memset(LogPath, 0 , sizeof(LogPath));
			sprintf(LogPath, "%s/Periodic/Periodic1H_%04d_%02d_%02d_%02d_%02d_CH4.CSV", A_DATA_PATH, _time.year, _time.month, _time.day
			, _time.hour, _time.min);
			printf("Periodic:%s\n", LogPath);
			per1hFFTData = fopen(LogPath, "at+");
			
			for(i = 0; i <= _sampleCnt/2; i++)
			{
				acc_value = (double)(sqrt(pow(alarmFFTDataBuf[i][0],2) + pow(alarmFFTDataBuf[i][1],2))/sqrt(10240))/9.8;
				fprintf(per1hFFTData, "%d, %lf\n", i, acc_value);
			}
			fclose(per1hFFTData);
		}
		free(alarmFFTDataBuf);
#endif/*FEATURE_SQLITE3*/	
#ifdef DEBUG_FFT_ 
		getdoubledt();
#endif
#ifdef FEATURE_SERIAL		
		memset(pSerialDataCh4, 0, sizeof(pSerialDataCh4));
		sprintf(pSerialDataCh4, "CH%d, %04d/%02d/%02d %02d:%02d:%02d,%06ld,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,0,%d\r\n",\
			channelNum,_time.year, _time.month, _time.day, _time.hour, _time.min, _time.sec, _time.usec, \
gParamCh4[INDEX_COLUMN_1], gParamCh4[INDEX_COLUMN_2], gParamCh4[INDEX_COLUMN_3], gParamCh4[INDEX_COLUMN_4], gParamCh4[INDEX_COLUMN_5],\
gParamCh4[INDEX_COLUMN_6], gParamCh4[INDEX_COLUMN_7], gParamCh4[INDEX_COLUMN_8], gParamCh4[INDEX_COLUMN_9], gParamCh4[INDEX_COLUMN_10],\
gParamCh4[INDEX_COLUMN_11], gParamCh4[INDEX_COLUMN_12], gParamCh4[INDEX_COLUMN_13], gParamCh4[INDEX_COLUMN_14], gParamCh4[INDEX_COLUMN_15],\
gParamCompDevPer5MinCh4[INDEX_COLUMN_1], gParamCompDevPer5MinCh4[INDEX_COLUMN_2], gParamCompDevPer5MinCh4[INDEX_COLUMN_3],gParamCompDevPer5MinCh4[INDEX_COLUMN_4], gParamCompDevPer5MinCh4[INDEX_COLUMN_5], \
gParamCompDevPer5MinCh4[INDEX_COLUMN_6], gParamCompDevPer5MinCh4[INDEX_COLUMN_7], gParamCompDevPer5MinCh4[INDEX_COLUMN_8],gParamCompDevPer5MinCh4[INDEX_COLUMN_9], gParamCompDevPer5MinCh4[INDEX_COLUMN_10], \
gParamCompDevPer5MinCh4[INDEX_COLUMN_11], gParamCompDevPer5MinCh4[INDEX_COLUMN_12], gParamCompDevPer5MinCh4[INDEX_COLUMN_13],gParamCompDevPer5MinCh4[INDEX_COLUMN_14], gParamCompDevPer5MinCh4[INDEX_COLUMN_15], \
			alertStatus);		
		
		fwrite(pSerialDataCh4, strlen(pSerialDataCh4)+2, 1, fpLogCh4);

		if(nCompDataCountCh4 == DATA_COUNT_PREMAINT)
		{
			memset(LogPath, 0 , sizeof(LogPath));

			sprintf(LogPath, "%s/PreMain/PreMain_%04d_%02d_%02d_%02d_%02d_%02d_CH4.CSV", A_DATA_PATH,_time.year, _time.month, _time.day, _time.hour, _time.min, _time.sec);
			printf("PREMAIN:%s\n", LogPath);			
			fpPreMainPer5minCh4 = fopen(LogPath, "wt+");
			
			fwrite(pSerialDataCh4, strlen(pSerialDataCh4)+2, 1, fpPreMainPer5minCh4);
			fclose(fpPreMainPer5minCh4);
			nCompDataCountCh4 = 0;
		}

		bFlushSerialCh4 = 1;
#endif/*FEATURE_SERIAL*/


		//if(ch1FlushCount % 10 == 0)
		//	fflush(fpLogCh1);
		ch4FlushCount++;
//		fprintf(fpLogCh1, "CH%d  Accellerate Peak : %lf\n", channelNum, gParamCh1.acc_peak);		
		if(readyToDBClosing & FLAG_AD1)
			readyToDBClosing |= FLAG_AD4;



#endif
#endif
//fclose(fpLogCh1);
#ifdef FEATURE_MQTT
	genPublishDataAD4();
#endif
}




int saveData(int fd, FILE* fp) 
{
  int i, j, ret; 
   
#ifdef DEBUG	
  printf(" Recodig Start to %s. \n", filename); 
#endif 
 
//  request_ready(fd); 

/*  gADMode = getADMode(fd); 
  
  if ( gADMode == 2 )
       setADMode(fd, gADMode);   */
       
  sampleNumber =  requestCnt(fd); 

#ifdef SAMPLECNT_DEBUG
  	printf(" SampleCnt : %d\n", sampleNumber); 
#endif 

//jonsama end

  if ( sampleNumber > 0 && sampleNumber <= MAX_SAMPLE )
  { 
 	if ( sampleNumber == ( MAX_SAMPLE -1 ) ) 
 		printf(" %d 777 OVERRUN \n", fd); 
 		
  	requestData_Start(fd, sampleNumber); 
    
  	for ( i = 0 ; i < 1 ; i++) // 512 sample(2048byte) * 400 = 204800 sample/s/ch = 51.2kS/s 
  	{
  	  ret = requestData_Read(fd, fp, printOption);
    
  	  if( ret < 0 )
  	  {
  //  		requestData_End(fd); 	
  //  		break; 
  	  }	
  	}
  	
  	requestData_End(fd); 
  } 
  else
  {
	if ( sampleNumber == 0 )
		printf("No Data\n"); 
		
  	//goto sample_count_error;
  }
  
#ifdef DEBUG	  
  printf(" Recoding Fininshed.\n"); 
#endif 
//  fclose(fp);   
  //jonsama
  //printf(" Recording Finished : %d\n",ret ); 
  
  return ret; 
//jonsama
sample_count_error:
 // printf(" sample count error\n"); 
  return 1;
}


void *AD1_fft_execute(void* arg)
{
	Element* element = NULL;
	int ret = 0;
	int n = 0;
	int length = 0;
	int queueLength = 0;
	int disableDataIndex = 0;
	int sleepDuration = 0;
	FILE* fp1 = NULL;
	//char* buffer = NULL;
	unsigned char* buffer=NULL;
	unsigned char* pBuf = NULL;

	unsigned char* A_DATA_PATH=NULL;
	unsigned char fileSavingPath2[256];

	
	while(1)
	{
		usleep(2000);
		queueLength = getLength(gLinkedQueueAD1);
		if(queueLength > 0 && !(readyToDBClosing & FLAG_AD1))
		{
#ifdef DEBUG_FFT_			
		printf("CH1 queueLength : %d\n", queueLength);
#endif
			pBuf = (char*)malloc(10240*2);

			element = dequeue(gLinkedQueueAD1);
				
			memcpy(pBuf, element->data, 10240*2);
				

#ifdef	FEATURE_RAWDATA
			A_DATA_PATH = getenv("A_PATH");
			memset(fileSavingPath2, 0, sizeof(fileSavingPath2));
			sprintf(fileSavingPath2, "%s/%s", A_DATA_PATH,element->fileName);
			fp1 = fopen(fileSavingPath2, "ab+");
#endif

			convert_N_ExtractionParamsChannel1(10240, pBuf, 1, fp1, element->time);
			queueLength = getLength(gLinkedQueueAD1);
			free(pBuf);
			
			free(element->fileName);
			free(element->data);
			free(element);
#if DEBUG_FFT
	if(queueLength > 10)
			printf("CH1 left Queue Length : %d\n", queueLength);
#endif
		}
	}
}

void *AD2_fft_execute(void* arg)
{
	Element* element = NULL;
	int ret = 0;
	int n = 0;
	int length = 0;
	int queueLength = 0;
	int disableDataIndex = 0;
	int sleepDuration = 0;
	FILE* fp1 = NULL;
	//char* buffer = NULL;
	unsigned char* buffer=NULL;
	unsigned char* pBuf = NULL;

	unsigned char* A_DATA_PATH=NULL;
	unsigned char fileSavingPath2[256];

	
	while(1)
	{
		usleep(2000);
		queueLength = getLength(gLinkedQueueAD2);
		if(queueLength > 0 && !(readyToDBClosing & FLAG_AD2))
		{
#ifdef DEBUG_FFT_
			printf("CH2queueLength : %d\n", queueLength);
#endif
		
			pBuf = (char*)malloc(10240*2);
			element = dequeue(gLinkedQueueAD2);
			
			memcpy(pBuf, element->data, 10240*2);
			
#ifdef	FEATURE_RAWDATA
			A_DATA_PATH = getenv("A_PATH");
			memset(fileSavingPath2, 0, sizeof(fileSavingPath2));

			sprintf(fileSavingPath2, "%s/%s", A_DATA_PATH,element->fileName);
			fp1 = fopen(fileSavingPath2, "ab+");
#endif
			convert_N_ExtractionParamsChannel2(10240, pBuf, 2, fp1,element->time);
			queueLength = getLength(gLinkedQueueAD2);

			free(pBuf);
			free(element->fileName);
			free(element->data);
			free(element);

			
#if DEBUG_FFT		
			if(queueLength > 10)
				printf("CH2 left Queue Length : %d\n", queueLength);
#endif
			
		}
	}
}

void *AD3_fft_execute(void* arg)
{
	Element* element = NULL;
	int ret = 0;
	int n = 0;
	int length = 0;
	int queueLength = 0;
	int disableDataIndex = 0;
	int sleepDuration = 0;
	FILE* fp1 = NULL;
	//char* buffer = NULL;
	unsigned char* buffer=NULL;
	unsigned char* pBuf = NULL;
	unsigned char* A_DATA_PATH=NULL;
	unsigned char fileSavingPath2[256];

	
	while(1)
	{
		usleep(2000);
		queueLength = getLength(gLinkedQueueAD3);
		if(queueLength > 0 && !(readyToDBClosing & FLAG_AD3))
		{
#ifdef DEBUG_FFT_			
		printf("CH3queueLength : %d\n", queueLength);
#endif
			pBuf = (char*)malloc(10240*2);
			element = dequeue(gLinkedQueueAD3);
			
			memcpy(pBuf, element->data, 10240*2);
#ifdef	FEATURE_RAWDATA
			A_DATA_PATH = getenv("A_PATH");
			memset(fileSavingPath2, 0, sizeof(fileSavingPath2));
			sprintf(fileSavingPath2, "%s/%s", A_DATA_PATH,element->fileName);
			fp1 = fopen(fileSavingPath2, "ab+");
#endif
			convert_N_ExtractionParamsChannel3(10240, pBuf, 3, fp1, element->time);
			queueLength = getLength(gLinkedQueueAD3);

			free(pBuf);
			free(element->fileName);
			free(element->data);
			free(element);

#if DEBUG_FFT
			if(queueLength > 10)
				printf("CH3 left Queue Length : %d\n", queueLength);
#endif
			
		}
	}
}

void *AD4_fft_execute(void* arg)
{
	Element* element = NULL;
	int ret = 0;
	int n = 0;
	int length = 0;
	int queueLength = 0;
	int disableDataIndex = 0;
	int sleepDuration = 0;
	FILE* fp1 = NULL;
	//char* buffer = NULL;
	unsigned char* buffer=NULL;
	unsigned char* pBuf = NULL;
	unsigned char* A_DATA_PATH=NULL;
	unsigned char fileSavingPath2[256];
	
	while(1)
	{
		usleep(2000);
		queueLength = getLength(gLinkedQueueAD4);
		if(queueLength > 0 && !(readyToDBClosing & FLAG_AD4))
		{
#ifdef DEBUG_FFT_
			printf("CH4queueLength : %d\n", queueLength);
#endif
		
			pBuf = (char*)malloc(10240*2);
			element = dequeue(gLinkedQueueAD4);
			
			memcpy(pBuf, element->data, 10240*2);
#ifdef	FEATURE_RAWDATA
			A_DATA_PATH = getenv("A_PATH");
			memset(fileSavingPath2, 0, sizeof(fileSavingPath2));
			sprintf(fileSavingPath2, "%s/%s", A_DATA_PATH,element->fileName);
			fp1 = fopen(fileSavingPath2, "ab+");
#endif
			convert_N_ExtractionParamsChannel4(10240, pBuf, 4, fp1, element->time);
			queueLength = getLength(gLinkedQueueAD4);
			
			free(pBuf);
			free(element->fileName);
			free(element->data);
			free(element);
#if DEBUG_FFT
			if(queueLength > 10)
				printf("CH4 left Queue Length : %d\n", queueLength);
#endif
			
		}
	}
}


#ifdef FEATURE_SERIAL
void *SerialSendThread(void* arg)
{

	while(1) {
		usleep(300);
		
		if(bFlushSerialCh1 == 1 && pSerialDataCh1 != NULL)
		{
			write(fdSerial, pSerialDataCh1, strlen(pSerialDataCh1)+2); 	
			bFlushSerialCh1 = 0;
		}
		
		if(bFlushSerialCh2 == 1 && pSerialDataCh2 != NULL)
		{
			write(fdSerial, pSerialDataCh2, strlen(pSerialDataCh2)+2); 			
			bFlushSerialCh2 = 0;
		}
		
		if(bFlushSerialCh3 == 1 && pSerialDataCh3 != NULL)
		{
			write(fdSerial, pSerialDataCh3, strlen(pSerialDataCh3)+2); 			
			bFlushSerialCh3 = 0;
		}
		
		if(bFlushSerialCh4 == 1 && pSerialDataCh4 != NULL)
		{
			write(fdSerial, pSerialDataCh4, strlen(pSerialDataCh4)+2); 			
			bFlushSerialCh4 = 0;
		}
	  }

}
#endif/*FEATURE_SERIAL*/

#ifdef FEATURE_MQTT
int AD1_Publish_thread(void* arg)
{
	int queueLength = 0;
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;
	Element* element = NULL;
	unsigned char* buffer=NULL;
	unsigned char* pBuf = NULL;
	int rc = MQTTCLIENT_SUCCESS;
	int qCount = 0;

	while(1)
	{
		int rePublishCount = 0;
		
		if(connectMqttServer(0) == MQTTCLIENT_SUCCESS)
		{
			while(1)
			{	
				qCount  = getLength(gAD1WillSendQ);

				if( qCount> 0)
				{
					
					if( rc == MQTTCLIENT_SUCCESS)
					{
						element = dequeue(gAD1WillSendQ);
							
						//pBuf = (char*)malloc(51200*2);
						//memcpy(pBuf, element->data, strlen(element->data));
							
						queueLength = getLength(gAD1WillSendQ);
						//free(pBuf);

						//printf("Result : %s\n", element->data);
						pubmsg.payload = element->data;
						pubmsg.payloadlen = strlen(element->data);
						pubmsg.qos = QOS;
						pubmsg.retained = 0;
					}
					else
					{
						if(rePublishCount > 10)
						{
							printf("ReConnect Server\n");
							rePublishCount = 10;
							break;
						}
					}					
					
					MQTTClient_publishMessage(bleNode[0].client, bleNode[0].topic, &pubmsg, &token);
				
				/*
					printf("Waiting for up to %d seconds for publication of %s\n"
					  "on topic %s for client with ClientID: %s\n",
						(int)(TIMEOUT/1000), PAYLOAD, TOPIC, CLIENTID);
				*/
				
					rc = MQTTClient_waitForCompletion(bleNode[0].client, token, TIMEOUT);
				
					if(rc != MQTTCLIENT_SUCCESS)
					{
						printf("Failed Publish\n");					
						usleep(1000000);
						rePublishCount++;
						
						continue;
					}
					
					free(element->data);
					free(element);
					printf("CH1 Message with delivery token %d delivered\n", token);
				}
				else
				{
					printf("AD1 No Data in the Send Q Length : %d\n", qCount);
					usleep(500000);
				}
			}
		}
	}
}


int AD2_Publish_thread(void* arg)
{
	int queueLength = 0;
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;
	Element* element = NULL;
	unsigned char* buffer=NULL;
	unsigned char* pBuf = NULL;
	int rc = MQTTCLIENT_SUCCESS;
	int qCount = 0;

	while(1)
	{
		int rePublishCount = 0;
		
		if(connectMqttServer(1) == MQTTCLIENT_SUCCESS)
		{
			while(1)
			{	
				qCount	= getLength(gAD2WillSendQ);

				if( qCount> 0)
				{
					
					if( rc == MQTTCLIENT_SUCCESS)
					{
						element = dequeue(gAD2WillSendQ);
							
						//pBuf = (char*)malloc(51200*2);
						//memcpy(pBuf, element->data, strlen(element->data));
							
						queueLength = getLength(gAD2WillSendQ);
						//free(pBuf);

						//printf("Result : %s\n", element->data);
						pubmsg.payload = element->data;
						pubmsg.payloadlen = strlen(element->data);
						pubmsg.qos = QOS;
						pubmsg.retained = 0;
					}
					else
					{
						if(rePublishCount > 10)
						{
							printf("ReConnect Server\n");
							rePublishCount = 10;
							break;
						}
					}					
					
					MQTTClient_publishMessage(bleNode[1].client, bleNode[1].topic, &pubmsg, &token);
				
				/*
					printf("Waiting for up to %d seconds for publication of %s\n"
					  "on topic %s for client with ClientID: %s\n",
						(int)(TIMEOUT/1000), PAYLOAD, TOPIC, CLIENTID);
				*/
				
					rc = MQTTClient_waitForCompletion(bleNode[1].client, token, TIMEOUT);
				
					if(rc != MQTTCLIENT_SUCCESS)
					{
						printf("Failed Publish\n"); 				
						usleep(1000000);
						rePublishCount++;
						
						continue;
					}
					
					free(element->data);
					free(element);
					printf("CH2 Message with delivery token %d delivered\n", token);
				}
				else
				{
					printf("AD2 No Data in the Send Q Length : %d\n", qCount);
					usleep(500000);
				}
			}
		}
	}
}


int AD3_Publish_thread(void* arg)
{
	int queueLength = 0;
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;
	Element* element = NULL;
	unsigned char* buffer=NULL;
	unsigned char* pBuf = NULL;
	int rc = MQTTCLIENT_SUCCESS;
	int qCount = 0;

	while(1)
	{
		int rePublishCount = 0;
		
		if(connectMqttServer(2) == MQTTCLIENT_SUCCESS)
		{
			while(1)
			{	
				qCount	= getLength(gAD3WillSendQ);

				if( qCount> 0)
				{
					
					if( rc == MQTTCLIENT_SUCCESS)
					{
						element = dequeue(gAD3WillSendQ);
							
						//pBuf = (char*)malloc(51200*2);
						//memcpy(pBuf, element->data, strlen(element->data));
							
						queueLength = getLength(gAD3WillSendQ);
						//free(pBuf);

						//printf("Result : %s\n", element->data);
						pubmsg.payload = element->data;
						pubmsg.payloadlen = strlen(element->data);
						pubmsg.qos = QOS;
						pubmsg.retained = 0;
					}
					else
					{
						if(rePublishCount > 10)
						{
							printf("ReConnect Server\n");
							rePublishCount = 10;
							break;
						}
					}					
					
					MQTTClient_publishMessage(bleNode[2].client, bleNode[2].topic, &pubmsg, &token);
				
				/*
					printf("Waiting for up to %d seconds for publication of %s\n"
					  "on topic %s for client with ClientID: %s\n",
						(int)(TIMEOUT/1000), PAYLOAD, TOPIC, CLIENTID);
				*/
				
					rc = MQTTClient_waitForCompletion(bleNode[2].client, token, TIMEOUT);
				
					if(rc != MQTTCLIENT_SUCCESS)
					{
						printf("Failed Publish\n"); 				
						usleep(1000000);
						rePublishCount++;
						
						continue;
					}
					
					free(element->data);
					free(element);
					printf("CH3 Message with delivery token %d delivered\n", token);
				}
				else
				{
					printf("AD3 No Data in the Send Q Length : %d\n", qCount);
					usleep(500000);
				}
			}
		}
	}
}

int AD4_Publish_thread(void* arg)
{
	int queueLength = 0;
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;
	Element* element = NULL;
	unsigned char* buffer=NULL;
	unsigned char* pBuf = NULL;
	int rc = MQTTCLIENT_SUCCESS;
	int qCount = 0;

	while(1)
	{
		int rePublishCount = 0;
		
		if(connectMqttServer(3) == MQTTCLIENT_SUCCESS)
		{
			while(1)
			{	
				qCount	= getLength(gAD4WillSendQ);

				if( qCount> 0)
				{
					
					if( rc == MQTTCLIENT_SUCCESS)
					{
						element = dequeue(gAD4WillSendQ);
							
						//pBuf = (char*)malloc(51200*2);
						//memcpy(pBuf, element->data, strlen(element->data));
							
						queueLength = getLength(gAD4WillSendQ);
						//free(pBuf);

						//printf("Result : %s\n", element->data);
						pubmsg.payload = element->data;
						pubmsg.payloadlen = strlen(element->data);
						pubmsg.qos = QOS;
						pubmsg.retained = 0;
					}
					else
					{
						if(rePublishCount > 10)
						{
							printf("ReConnect Server\n");
							rePublishCount = 10;
							break;
						}
					}					
					
					MQTTClient_publishMessage(bleNode[3].client, bleNode[3].topic, &pubmsg, &token);
				
				/*
					printf("Waiting for up to %d seconds for publication of %s\n"
					  "on topic %s for client with ClientID: %s\n",
						(int)(TIMEOUT/1000), PAYLOAD, TOPIC, CLIENTID);
				*/
				
					rc = MQTTClient_waitForCompletion(bleNode[3].client, token, TIMEOUT);
				
					if(rc != MQTTCLIENT_SUCCESS)
					{
						printf("Failed Publish\n"); 				
						usleep(1000000);
						rePublishCount++;
						
						continue;
					}
					
					free(element->data);
					free(element);
					printf("CH4 Message with delivery token %d delivered\n", token);
				}
				else
				{
					printf("AD4 No Data in the Send Q Length : %d\n", qCount);
					usleep(500000);
				}
			}
		}
	}
}


#endif

int AD1_LowPassFilter_execute(void* arg)
{
	int ret = 0;
	int i= 0;

	float tempVar = 0.0;
	
	while(1)
	{
		usleep(2000);
		if(gAD1FlagLowPassFilterEnable == TRUE)
		{

			if(convertDataBuf1[0] < 0)
				tempVar = (float)(convertDataBuf1[0]*(-1));
			else
				tempVar = (float)convertDataBuf1[0];		
			lowPassFilterBuf1[0] = tempVar;

			for(i = 1; i<51200 ;i++)
			{
				if(convertDataBuf1[i] < 0)
					tempVar = (float)(convertDataBuf1[i]*(-1));
				else
					tempVar = (float)convertDataBuf1[i];
				
				lowPassFilterBuf1[i] = (float)(((gTauValue*lowPassFilterBuf1[i-1]) +(tempVar*time_per_samples))/(time_per_samples+gTauValue));
#if DEBUG_LPF&DEBUG_LPF_AD1
				printf("AD1  LFP, %f	,%f \n", lowPassFilterBuf1[i] , 	tempVar);						
#endif
			}
			
			gAD1FlagLowPassFilterEnable = FALSE;
		}
	}
	return ret;

}

int AD2_LowPassFilter_execute(void* arg)
{
	int ret = 0;
	int i= 0;

	float tempVar = 0.0;
	
	while(1)
	{
		usleep(2000);
		if(gAD2FlagLowPassFilterEnable == TRUE)
		{

			if(convertDataBuf2[0] < 0)
				tempVar = (float)(convertDataBuf2[0]*(-1));
			else
				tempVar = (float)convertDataBuf2[0];		
			lowPassFilterBuf2[0] = tempVar;

			for(i = 1; i<51200 ;i++)
			{
				if(convertDataBuf2[i] < 0)
					tempVar = (float)(convertDataBuf2[i]*(-1));
				else
					tempVar = (float)convertDataBuf2[i];
				
				lowPassFilterBuf2[i] = (float)(((gTauValue*lowPassFilterBuf2[i-1]) +(tempVar*time_per_samples))/(time_per_samples+gTauValue));
#if DEBUG_LPF&DEBUG_LPF_AD2
				printf("AD2  LFP,%f	,%f \n", lowPassFilterBuf2[i] , 	tempVar);						
#endif
			}
			
			gAD2FlagLowPassFilterEnable = FALSE;
		}
	}
	return ret;

}

int AD3_LowPassFilter_execute(void* arg)
{
	int ret = 0;
	int i= 0;

	float tempVar = 0.0;
	
	while(1)
	{
		usleep(2000);
		if(gAD3FlagLowPassFilterEnable == TRUE)
		{

			if(convertDataBuf3[0] < 0)
				tempVar = (float)(convertDataBuf3[0]*(-1));
			else
				tempVar = (float)convertDataBuf3[0];		
			lowPassFilterBuf3[0] = tempVar;

			for(i = 1; i<51200 ;i++)
			{
				if(convertDataBuf3[i] < 0)
					tempVar = (float)(convertDataBuf3[i]*(-1));
				else
					tempVar = (float)convertDataBuf3[i];
				
				lowPassFilterBuf3[i] = (float)(((gTauValue*lowPassFilterBuf3[i-1]) +(tempVar*time_per_samples))/(time_per_samples+gTauValue));
#if DEBUG_LPF&DEBUG_LPF_AD3

				printf("AD3  LFP, %f	,%f \n", lowPassFilterBuf3[i] , 	tempVar);						
#endif
			}
			
			gAD3FlagLowPassFilterEnable = FALSE;
		}
	}
	return ret;

}


int AD4_LowPassFilter_execute(void* arg)
{
	int ret = 0;
	int i= 0;

	float tempVar = 0.0;
	
	while(1)
	{
		usleep(2000);
		if(gAD4FlagLowPassFilterEnable == TRUE)
		{

			if(convertDataBuf4[0] < 0)
				tempVar = (float)(convertDataBuf4[0]*(-1));
			else
				tempVar = (float)convertDataBuf4[0];		
			lowPassFilterBuf4[0] = tempVar;

			for(i = 1; i<51200 ;i++)
			{
				if(convertDataBuf4[i] < 0)
					tempVar = (float)(convertDataBuf4[i]*(-1));
				else
					tempVar = (float)convertDataBuf4[i];
				
				lowPassFilterBuf4[i] = (float)(((gTauValue*lowPassFilterBuf4[i-1]) +(tempVar*time_per_samples))/(time_per_samples+gTauValue));
#if DEBUG_LPF&DEBUG_LPF_AD4
				printf("AD4  LFP, %f	,%f \n", lowPassFilterBuf4[i] , 	tempVar);						
#endif
			}
			
			gAD4FlagLowPassFilterEnable = FALSE;
		}
	}
	return ret;

}

#ifdef FEATURE_MQTT
genPublishDataAD1()
{
	cJSON *jsonResult;
	cJSON *jsonArray;

	cJSON *sensor[ACCVEL_MAX];
	cJSON *data;
	cJSON *sensorArray;
	cJSON *dataPointArray[ACCVEL_MAX];
	cJSON *dataPointItem[ACCVEL_MAX];

 	char* pResult = NULL;
	char* pBuf = NULL;
	struct timeval val;
	struct tm *ptm;
	float value = 0;
	
	int rc;
	int i=0;
	char timeBuf[128]={0,};
	char usec[7]={0,};
	char usec_use[4]={0,};
	Element* element;

	//jsonResult =cJSON_CreateObject();
	//jsonArray = cJSON_CreateArray();

	//data = cJSON_CreateObject();
	sensorArray = cJSON_CreateArray();

	
	sensor[ACC_OVERALL] = cJSON_CreateObject();
	sensor[ACC_A1] = cJSON_CreateObject();
	sensor[ACC_A2] = cJSON_CreateObject();
	sensor[ACC_A3] = cJSON_CreateObject();
	
	sensor[VEL_OVERALL] = cJSON_CreateObject();
	sensor[VEL_V1] = cJSON_CreateObject();
	sensor[VEL_V2] = cJSON_CreateObject();
 

	dataPointArray[ACC_OVERALL] = cJSON_CreateArray();
	dataPointItem[ACC_OVERALL] =cJSON_CreateObject();

	/*CHANNEL 1 ACC overall*/
	cJSON_AddStringToObject(sensor[ACC_OVERALL], "s", Channel1SensorId[equipmentNumber][ACC_OVERALL]);
	cJSON_AddNumberToObject(dataPointItem[ACC_OVERALL], "v", gParamCh1.acc_overall);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[ACC_OVERALL], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[ACC_OVERALL], dataPointItem[ACC_OVERALL]);
	cJSON_AddItemToObject(sensor[ACC_OVERALL], "d", dataPointArray[ACC_OVERALL]);
	cJSON_AddItemToArray(sensorArray, sensor[ACC_OVERALL]);

	/*CHANNEL 1 ACC A1*/
	dataPointArray[ACC_A1] = cJSON_CreateArray();
	dataPointItem[ACC_A1] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[ACC_A1], "s", Channel1SensorId[equipmentNumber][ACC_A1]);
	cJSON_AddNumberToObject(dataPointItem[ACC_A1], "v", gParamCh1.acc_a1);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[ACC_A1], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[ACC_A1], dataPointItem[ACC_A1]);
	cJSON_AddItemToObject(sensor[ACC_A1], "d", dataPointArray[ACC_A1]);
	cJSON_AddItemToArray(sensorArray, sensor[ACC_A1]);

	/*CHANNEL 1 ACC A2*/
	dataPointArray[ACC_A2] = cJSON_CreateArray();
	dataPointItem[ACC_A2] =cJSON_CreateObject();
		
	cJSON_AddStringToObject(sensor[ACC_A2], "s", Channel1SensorId[equipmentNumber][ACC_A2]);
	cJSON_AddNumberToObject(dataPointItem[ACC_A2], "v", gParamCh1.acc_a2);
	
	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);
	
	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
		
		
	cJSON_AddStringToObject(dataPointItem[ACC_A2], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[ACC_A2], dataPointItem[ACC_A2]);
	cJSON_AddItemToObject(sensor[ACC_A2], "d", dataPointArray[ACC_A2]);
	cJSON_AddItemToArray(sensorArray, sensor[ACC_A2]);

	/*CHANNEL 1 ACC A3*/
	dataPointArray[ACC_A3] = cJSON_CreateArray();
	dataPointItem[ACC_A3] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[ACC_A3], "s", Channel1SensorId[equipmentNumber][ACC_A3]);
	cJSON_AddNumberToObject(dataPointItem[ACC_A3], "v", gParamCh1.acc_a3);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[ACC_A3], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[ACC_A3], dataPointItem[ACC_A3]);
	cJSON_AddItemToObject(sensor[ACC_A3], "d", dataPointArray[ACC_A3]);
	cJSON_AddItemToArray(sensorArray, sensor[ACC_A3]);


	/*CHANNEL 1 VEL overall*/
	dataPointArray[VEL_OVERALL] = cJSON_CreateArray();
	dataPointItem[VEL_OVERALL] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[VEL_OVERALL], "s", Channel1SensorId[equipmentNumber][VEL_OVERALL]);
	cJSON_AddNumberToObject(dataPointItem[VEL_OVERALL], "v", gParamCh1.vel_overall);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[VEL_OVERALL], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[VEL_OVERALL], dataPointItem[VEL_OVERALL]);
	cJSON_AddItemToObject(sensor[VEL_OVERALL], "d", dataPointArray[VEL_OVERALL]);
	cJSON_AddItemToArray(sensorArray, sensor[VEL_OVERALL]);

	/*CHANNEL 1 VEL V1*/
	dataPointArray[VEL_V1] = cJSON_CreateArray();
	dataPointItem[VEL_V1] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[VEL_V1], "s", Channel1SensorId[equipmentNumber][VEL_V1]);
	cJSON_AddNumberToObject(dataPointItem[VEL_V1], "v", gParamCh1.vel_v1);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[VEL_V1], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[VEL_V1], dataPointItem[VEL_V1]);
	cJSON_AddItemToObject(sensor[VEL_V1], "d", dataPointArray[VEL_V1]);
	cJSON_AddItemToArray(sensorArray, sensor[VEL_V1]);

	/*CHANNEL 1 VEL V2*/
	dataPointArray[VEL_V2] = cJSON_CreateArray();
	dataPointItem[VEL_V2] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[VEL_V2], "s", Channel1SensorId[equipmentNumber][VEL_V2]);
	cJSON_AddNumberToObject(dataPointItem[VEL_V2], "v", gParamCh1.vel_v2);
	
	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);
	
	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
		
		
	cJSON_AddStringToObject(dataPointItem[VEL_V2], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[VEL_V2], dataPointItem[VEL_V2]);
	cJSON_AddItemToObject(sensor[VEL_V2], "d", dataPointArray[VEL_V2]);
	cJSON_AddItemToArray(sensorArray, sensor[VEL_V2]);


	pResult = cJSON_Print(sensorArray);

	element = (Element*)malloc(sizeof(Element));
	pBuf = (char*)malloc(strlen(pResult));
	memset(pBuf, 0, strlen(pResult));
	memcpy(pBuf, pResult, strlen(pResult));
	element->data =  pBuf;
	element->size = strlen(pResult);
	enqueue(gAD1WillSendQ, element);

	free(pResult);
	cJSON_Delete(sensorArray);

	return rc;
}


genPublishDataAD2()
{
	cJSON *jsonResult;
	cJSON *jsonArray;

	cJSON *sensor[ACCVEL_MAX];
	cJSON *data;
	cJSON *sensorArray;
	cJSON *dataPointArray[ACCVEL_MAX];
	cJSON *dataPointItem[ACCVEL_MAX];

 	char* pResult = NULL;
	char* pBuf = NULL;
	struct timeval val;
	struct tm *ptm;
	float value = 0;
	
	int rc;
	int i=0;
	char timeBuf[128]={0,};
	char usec[7]={0,};
	char usec_use[4]={0,};
	Element* element;

	sensorArray = cJSON_CreateArray();

	
	sensor[ACC_OVERALL] = cJSON_CreateObject();
	sensor[ACC_A1] = cJSON_CreateObject();
	sensor[ACC_A2] = cJSON_CreateObject();
	sensor[ACC_A3] = cJSON_CreateObject();
	
	sensor[VEL_OVERALL] = cJSON_CreateObject();
	sensor[VEL_V1] = cJSON_CreateObject();
	sensor[VEL_V2] = cJSON_CreateObject();

	dataPointArray[ACC_OVERALL] = cJSON_CreateArray();
	dataPointItem[ACC_OVERALL] =cJSON_CreateObject();

	/*CHANNEL 2 ACC overall*/
	cJSON_AddStringToObject(sensor[ACC_OVERALL], "s", Channel2SensorId[equipmentNumber][ACC_OVERALL]);
	cJSON_AddNumberToObject(dataPointItem[ACC_OVERALL], "v", gParamCh2.acc_overall);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[ACC_OVERALL], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[ACC_OVERALL], dataPointItem[ACC_OVERALL]);
	cJSON_AddItemToObject(sensor[ACC_OVERALL], "d", dataPointArray[ACC_OVERALL]);
	cJSON_AddItemToArray(sensorArray, sensor[ACC_OVERALL]);

	/*CHANNEL 2 ACC A1*/
	dataPointArray[ACC_A1] = cJSON_CreateArray();
	dataPointItem[ACC_A1] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[ACC_A1], "s", Channel2SensorId[equipmentNumber][ACC_A1]);
	cJSON_AddNumberToObject(dataPointItem[ACC_A1], "v", gParamCh2.acc_a1);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[ACC_A1], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[ACC_A1], dataPointItem[ACC_A1]);
	cJSON_AddItemToObject(sensor[ACC_A1], "d", dataPointArray[ACC_A1]);
	cJSON_AddItemToArray(sensorArray, sensor[ACC_A1]);

	/*CHANNEL 2 ACC A2*/
	dataPointArray[ACC_A2] = cJSON_CreateArray();
	dataPointItem[ACC_A2] =cJSON_CreateObject();
		
	cJSON_AddStringToObject(sensor[ACC_A2], "s", Channel2SensorId[equipmentNumber][ACC_A2]);
	cJSON_AddNumberToObject(dataPointItem[ACC_A2], "v", gParamCh2.acc_a2);
	
	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);
	
	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
		
		
	cJSON_AddStringToObject(dataPointItem[ACC_A2], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[ACC_A2], dataPointItem[ACC_A2]);
	cJSON_AddItemToObject(sensor[ACC_A2], "d", dataPointArray[ACC_A2]);
	cJSON_AddItemToArray(sensorArray, sensor[ACC_A2]);

	/*CHANNEL 2 ACC A3*/
	dataPointArray[ACC_A3] = cJSON_CreateArray();
	dataPointItem[ACC_A3] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[ACC_A3], "s", Channel2SensorId[equipmentNumber][ACC_A3]);
	cJSON_AddNumberToObject(dataPointItem[ACC_A3], "v", gParamCh2.acc_a3);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[ACC_A3], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[ACC_A3], dataPointItem[ACC_A3]);
	cJSON_AddItemToObject(sensor[ACC_A3], "d", dataPointArray[ACC_A3]);
	cJSON_AddItemToArray(sensorArray, sensor[ACC_A3]);


	/*CHANNEL 2 VEL overall*/
	dataPointArray[VEL_OVERALL] = cJSON_CreateArray();
	dataPointItem[VEL_OVERALL] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[VEL_OVERALL], "s", Channel2SensorId[equipmentNumber][VEL_OVERALL]);
	cJSON_AddNumberToObject(dataPointItem[VEL_OVERALL], "v", gParamCh2.vel_overall);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[VEL_OVERALL], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[VEL_OVERALL], dataPointItem[VEL_OVERALL]);
	cJSON_AddItemToObject(sensor[VEL_OVERALL], "d", dataPointArray[VEL_OVERALL]);
	cJSON_AddItemToArray(sensorArray, sensor[VEL_OVERALL]);

	/*CHANNEL 2 VEL V1*/
	dataPointArray[VEL_V1] = cJSON_CreateArray();
	dataPointItem[VEL_V1] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[VEL_V1], "s", Channel2SensorId[equipmentNumber][VEL_V1]);
	cJSON_AddNumberToObject(dataPointItem[VEL_V1], "v", gParamCh2.vel_v1);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[VEL_V1], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[VEL_V1], dataPointItem[VEL_V1]);
	cJSON_AddItemToObject(sensor[VEL_V1], "d", dataPointArray[VEL_V1]);
	cJSON_AddItemToArray(sensorArray, sensor[VEL_V1]);

	/*CHANNEL 2 VEL V2*/
	dataPointArray[VEL_V2] = cJSON_CreateArray();
	dataPointItem[VEL_V2] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[VEL_V2], "s", Channel2SensorId[equipmentNumber][VEL_V2]);
	cJSON_AddNumberToObject(dataPointItem[VEL_V2], "v", gParamCh2.vel_v2);
	
	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);
	
	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
		
		
	cJSON_AddStringToObject(dataPointItem[VEL_V2], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[VEL_V2], dataPointItem[VEL_V2]);
	cJSON_AddItemToObject(sensor[VEL_V2], "d", dataPointArray[VEL_V2]);
	cJSON_AddItemToArray(sensorArray, sensor[VEL_V2]);

	pResult = cJSON_Print(sensorArray);
	
	element = (Element*)malloc(sizeof(Element));
	pBuf = (char*)malloc(strlen(pResult));
	memset(pBuf, 0, strlen(pResult));
	memcpy(pBuf, pResult, strlen(pResult));
	element->data =  pBuf;
	element->size = strlen(pResult);
	enqueue(gAD2WillSendQ, element);

	free(pResult);
	cJSON_Delete(sensorArray);

	return rc;
}


genPublishDataAD3()
{
	cJSON *jsonResult;
	cJSON *jsonArray;

	cJSON *sensor[ACCVEL_MAX];
	cJSON *data;
	cJSON *sensorArray;
	cJSON *dataPointArray[ACCVEL_MAX];
	cJSON *dataPointItem[ACCVEL_MAX];

 	char* pResult = NULL;
	char* pBuf = NULL;
	struct timeval val;
	struct tm *ptm;
	float value = 0;
	
	int rc;
	int i=0;
	char timeBuf[128]={0,};
	char usec[7]={0,};
	char usec_use[4]={0,};
	Element* element;
	
	sensorArray = cJSON_CreateArray();

	
	sensor[ACC_OVERALL] = cJSON_CreateObject();
	sensor[ACC_A1] = cJSON_CreateObject();
	sensor[ACC_A2] = cJSON_CreateObject();
	sensor[ACC_A3] = cJSON_CreateObject();
	
	sensor[VEL_OVERALL] = cJSON_CreateObject();
	sensor[VEL_V1] = cJSON_CreateObject();
	sensor[VEL_V2] = cJSON_CreateObject();
 

	dataPointArray[ACC_OVERALL] = cJSON_CreateArray();
	dataPointItem[ACC_OVERALL] =cJSON_CreateObject();

	/*CHANNEL 3 ACC overall*/
	cJSON_AddStringToObject(sensor[ACC_OVERALL], "s", Channel3SensorId[equipmentNumber][ACC_OVERALL]);
	cJSON_AddNumberToObject(dataPointItem[ACC_OVERALL], "v", gParamCh3.acc_overall);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[ACC_OVERALL], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[ACC_OVERALL], dataPointItem[ACC_OVERALL]);
	cJSON_AddItemToObject(sensor[ACC_OVERALL], "d", dataPointArray[ACC_OVERALL]);
	cJSON_AddItemToArray(sensorArray, sensor[ACC_OVERALL]);

	/*CHANNEL 3 ACC A1*/
	dataPointArray[ACC_A1] = cJSON_CreateArray();
	dataPointItem[ACC_A1] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[ACC_A1], "s", Channel3SensorId[equipmentNumber][ACC_A1]);
	cJSON_AddNumberToObject(dataPointItem[ACC_A1], "v", gParamCh3.acc_a1);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[ACC_A1], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[ACC_A1], dataPointItem[ACC_A1]);
	cJSON_AddItemToObject(sensor[ACC_A1], "d", dataPointArray[ACC_A1]);
	cJSON_AddItemToArray(sensorArray, sensor[ACC_A1]);

	/*CHANNEL 3 ACC A2*/
	dataPointArray[ACC_A2] = cJSON_CreateArray();
	dataPointItem[ACC_A2] =cJSON_CreateObject();
		
	cJSON_AddStringToObject(sensor[ACC_A2], "s", Channel3SensorId[equipmentNumber][ACC_A2]);
	cJSON_AddNumberToObject(dataPointItem[ACC_A2], "v", gParamCh3.acc_a2);
	
	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);
	
	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
		
		
	cJSON_AddStringToObject(dataPointItem[ACC_A2], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[ACC_A2], dataPointItem[ACC_A2]);
	cJSON_AddItemToObject(sensor[ACC_A2], "d", dataPointArray[ACC_A2]);
	cJSON_AddItemToArray(sensorArray, sensor[ACC_A2]);

	/*CHANNEL 3 ACC A3*/
	dataPointArray[ACC_A3] = cJSON_CreateArray();
	dataPointItem[ACC_A3] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[ACC_A3], "s", Channel3SensorId[equipmentNumber][ACC_A3]);
	cJSON_AddNumberToObject(dataPointItem[ACC_A3], "v", gParamCh3.acc_a3);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[ACC_A3], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[ACC_A3], dataPointItem[ACC_A3]);
	cJSON_AddItemToObject(sensor[ACC_A3], "d", dataPointArray[ACC_A3]);
	cJSON_AddItemToArray(sensorArray, sensor[ACC_A3]);

	/*CHANNEL 3 VEL overall*/
	dataPointArray[VEL_OVERALL] = cJSON_CreateArray();
	dataPointItem[VEL_OVERALL] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[VEL_OVERALL], "s", Channel3SensorId[equipmentNumber][VEL_OVERALL]);
	cJSON_AddNumberToObject(dataPointItem[VEL_OVERALL], "v", gParamCh3.vel_overall);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[VEL_OVERALL], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[VEL_OVERALL], dataPointItem[VEL_OVERALL]);
	cJSON_AddItemToObject(sensor[VEL_OVERALL], "d", dataPointArray[VEL_OVERALL]);
	cJSON_AddItemToArray(sensorArray, sensor[VEL_OVERALL]);

	/*CHANNEL 3 VEL V1*/
	dataPointArray[VEL_V1] = cJSON_CreateArray();
	dataPointItem[VEL_V1] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[VEL_V1], "s", Channel3SensorId[equipmentNumber][VEL_V1]);
	cJSON_AddNumberToObject(dataPointItem[VEL_V1], "v", gParamCh3.vel_v1);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[VEL_V1], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[VEL_V1], dataPointItem[VEL_V1]);
	cJSON_AddItemToObject(sensor[VEL_V1], "d", dataPointArray[VEL_V1]);
	cJSON_AddItemToArray(sensorArray, sensor[VEL_V1]);

	/*CHANNEL 3 VEL V2*/
	dataPointArray[VEL_V2] = cJSON_CreateArray();
	dataPointItem[VEL_V2] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[VEL_V2], "s", Channel3SensorId[equipmentNumber][VEL_V2]);
	cJSON_AddNumberToObject(dataPointItem[VEL_V2], "v", gParamCh3.vel_v2);
	
	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);
	
	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
		
		
	cJSON_AddStringToObject(dataPointItem[VEL_V2], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[VEL_V2], dataPointItem[VEL_V2]);
	cJSON_AddItemToObject(sensor[VEL_V2], "d", dataPointArray[VEL_V2]);
	cJSON_AddItemToArray(sensorArray, sensor[VEL_V2]);

	pResult = cJSON_Print(sensorArray);
	
	element = (Element*)malloc(sizeof(Element));
	pBuf = (char*)malloc(strlen(pResult));
	memset(pBuf, 0, strlen(pResult));
	memcpy(pBuf, pResult, strlen(pResult));
	element->data =  pBuf;
	element->size = strlen(pResult);
	enqueue(gAD3WillSendQ, element);

	free(pResult);
	cJSON_Delete(sensorArray);

	return rc;
}



genPublishDataAD4()
{
	cJSON *jsonResult;
	cJSON *jsonArray;

	cJSON *sensor[MIC_MAX];
	cJSON *data;
	cJSON *sensorArray;
	cJSON *dataPointArray[MIC_MAX];
	cJSON *dataPointItem[MIC_MAX];

 	char* pResult = NULL;
	char* pBuf = NULL;
	struct timeval val;
	struct tm *ptm;
	float value = 0;
	
	int rc;
	int i=0;
	char timeBuf[128]={0,};
	char usec[7]={0,};
	char usec_use[4]={0,};
	Element* element;

	sensorArray = cJSON_CreateArray();

	
	sensor[MIC_OVERALL] = cJSON_CreateObject();
	sensor[MIC_M1] = cJSON_CreateObject();
	sensor[MIC_M2] = cJSON_CreateObject();
	sensor[MIC_M3] = cJSON_CreateObject();
	sensor[MIC_M4] = cJSON_CreateObject();

	dataPointArray[MIC_OVERALL] = cJSON_CreateArray();
	dataPointItem[MIC_OVERALL] =cJSON_CreateObject();
	
	/*CHANNEL 4 MIC overall*/
	cJSON_AddStringToObject(sensor[MIC_OVERALL], "s", Channel4SensorId[equipmentNumber][MIC_OVERALL]);
	cJSON_AddNumberToObject(dataPointItem[MIC_OVERALL], "v", gParamCh4.mic_overall);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[MIC_OVERALL], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[MIC_OVERALL], dataPointItem[MIC_OVERALL]);
	cJSON_AddItemToObject(sensor[MIC_OVERALL], "d", dataPointArray[MIC_OVERALL]);
	cJSON_AddItemToArray(sensorArray, sensor[MIC_OVERALL]);

	/*CHANNEL 4 MIC M1*/
	dataPointArray[MIC_M1] = cJSON_CreateArray();
	dataPointItem[MIC_M1] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[MIC_M1], "s", Channel4SensorId[equipmentNumber][MIC_M1]);
	cJSON_AddNumberToObject(dataPointItem[MIC_M1], "v", gParamCh4.mic_m1);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[MIC_M1], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[MIC_M1], dataPointItem[MIC_M1]);
	cJSON_AddItemToObject(sensor[MIC_M1], "d", dataPointArray[MIC_M1]);
	cJSON_AddItemToArray(sensorArray, sensor[MIC_M1]);

	/*CHANNEL 4 MIC M2*/
	dataPointArray[MIC_M2] = cJSON_CreateArray();
	dataPointItem[MIC_M2] =cJSON_CreateObject();
		
	cJSON_AddStringToObject(sensor[MIC_M2], "s", Channel4SensorId[equipmentNumber][MIC_M2]);
	cJSON_AddNumberToObject(dataPointItem[MIC_M2], "v", gParamCh4.mic_m2);
	
	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);
	
	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3); 
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
		
		
	cJSON_AddStringToObject(dataPointItem[MIC_M2], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[MIC_M2], dataPointItem[MIC_M2]);
	cJSON_AddItemToObject(sensor[MIC_M2], "d", dataPointArray[MIC_M2]);
	cJSON_AddItemToArray(sensorArray, sensor[MIC_M2]);

	/*CHANNEL 4 MIC M3*/
	dataPointArray[MIC_M3] = cJSON_CreateArray();
	dataPointItem[MIC_M3] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[MIC_M3], "s", Channel4SensorId[equipmentNumber][MIC_M3]);
	cJSON_AddNumberToObject(dataPointItem[MIC_M3], "v", gParamCh4.mic_m3);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[MIC_M3], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[MIC_M3], dataPointItem[MIC_M3]);
	cJSON_AddItemToObject(sensor[MIC_M3], "d", dataPointArray[MIC_M3]);
	cJSON_AddItemToArray(sensorArray, sensor[MIC_M3]);

	/*CHANNEL 4 MIC M4*/
	dataPointArray[MIC_M4] = cJSON_CreateArray();
	dataPointItem[MIC_M4] =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor[MIC_M4], "s", Channel4SensorId[equipmentNumber][MIC_M4]);
	cJSON_AddNumberToObject(dataPointItem[MIC_M4], "v", gParamCh4.mic_m4);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s+09:00"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem[MIC_M4], "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray[MIC_M4], dataPointItem[MIC_M4]);
	cJSON_AddItemToObject(sensor[MIC_M4], "d", dataPointArray[MIC_M4]);
	cJSON_AddItemToArray(sensorArray, sensor[MIC_M4]);


	pResult = cJSON_Print(sensorArray);
	
	element = (Element*)malloc(sizeof(Element));
	pBuf = (char*)malloc(strlen(pResult));
	memset(pBuf, 0, strlen(pResult));
	memcpy(pBuf, pResult, strlen(pResult));
	element->data =  pBuf;
	element->size = strlen(pResult);
	enqueue(gAD4WillSendQ, element);

	free(pResult);
	cJSON_Delete(sensorArray);

	return rc;
}
#endif/*FEATURE_MQTT*/


void readADDataThread(void)

{
	int i, policy;
	struct timeval startVal;	
	struct timeval nowVal;	
	char dbPath[128];
	int rc;
	unsigned char* A_PATH=NULL;
	A_PATH=getenv("A_PATH");	
	memset(dbPath, 0, sizeof(dbPath));
	sprintf(dbPath,"%s/a1.db",A_PATH);
	printf("DB PATH:%s\n", dbPath);	

	
	//FILE* fp1;
	//FILE* fp2;
	//FILE* fp3;
	//FILE* fp4;
	//fp_ch4 = fopen("/mnt/mmc/snow/SS/src/AnalogSensor_Asocket_SingleChannel/ch4.dat", "w");


	//fp1 = fopen("/mnt/mmc/snow/SS/src/AnalogSensor_4ch_4thread/ch1.dat", "ab+");
	//fp2 = fopen("/mnt/mmc/snow/SS/src/AnalogSensor_4ch_4thread/ch2.dat", "ab+");
	//fp3 = fopen("/mnt/mmc/snow/SS/src/AnalogSensor_4ch_4thread/ch3.dat", "ab+");
	//fp4 = fopen("/mnt/mmc/snow/SS/src/AnalogSensor_4ch_4thread/ch4.dat", "ab+");
#ifdef FEATURE_SINGLE_CHANNEL
	gLinkedQueue = createNode();
#else
	gLinkedQueueAD1 = createNode();
	gLinkedQueueAD2 = createNode();
	gLinkedQueueAD3 = createNode();
	gLinkedQueueAD4 = createNode();
#endif/*FEATURE_SINGLE_CHANNEL*/
	gAD1WillSendQ = createNode();
	gAD2WillSendQ = createNode();
	gAD3WillSendQ = createNode();
	gAD4WillSendQ = createNode();

#ifdef FEATURE_SINGLE_CHANNEL
	pthread_mutex_init(&singleQueueMutex, NULL);	
	pthread_cond_init(&gSignal,NULL);

#else
	pthread_mutex_init(&ch1QueueMutex, NULL);
	pthread_mutex_init(&ch2QueueMutex, NULL);
	pthread_mutex_init(&ch3QueueMutex, NULL);
	pthread_mutex_init(&ch4QueueMutex, NULL);	
#endif/*FEATURE_SINGLE_CHANNEL*/

	//if(!AD_PowerCheck(verbose))
	{
printf("before checkADModule\n");
		if( checkADModule() != 0)
		{
printf("after checkADModule1\n");

			
			while(1)
			{
				if(readyToDBClosing == FLAG_AD_ALL)
				{
				int i =0;	
					rc = sqlite3_close(db);
					if(rc != SQLITE_OK)
					{
						printf("[[ReadADataThread]] DB closing Error %d!!!!!!!!\n", rc);
				while( i< 10 && rc != SQLITE_OK)
				{
					usleep(50000);
					rc = sqlite3_close(db);
					printf("DB close:%d\n", rc);
				}
					if(rc != SQLITE_OK)
					return -1;
					}
					else
					{
					printf("DB closing OK\n");
					}
					rc=sqlite3_shutdown();
					printf("shutdown Rc:%d\n", rc);
					usleep(2000);
				rc=	sqlite3_initialize();
					printf("init Rc:%d\n", rc);

					rc = sqlite3_open_v2(dbPath, &db,SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
					if(rc != SQLITE_OK)
					{
printf("%s\n", dbPath);
						printf("[[ReadADataThread]]  DB opening Error:%d !!!!!!!!\n", rc);
						return -1;
					}
					readyToDBClosing = 0;
					
				}
			
				if(isNeedSetADMode)
				{
					isNeedSetADMode = 0;

#ifdef FEATURE_SINGLE_CHANNEL
					if(gChannel == MODULE_AD1)
						fdx = fd1;
					else if(gChannel == MODULE_AD2)
						fdx = fd2;
					else if(gChannel == MODULE_AD3)
						fdx = fd3;
					else if(gChannel == MODULE_AD4)
						fdx = fd4;
					else
						fdx = fd1;

					AD_EnableModule(gChannel);		
					printf("setmode %d channel : %d \n", gADMode, gChannel);
					setADMode(fdx, gADMode); 
					printf("setmode %d channel : %d \n", gADMode, gChannel);

					AD_DisableModule(gChannel);

#else

					AD_EnableModule(MODULE_AD1);							
					setADMode(fd1, 1); 
					AD_DisableModule(MODULE_AD1);

					AD_EnableModule(MODULE_AD2);							
					setADMode(fd2, 1); 
					AD_DisableModule(MODULE_AD2);
					AD_EnableModule(MODULE_AD3);							
					setADMode(fd3, 1); 
					AD_DisableModule(MODULE_AD3);							
					
					AD_EnableModule(MODULE_AD4);							
					setADMode(fd4, 1); 
					AD_DisableModule(MODULE_AD4);
#endif /*FEATURE_SINGLE_CHANNEL*/							

				}
				

#ifdef FEATURE_SINGLE_CHANNEL
					
					AD_EnableModule(gChannel);

					//if(readData(fd4, MODULE_AD4, gLinkedQueueAD4) == 0)
					if(readDataFromADModulesbySPI(fdx, gChannel, gLinkedQueue) == 0)
					{
						//AD_DisableModule(MODULE_AD4);
						//break;
					}
					AD_DisableModule(gChannel);


#else
					AD_EnableModule(MODULE_AD1);
					//if(readData(fd1, MODULE_AD1, gLinkedQueueAD1) == 0)
					
					if(readDataFromADModulesbySPI(fd1, MODULE_AD1, gLinkedQueueAD1) == 1)
					{
						
						//AD_DisableModule(MODULE_AD1);
						//break;
					}
					
					AD_DisableModule(MODULE_AD1);

					AD_EnableModule(MODULE_AD2);
				
					//if(readData(fd2, MODULE_AD2, gLinkedQueueAD2) == 0)
					if(readDataFromADModulesbySPI(fd2,MODULE_AD2, gLinkedQueueAD2) == 1)
					{
						//AD_DisableModule(FLAG_AD2);
						//break;
					}
					AD_DisableModule(MODULE_AD2);
				
					AD_EnableModule(MODULE_AD3);
				
					//if(readData(fd3, MODULE_AD3, gLinkedQueueAD3) == 0)
					if(readDataFromADModulesbySPI(fd3, MODULE_AD3, gLinkedQueueAD3) == 1)
					{
						//AD_DisableModule(MODULE_AD3);
						//break;
					}
					AD_DisableModule(MODULE_AD3);
					
					AD_EnableModule(MODULE_AD4);
					
					//if(readData(fd4, MODULE_AD4, gLinkedQueueAD4) == 0)
					if(readDataFromADModulesbySPI(fd4, MODULE_AD4, gLinkedQueueAD4) == 1)
					{
						//AD_DisableModule(MODULE_AD4);
						//break;
					}
					AD_DisableModule(MODULE_AD4);

#endif /*FEATURE_SINGLE_CHANNEL*/							
					
			}
		}
		printf("after checkADModule2\n");
	}
	sqlite3_close(db);
}


void setTrigger(void)
{
	isStartAll = 1;
	isNeedSetADMode = 1;
	//pthread_cond_signal(&gSignal);
	
	 usleep(5000);
}

int sendData(int fp, int channelNum, unsigned char* buf)
{
#if 0
	Element* element = NULL;
	LinkedQueue* queue = NULL;
	int ret = 0;
	int n = 0;
	int length = 0;
	int queueLength = 0;
	int disableDataIndex = 0;
	int sleepDuration = 0;
	FILE* fp1 = NULL;
	//char* buffer = NULL;
	int readyCh1 = 0;
	int readyCh2 = 0;
	int readyCh3 = 0;
	int readyCh4 = 0;
#ifdef FEATURE_SINGLE_CHANNEL
	unsigned char* buffer=NULL;
#else
	int i;
	char buffer=0;
#endif/*FEATURE_SINGLE_CHANNEL*/	

	//fp1 = fopen("/mnt/mmc/snow/SS/src/AnalogSensor_4ch_4thread/ch1.dat", "ab+");
	//printf("[[sendData]] %d\n", channelNum);

#ifdef FEATURE_SINGLE_CHANNEL
	queue = gLinkedQueue;
#else/*FEATURE_SINGLE_CHANNEL*/
	
	switch(channelNum)
	{
		case MODULE_AD1:
			queue = gLinkedQueueAD1;
			break;
		case MODULE_AD2:
			queue = gLinkedQueueAD2;			
			break;
		case MODULE_AD3:
			queue = gLinkedQueueAD3;			
			break;
		case MODULE_AD4:
			queue = gLinkedQueueAD4;			
			break;
			
		default:
			ret = -1;
			break;
	}
	
	if(ret != 0)
		return ret;

	if(queueLength < 100)
						sleepDuration = 250000;
					else if(queueLength >= 100 && queueLength < 200)
						sleepDuration = 230000;
					else if(queueLength >= 200 && queueLength < 300)
						sleepDuration = 210000;
					else
						sleepDuration = 200000;
#endif/*FEATURE_SINGLE_CHANNEL*/



pthread_mutex_lock(&singleQueueMutex); 
queueLength = getLength(gLinkedQueue) ;
if(queueLength > 200)
	disableDataIndex = queueLength - 50;

pthread_mutex_unlock(&singleQueueMutex);							

if( queueLength > 50 )
{
	while(n < 50)
	{
		pthread_mutex_lock(&singleQueueMutex); 
		element = dequeue(gLinkedQueue);
		
		pthread_mutex_unlock(&singleQueueMutex);							
		if(element != NULL)
		{
			if(element == NULL)
				printf("[[sendData]] elementis NULL\n");					
			if(element->data == NULL)
				printf("[[sendData]] element->data is NULL\n"); 				
			
				//fwrite(element->data, element->size, 1, fp1);
			if(buf != NULL && disableDataIndex == 0)
			{
				memcpy(&buf[n*2048], element->data, 2048);
				n++;
			}
			else
			{
				disableDataIndex--;
			}
			//readyCh4 = 1;

			// usleep(100);
			//printf("[[sendData]] send length %d\n", length);
			free(element->data);
			free(element);
		}
		
	}
	ret = 50;
}
else
{
	while(n < queueLength)
	{
		pthread_mutex_lock(&singleQueueMutex); 
		element = dequeue(gLinkedQueue);
		
		pthread_mutex_unlock(&singleQueueMutex);							
		if(element != NULL)
		{
			if(element == NULL)
				printf("[[sendData]] elementis NULL\n");					
			if(element->data == NULL)
				printf("[[sendData]] element->data is NULL\n"); 				
			
				//fwrite(element->data, element->size, 1, fp1);
			if(buf != NULL)
			{
				memcpy(&buf[n*2048], element->data, 2048);
				n++;
			}
			//readyCh4 = 1;

			// usleep(100);
			//printf("[[sendData]] send length %d\n", length);
			free(element->data);
			free(element);
		}
		
	}
	//pthread_mutex_unlock(&ch4QueueMutex);							
	ret = queueLength;
}
#endif/*#if 0*/
return 1;



}

int readDataFromADModulesbySPI(int fd, int channelNum, LinkedQueue* queue)
{
	int ret;
	int i, pullingSampleCnt = 0;
	sampleNumber =  requestCnt(fd); 
//	printf("readData CH:%d, sampleCnt = %d\n", channelNum,  sampleNumber);
	struct timeval	startTime, endTime;

#ifdef SAMPLECNT_DEBUG
  	printf(" SampleCnt : %d\n", sampleNumber); 
#endif 
#if 0
if(sampleNumber > 0)
{
	gettimeofday( &startTime, NULL );
	printf("start count : %d     time:	 %ld:%ld\n", sampleNumber, startTime.tv_sec, startTime.tv_usec);
}
#endif
//jonsama end
//printf("[[readData]] channel : %d dataCount :%d\n", channelNum, sampleNumber);
  if ( sampleNumber > 0 && sampleNumber <= MAX_SAMPLE )
  { 
 	if ( sampleNumber == ( MAX_SAMPLE -1 ) ) 
 		printf("[[OVERRUN(%d)]] \n", fd); 
 	
	
  	requestData_Start(fd, sampleNumber); 
/*
    	if(channelNum == 1)
			ch1CountTest += sampleNumber;
		else if(channelNum == 2)
			ch2CountTest += sampleNumber;
		else if(channelNum == 3)
			ch3CountTest += sampleNumber;
		else if(channelNum == 4)
			ch4CountTest += sampleNumber;

printf("ch1:%d	ch2:%d	ch3:%d	ch4:%d\n", ch1CountTest, ch2CountTest, ch3CountTest, ch4CountTest);
*/
		
  	for ( i = 0 ; i < sampleNumber ; i++) // 512 sample(2048byte) * 400 = 204800 sample/s/ch = 51.2kS/s 
  	{
  	/*
		if(getRequestedStopStreamingStatus() == DEF_TRUE)
		{
			
			DBG("Stopped by request PC\n");
			requestData_End(fd); 
			isNeedSetADMode = DEF_TRUE;
			AD_DisableModule(channelNum); 		
			return 0;
		}
		*/
  	  ret = requestData_ReadBuffer(fd, queue, channelNum, printOption, sampleNumber, i);
    	
  	  if( ret < 0 )
  	  {
  //  		requestData_End(fd); 	
  //  		break; 
  	  }	

  	}
  	
  	requestData_End(fd); 
  } 
  else
  {
	//if ( sampleNumber == 0 )
	//	printf(" No Data(%d)\n", fd); 
		
  	//goto sample_count_error;
  }
  
#if 0
	if(sampleNumber > 0)
	{
		gettimeofday( &endTime, NULL );
		printf("end count : %d	   time:   %ld:%ld\n", sampleNumber, endTime.tv_sec, endTime.tv_usec);
	}
#endif
	return ret;
}


#if 0
int readData_withSaving(int fd, FILE* fp,  int channelNum, LinkedQueue* queue)
{
	int ret;
	int i, pullingSampleCnt = 0;
	sampleNumber =  requestCnt(fd); 

#ifdef SAMPLECNT_DEBUG
  	printf(" SampleCnt : %d\n", sampleNumber); 
#endif 

//jonsama end
//printf("[[readData]] channel : %d dataCount :%d\n", channelNum, sampleNumber);
  if ( sampleNumber > 0 && sampleNumber <= MAX_SAMPLE )
  { 
 	if ( sampleNumber == ( MAX_SAMPLE -1 ) ) 
 		printf(" OVERRUN(%d) \n", fd); 
 	
	
  	requestData_Start(fd, sampleNumber); 
    	
  	for ( i = 0 ; i < sampleNumber ; i++) // 512 sample(2048byte) * 400 = 204800 sample/s/ch = 51.2kS/s 
  	{
		if(getRequestedStopStreamingStatus() == DEF_TRUE)
		{
			
			DBG("Stopped by request PC\n");
			requestData_End(fd); 
			isNeedSetADMode = DEF_TRUE;
			AD_DisableModule(channelNum); 		
			return 0;
		}
		requestData_ReadBuffer_withSaving(fd, fp, queue, printOption, sampleNumber, i);
    
  	  if( ret < 0 )
  	  {
  //  		requestData_End(fd); 	
  //  		break; 
  	  }	
  	}
  	
  	requestData_End(fd); 
  } 
  else
  {
	if ( sampleNumber == 0 )
		printf(" No Data(%d)\n", fd); 
		
  	//goto sample_count_error;
  }
	return 1;
}

#endif


int streamingData(int socket_fd, int channelIndex) 
{
#if 0
  int i, j, ret; 
  int channel_fd;
#ifdef DEBUG	
  printf(" Recodig Start to %s. \n", filename); 
#endif 
 int sampleCnt;
	 FILE *ffdx;
	
	//ffdx = fopen("ad_test.dat", "ab+");
int mode;
	channel_fd = getADFileDiscription(channelIndex);

   	

	
	AD_EnableModule(channelIndex);			
	//mode = getADMode(channel_fd);
	//if(mode  != 1)
	//setADMode(channel_fd, 1); 
  sampleNumber =  requestCnt(channel_fd); 

#if 1//def SAMPLECNT_DEBUG
  	printf(" Channel %d ,channelFd %d,  SampleCnt : %d\n", channelIndex, channel_fd, sampleNumber); 
#endif 

//jonsama end

  if ( sampleNumber > 0 && sampleNumber <= MAX_SAMPLE )
  { 
 	if ( sampleNumber == ( MAX_SAMPLE -1 ) ) 
	{
 		printf(" %d OVERRUN  \n", channel_fd); 
	}
 	else
	{
 		printf(" %d sampleNumber \n", sampleNumber); 		
	}
  	requestData_Start(channel_fd, sampleNumber); 
    
  	for ( i = 0 ; i < sampleNumber ; i++) // 512 sample(2048byte) * 400 = 204800 sample/s/ch = 51.2kS/s 
  	{
		if(getRequestedStopStreamingStatus() == DEF_TRUE)
		{
			initRequestedStopStreamingStatus();
			DBG("Stopped by request PC\n");
			requestData_End(channel_fd); 
			
			AD_DisableModule(channelIndex); 		
			return 0;
		}
  	
  	  ret = requestData_ReadNet(channel_fd, socket_fd, ffdx, 0);
    
  	  if( ret < 0 )
  	  {
  //  		requestData_End(fd); 	
  //  		break; 
  	  }	
  	}
  	
  	requestData_End(channel_fd); 
	
  } 
  else
  {
	if ( sampleNumber == 0 )
		{
		DBG("No Data\n"); 
		}
	else
		{
		DBG("Only %d Data\n", sampleNumber); 
		}
  	//goto sample_count_error;
  }
  
  AD_DisableModule(channelIndex);		
  ret  = 1;
// request_ready(fdx); 
  
//  gADMode = getADMode(fdx); 
  
//  DBG(" gADMode : %d\n", gADMode); 
  //if ( gADMode == 2 )
 
//	   AD_DisableModule(channel_fd);
	 //  setADMode(fdx, 1); 
  

 // sleep(1);
  
 DBG(" Streaming Fininshed.\n"); 
   // if ( gADMode == 2 )
       //setADMode(fdx, 1);  
#ifdef DEBUG	  
  printf(" Recoding Fininshed.\n"); 
#endif 
 // fclose(ffdx);   
  //jonsama
  //printf(" Recording Finished : %d\n",ret ); 
  
  return ret; 
//jonsama
sample_count_error:
 // printf(" sample count error\n"); 
 #endif
  return 1;
}

void PrintRegisters(int fd)
{
  int i; 
  
  //requestReg_ReadAll(fd);
  
  for(i=0;i<NB_REGISTER;i++)
  {
    printf("  R%d : 0x%x\n", i, REGDATA_inner[i]); 
  }
}

void PrintRegister(int fd, int num)
{
  printf("  R%d : 0x%x\n", num, REGDATA_inner[num]); 
}



void readRegister(int fd, int flag)
{
	if ( flag ) 
	{
	  requestReg_ReadAll(fd);
	  PrintRegisters(fd); 
	}  
	else 
	{
		if (rnumber == REGISTER_ADMODE_ID) {
			gADMode = getADMode(fd); 
	
		#ifdef DEBUG	
			printf("Mode : %d\n", gADMode); 			
		#else
			printf("%d", gADMode); 	
		#endif 
		 
			
		} else  {
			requestReg_Read(fd, rnumber);
	  
			if (rnumber == 0) 
				printf("%d\n", REGDATA_inner[0]);
			else  
				PrintRegister(fd, rnumber); 	
		}
	}
}

void init(int fd, int argc, char *argv[])
{
  printf("\nRead Module Registers \n");
  printf("===================== \n"); 
  readRegister(fd, 1);
  
  if(argc == 1) {
    print_usage(argv[0]);
  }
}

int setRegister(int fd)
{
  int ret = 0; 
  
  if(!flag) 
    REGDATA_inner[rnumber] = rdata; 
  else 
  {
    char szData[50] = {0}; 
    int i, cnt;
    uint32_t data, index; 
    FILE* rf; 
        
    rf = fopen(filename,"r");
        
    if(rf) {
      
      if ( fgetc(rf) == '#') {
        
        for ( i = 0; i < NB_REGISTER ; i++)
        {
          fscanf(rf, "%d%d", &index, &data); 
          printf("  index : %d,\t data : %d\n", index, data);
              
          REGDATA_inner[index] = data; 
        }  
      }
      else {
        printf("This file is not register of module\n"); 
        ret = -1; 
      }
    }
    fclose(rf); 
  }  

  if(ret != -1)   
  {
    if (flag) 
    {
      requestReg_SetAll(fd, REGDATA_inner);
      setADMode(fd, REGDATA_inner[0]); 
    }
    else   
      requestReg_Set(fd, rnumber, REGDATA_inner[rnumber]);
  }
}


char* getDeviceName(const char *device)
{
	if(!strcmp(device, "/dev/spidev1.0"))
		return "AD1";
	else  if(!strcmp(device, "/dev/spidev1.1"))
		return "AD2"; 
	else  if(!strcmp(device, "/dev/spidev1.2"))
		return "AD3"; 
	else  if(!strcmp(device, "/dev/spidev1.3"))
		return "AD4";
	else 
		return "NULL";	
} 

int getADFileDiscription(int channel)
{
	int ret_value;
	
	switch(channel)
	{
		case 1:
			ret_value = fd1;
		break;

		case 2:
			ret_value = fd2;
		break;

		case 3:
			ret_value = fd3;
			break;
		case 4:
			ret_value = fd4;
			break;
	}

	return ret_value;
}

int checkParamType(char* _type)
{
	int i = 0;
	for(;i< INDEX_MAX;i++)//INDEX_MAX+2
	{
		if(strstr(_type, gParamTypeArray[i]) != NULL)
		{
			printf(" PARAM TYPE : %s\n", gParamTypeArray[i]);
			return i;
		}

	}
}
int checkParamTypeMIC(char* _type)
{
	int i = 0;
	for(;i< INDEX_MIC_MAX;i++)//INDEX_MAX+2
	{
		if(strstr(_type, gParamTypeArrayMIC[i]) != NULL)
		{
			printf(" PARAM TYPE : %s\n", gParamTypeArrayMIC[i]);
			return i;
		}

	}
}

int checkRequiredParam(RequiredParam* _requiredParam, char* _filePath, AlarmCondition* cond)
{
	int i = 0;
	int typeIndex = 0;
	FILE *fpParam;
	char buf[512] = {0,};
	char token[] = ":,\n";
	char *p = NULL;
	int paramType = -1;
	
	fpParam  = fopen(_filePath, "r");
	fread(buf, sizeof(buf), 1, fpParam);
	p = strtok(buf, token);
		
	if(p != NULL)
	{
		paramType = checkParamType(p);
		if(paramType < INDEX_ALARM_VALUE)
		{
			_requiredParam[typeIndex].paramType = paramType;
			p = strtok(NULL, token);
			
			_requiredParam[typeIndex].paramF1 = atoi(p);
			p = strtok(NULL, token);
			_requiredParam[typeIndex].paramF2 = atoi(p); 	
			_requiredParam[typeIndex].hasParam = TRUE;				
		}
		else
		{
			if(paramType == INDEX_ALARM_VALUE)
			{
					p = strtok(NULL, token);
					cond->alarm1= atof(p);
					p = strtok(NULL, token);
					cond->alarm2= atof(p);		
				if(cond->alarmCheckType != TYPE_ALARM_RATE)
					cond->alarmCheckType = TYPE_ALARM_VALUE;
				
			}
			else if(paramType == INDEX_ALARM_RATE1)//needed 4 values
			{
				p = strtok(NULL, token);
				cond->alarm1_f1= atof(p);
				p = strtok(NULL, token);
				cond->alarm1_f2= atof(p);				
				
				if(cond->alarmCheckType != TYPE_ALARM_VALUE)					
					cond->alarmCheckType = TYPE_ALARM_RATE;					
			}
			else if(paramType == INDEX_ALARM_RATE2)
			{
				p = strtok(NULL, token);
				cond->alarm2_f1= atof(p);
				p = strtok(NULL, token);
				cond->alarm2_f2= atof(p);				
				if(cond->alarmCheckType != TYPE_ALARM_VALUE)					
					cond->alarmCheckType = TYPE_ALARM_RATE;					
			}
			else if(paramType == INDEX_BASELINE)
			{
				p = strtok(NULL, token);
				
				nBaselineCount = atoi(p);
				nBaselineCount = nBaselineCount*60*5;
				printf("!!Baseline Count : %d\n", nBaselineCount);
			}
			else if(paramType == INDEX_SENSOR_VLT)
			{
				p = strtok(NULL, token);
				
				//nSensorVoltage= atoi(p);
				cond->sensorVoltage = atoi(p);
			}
			
		}
	}
	

	while(p != NULL)
	{
	
		if(p != NULL)
		{
			if(paramType < INDEX_ALARM_VALUE)
				typeIndex++;
		
			p = strtok(NULL, token);
			if(p != NULL)
			{
				paramType = checkParamType(p);

				if(paramType < INDEX_ALARM_VALUE)
				{
					_requiredParam[typeIndex].paramType = paramType;
					if(p != NULL)
					{
						p = strtok(NULL, token);
						if(p != NULL)
						_requiredParam[typeIndex].paramF1 = atoi(p);
					}
					
					if(p != NULL)
					{
						p = strtok(NULL, token);
						if(p != NULL)
						_requiredParam[typeIndex].paramF2 = atoi(p);
					}
					
					_requiredParam[typeIndex].hasParam = TRUE;				

				}
				else
				{
					if(paramType == INDEX_ALARM_VALUE)
					{
						printf("alarmCheckType init Value:%d\n", cond->alarmCheckType);
						
							p = strtok(NULL, token);
							cond->alarm1= atof(p);
							p = strtok(NULL, token);
							cond->alarm2= atof(p);		
						if(cond->alarmCheckType != TYPE_ALARM_RATE)
							cond->alarmCheckType = TYPE_ALARM_VALUE;
					}
					else if(paramType == INDEX_ALARM_RATE1)//needed 4 values
					{
						p = strtok(NULL, token);
						cond->alarm1_f1= atof(p);
						p = strtok(NULL, token);
						cond->alarm1_f2= atof(p);				
						
						if(cond->alarmCheckType != TYPE_ALARM_VALUE)					
							cond->alarmCheckType = TYPE_ALARM_RATE; 				
					}
					else if(paramType == INDEX_ALARM_RATE2)
					{
						p = strtok(NULL, token);
						cond->alarm2_f1= atof(p);
						p = strtok(NULL, token);
						cond->alarm2_f2= atof(p);				
						if(cond->alarmCheckType != TYPE_ALARM_VALUE)					
							cond->alarmCheckType = TYPE_ALARM_RATE; 				
					}
					else if(paramType == INDEX_BASELINE)
					{
						p = strtok(NULL, token);
						
						nBaselineCount = atoi(p);
						nBaselineCount = nBaselineCount * 60*5;
						
					}
					else if(paramType == INDEX_SENSOR_VLT)
					{
						p = strtok(NULL, token);
						
						//nSensorVoltage= atoi(p);
						cond->sensorVoltage = atoi(p);
					}
					
				}
			}
		}
		
		
		
	}
	fclose(fpParam);
	return typeIndex;
}


int checkRequiredParamMIC(RequiredParam* _requiredParam, char* _filePath, AlarmCondition* cond)
{
	int i = 0;
	int typeIndex = 0;
	FILE *fpParam;
	char buf[512] = {0,};
	char token[] = ":,\n";
	char *p = NULL;
	int paramType = -1;
	
	fpParam  = fopen(_filePath, "r");
	fread(buf, sizeof(buf), 1, fpParam);
	p = strtok(buf, token);
		
	if(p != NULL)
	{
		paramType = checkParamTypeMIC(p);
		
		if(paramType < INDEX_MIC_ALARM_VALUE)
		{
			_requiredParam[typeIndex].paramType = paramType;
			p = strtok(NULL, token);
			
			_requiredParam[typeIndex].paramF1 = atoi(p);
			p = strtok(NULL, token);
			_requiredParam[typeIndex].paramF2 = atoi(p); 	
			_requiredParam[typeIndex].hasParam = TRUE;				
		}
		else
		{
			if(paramType == INDEX_MIC_ALARM_VALUE)
			{
					p = strtok(NULL, token);
					cond->alarm1= atof(p);
					p = strtok(NULL, token);
					cond->alarm2= atof(p);		
				if(cond->alarmCheckType != TYPE_ALARM_RATE)
					cond->alarmCheckType = TYPE_ALARM_VALUE;
				
			}
			else if(paramType == INDEX_MIC_ALARM_RATE1)//needed 4 values
			{
				p = strtok(NULL, token);
				cond->alarm1_f1= atof(p);
				p = strtok(NULL, token);
				cond->alarm1_f2= atof(p);				
				
				if(cond->alarmCheckType != TYPE_ALARM_VALUE)					
					cond->alarmCheckType = TYPE_ALARM_RATE;					
			}
			else if(paramType == INDEX_MIC_ALARM_RATE2)
			{
				p = strtok(NULL, token);
				cond->alarm2_f1= atof(p);
				p = strtok(NULL, token);
				cond->alarm2_f2= atof(p);				
				if(cond->alarmCheckType != TYPE_ALARM_VALUE)					
					cond->alarmCheckType = TYPE_ALARM_RATE;					
			}
			else if(paramType == INDEX_MIC_SENSOR_VLT)
			{
				p = strtok(NULL, token);
				
				//nSensorVoltage= atoi(p);
				cond->sensorVoltage = atoi(p);
			}
		}
	}	

	while(p != NULL)
	{
	
		if(p != NULL)
		{
			if(paramType < INDEX_MIC_ALARM_VALUE)
				typeIndex++;
		
			p = strtok(NULL, token);
			if(p != NULL)
			{
				paramType = checkParamTypeMIC(p);
				if(paramType < INDEX_MIC_ALARM_VALUE)
				{

					_requiredParam[typeIndex].paramType = paramType;
					if(p != NULL)
					{
						p = strtok(NULL, token);
						if(p != NULL)
						_requiredParam[typeIndex].paramF1 = atoi(p);
					}
					
					if(p != NULL)
					{
						p = strtok(NULL, token);
						if(p != NULL)
						_requiredParam[typeIndex].paramF2 = atoi(p);
					}
					_requiredParam[typeIndex].hasParam = TRUE;									
				}
				else
				{
					if(paramType == INDEX_MIC_ALARM_VALUE)
					{
							p = strtok(NULL, token);
							cond->alarm1= atof(p);
							p = strtok(NULL, token);
							cond->alarm2= atof(p);		
						if(cond->alarmCheckType != TYPE_ALARM_RATE)
							cond->alarmCheckType = TYPE_ALARM_VALUE;
						
					}
					else if(paramType == INDEX_MIC_ALARM_RATE1)//needed 4 values
					{
						p = strtok(NULL, token);
						cond->alarm1_f1= atof(p);
						p = strtok(NULL, token);
						cond->alarm1_f2= atof(p);				
						
						if(cond->alarmCheckType != TYPE_ALARM_VALUE)					
							cond->alarmCheckType = TYPE_ALARM_RATE; 				
					}
					else if(paramType == INDEX_MIC_ALARM_RATE2)
					{
						p = strtok(NULL, token);
						cond->alarm2_f1= atof(p);
						p = strtok(NULL, token);
						cond->alarm2_f2= atof(p);				
						if(cond->alarmCheckType != TYPE_ALARM_VALUE)					
							cond->alarmCheckType = TYPE_ALARM_RATE; 				
					}
					else if(paramType == INDEX_MIC_SENSOR_VLT)
					{
						p = strtok(NULL, token);
						
						//nSensorVoltage= atoi(p);
						cond->sensorVoltage = atoi(p);
					}
				}



			}
		}
		
		
	}
	fclose(fpParam);
	return typeIndex;
}



int checkADModule(void)						
{
	char buf[256]; 
	int ret = 0; 
#if 1//ndef FEATURE_SINGLE_CHANNEL
	fd1 = AnalogModule_Start(device1);  
	printf("fd1 : %d\n", fd1);
	AD_EnableModule(MODULE_AD1);
	ret = requestState_Read(fd1); 
		printf("ret : %d\n", ret);
	if ( ret != 0 && ret != -1) { 
	//if ( requestState_Read(fd1) != 0 && requestState_Read ) {
		deviceflag = deviceflag | FLAG_AD1 ; 
		sprintf(buf, "%s/%s", filename, "AD1.dat");
		//printf("%s\n", buf); 	
		fp1 = fopen(buf, "ab+"); 
	} else {
		printf (" -- Not Found. %s.\n", getDeviceName(device1)); 		
		AnalogModule_Error(); 
		AnalogModule_Stop(fd1);
	}  
	AD_DisableModule(MODULE_AD1); 

	fd2 = AnalogModule_Start(device2); 
	AD_EnableModule(MODULE_AD2); 
        ret = requestState_Read(fd2);
        
        if ( ret != 0 && ret != -1) {
              
	//if ( requestState_Read(fd2) != -1 ) {
		deviceflag = deviceflag | FLAG_AD2 ; 
		sprintf(buf, "%s/%s", filename, "AD2.dat"); 
		//printf("%s\n", buf); 
		fp2 = fopen(buf, "ab+"); 
	} else  {
		printf (" -- Not Found. %s.\n", getDeviceName(device2)); 		
 		AnalogModule_Error(); 
		AnalogModule_Stop(fd2);
	}  
	AD_DisableModule(MODULE_AD2);
	
	fd3 = AnalogModule_Start(device3); 
	AD_EnableModule(MODULE_AD3); 
      	
      	ret = requestState_Read(fd3);
        if ( ret != 0 && ret != -1) {
//	if ( requestState_Read(fd3) != -1 ) {
		deviceflag = deviceflag | FLAG_AD3 ;
		sprintf(buf, "%s/%s", filename, "AD3.dat");
		//printf("%s\n", buf);
		fp3 = fopen(buf, "ab+"); 		
	} else {
		printf (" -- Not Found. %s.\n", getDeviceName(device3)); 		
		AnalogModule_Error(); 
		AnalogModule_Stop(fd3);

	}  
	AD_DisableModule(MODULE_AD3);
#endif/*FEATURE_SINGLE_CHANNEL*/
	fd4 = AnalogModule_Start(device4); 
	AD_EnableModule(MODULE_AD4); 
      	
      	ret = requestState_Read(fd4);
        if ( ret != 0 && ret != -1) {
	//if ( requestState_Read(fd4) != -1 ) {
		deviceflag = deviceflag | FLAG_AD4 ; 
		sprintf(buf, "%s/%s", filename, "AD4.dat");
		//printf("%s\n", buf);
		fp4 = fopen(buf, "ab+"); 
	} else {
		printf (" -- Not Found. %s.\n", getDeviceName(device4)); 		
		AnalogModule_Error(); 
		AnalogModule_Stop(fd4);
	}  
	AD_DisableModule(MODULE_AD4);
	
	return deviceflag; 
}
#if 1
int main(int argc, char *argv[])
{
	int fd = -1;  

	
	fd = openADC("/dev/spidev2.0"); 
	printf(" fd = %d\n", fd); 

	close(fd); 
}
#else

int main(int argc, char *argv[])
{

	int ret = 0, i,j=0;
	int size;
	pthread_t server_pthread;
	int fd;
	int rpm = 0;
	char* serial[16]={0,};
	char *err_msg = 0;
	char *szErrMsg = NULL;
	sqlite3_stmt *res;
	int rc;
	
	unsigned char* APATH=NULL;
	unsigned char* A_PATH=NULL;
	unsigned char* PARAM_PATH = NULL;
	int valA;
	int valB;
	char paramPath[128];
	char dbPath[128];
	char paramConfigPath[128];	
	parse_opts(argc, argv);

	if(runf == PROCESS_AUTOSAVE)
	{	
	APATH = getenv("APATH");
	A_PATH = getenv("A_PATH");
	PARAM_PATH = getenv("PARAM_PATH");

	sqlite3_initialize();
	memset(dbPath, 0, sizeof(dbPath));
	sprintf(dbPath,"%s/a1.db",A_PATH);
	printf("DB PATH:%s\n", dbPath);	
	getdoubledt();

	rc = sqlite3_open_v2(dbPath, &db,SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
//		rc = sqlite3_exec(db, "COMMIT;", 0, 0, &err_msg);
//		rc = sqlite3_exec(db, "PRAGMA journal_mode=DELETE;", 0, 0, &err_msg);
	rc = sqlite3_exec(db, "PRAGMA journal_mode=wal;", 0, 0, &err_msg);
	rc = sqlite3_exec(db, "PRAGMA wal_autocheckpoint;", 0,0 , &err_msg);
//		sqlite3_wal_checkpoint_v2(&db,NULL,SQLITE_CHECKPOINT_FULL, &valA, &valB);
printf("journal_mode : %d\n", rc);

	rc = sqlite3_exec(db, "PRAGMA temp_store=1;", 0,0, &err_msg);
	printf("temp_store:%d\n", rc);

//	rc = sqlite3_exec(db, "BEGIN;", 0,0, &err_msg);
//	printf("BEGIN rc:%d\n", rc);
	alarmContionCH1.alarmCheckType = TYPE_ALARM_NONE;
	alarmContionCH2.alarmCheckType = TYPE_ALARM_NONE;
	alarmContionCH3.alarmCheckType = TYPE_ALARM_NONE;
	alarmContionCH4.alarmCheckType = TYPE_ALARM_NONE;
	
	memset(paramPath, 0, sizeof(paramPath));	
	sprintf(paramPath,"%s/param1.conf",PARAM_PATH);
	printf("param2.conf path : %s\n", paramPath);
	gParamCountCH1 = checkRequiredParam(requiredParamCH1, paramPath, &alarmContionCH1);
	memset(paramPath, 0, sizeof(paramPath));
	
	sprintf(paramPath,"%s/param2.conf",PARAM_PATH);
	printf("param2.conf path : %s\n", paramPath);
	gParamCountCH2 = checkRequiredParam(requiredParamCH2, paramPath, &alarmContionCH2);
	if(alarmContionCH2.alarmCheckType == TYPE_ALARM_NONE)
	{
		alarmContionCH2.alarm1 = alarmContionCH1.alarm1;
		alarmContionCH2.alarm2 = alarmContionCH1.alarm2;		
		alarmContionCH2.alarm1_f1= alarmContionCH1.alarm1_f1;
		alarmContionCH2.alarm1_f2= alarmContionCH1.alarm1_f2;		
		alarmContionCH2.alarm2_f1= alarmContionCH1.alarm2_f1;
		alarmContionCH2.alarm2_f2= alarmContionCH1.alarm2_f2;		
		alarmContionCH2.alarmCheckType = alarmContionCH1.alarmCheckType;
	}
	memset(paramPath, 0, sizeof(paramPath));
	sprintf(paramPath,"%s/param3.conf",PARAM_PATH);
	gParamCountCH3 = checkRequiredParam(requiredParamCH3, paramPath, &alarmContionCH3);
	if(alarmContionCH3.alarmCheckType == TYPE_ALARM_NONE)
	{
		alarmContionCH3.alarm1 = alarmContionCH1.alarm1;
		alarmContionCH3.alarm2 = alarmContionCH1.alarm2;		
		alarmContionCH3.alarm1_f1= alarmContionCH1.alarm1_f1;
		alarmContionCH3.alarm1_f2= alarmContionCH1.alarm1_f2;		
		alarmContionCH3.alarm2_f1= alarmContionCH1.alarm2_f1;
		alarmContionCH3.alarm2_f2= alarmContionCH1.alarm2_f2;		
		alarmContionCH3.alarmCheckType = alarmContionCH1.alarmCheckType;
	}	
	memset(paramPath, 0, sizeof(paramPath));
	sprintf(paramPath,"%s/param4.conf",PARAM_PATH);
	gParamCountCH4 = checkRequiredParamMIC(requiredParamCH4, paramPath, &alarmContionCH4);
	if(alarmContionCH4.alarmCheckType == TYPE_ALARM_NONE)
	{
		alarmContionCH4.alarm1 = alarmContionCH1.alarm1;
		alarmContionCH4.alarm2 = alarmContionCH1.alarm2;		
		alarmContionCH4.alarm1_f1= alarmContionCH1.alarm1_f1;
		alarmContionCH4.alarm1_f2= alarmContionCH1.alarm1_f2;		
		alarmContionCH4.alarm2_f1= alarmContionCH1.alarm2_f1;
		alarmContionCH4.alarm2_f2= alarmContionCH1.alarm2_f2;		
		alarmContionCH4.alarmCheckType = alarmContionCH1.alarmCheckType;
	}

	//CH1 ALARM CONFIGURATION
	if(alarmContionCH1.alarmCheckType == TYPE_ALARM_VALUE)
		printf("CH1:%f, %f\n", alarmContionCH1.alarm1,  alarmContionCH1.alarm2);
	else{
		printf("CH1:%f, %f\n", alarmContionCH1.alarm1_f1,  alarmContionCH1.alarm1_f2);
		printf("CH1:%f, %f\n", alarmContionCH1.alarm2_f1,  alarmContionCH1.alarm2_f2);		
	}
	//CH2 ALARM CONFIGURATION
	if(alarmContionCH2.alarmCheckType == TYPE_ALARM_VALUE)
		printf("CH2:%f, %f\n", alarmContionCH2.alarm1,  alarmContionCH2.alarm2);
	else{
		printf("CH2:%f, %f\n", alarmContionCH2.alarm1_f1,  alarmContionCH2.alarm1_f2);
		printf("CH2:%f, %f\n", alarmContionCH2.alarm2_f1,  alarmContionCH2.alarm2_f2);		
	}
	//CH3 ALARM CONFIGURATION
	if(alarmContionCH3.alarmCheckType == TYPE_ALARM_VALUE)
		printf("CH3:%f, %f\n", alarmContionCH3.alarm1,  alarmContionCH3.alarm2);
	else{
		printf("CH3:%f, %f\n", alarmContionCH3.alarm1_f1,  alarmContionCH3.alarm1_f2);
		printf("CH3:%f, %f\n", alarmContionCH3.alarm2_f1,  alarmContionCH3.alarm2_f2);		
	}
	//CH4 ALARM CONFIGURATION
	if(alarmContionCH4.alarmCheckType == TYPE_ALARM_VALUE)
		printf("CH4:%f, %f\n", alarmContionCH4.alarm1,  alarmContionCH4.alarm2);
	else{
		printf("CH4:%f, %f\n", alarmContionCH4.alarm1_f1,  alarmContionCH4.alarm1_f2);
		printf("CH4:%f, %f\n", alarmContionCH4.alarm2_f1,  alarmContionCH4.alarm2_f2);		
	}

	printf("BaseLine Count : %d\n", nBaselineCount);
	printf("Sensor Voltage1 : %d\n", alarmContionCH1.sensorVoltage);
	printf("Sensor Voltage2 : %d\n", alarmContionCH2.sensorVoltage);
	printf("Sensor Voltage3 : %d\n", alarmContionCH3.sensorVoltage);
	printf("Sensor Voltage4 : %d\n", alarmContionCH4.sensorVoltage);

	//Check BASELINE var value & CHANNEL1_BASELINE_TB table
	if(bCleanTable == TRUE)
	{
		printf("Clean all tables\n");
		SQLITE_CleanTables();
	}
	else
	{
		printf("Do not clean all tables\n");		
	}
	
	bHasBaselineCh1 =  SQLITE_CheckBaselineDataTables(TABLE_NAME_CHANNEL1_BASELINE, gMeanBaselineParamCh1, gStdevBaselineParamCh1, gParamCountCH1, requiredParamCH1);
	bHasBaselineCh2 =  SQLITE_CheckBaselineDataTables(TABLE_NAME_CHANNEL2_BASELINE, gMeanBaselineParamCh2, gStdevBaselineParamCh2, gParamCountCH2, requiredParamCH2);
	bHasBaselineCh3 =  SQLITE_CheckBaselineDataTables(TABLE_NAME_CHANNEL3_BASELINE, gMeanBaselineParamCh3, gStdevBaselineParamCh3, gParamCountCH3, requiredParamCH3);
	bHasBaselineCh4 =  SQLITE_CheckBaselineDataTablesMIC(TABLE_NAME_CHANNEL4_BASELINE, gMeanBaselineParamCh4, gStdevBaselineParamCh4, gParamCountCH4, requiredParamCH4);

	nInitDataCountCh1 =  SQLITE_CheckInitDataTables(TABLE_NAME_CHANNEL1_INIT_DATA, gParamCountCH1, requiredParamCH1);
	nInitDataCountCh2 =  SQLITE_CheckInitDataTables(TABLE_NAME_CHANNEL2_INIT_DATA, gParamCountCH2, requiredParamCH2);
	nInitDataCountCh3 =  SQLITE_CheckInitDataTables(TABLE_NAME_CHANNEL3_INIT_DATA, gParamCountCH3, requiredParamCH3);
	nInitDataCountCh4 =  SQLITE_CheckInitDataTablesMIC(TABLE_NAME_CHANNEL4_INIT_DATA, gParamCountCH4, requiredParamCH4);

	SQLITE_CheckDataTables(TABLE_NAME_CHANNEL1_REALTIME_DATA, 1);
	SQLITE_CheckDataTables(TABLE_NAME_CHANNEL2_REALTIME_DATA, 2);
	SQLITE_CheckDataTables(TABLE_NAME_CHANNEL3_REALTIME_DATA, 3);
	SQLITE_CheckDataTables(TABLE_NAME_CHANNEL4_REALTIME_DATA, 4);
	SQLITE_CheckDataTables(TABLE_NAME_CHANNEL1_PRE_MAINT_DATA, 5);
	SQLITE_CheckDataTables(TABLE_NAME_CHANNEL2_PRE_MAINT_DATA, 6);
	SQLITE_CheckDataTables(TABLE_NAME_CHANNEL3_PRE_MAINT_DATA, 7);
	SQLITE_CheckDataTables(TABLE_NAME_CHANNEL4_PRE_MAINT_DATA, 8);
					


	printf("===============================================================================\n");
	printf("				INIT DATA COUNT CH1:%d CH2:%d CH3:%d CH4:%d\n", nInitDataCountCh1, nInitDataCountCh2, nInitDataCountCh3, nInitDataCountCh4);
	printf("===============================================================================\n");

	
	getdoubledt();
	
	if(equipmentNumber == -1)
	{
		printf("*Equipment Number error*\n");
		return -1;
	}
}	
	if(!AD_PowerCheck(verbose))
	{
		printf("now, AD_Power - Off\n"); 

		// Sensor ERR LED ON 
		//AnalogModule_Error();
		ret = -1; 
		
//		printf("%d\n", ret); 
		//return ret; 
	}

	adDataInit();
	
#ifdef FEATURE_MQTT
	node_init();
#endif

#ifdef FEATURE_SERIAL
	fdSerial = open(DEV_SERIAL, O_RDWR|O_NOCTTY);

	if(fdSerial < 0)
	{
		fprintf(stderr, "ERR\n");
		exit(-1);
	}

	memset( &newtio, 0, sizeof(newtio)); 

	newtio.c_cflag = B115200; 
	newtio.c_cflag |= CS8; 
	newtio.c_cflag |= CLOCAL; 
	newtio.c_cflag |= CREAD; 
	newtio.c_iflag = IGNPAR; 
	// newtio.c_iflag = ICRNL; 
	newtio.c_oflag = 0; 
	//newtio.c_lflag = 0; 
	newtio.c_lflag = ~(ICANON | ECHO | ECHOE | ISIG);	
	newtio.c_cc[VTIME] = 0; 
	newtio.c_cc[VMIN] = 0; 

	tcflush (fdSerial, TCIFLUSH); 
	tcsetattr(fdSerial, TCSANOW, &newtio); 
	usleep(500000);
	
#endif/*FEATURE_SERIAL*/
	
	pthread_mutex_init(&writeFileMutex, NULL);

	if ( runf == PROCESS_AUTOSAVE )
	{
#ifdef FEATURE_TACHO	
		if(pthread_create(&tacho_pthread, NULL, checkRPM, NULL) != 0) {
			DBG("could not launch another client thread\n");
			return NULL;
		}					
#endif
		getdoubledt();
		adDataStart();

#if 1
		//fftw_init_threads();
		//		fftw_plan_with_nthreads(4);

		if(pthread_create(&ad1_fft_pthread, NULL, AD1_fft_execute, NULL) != 0) {
			DBG("could not launch another client thread AD1\n");
			return;
		}		
#if 1		
		if(pthread_create(&ad2_fft_pthread, NULL, AD2_fft_execute, NULL) != 0) {
			DBG("could not launch another client thread AD2\n");
			return;
		}

		if(pthread_create(&ad3_fft_pthread, NULL, AD3_fft_execute, NULL) != 0) {
			DBG("could not launch another client thread AD3\n");
			return;
		}
#if 1//def FEATURE_MIC		
		if(pthread_create(&ad4_fft_pthread, NULL, AD4_fft_execute, NULL) != 0) {
			DBG("could not launch another client thread AD4\n");
			return;
		}			
#endif /*FEATIRE_MIC*/		
#endif
#ifdef FEATURE_SERIAL
		if(pthread_create(&serial_pthread, NULL, SerialSendThread, NULL) != 0) {
			DBG("Exit Serial Thread\n");
			return;
		}					
#endif/*FEATURE_SERIAL*/

#endif/*1*/

#ifdef FEATURE_LPF
		if(pthread_create(&ad1_lpf_pthread, NULL, AD1_LowPassFilter_execute, NULL) != 0) {
			DBG("could not launch ad1_lpf_pthread\n");
			return NULL;
		}					
		if(pthread_create(&ad2_lpf_pthread, NULL, AD2_LowPassFilter_execute, NULL) != 0) {
			DBG("could not launch ad2_lpf_pthread\n");
			return NULL;
		}
		if(pthread_create(&ad3_lpf_pthread, NULL, AD3_LowPassFilter_execute, NULL) != 0) {
			DBG("could not launch ad3_lpf_pthread\n");
			return NULL;
		}
		if(pthread_create(&ad4_lpf_pthread, NULL, AD4_LowPassFilter_execute, NULL) != 0) {
			DBG("could not launch ad4_lpf_pthread\n");
			return NULL;
		}				
#endif

#ifdef FEATURE_MQTT
				if(pthread_create(&ad1_lpf_pthread, NULL, AD1_Publish_thread, NULL) != 0) {
					DBG("could not launch ad1_lpf_pthread\n");
					return NULL;
				}			

				if(pthread_create(&ad2_lpf_pthread, NULL, AD2_Publish_thread, NULL) != 0) {
					DBG("could not launch ad2_lpf_pthread\n");
					return NULL;
				}

				if(pthread_create(&ad3_lpf_pthread, NULL, AD3_Publish_thread, NULL) != 0) {
					DBG("could not launch ad3_lpf_pthread\n");
					return NULL;
				}

				if(pthread_create(&ad4_lpf_pthread, NULL, AD4_Publish_thread, NULL) != 0) {
					DBG("could not launch ad4_lpf_pthread\n");
					return NULL;
				}				
#endif
		readADDataThread();

	}

	switch (runf) {
		case PROCESS_STREAM:
		{
			int fd;
			char* serial[16]={0,};
			//adDataInit();

			fd = open("/dev/mtd0", O_RDONLY);
			if(fd != -1)
			{
				lseek(fd, 0x30025, 0);
				read(fd, serial, 12);
				DBG("Serial : %s\n", serial);
			}
			 //pthread_t readThread;

			// if(pthread_create(&readThread, NULL, readADDataThread, NULL) != 0) {

			//					DBG("could not launch another client thread\n");

			// return NULL;

			//}



			//pthread_create(&server_pthread, NULL, server_thread, serial);

			//pthread_join(server_pthread, NULL);

			

		}

		break;



					

		case PROCESS_SETREG: 

			

			AD_EnableModule(midx); 

			setRegister(fdx);

			AD_DisableModule(midx); 

		break; 

	

		case PROCESS_READREG: 

			AD_EnableModule(midx); 

#ifdef DEBUG	

			printf("read reg : %d \n"); 

#endif       

			readRegister(fdx, flag); 

			AD_DisableModule(midx); 

		break; 

		

		case PROCESS_MODESET:

             AD_EnableModule(midx);

			setADMode(fdx, gADMode); 

		  	AD_DisableModule(midx); 

		break; 

	}  // switch 

	

	AD_DisableModule(midx); 

	AnalogModule_Stop(fdx);  

return ret;

}


#endif 



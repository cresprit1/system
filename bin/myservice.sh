#!/bin/sh 

source /root/.profile

pName = "myservice"

if [ $1 = "start" ]
then 
	echo start
	
	pCount=$(ps -ef | grep $pName | grep -v grep | grep -v $pName.sh | wc -l) 
	if [ $pCount -eq 0 ]
	then 
		nohup /root/project/OSLO/myservice.sh 1> /dev/null 2>&1 &
		rm -f /root/project/OSLO/myservice.pid 
		ps -ef | grep $pName | grep -v grep | grep -v $pName.sh | awk '{print $2}' >> /root/project/OSLO/myservice.pid 
	fi 

elif [ $1 = "stop" ]
then 
	echo stop
	kill -i `ps -ef | grep $pName | grep -v grep | grep -v $pName.sh | awk '{print $2}'`
else
	echo 'must input a valid parameter.'
	echo 'Usage : NT_callProcessed.sh {start|stop}'
fi 

cd /root/project/OSLO/system_git

node requestDeviceInfo.js 

node sendDeviceInfo.js 


echo "+++++++++++++++"
echo " Start Servcie "
echo "+++++++++++++++"

/root/project/OSLO/system_git/blockSW/BL_S_1/a.out #app.js


#!/bin/sh
sleep 3
wlanIP=$(ip addr show wlan0 | awk '/inet / {print $2}' | cut -d/ -f 1)
ethIP=$(ip addr show eth0 | awk '/inet / {print $2}' | cut -d/ -f 1)
ethState=$(cat /sys/class/net/eth0/operstate)
wlanState=$(cat /sys/class/net/wlan0/operstate)
connectedIf="none"


#do rdate


if [ -n "$wlanIP" -a "$wlanState" = "up" ] ; then
connectedIf="wlan"
fi

if [ -n "$ethIP" -a "$ethState" = "up" ] ; then
if [ "$connectedIf" = "wlan" ] ; then
connectedIf="both"
else
connectedIf="eth"
fi
fi
echo "$connectedIf"

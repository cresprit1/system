#ifndef __SMART_H__
#define __SMART_H__

#define  SIGIO_IOCTL_MAGIC  '1'

#define SIGIO_SET	_IO(SIGIO_IOCTL_MAGIC, 0)
#define SIGIO_CLEAR	_IO(SIGIO_IOCTL_MAGIC, 1)
//#define LED_RED_ON  _IO(LED_IOCTL_MAGIC, 1)
//#define LED_RED_OFF  _IO(LED_IOCTL_MAGIC, 2)
//#define LED_ON_OFF  _IOW(LED_IOCTL_MAGIC, 7, int)

#define SIGIO_IOCTL_MAX  8

#endif // __SMART_H__

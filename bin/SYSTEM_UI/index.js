var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs');
var async = require('async');

process.env.NPATH=process.argv[2]; 
var W_INTERFACE=process.argv[3]; 

console.log(process.env.NPATH); 
console.log(W_INTERFACE); 

//var spawn = require('child_process').spawn,subprocess;
var exec = require('child_process').exec;
//var INIT_PATH = process.env.IPATH;
var INIT_PATH = './init/';
var index = 0;
var temp = 0;

var dataSize = 256;
var bufferSize = dataSize * 2;
var value = new Array(bufferSize / 2);
var apArray = new Array();
var dataList;
var index2 = 0;
const WLANINFO_DONE = 1;
const ETHINFO_DONE = 2;
const APLIST_DONE = 4;
const SSID_DONE = 8;
const TYPE_DONE = 16;
const CHECK_IF_DONE = 32;
const CHECK_IF_TYPE_DONE = 64;
const CHECK_AUTH_DONE = 128;
const IF_WLAN = 1;
const IF_ETHERNET = 2;

const CHANNEL1 = 1;
const CHANNEL2 = 2;
const CHANNEL3 = 4;
const CHANNEL4 = 8;
const SETSELECTEDCHANNEL = 16;
const SETCHANNELNAME = 32;
const GETSELECTEDCHANNEL = 16;
const GETCHANNELNAME = 32;
const RUNNING_COLLECTION_DATA = 1;
const RUNNING_STREAM_DATA = 2;

const CMD_CHECK_RUNNING_PROCESS = 0;	
const CMD_START_COLLECTING_DATA = 1;	
const CMD_START_STREAMING_DATA = 2;	
const CMD_RESTART_COLLECTING_DATA = 3;	
const CMD_RESTART_STREAMING_DATA = 4;
const CMD_STOP_PROCESSING = 5;
const CMD_MAX = 6;

const CMD_STATUS_WRONG_COMMAND = -2;
const CMD_STATUS_NOK = -1;
const CMD_STATUS_OK = 0;
const CMD_STATUS_ALREADY_RUNNING = 1;


var index3 = 0;
var connected = 0;
var flagDhcpInfoJob = 0;
var flagKillProcess = 0;
var resJsonDataOk = JSON.stringify({ 'result': { 'status': 'ok' } });
var resJsonDataErr = JSON.stringify({ 'result': { 'status': 'error' } });

var engine = require('ejs-locals');
app.engine('ejs', engine);
app.set('views', __dirname + '/public');
app.set('view engine', 'ejs'); //adding



function testfunction(data) {};

function on_child_stdout(data) {
    var k = 0;
    var temp = 0;
    var number;
    console.log('str :' + data);
};
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', function(req, res, next) {
    res.render('index');
});

app.get('/index.html', function(req, res) {
    res.render('index');
});

app.get('/network2.html', function(req, res) {
    res.render('network2');
}); 

app.get('/host_setup.html', function(req, res, next) {
    res.render('host_setup');
});

app.get('/network_setup.html', function(req, res) {
    // child = exec('./runADWebServer.sh', function(error, stdout, stderr) {
    //     child.stdout.setEncoding('utf8');
    //     child.stdout.on('data', on_child_stdout);
    //     child.stderr.on('data', on_child_stdout);
    //     child.on('exit', testfunction);
    // });
    res.render('network_setup');
});

app.get('/checkIsStoppedProcess', function(req, res) {
    var checkIsStoppedProcess = {
        'checkIsStoppedProcess': {
            'status': flagKillProcess,
        }
    };
    console.log("checkIsStoppedProcess:" + flagKillProcess);
    var dataString = JSON.stringify(checkIsStoppedProcess);
    res.send(dataString);
});

app.get('/checkRunningProcess', function(req, res) {
    var query_crontab = "crontab -l";
    var query_4chStream = "ps | grep \"ADStreamServer\" | grep -v grep";
    var runningProcess = 0;
	var isCompleteCheckCollectingData = false;
	var isCompleteCheckStreamingData = false;

    exec(query_4chStream, function(error, stdout, stderr) {

        if (error != null) {
            console.log("error"+error);
        } else {
            check = stdout.indexOf('ADStreamServer');
			
            if (check == -1) {
                console.log("4 Channel Streaming is not running");
            } else {
                console.log("4 Channel Streaming is running");
                runningProcess |= RUNNING_STREAM_DATA;
            }
        }
		isCompleteCheckStreamingData = true;
    });

    exec(query_crontab, function(error, stdout, stderr) {

        ch4Mode = stdout;
        if (error != null) {
			console.log("crontab is not running");
        } else {
            check = stdout.indexOf('autosave');
            if (check == -1) {
                console.log("Manager_Sensor is not running");
            } else {
                console.log("Manager_Sensor is running");
				runningProcess |= RUNNING_COLLECTION_DATA;
			}
                    }
		isCompleteCheckCollectingData = true;
                });

	
		var interval = setInterval(function() {
			
	        if(isCompleteCheckCollectingData == true && isCompleteCheckStreamingData == true) {
	            isCompleteCheckCollectingData = false;
	            isCompleteCheckStreamingData = false;
				console.log("runningProcess :"+runningProcess);			
	            var running_process = {
	                'running_process': {
	                    'status': runningProcess,
            }
	            };
				
	            var dataString = JSON.stringify(running_process);
	            res.send(dataString);
	            clearInterval(interval);
					
	            console.log("+++++++++++++++++++Complete+++++++++++++++++++++++");
        }
	    }, 500);
	
});


app.post('/cmdEzConn', function(req, res){
	var result = 0;
	var jsonData='';
	var i = 0;
	var isCompleteCheckCollectingData = false;
	var isCompleteCheckStreamingData = false;
    var query_crontab = "crontab -l";
    var query_4chStream = "ps | grep \"ADStreamServer\" | grep -v grep";
	var query_killWebStream = "ps | grep \"ADWebStreamServer\" | grep -v grep | awk \'{print $1}\' | xargs kill -9 > /dev/null";
    var query_start_CollectingData = "$SYSTEM/bin/Manager_Sensor.sh start&";
    var query_start_StreamingData = "$RTPATH/runADServer.sh&";

	var isKilledWebStreamingData = false;
	var isAlreadyRunning = false;
	
	console.log("req.body.json:"+req.body.json);	
	jsonData = JSON.parse(req.body.json);
	if(jsonData.cmd > CMD_CHECK_RUNNING_PROCESS && jsonData.cmd < CMD_MAX){
		isAlreadyRunning = false;
		exec(query_killWebStream, function(error, stdout, stderr) {

		        isKilledWebStreamingData = true;
    });

    exec(query_4chStream, function(error, stdout, stderr) {

        if (error != null) {
            console.log("error");
					console.log("Realtime is not running");

					isCompleteCheckStreamingData = true;
        } else {
            check = stdout.indexOf('ADStreamServer');
					
            if (check == -1) {
                console.log("4Channel Streaming is not running");
						isCompleteCheckStreamingData = true;
            } else {
		            	if(jsonData.cmd == CMD_START_COLLECTING_DATA || jsonData.cmd == CMD_RESTART_STREAMING_DATA || jsonData.cmd == CMD_STOP_PROCESSING)
	            		{
                var query_4chStreamStop = "ps | grep \"ADStreamServer\" | grep -v grep | awk \'{print $1}\' | xargs kill -9 > /dev/null";
                exec(query_4chStreamStop, function(error, stdout, stderr) {
                    if (error == null) {
			                       console.log("stop realtime stdout:"+stdout);
                        } else {
			                        console.log("4ch Streaming Data process was killed");

			                    }
								isCompleteCheckStreamingData = true;
			                });
	            		}
						else{
							isCompleteCheckStreamingData = true;
							isAlreadyRunning = true;						
                        }
		            }
		        }
		});
		exec(query_crontab, function(error, stdout, stderr) {

			ch4Mode = stdout;
			if (error != null) {
				isCompleteCheckCollectingData = true;
				console.log("crontab is not running");
			} else {
				check = stdout.indexOf('autosave');
				if (check == -1) {
					isCompleteCheckCollectingData = true;
					console.log("Manager_Sensor is not running");
                    } else {
		            	if(jsonData.cmd == CMD_START_STREAMING_DATA || jsonData.cmd == CMD_RESTART_COLLECTING_DATA || jsonData.cmd == CMD_STOP_PROCESSING){
							var query_mgrStop = "$SYSTEM/bin/Manager_Sensor.sh stop";
		                	exec(query_mgrStop, function(error, stdout, stderr) {
			                    if (error == null) {
									console.log("stop crontab stdout:"+stdout);
			                    } else {
			                        console.log("Can't kill Manager_Sensor process");
                    }
		                        isCompleteCheckCollectingData = true;
                });
            }
						else{
							isAlreadyRunning = true;				
							isCompleteCheckCollectingData = true						
						}
						
				}
        }
    });

    var interval = setInterval(function() {

	        if (isCompleteCheckCollectingData == true && isCompleteCheckStreamingData == true && isKilledWebStreamingData == true) {
		            isCompleteCheckCollectingData = false;
		            isCompleteCheckStreamingData = false;
					isKilledWebStreamingData = false;

				if(jsonData.cmd == CMD_STOP_PROCESSING){
		            var checkingprocess = {
		                'running_process': {
		                    'status': CMD_STATUS_OK ,//alreadyRunning
		                }
		            };
					var dataString = JSON.stringify(checkingprocess);
		            res.send(dataString);
						
				}
				else if(isAlreadyRunning == true){
		            var checkingprocess = {
		                'running_process': {
		                    'status': CMD_STATUS_ALREADY_RUNNING ,//alreadyRunning
		                }
		            };
					var dataString = JSON.stringify(checkingprocess);
		            res.send(dataString);

				}
				else{
					var query_cmd;
					console.log("cmd:"+jsonData.cmd);
					if(jsonData.cmd == CMD_START_COLLECTING_DATA || jsonData.cmd == CMD_RESTART_COLLECTING_DATA )
						query_cmd = query_start_CollectingData;
				 	else 
						query_cmd = query_start_StreamingData;
					
						exec(query_cmd, function(error, stdout, stderr){
								console.log("start query_cmd stdout:"+stdout);

	                        if(error == null)
	                        {
								/*	                        	
            var checkingprocess = {
					                'running_process': {
					                    'status': CMD_STATUS_OK ,//complete
					                }
					            };

								var dataString = JSON.stringify(checkingprocess);
					            res.send(dataString);
					            */
					            isCompleteCheckCollectingData = false;
					            isCompleteCheckStreamingData = false;
								isKilledWebStreamingData = false;
					            
								console.log("sent data1");

								if(jsonData.cmd == CMD_START_COLLECTING_DATA || jsonData.cmd == CMD_RESTART_COLLECTING_DATA)
		                            console.log("Manager_Sensor is running");
								else
									console.log("Realtime Streaming is running");					           							
	                        }
	                        else
	                        {
	                        	/*
    				            var checkingprocess = {
					                'running_process': {
					                    'status': CMD_STATUS_NOK ,//complete
					                }
					            };
								var dataString = JSON.stringify(checkingprocess);
								res.send(dataString);
								*/
								isCompleteCheckCollectingData = false;
								isCompleteCheckStreamingData = false;
								isKilledWebStreamingData = false;

								
								console.log("sent data2");
								if(jsonData.cmd == CMD_START_COLLECTING_DATA || jsonData.cmd == CMD_RESTART_COLLECTING_DATA)
		                            console.log("Can't start Manager_Sensor");
								else
									console.log("Can't start Realtime Streaming");

	                        }
	                    });			
    				            var checkingprocess = {
					                'running_process': {
					                    'status': CMD_STATUS_OK ,//complete
                }
            };

            var dataString = JSON.stringify(checkingprocess);
            res.send(dataString);
					}

            clearInterval(interval);


				
            console.log("+++++++++++++++++++Complete+++++++++++++++++++++++");
        }

    }, 500);
	}
	else{
	            var checkingprocess = {
	                'running_process': {
	                    'status': CMD_STATUS_WRONG_COMMAND ,
	                }
	            };
	            var dataString = JSON.stringify(checkingprocess);
	            res.send(dataString);
	}
});


app.get('/resumeProcessRunningnKillWebStream', function(req, res) {
    var query_killWebStream = "ps | grep \"ADWebStreamServer\" | grep -v grep | awk \'{print $1}\' | xargs kill -9 > /dev/null";
    var query_resume_CollectingData = "$SYSTEM/bin/Manager_Sensor.sh start";
    var query_resume_StreamingData = "$APATH/ezConn/runADServer.sh";


    exec(query_killWebStream, function(error, stdout, stderr) {
        console.log("stdout :" + stdout);
        console.log("error :" + error);
        ch4Mode = stdout;
        if (error != null && error != "") {
            console.log("Can't kill WebStreamServer");
            res.send(resJsonDataErr);
        } else {
            if (flagKillProcess & RUNNING_COLLECTION_DATA) {
                exec(query_resume_CollectingData);
                /*
                        exec(query_resume_CollectingData, function(error, stdout, stderr){

                        if(error == null)
                        {
                            res.send(resJsonDataOk);
                            console.log("Manager_Sensor is running again");
                        }
                        else
                        {
                            res.send(resJsonDataErr);
                            console.log("Can't start Manager_Sensor");
                        }
                    });
                */
            } else if (flagKillProcess & RUNNING_STREAM_DATA ) {
                exec(query_resume_StreamingData);
                /*
                exec(query_resume_StreamingData, function(error, stdout, stderr){
                    if(error == null)
                    {
                        res.send(resJsonDataOk);
                        console.log("4 Channel Streaming is running again");
                    }
                    else
                    {
                        res.send(resJsonDataErr);
                        console.log("Can't start 4 Channel Streaming");
                    }
                });
                */
            }
            res.send(resJsonDataOk);
            flagKillProcess = 0;
        }
    });

});


app.get('/resumeProcessRunning', function(req, res) {
    var query_resume_CollectingData = "$SYSTEM/bin/Manager_Sensor.sh start&";
    var query_resume_StreamingData = "$APATH/ezConn/runADServer.sh&";

    if (flagKillProcess & RUNNING_COLLECTION_DATA) {
        exec(query_resume_CollectingData, function(error, stdout, stderr) {
            if (error == null) {
                console.log("Manager_Sensor is running again");
            } else {
                console.log("Can't start Manager_Sensor");
            }
        });
        res.send(resJsonDataOk);

    } else if (flagKillProcess & RUNNING_STREAMING_DATA) {
        exec(query_resume_StreamingData, function(error, stdout, stderr) {
            if (error == null) {
                console.log("4 Channel Streaming is running again");
            } else {

                console.log("Can't start 4 Channel Streaming");
            }
        });
        res.send(resJsonDataOk);
    } else {
        res.send(resJsonDataOk);
    }
    flagKillProcess = 0;

});
app.get('/getAnalogConfig', function(req, res){
	var query_spi_power = "$APATH/spipower.sh ";
	var query1 = "$APATH/readReg AD1 99";
	var query2 = "$APATH/readReg AD2 99";
	var query3 = "$APATH/readReg AD3 99";
	var query4 = "$APATH/readReg AD4 99";
	var query5 = "cat config/sel_a_ch.cfg";
	
	var ch1Mode = 0;
	var ch2Mode = 0;
	var ch3Mode = 0;
	var ch4Mode = 0;
	
	var ch1status = 'disable';
	var ch2status = 'disable';
	var ch3status = 'disable';
	var ch4status = 'disable';
	
	var ch1Name = '';
	var ch2Name = '';
	var ch3Name = '';
	var ch4Name = '';

	var result = 0;
	var selectedChannel = 0;
	
	exec(query_spi_power+"0", function(error, stdout, stderr) {
		console.log("spi power off :"+stdout);		
			
		exec(query_spi_power+"1", function(error, stdout, stderr) {
			console.log("spi power on :"+stdout);		
				
			async.parallel([
				function(callback){
					exec(query1, function(error, stdout, stderr) {
						console.log("ch1 mode :"+stdout);		
						ch1Mode = stdout;
						
						exec(query2, function(error, stdout, stderr) {
							console.log("ch2 mode :"+stdout);		
							ch2Mode = stdout;
							
							exec(query3, function(error, stdout, stderr) {
								console.log("ch3 mode :"+stdout);		
								ch3Mode = stdout;
								
								exec(query4, function(error, stdout, stderr) {
									console.log("ch4 mode :"+stdout);		
									ch4Mode = stdout;
									
									callback(error, CHANNEL1);
								});
							});
						});
					});
				},


				
				function(callback){
					fs.exists(ANALOG_SELECTED_CHANNEL_CFG_FILE_PATH, function(exists){
						console.log("sel_a_ch.cfg exists:"+exists);
						if(exists == true)
						{
							fs.readFile(ANALOG_SELECTED_CHANNEL_CFG_FILE_PATH, function(error, data) {
								if(error == null)
								{
									var selectedChannels = Number(data);
									if( selectedChannels & CHANNEL1)
										ch1status = "enable";
									if( selectedChannels & CHANNEL2)
										ch2status = "enable";
									if( selectedChannels & CHANNEL3)	
										ch3status = "enable";						
									if( selectedChannels & CHANNEL4)		
										ch4status = "enable";									
								}
								else
								{
									console.log("sel_a_ch.cfg:"+error);
								}
									console.log("3 callback");				
									callback(error, GETSELECTEDCHANNEL);				
							});
						}
						else
						{
							console.log("3 No sel_a_ch.cfg file");	
							callback(null, GETSELECTEDCHANNEL);				
						}
					});
				},
				function(callback){
					fs.exists(ANALOG_CFG_FILE_PATH, function(exists){
						if(exists == true)
						{
							fs.readFile(ANALOG_CFG_FILE_PATH, function(error, data) {
									try{
										var jsonNames = JSON.parse(data);
										ch1Name = jsonNames.ch1.name;
										console.log("ch1Name in a.cfg file:"+ch1Name);
										ch2Name = jsonNames.ch2.name;
										ch3Name = jsonNames.ch3.name;
										ch4Name = jsonNames.ch4.name;
									}catch(exception){
										
										callback(error, GETCHANNELNAME);
										return;
									}
									
									console.log("called callback with ch1 Name:"+ch1Name);
									callback(error, GETCHANNELNAME);
							});
						}
						else
						{
							callback(error, GETCHANNELNAME);
						}
					});
				}
			],
		
			function(err, job){
				
				console.log(job);

				var jsonStr = {
					'analogConfigInfo': {
											'ch1':{
												'name':ch1Name,
												'status':ch1status,
												'mode':ch1Mode,
												},
											'ch2':{
												'name':ch2Name,
												'status':ch2status,
												'mode':ch2Mode,
												},
											'ch3':{
												'name':ch3Name,
												'status':ch3status,
												'mode':ch3Mode,
												},																	
											'ch4':{
												'name':ch4Name,
												'status':ch4status,
												'mode':ch4Mode,
												},
											
										}
					};

					var jsonData = JSON.stringify(jsonStr);
					res.send(jsonData);
					
					result = 0;
				
				});
		});
	});
});



app.post('/setAnalogConfig', function(req, res){
	var query1 = "$APATH/setMode AD1 ";
	var query2 = "$APATH/setMode AD2 ";
	var query3 = "$APATH/setMode AD3 ";
	var query4 = "$APATH/setMode AD4 ";

	var result = 0;
	var jsonData='';
	var i = 0;
	
	console.log("req.body.json:"+req.body.json);	
	var jsonData = JSON.parse(req.body.json);
	
	async.parallel([
		function(callback){
			exec(query1+jsonData.analogConfigInfo.ch1.mode, function(error, stdout, stderr) {
				console.log("ch1 mode :"+stdout+"  error :"+error);	
				exec(query2+jsonData.analogConfigInfo.ch2.mode, function(error, stdout, stderr) {
					console.log("ch2 mode :"+stdout+"  error :"+error);			
					exec(query3+jsonData.analogConfigInfo.ch3.mode, function(error, stdout, stderr) {
						console.log("ch3 mode :"+stdout+"  error :"+error);				
							exec(query4+jsonData.analogConfigInfo.ch4.mode, function(error, stdout, stderr) {
							console.log("ch4 mode :"+stdout+"  error :"+error);				
							callback(error, CHANNEL1);
						});
					});
				});
			});
		},

		function(callback){
			var channelStatus = 0;
			if(jsonData.analogConfigInfo.ch1.status == "enable")
				channelStatus |= CHANNEL1;
			if(jsonData.analogConfigInfo.ch2.status == "enable")
				channelStatus |= CHANNEL2;	
			if(jsonData.analogConfigInfo.ch3.status == "enable")
				channelStatus |= CHANNEL3;	
			if(jsonData.analogConfigInfo.ch4.status == "enable")
				channelStatus |= CHANNEL4;					
			fs.writeFile(ANALOG_SELECTED_CHANNEL_CFG_FILE_PATH, channelStatus , function(err){
				callback(err, SETSELECTEDCHANNEL);
			});
		},
		
		function(callback){
			fs.readFile(ANALOG_CFG_FILE_PATH, function(error, data) {

					if (error)
					{
						console.log(error);		

						var jsonFile = {
									'ch1':{ 'name':jsonData.analogConfigInfo.ch1.name},
									'ch2':{ 'name':jsonData.analogConfigInfo.ch2.name},
									'ch3':{ 'name':jsonData.analogConfigInfo.ch3.name},
									'ch4':{'name':jsonData.analogConfigInfo.ch4.name}
						};
						
						jsonStr1 = JSON.stringify(jsonFile);
						fs.writeFile(ANALOG_CFG_FILE_PATH, jsonStr1 , function(err){
							if(err == null)
							{			
								console.log("err is null");
								exec('sync', function(error, stdout, stderr){
									callback(err, SETCHANNELNAME);	
								});
								
							}
							else
							{
								console.log("err:"+err);	
								callback(err, SETCHANNELNAME);											
							}
						});
					}
					else
					{
						var i=0;
						var jsonStr1='';
						var jsonFile = JSON.parse(data);
						console.log(jsonFile);
						

						jsonFile.ch1.name = jsonData.analogConfigInfo.ch1.name;
						jsonFile.ch2.name = jsonData.analogConfigInfo.ch2.name;
						jsonFile.ch3.name = jsonData.analogConfigInfo.ch3.name;
						jsonFile.ch4.name = jsonData.analogConfigInfo.ch4.name;
						
						jsonStr1 = JSON.stringify(jsonFile);
						console.log("-----------a.cfg result:"+jsonStr1);				
						fs.writeFile(ANALOG_CFG_FILE_PATH, jsonStr1 , function(err){
							if(err == null)
							{			
								console.log("err is null");
								exec('sync', function(error, stdout, stderr){
									callback(err, SETCHANNELNAME);												
								});
								
							}
							else
							{
								callback(err, SETCHANNELNAME);	
								console.log("err:"+err);	
							}
						});
					}
				});
			}
		],
		
		function(err, job){
			
			console.log("err:"+err);
			console.log("job:"+job);
			
			var resJsonData = null;
			
			if(err != '' && err != null)
				resJsonData = JSON.stringify({'result':{'status':'error', 'errmsg':err}});
			else
				resJsonData = JSON.stringify({'result':{'status':'ok'}});
			res.send(resJsonData);


		});
					
});


app.get('/getDhcpNetworkInfo', function(req, res) {
    var ssid = null;
    var wlanAddr = null;
    var ethAddr = null;
    var macAddr = null;
    var subnetMask = null;
    var connectedInterface = null;
    var authentification = null;
    var type = "";

    var query = "iwlist "+W_INTERFACE+" scanning | awk '/ESSID/' |cut -d : -f2 ";
    exec(query, function(error, stdout, stderr) {
        console.log("aplist:" + stdout);
        var aplist = stdout;
        var apName;
        var index = 0;
        var interfaceType = '';

        while (aplist.length > 0) {
            var start = aplist.indexOf("\"");
            var end = aplist.indexOf("\n");

            apName = aplist.slice(start + 1, end - 1);
            if (apName.length != 0)
                apArray[index++] = apName;
            console.log("apName:" + apName + "apNameLength:" + apName.length);

            aplist = aplist.slice(end + 1, aplist.length);
        }

        flagDhcpInfoJob += APLIST_DONE;
        for (i in apArray)
            console.log('ap[' + i + '] :' + apArray[i]);
    });

    var query2 = "ifconfig "+W_INTERFACE;
    exec(query2, function(error, stdout, stderr) {
        var pos = stdout.indexOf("HWaddr ");
        var mac = stdout.slice(pos + 7, pos + 24);
        console.log("mac:" + mac);
        pos = stdout.indexOf("addr:");
        var addrBody = stdout.slice(pos + 5, stdout.length);
        pos = addrBody.indexOf(" ");
        wlanAddr = addrBody.slice(0, pos);
        console.log('addr:' + wlanAddr);
        flagDhcpInfoJob += WLANINFO_DONE;

    });

    var query3 = "ifconfig eth0";
    exec(query3, function(error, stdout, stderr) {
        var pos = stdout.indexOf("HWaddr ");
        var end = stdout.indexOf("\n");
        macAddr = stdout.slice(pos + 7, end - 2);
        console.log("eth0 mac:" + macAddr);
        var addrBody = stdout.slice(end + 1, stdout.length);
        var pos = addrBody.indexOf("inet addr");

        if (pos != -1) {
            var end = addrBody.indexOf("Bcast");
            ethAddr = addrBody.slice(pos + 10, end - 2);
            console.log("eth0 addr:" + ethAddr);

            var pos = addrBody.indexOf("Mask:");
            var end = addrBody.indexOf("\n");
            subnetMask = addrBody.slice(pos + 5, end);
            console.log("mask :" + subnetMask);
        }
        flagDhcpInfoJob += ETHINFO_DONE;
    });

    var query4 = "iwconfig"
    exec(query4, function(error, stdout, stderr) {


        var wlanPos = stdout.indexOf("wlan1 ");
        var wlanBody = stdout.slice(wlanPos, stdout.length);

	console.log(wlanBody); 

        var pos = wlanBody.indexOf("ESSID:");
        var end = wlanBody.indexOf("Nickname");
        ssid = wlanBody.slice(pos + 7, end - 3);
        console.log("pos:" + pos + "end:" + end + " ssid:" + ssid);
        flagDhcpInfoJob += SSID_DONE;

    });

    console.log("NPTAH:" + process.env.NPATH); 
    var query5 = "cat $NPATH/config/network.conf";
    exec(query5, function(error, stdout, stderr) {
        type = stdout;

        console.log("type:" + type);
        flagDhcpInfoJob += TYPE_DONE;

    });

    var query6 = "$NPATH/checkConnectedIf.sh";
    exec(query6, function(error, stdout, stderr) {
        connectedInterface = stdout;


        console.log("connectedIf:" + connectedInterface);
        flagDhcpInfoJob += CHECK_IF_DONE;

    });

    var query7 = "cat $NPATH/config/interfaceType.conf";
    exec(query7, function(error, stdout, stderr) {
        interfaceType = stdout;

        console.log("connectedIf:" + connectedInterface);
        flagDhcpInfoJob += CHECK_IF_TYPE_DONE;

    });
	
//	var query8 = "cat /mnt/mmc/snow/.wpa2.conf";
	var query8 = "cat $NPATH/config/wpa_supplicant.conf"; 

    exec(query8, function(error, stdout, stderr) {
		var auth = stdout;
        authentification 
		if(auth.indexOf("psk") != -1)
			authentification = "WPA/WPA2";
		else if(auth.indexOf("wep_key") != -1)
			authentification = "WEP";
		else
		{
			if(auth.indexOf("key_mgmt") != -1)
				authentification = "OPEN";
		}
		
        console.log("connectedIf:" + connectedInterface);
        flagDhcpInfoJob += CHECK_AUTH_DONE;

    });
	
    var interval = setInterval(function() {

        if (flagDhcpInfoJob & WLANINFO_DONE && flagDhcpInfoJob & ETHINFO_DONE 
		&& flagDhcpInfoJob & APLIST_DONE && flagDhcpInfoJob & SSID_DONE 
		&& flagDhcpInfoJob & TYPE_DONE && flagDhcpInfoJob & CHECK_IF_DONE 
		&& flagDhcpInfoJob & CHECK_IF_TYPE_DONE && flagDhcpInfoJob & CHECK_AUTH_DONE) {
            var dhcpNetInfo = {
                'dhcpNetInfo': {
                    'type': type,
                    'connnectedIf': connectedInterface,
                    'interfaceType': interfaceType,
                    'wlan': {
                        'ssid': ssid,
                        'ipAddr': wlanAddr,
                        'Auth': authentification,
                        'apList': apArray,
                    },
                    'eth': {
                        'macAddr': macAddr,
                        'ipAddr': ethAddr,
                        'subnetMask': subnetMask,
                        'gateway': "gateway",
                    },
                }
            };
            var dataString = JSON.stringify(dhcpNetInfo);
            res.send(dataString);
            clearInterval(interval);
            flagDhcpInfoJob = 0;
            console.log("+++++++++++++++++++Complete+++++++++++++++++++++++");
        } else {
            console.log("flagDhcpInfoJob:" + flagDhcpInfoJob);
        }
    }, 500);

});

app.get('/getStaticEthNetworkInfo', function(req, res) {
//    var query = "cat /etc/network/interfaces";
/** STATIC ETH0 (Fixed) **/

    var query = ""; // "cat /etc/network/interfaces.d/eth0.cfg";

        var if_bit = 0;
        var ethAddr = "";
        var ethNetMask = "";
        var ethGw = "";
        var ethDns1 = "";
        var ethDns2 = "";

    if (fs.existsSync("/etc/network/interfaces.d/main_eth.cfg")) 
	query = "cat /etc/network/interfaces.d/main_eth.cfg"; 
    else if(fs.existsSync("/etc/network/interfaces.d/main_both.cfg")) 
	query = "cat /etc/network/interfaces.d/main_both.cfg"; 
    else
	query = "cat "+process.env.NPATH +"/config/default.cfg";

    console.log("query"+query); 

    exec(query, function(error, stdout, stderr) {
        console.log("static Info:" + stdout);

        var ethInterfaces_stdout = stdout;
        var ethInterfaces_pos = ethInterfaces_stdout.indexOf("iface eth0");
	var ethInterfacesBody = ethInterfaces_stdout.slice(ethInterfaces_pos, ethInterfaces_stdout.length); 
        var ethInterfacesEnd = ethInterfacesBody.indexOf("\n");
        var checkString = ethInterfacesBody.slice(0, ethInterfacesEnd);

	console.log("checkString : " + checkString); 
	console.log("ethInterfaceBody : " + ethInterfacesBody); 

        if (ethInterfaces_pos != -1)
            if_bit += IF_ETHERNET;

        if (if_bit & IF_ETHERNET) {

	 //if ( ethInterfaces_stdout.indexOf("static") != -1 )
	 if ( checkString.indexOf("static") != -1)
	 { 
            var ethPos = ethInterfacesBody.indexOf("address ");
            var ethBody = ethInterfacesBody.slice(ethPos, ethInterfacesBody.length);
            var ethEnd = ethBody.indexOf("\n");
            ethAddr = ethBody.slice(8, ethEnd);

            var maskPos = ethBody.indexOf("netmask ");
            var maskBody = ethBody.slice(maskPos, ethBody.length);
            var maskEnd = maskBody.indexOf("\n");
            ethNetMask = maskBody.slice(8, maskEnd);

            var gwPos = maskBody.indexOf("gateway ");
            var gwBody = maskBody.slice(gwPos, maskBody.length);
            var gwEnd = gwBody.indexOf("\n");
            ethGw = gwBody.slice(8, gwEnd);

            var dnsPos = gwBody.indexOf("dns-nameservers ");
            var dnsBody = gwBody.slice(dnsPos, gwBody.length);

	    if ( dnsBody.length > 1 ) 
	    { 
		console.log("dnsBody.length="+dnsBody.length); 
            	var dnsEnd = dnsBody.indexOf("\n");
            	var dnsArray=dnsBody.split(' ');
            	console.log(dnsArray[0]+','+dnsArray[1]+','+dnsArray[2]);

            	ethDns1 = dnsArray[1];
            	console.log("ethDns1:"+ethDns1+","+ethDns1.length);

            	var dnsEnd = dnsArray[2].indexOf("\n");
            	ethDns2 = dnsArray[2].slice(0, dnsEnd); //; // dns2Body.slice(2, dns2End);
            	console.log("ethDns2:"+ethDns2);
            }
          }
        }

        var staticNetInfo = {
            'staticNetInfo': {
                'eth': {
                    'ipAddr': ethAddr,
                    'subnetMask': ethNetMask,
                    'gateway': ethGw,
                    'dns1': ethDns1,
                    'dns2': ethDns2,
                },
            }
        };
        var dataString = JSON.stringify(staticNetInfo);
        res.send(dataString);
    }); 
});

app.get('/getStaticNetworkInfo', function(req, res) {
//    var query = "cat /etc/network/interfaces";

/** STATIC WLANx **/ 

    var query = ""; // "cat /etc/network/interfaces.d/"+W_INTERFACE+".cfg";

        var if_bit = 0;
        var wlanAddr = "";
        var wlanNetMask = "";
        var wlanGw = "";
        var wlanDns1 = "";
        var wlanDns2 = "";
        var ssid = "";
        var bssid = "";

    if (fs.existsSync("/etc/network/interfaces.d/main_wlan.cfg")) 
        query = "cat /etc/network/interfaces.d/main_wlan.cfg";
    else if (fs.existsSync("/etc/network/interfaces.d/main_both.cfg")) 
        query = "cat /etc/network/interfaces.d/main_both.cfg";
    else
        query = "cat "+process.env.NPATH +"/config/default.cfg",

    console.log(query); 
    exec(query, function(error, stdout, stderr) {
        console.log("static Info:" + stdout);

	var wlanInterfaces_stdout = stdout; 
        var wlanInterfaces = wlanInterfaces_stdout.indexOf("iface "+W_INTERFACE);
        console.log(wlanInterfaces); 
 
	var wlanInterfacesBody = wlanInterfaces_stdout.slice(wlanInterfaces, wlanInterfaces_stdout.length);  
	var wlanInterfacesEnd = wlanInterfacesBody.indexOf("\n"); 
        var checkString = wlanInterfacesBody.slice(0, wlanInterfacesEnd); 

	console.log(wlanInterfacesBody); 
  	if (wlanInterfaces != -1)
            if_bit += IF_WLAN;

            if (if_bit & IF_WLAN) {
            // if(wlanInterfaces_stdout.indexOf("static") != -1) 
             if ( checkString.indexOf("static") != -1) 
             {
                var wlanPos = wlanInterfacesBody.indexOf("address ");
                var wlanBody = wlanInterfacesBody.slice(wlanPos, wlanInterfacesBody.length);
                var wlanEnd = wlanBody.indexOf("\n");
                wlanAddr = wlanBody.slice(8, wlanEnd);

                var maskPos = wlanBody.indexOf("netmask ");
                var maskBody = wlanBody.slice(maskPos, wlanBody.length);
                var maskEnd = maskBody.indexOf("\n");
                wlanNetMask = maskBody.slice(8, maskEnd);

                var gwPos = maskBody.indexOf("gateway ");
                var gwBody = maskBody.slice(gwPos, maskBody.length);
                var gwEnd = gwBody.indexOf("\n");
                wlanGw = gwBody.slice(8, gwEnd);
/* 
                var dns1Pos = gwBody.indexOf("dns-nameservers ");
                var dns1Body = gwBody.slice(dns1Pos, gwBody.length);
                var dns1End = dns1Body.indexOf(" ");
                wlanDns1 = dns1Body.slice(16, dns1End);

                var dns2Pos = dns1Body.indexOf(" ");
                var dns2Body = dns1Body.slice(dns2Pos, dns1Body.length);
                var dns2End = dns2Body.indexOf("\n");
                wlanDns2 = dns2Body.slice(2, dns2End);
*/ 
            var dnsPos = gwBody.indexOf("dns-nameservers ");
            var dnsBody = gwBody.slice(dnsPos, gwBody.length);
	   
	    if ( dnsBody.length > 1 ) 
            {
            	var dnsEnd = dnsBody.indexOf("\n");
            	var dnsArray=dnsBody.split(' ');
            	console.log(dnsArray[0]+','+dnsArray[1]+','+dnsArray[2]);

            	wlanDns1 = dnsArray[1];
            	console.log("wlanDns1:"+wlanDns1);

            	var dnsEnd = dnsArray[2].indexOf("\n");
            	wlanDns2 = dnsArray[2].slice(0, dnsEnd); //; // dns2Body.slice(2, dns2End);
            	console.log("wlanDns2:"+wlanDns2);

          //  if (dnsArray[2].indexOf("wpa-ssid ") != -1) {
	    	if (dnsBody.indexOf("wpa-ssid ") != -1) {

                 //   var ssidPos = dnsArray[2].indexOf("wpa-ssid ");
		    console.log("dnsBody:" +dnsBody); 
                    var ssidPos = dnsBody.indexOf("wpa-ssid "); 
                    console.log("ssidPos:" + ssidPos);
                    var ssidBody = dnsBody.slice(ssidPos, dnsBody.length);
		    console.log("ssidBody:" + ssidBody); 
                    var ssidEnd = ssidBody.indexOf("\n");
                    ssid = ssidBody.slice(9, ssidEnd);

                    var bssidPos = ssidBody.indexOf("wpa-bssid ");
                    if (bssidPos != -1) {
                        var bssidBody = ssidBody.slice(bssidPos, ssidBody.length);
                        var bssidEnd = bssidBody.indexOf("\n");
                        console.log("bssidBody:" + bssidBody + " bssidEnd:" + bssidEnd);
                        bssid = bssidBody.slice(10, bssidEnd);
                    } else
                        bssid = '';
                    console.log("bssid:" + bssid);
                } else {
                    var ssidPos = dnsBody.indexOf("wireless-essid ");
                    console.log("wireless-essidPos:" + ssidPos);
                    var ssidBody = dnsBody.slice(ssidPos, dnsBody.length);
                    console.log("ssidBody:" + ssidBody);
                    var ssidEnd = ssidBody.indexOf("\n");
                    console.log("ssidEnd:" + ssidEnd);
                    if (ssidEnd != -1)
                        ssid = ssidBody.slice(15, ssidEnd);
                    else
                        ssid = ssidBody.slice(15, ssidBody.length);

                    var bssidPos = ssidBody.indexOf("wpa-bssid ");
                    if (bssidPos != -1) {
                        var bssidBody = ssidBody.slice(bssidPos, ssidBody.length);
                        var bssidEnd = bssidBody.indexOf("\n");
                        console.log("bssidBody:" + bssidBody + " bssidEnd:" + bssidEnd);
                        bssid = bssidBody.slice(10, bssidEnd);
                    } else
                        bssid = '';
                    console.log("bssid:" + bssid);
		  }
                }
            }  
        } 
        
	var staticNetInfo = {
            'staticNetInfo': {
                'wlan': {
                    'ssid': ssid,
                    'bssid': bssid,
                    'ipAddr': wlanAddr,
                    'auth': "auth",
                    'subnetMask': wlanNetMask,
                    'gateway': wlanGw,
                    'dns1': wlanDns1,
                    'dns2': wlanDns2,
                },
            }
        };
        var dataString = JSON.stringify(staticNetInfo);
        res.send(dataString);
    }); 
});

function writeInterfaceType(type) {
    //type can be both, wlan, eth
    fs.writeFile(process.env.NPATH + "/config/interfaceType.conf", type, function(err) {
        if (err == null) {
            exec('sync', function(error, stdout, stderr) {});
            console.log('Complete');
        } else
            console.log(err);
    });
}

function writeNetworkConf() {
    fs.writeFile(process.env.NPATH + "/config/network.conf", 'dhcp', function(err) {
        if (err == null) {
	    exec(process.env.NPATH+"/auto.sh", function(error, stdout, stderr) {}); 
            exec('sync', function(error, stdout, stderr) {});
            console.log('Complete');
        } else
            console.log(err);
    });
}


function writeInterfaceStatic(data, kindofinterface) {
	console.log("writeInterface"+kindofinterface); 
	fs.writeFile(process.env.NPATH+ "/config/main_"+kindofinterface+".cfg", data, function(err) {
 
 		var query = 'cp ' + process.env.NPATH + '/config/main_'+kindofinterface+'.cfg /etc/network/interfaces.d';
	console.log("writeInterfaceStatic:"+query); 

        if (err == null) {
            exec(query, function(error, stdout, stderr) {
                if (error == null) {
                    fs.writeFile(process.env.NPATH + "/config/network.conf", 'static', function(err) {
                        if (err == null) {
			    var query1= process.env.NPATH+'/auto_static.sh'; 
       			    console.log(query1);                      
			    exec(query1, function(error, stdout, stderr) {
				console.log(stdout)});
                            exec('sync', function(error, stdout, stderr) {});
                            console.log('Complete');
                        } else
                            console.log(err);

                    });
                }
            });
        } else
            console.log(err);
    });
}

function writeInterfaceDhcp(data, kindofinterface) {
        console.log("writeInterfaceDhcp"+kindofinterface);

        fs.writeFile(process.env.NPATH+ "/config/main_"+kindofinterface+".cfg", data, function(err) {

                var query = 'cp ' + process.env.NPATH + '/config/main_'+kindofinterface+'.cfg /etc/network/interfaces.d';
        console.log("writeInterfaceDhcp:"+query);

        if (err == null) {
            exec(query, function(error, stdout, stderr) {
                if (error == null) {
                    fs.writeFile(process.env.NPATH + "/config/network.conf", 'static', function(err) {
                        if (err == null) {
                            var query1= process.env.NPATH+'/auto_dhcp.sh';
                            console.log(query1);
                            exec(query1, function(error, stdout, stderr) {
                                console.log(stdout)});
                            exec('sync', function(error, stdout, stderr) {});
                            console.log('Complete');
                        } else
                            console.log(err);

                    });
                }
            });
        } else
            console.log(err);
    });
}

function writeInterface(data) {
    fs.writeFile(process.env.NPATH + "/config/interfaces", data, function(err) {
        var query = 'cp ' + process.env.NPATH + '/config/interfaces /etc/network/interfaces';
        if (err == null) {
            exec(query, function(error, stdout, stderr) {
                if (error == null) {
                    fs.writeFile(process.env.NPATH + "/config/network.conf", 'static', function(err) {
                        if (err == null) {
			    var query1 = process.env.NPATH+'/auto_static.sh';
			    console.log(query1);  
			    exec(query1, function(error, stdout, stderr) {
				console.log(stdout)}); 
                            exec('sync', function(error, stdout, stderr) {});
                            console.log('Complete');
                        } else
                            console.log(err);
                    });
                }
            });
        } else
            console.log(err);
    });
}


app.post('/setNetworkInfo', function(req, res) {
    var data = '';

    var jsonData = JSON.parse(req.body.json);
    var ssid = '';
    var passwd = '';
    var bssid = '';
    var authtype = '';
    var staticIfaces = '\n'; 
    var dhcpIfaces = '\n'; 
    var interfaceType = '';

    if (req.body.type == "dhcp") {

        interfaceType = jsonData.dhcpNetInfo.interfaceType;
        console.log("interfaceType :" + interfaceType);

        if (interfaceType == "both" || interfaceType == "wlan") {

            dhcpIfaces += "\n\nallow-hotplug "+W_INTERFACE; 
            dhcpIfaces += "\niface "+W_INTERFACE+" inet manual";
            dhcpIfaces += "\nwpa-roam /etc/wpa_supplicant/wpa_supplicant.conf"; 
            dhcpIfaces += "\niface default inet dhcp"; 

            ssid = jsonData.dhcpNetInfo.wlan.ssid;
            passwd = jsonData.dhcpNetInfo.wlan.passwd;
            authtype = jsonData.dhcpNetInfo.wlan.auth;
	}	
	
        if (interfaceType == "both" || interfaceType == "eth") 	
	{
       	    dhcpIfaces += "\n\nauto eth0\niface eth0 inet dhcp";
	}
        
    } else {

	/* static */ 
        interfaceType = jsonData.staticNetInfo.interfaceType;
        console.log("[static] interfaceType :" + interfaceType);

        if (interfaceType == "both" || interfaceType == "eth") {
            if (jsonData.staticNetInfo.eth != null && jsonData.staticNetInfo.eth != undefined) {
                staticIfaces += "\nauto eth0\niface eth0 inet static";
                staticIfaces += "\naddress " + jsonData.staticNetInfo.eth.ipAddr;
                staticIfaces += "\nnetmask " + jsonData.staticNetInfo.eth.subnetMask;
                staticIfaces += "\ngateway " + jsonData.staticNetInfo.eth.gateway;
                staticIfaces += "\ndns-nameservers " + jsonData.staticNetInfo.eth.dns1 + " " + jsonData.staticNetInfo.eth.dns2 + "\n";
            }
        }

        if (interfaceType == "both" || interfaceType == "wlan") {
            ssid = jsonData.staticNetInfo.wlan.ssid;
            bssid = jsonData.staticNetInfo.wlan.bssid;
            passwd = jsonData.staticNetInfo.wlan.passwd;
            authtype = jsonData.staticNetInfo.wlan.auth;

            if (jsonData.staticNetInfo.wlan != null && jsonData.staticNetInfo.wlan != undefined) {
                staticIfaces += "\nauto "+W_INTERFACE+"\niface "+W_INTERFACE+" inet static";
                staticIfaces += "\naddress " + jsonData.staticNetInfo.wlan.ipAddr;
                staticIfaces += "\nnetmask " + jsonData.staticNetInfo.wlan.subnetMask;
                staticIfaces += "\ngateway " + jsonData.staticNetInfo.wlan.gateway;
                staticIfaces += "\ndns-nameservers " + jsonData.staticNetInfo.wlan.dns1 + " " + jsonData.staticNetInfo.wlan.dns2;

                if (authtype == "WPA/WPA2") {
                    staticIfaces += "\nwpa-ssid " + ssid;
                    if (jsonData.staticNetInfo.wlan.bssid != null && jsonData.staticNetInfo.wlan.bssid != undefined && jsonData.staticNetInfo.wlan.bssid != "")
                        staticIfaces += "\nwpa-bssid " + bssid;

                    //      interfaces += "\nwpa-psk "+req.body.psk;
                } else if (authtype == "WEP") {
                    staticIfaces += "\nwireless-essid " + ssid;
                    if (jsonData.staticNetInfo.wlan.bssid != null && jsonData.staticNetInfo.wlan.bssid != undefined && jsonData.staticNetInfo.wlan.bssid != "")
                        staticIfaces += "\nwireless-bssid " + bssid;
                    staticIfaces += "\nwireless-key " + passwd;
                } else if (authtype == "OPEN") {
                    staticIfaces += "\nwireless-essid " + ssid;
                    if (jsonData.staticNetInfo.wlan.bssid != null && jsonData.staticNetInfo.wlan.bssid != undefined && jsonData.staticNetInfo.wlan.bssid != "")
                        staticIfaces += "\nwireless-bssid " + bssid;
                }
            }
	    else console.log("wlan interface undefined"+W_INTERFACE); 
        }
    }

    console.log("authtype :" + authtype);

    if (interfaceType == "both" || interfaceType == "wlan") {
        if (authtype == "WPA/WPA2") {
            var query_wpa = 'wpa_passphrase ' + ssid + ' ' + passwd + ' > $NPATH/config/wpa_supplicant.conf';
            console.log("query:" + query_wpa);
            exec(query_wpa, function(error, stdout, stderr) {
                if (error == null) {
                    writeInterfaceType(interfaceType);
                    if (req.body.type == "static") {
                        exec('grep psk $NPATH/config/wpa_supplicant.conf | grep -Ev \'#\' | awk -F = \'{print $2}\'', function(error, stdout, stderr) {
                            staticIfaces += "\nwpa-psk " + stdout;

                            writeInterfaceStatic(staticIfaces, interfaceType);
                            res.send(resJsonDataOk);
                            //res.render('wifisetup_ip_result');

                        });
                    } else {
                        writeNetworkConf();
			writeInterfaceDhcp(dhcpIfaces, interfaceType); 
                        res.send(resJsonDataOk);
                        //res.render('wifisetup_result');
                    }
                } else {
                    console.log(error);
                    res.send(resJsonDataErr);
                }
            });
        } else /*WEP, OPEN*/ {
            if (authtype === "WEP") {
                data = "ctrl_interface=/var/run/wpa_supplicant\n\nnetwork={\n\ssid=" + "\"" + ssid + "\"\n\wep_key0=\"" + passwd + "\"\n\key_mgmt=NONE\n\}";
            } else if (authtype === "OPEN") {
                data = "ctrl_interface=/var/run/wpa_supplicant\n\nnetwork={\n\ssid=" + "\"" + ssid + "\"\n\key_mgmt=NONE\n\}";
            }
 			
            fs.writeFile(process.env.NPATH + "/config/wpa_supplicant.conf", data, function(err) {
                if (err) {
                    console.log(err);
                    res.send(resJsonDataErr);
                } else {
                    console.log("The file was saved!");
                    writeInterfaceType(interfaceType);
                    if (req.body.type == "static") //static IP Setting
                    {
                        writeInterfaceStatic(staticIfaces, interfaceType);
                        //res.render('wifisetup_ip_result');
                    } else {
                        writeNetworkConf();
			writeInterfaceDhcp(dhcpIfaces, interfaceType); 
                        //res.render('wifisetup_result');
                    }
                    res.send(resJsonDataOk);
                }
            });
        }
    } 
    else //Only Ethernet
    {	
	console.log("Only Eth"); 
        writeInterfaceType(interfaceType);
        if (req.body.type == "static") {
	    console.log("Only Eth0 1"); 
            writeInterfaceStatic(staticIfaces, "eth");
            res.send(resJsonDataOk);
        } else {
            writeNetworkConf();
	    writeInterfaceDhcp(dhcpIfaces, "eth"); 
            res.send(resJsonDataOk);
        }
    }
});





app.get('/getDaqSetupInfo', function(req, res) {
    var wlan_ipAddr = "";
    var eth_ipAddr = "";
    var ipAddr = "";

    var m_time;
    var a_time;
    var d_time;
    var u_time;
    var flag_interface = 0;

    var queryWlan = "ifconfig "+W_INTERFACE+" | awk '/inet addr:/'";

    exec(queryWlan, function(error, stdout, stderr) {
        var pos = stdout.indexOf("inet addr:");
        if (pos != -1) {
            var end = stdout.indexOf("Bcast:");
            wlan_ipAddr = stdout.slice(pos + 10, end - 1);

            console.log(W_INTERFACE+" ipAddr:" + wlan_ipAddr);
        }
        flag_interface |= IF_WLAN;
    });

    var queryEth = "ifconfig eth0 | awk '/inet addr:/'";


    exec(queryEth, function(error, stdout, stderr) {
        var pos = stdout.indexOf("inet addr:");
        if (pos != -1) {
            var end = stdout.indexOf("Bcast:");
            eth_ipAddr = stdout.slice(pos + 10, end - 2);

            console.log("eth0 ipAddr:" + eth_ipAddr);
        }
        flag_interface |= IF_ETHERNET;
    });


    m_time = process.env.M_TIME;
    var pos = m_time.indexOf(" ");
    var m_min = m_time.slice(0, pos);
    m_time = m_time.slice(pos + 1, m_time.length);
    pos = m_time.indexOf(" ");
    var m_hour = m_time.slice(0, pos);
    m_time = m_time.slice(pos + 1, m_time.length);
    pos = m_time.indexOf(" ");
    var m_day = m_time.slice(0, pos);
    m_time = m_time.slice(pos + 1, m_time.length);
    pos = m_time.indexOf(" ");
    var m_month = m_time.slice(0, pos);
    m_time = m_time.slice(pos + 1, m_time.length);
    var m_dayofweek = m_time.slice(0, m_time.length);

    a_time = process.env.A_TIME;
    var pos = a_time.indexOf(" ");
    var a_min = a_time.slice(0, pos);
    a_time = a_time.slice(pos + 1, a_time.length);
    pos = a_time.indexOf(" ");
    var a_hour = a_time.slice(0, pos);
    a_time = a_time.slice(pos + 1, a_time.length);
    pos = a_time.indexOf(" ");
    var a_day = a_time.slice(0, pos);
    a_time = a_time.slice(pos + 1, a_time.length);
    pos = a_time.indexOf(" ");
    var a_month = a_time.slice(0, pos);
    a_time = a_time.slice(pos + 1, a_time.length);
    var a_dayofweek = a_time.slice(0, a_time.length);

    d_time = process.env.D_TIME;
    var pos = d_time.indexOf(" ");
    var d_min = d_time.slice(0, pos);
    d_time = d_time.slice(pos + 1, d_time.length);
    pos = d_time.indexOf(" ");
    var d_hour = d_time.slice(0, pos);
    d_time = d_time.slice(pos + 1, d_time.length);
    pos = d_time.indexOf(" ");
    var d_day = d_time.slice(0, pos);
    d_time = d_time.slice(pos + 1, d_time.length);
    pos = d_time.indexOf(" ");
    var d_month = d_time.slice(0, pos);
    d_time = d_time.slice(pos + 1, d_time.length);
    var d_dayofweek = d_time.slice(0, d_time.length);

    u_time = process.env.U_TIME;
    var pos = u_time.indexOf(" ");
    var u_min = u_time.slice(0, pos);
    u_time = u_time.slice(pos + 1, u_time.length);
    pos = u_time.indexOf(" ");
    var u_hour = u_time.slice(0, pos);
    u_time = u_time.slice(pos + 1, u_time.length);
    pos = u_time.indexOf(" ");
    var u_day = u_time.slice(0, pos);
    u_time = u_time.slice(pos + 1, u_time.length);
    pos = u_time.indexOf(" ");
    var u_month = u_time.slice(0, pos);
    u_time = u_time.slice(pos + 1, u_time.length);
    var u_dayofweek = u_time.slice(0, u_time.length);

    console.log(m_min + ":" + m_hour + ":" + m_day + ":" + m_month + ":" + m_dayofweek);
    console.log(a_min + ":" + a_hour + ":" + a_day + ":" + a_month + ":" + a_dayofweek);
    console.log(d_min + ":" + d_hour + ":" + d_day + ":" + d_month + ":" + d_dayofweek);
    console.log(u_min + ":" + u_hour + ":" + u_day + ":" + u_month + ":" + u_dayofweek);



    var interval = setInterval(function() {

        if (flag_interface & IF_WLAN && flag_interface & IF_ETHERNET) {
            if (eth_ipAddr != "") {
                ipAddr = eth_ipAddr;
                console.log("ftp eth0 ipAddr:" + ipAddr);
            } else {
                if (wlan_ipAddr != "") {
                    ipAddr = wlan_ipAddr;
                    console.log("ftp "+W_INTERFACE+" ipAddr:" + ipAddr);
                }
            }


            var daqSetupInfo = {
                'daqSetupInfo': {
                    'ipAddr': ipAddr,
                    'daq_name': process.env.DAQ_NAME,
                    'm_path': process.env.DATAPATH,
                    'a_path': process.env.A_PATH,
                    'd_path': process.env.D_PATH,
                    'u_path': process.env.U_PATH,
                    'r_path': process.env.R_PATH,
                    'm_flag': process.env.M_FLAG,
                    'm_disksize': process.env.M_DISKSIZE,
                    'm_day': process.env.M_DAY,
                    'a_duration': process.env.A_TIMECNT,
                    'u_duration': process.env.U_DURATION,
                    'm_time': {
                        'min': m_min,
                        'hour': m_hour,
                        'day': m_day,
                        'month': m_month,
                        'dayofweek': m_dayofweek,
                    },
                    'a_time': {
                        'min': a_min,
                        'hour': a_hour,
                        'day': a_day,
                        'month': a_month,
                        'dayofweek': a_dayofweek,
                    },
                    'd_time': {
                        'min': d_min,
                        'hour': d_hour,
                        'day': d_day,
                        'month': d_month,
                        'dayofweek': d_dayofweek,
                    },
                    'u_time': {
                        'min': u_min,
                        'hour': u_hour,
                        'day': u_day,
                        'month': u_month,
                        'dayofweek': u_dayofweek,
                    },
                    'a_sampling_rate':process.env.SAMPLING_RATE,

                }
            };
            var dataString = JSON.stringify(daqSetupInfo);
            res.send(dataString);
            clearInterval(interval);
            flag_interface = 0;
            console.log("+++++++++++++++++++Complete+++++++++++++++++++++++");
        } else {
            console.log("flagDhcpInfoJob:" + flagDhcpInfoJob);
        }
    }, 500);


});

app.post('/setDaqSetupInfo', function(req, res) {
	var query = "mkdir -p ";

    var jsonData = JSON.parse(req.body.json);
	
    var daqInfo = jsonData.daqSetupInfo;
	var userConf = "#!/bin/sh\nexport DAQ_NAME=" + daqInfo.daq_name + "\n";

    userConf += "export M_FLAG=\"" + daqInfo.m_flag + "\"\n";
    userConf += "export M_TIME=\"" + daqInfo.m_time.min + ' ' + daqInfo.m_time.hour + ' ' + daqInfo.m_time.day + ' ' + daqInfo.m_time.month + ' ' + daqInfo.m_time.dayofweek + "\"\n";
    userConf += "export M_DISKSIZE=" + daqInfo.m_disksize + "\n";
    userConf += "export M_DAY=" + daqInfo.m_day + "\n";
    userConf += "export A_TIME=\"" + daqInfo.a_time.min + ' ' + daqInfo.a_time.hour + ' ' + daqInfo.a_time.day + ' ' + daqInfo.a_time.month + ' ' + daqInfo.a_time.dayofweek + "\"\n";
    userConf += "export A_TIMECNT=" + daqInfo.a_duration + "\n";
    userConf += "export D_TIME=\"" + daqInfo.d_time.min + ' ' + daqInfo.d_time.hour + ' ' + daqInfo.d_time.day + ' ' + daqInfo.d_time.month + ' ' + daqInfo.d_time.dayofweek + "\"\n";
    userConf += "export U_TIME=\"" + daqInfo.u_time.min + ' ' + daqInfo.u_time.hour + ' ' + daqInfo.u_time.day + ' ' + daqInfo.u_time.month + ' ' + daqInfo.u_time.dayofweek + "\"\n";
    userConf += "export U_DURATION=" + daqInfo.u_duration + "\n";
	userConf += "export SAMPLING_RATE="+daqInfo.a_sampling_rate + "\n";
	async.series([
		function(callback){
			var path = process.env.SYSTEM+'/'+daqInfo.m_path;
			console.log("DATAPATH:"+path);
			
			if(path != process.env.DATAPATH){
				fs.exists(path, function(exist){
					if(exist == false){
						exec(query+path, function(error, stdout, stderr) {
							if(error == null){
								userConf += "export DATAPATH=" + path + "\n";
								callback(null,path);
							}
							else{
								userConf += "export DATAPATH=" + process.env.DATAPATH+ "\n";
								callback(error,process.env.DATAPATH);
							}
						});
					}
					else{
						userConf += "export DATAPATH=" + path + "\n";
						callback(null,path);
					}
				});
			}
			else{
				userConf += "export DATAPATH=" + process.env.DATAPATH+ "\n";
				callback(null,process.env.DATAPATH);
			}		
		},
		
		function(callback){
			var path = process.env.SYSTEM+'/'+daqInfo.a_path;
			console.log("A_PATH:"+path);
			if(path != process.env.A_PATH){
				fs.exists(path, function(exist){
					if(exist == false){
						exec(query+path, function(error, stdout, stderr) {
							if(error == null){
								userConf += "export A_PATH=" + path+ "\n";
								callback(null,path);
							}
							else{
								userConf += "export A_PATH=" + process.env.A_PATH+ "\n";
								callback(error,process.env.A_PATH);
							}
						});
					}
					else{
						userConf += "export A_PATH=" + path + "\n";
						callback(null,path);
					}
				});
			}
			else{
				userConf += "export A_PATH=" + process.env.A_PATH+ "\n";
				callback(null,process.env.A_PATH);
			}
		},

		function(callback){
			var path = process.env.SYSTEM+'/'+daqInfo.d_path;
			console.log("D_PATH:"+path);
			if(path != process.env.D_PATH){
				fs.exists(path, function(exist){
					if(exist == false){
						exec(query+path, function(error, stdout, stderr) {
							if(error == null){
								userConf += "export D_PATH=" + path+ "\n";
								callback(null,path);
							}
							else{
								userConf += "export D_PATH=" + process.env.D_PATH+ "\n";
								callback(error,process.env.D_PATH);
							}
						});
					}
					else{
						userConf += "export D_PATH=" + path + "\n";
						callback(null,path);
					}
				});
			}
			else{
				userConf += "export D_PATH=" + process.env.D_PATH+ "\n";
				callback(null,process.env.D_PATH);
			}
		},

		function(callback){
			var path = process.env.SYSTEM+'/'+daqInfo.u_path;
			console.log("U_PATH:"+path);
			if(path != process.env.U_PATH){
				fs.exists(path, function(exist){
					if(exist == false){
						exec(query+path, function(error, stdout, stderr) {
							if(error == null){
								userConf += "export U_PATH=" + path+ "\n";
								callback(null,path);
							}
							else{
								userConf += "export U_PATH=" + process.env.U_PATH+ "\n";
								callback(error,process.env.U_PATH);
							}
						});
					}
					else{
						userConf += "export U_PATH=" + path + "\n";
						callback(null,path);
					}
				});
			}
			else{
				userConf += "export U_PATH=" + process.env.U_PATH+ "\n";
				callback(null,process.env.U_PATH);
			}
		},

		function(callback){
			var path = process.env.SYSTEM+'/'+daqInfo.r_path;
			console.log("R_PATH:"+path);
			if(path != process.env.R_PATH){
				fs.exists(path, function(exist){
					if(exist == false){
						exec(query+path, function(error, stdout, stderr) {
							if(error == null){
								userConf += "export R_PATH=" + path+ "\n";
								callback(null,path);
							}
							else{
								userConf += "export R_PATH=" + process.env.R_PATH+ "\n";
								callback(error,process.env.R_PATH);
							}
						});
					}
					else{
						userConf += "export R_PATH=" + path + "\n";
						callback(null,path);
					}
				});
			}
			else{
				userConf += "export R_PATH=" + process.env.R_PATH+ "\n";
				callback(null,process.env.R_PATH);
			}
		}
		
	],
				
	function(err, job){
		if(err == null){
    fs.writeFile(process.env.SYSTEM + "/user.conf", userConf, function(err) {

        if (err == null)
			{
            res.send(resJsonDataOk);
				exec('sync', function(error, stdout, stderr){});
			}
        else {
            res.send(resJsonDataErr);
            console.log("error:" + err);
        }
    });
		}
		else{
			console.log("err reason:"+err);
			var resJsonErr = JSON.stringify({ 'result': { 'status': 'error', 'reason': err} });
			res.send(resJsonErr);
		}
	});		
});

app.post('/setHostIp', function(req, res){
        var result = 0;
        var jsonData='';
        var i = 0;

        console.log("req.body.json:"+req.body.json);
        jsonData = JSON.parse(req.body.json);

	var userConf = "#!/bin/sh\nexport HOSTIP=" + jsonData.hostInfo.ip + "\n";


        //jsonStr = JSON.stringify(jsonFile);
        console.log("-----------recv Host IP:"+jsonData);
        fs.writeFile(process.env.HOME+'/envhostip', userConf , function(err){
                        var resJsonData = null;

                        if(err != '' && err != null)
                                resJsonData = JSON.stringify({'result':{'status':'error', 'errmsg':err}});
                        else
                                resJsonData = JSON.stringify({'result':{'status':'ok'}});
                        res.send(resJsonData);
        });
});


app.get('/getHostIp', function(req, res){
        var jsonData='';

                        fs.exists(process.env.HOME+'/envhostip', function(exists){
                        console.log("envhostip exists:"+exists);
                                if(exists == true){
                                        fs.readFile(process.env.HOME+'/envhostip', function(error, data) {
                                                if(error == null){
                                                console.log("jsonData:"+data);
                                                
                                                }
                                        });
                                }
                                else{
									console.log("Only Env HostIP : "+process.env.HOSTIP);
                                }
								var HostInfo = {
									'hostInfo': {
										'ip': process.env.HOSTIP,
                    
									}
								};

            var dataString = JSON.stringify(HostInfo);
			 res.send(dataString);
        });
});


app.get('/startWebStream', function(req, res) {
    var query = "ps | grep \"ADTcpStreamServer\" | grep -v grep | awk \'{print $1}\' | xargs kill -9 > /dev/null";

    exec(query, function(error, stdout, stderr) {

        child = exec('./runADWebServer.sh');
        child.stdout.setEncoding('utf8');
        child.stdout.on('data', on_child_stdout);
        child.stderr.on('data', on_child_stdout);
        child.on('exit', testfunction);
        res.send(null);
    });
});

app.get('/startTcpStream', function(req, res) {
    var query = "ps | grep \"ADWebStreamServer\" | grep -v grep | awk \'{print $1}\' | xargs kill -9 > /dev/null";

    exec(query, function(error, stdout, stderr) {

        child = exec('./runADTcpServer.sh');
        child.stdout.setEncoding('utf8');
        child.stdout.on('data', on_child_stdout);
        child.stderr.on('data', on_child_stdout);
        child.on('exit', testfunction);
        res.send(null);
    });
});



app.use(express.static(path.join(__dirname, 'public')));


io.on('connection', function(socket) {
    console.log('a user connected');
    connected = 1;
    socket.on('disconnect', function() {
        console.log('user disconnected');
    });
    socket.on('chat message', function(msg) {
        console.log(msg);
        connected = 0;
        //    io.emit('chat message', msg);
    });
});

http.listen(3000, function() {
    console.log('listening on *:3000');
});

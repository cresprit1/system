#include <stdio.h>         // printf()
#include <string.h>        // strlen()
#include <fcntl.h>         // O_WRONLY
#include <unistd.h>        // write(), close()
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <math.h>
#include <termios.h>

#include "digital.h"
#include "usergpio.h"

static uint32_t mode = 1;
static uint8_t bits = 8;
static uint32_t speed = 8000000; // 50000; // 25*1000*1000; // 50000;
static uint16_t delay = 0;
static int verbose;

char tx[10]; 
char rx[10]; 

struct spi_ioc_transfer xfer[2]; 

static void pabort(const char *s)
{
	perror(s);
	abort();
}

#define MAX_BUF_SIZE 	4096*3 // 3s   

struct buffer {
    unsigned int size;
    unsigned int start;
    
    /* Tracking start and end of buffer would waste
     * one position. A full buffer would always have
     * to leave last position empty or otherwise
     * it would look empty. Instead this buffer uses
     * count to track if buffer is empty or full
     */
    unsigned int count; // number of elements in buffer
    /* Two ways to make buffer element type opaque
     * First is by using typedef for the element
     * pointer. Second is by using void pointer.
     */
    /* different types of buffer: 
    int *element;   // array of integers
    char *element;  // array of characters 
    void *element;  // array of void type (could cast to int, char, etc)
    char **element; //array of char pointers (array of strings)
    void **element; // array of void pointers
    Choosing array of void pointers since it's the most flexible */
    //void **element; 
    int32_t element[MAX_BUF_SIZE]; 
};
 
typedef struct buffer buffer_t;
 
buffer_t buffer; 

void init(buffer_t *buffer, unsigned int size) {
    buffer->size = MAX_BUF_SIZE;
    buffer->start = 0;
    buffer->count = 0;
//    buffer->element = malloc(sizeof(buffer->element)*size);
}

int count(buffer_t *buffer) {
	return buffer->count; 
}
 
int full(buffer_t *buffer) {
    if (buffer->count == buffer->size) { 
        return 1;
    } else {
        return 0;
    }
}
 
int empty(buffer_t *buffer) {
    if (buffer->count == 0) {
        return 1;
    } else {
        return 0;
    }
}

void push(buffer_t *buffer, int32_t data) { // void *data) {

	int index; 
	if (full(buffer)) {
		printf("Buffer overflow\n"); 
	} else {

		index = buffer->start + buffer->count++; 

		if (index >= buffer->size) {
			printf(" re-cycle[%d] - %d, %d\n", index, buffer->start, buffer->count); 		
			index = index - buffer->size; 
		}
		buffer->element[index] = data; 
	}
}

//void * popqueue(buffer_t *buffer) {
int32_t popqueue(buffer_t *buffer) {
//    void * element; 
	int32_t element; 

	if (empty(buffer)) {
		printf("Buffer underflow\n"); 
		return 0; 
	} else {
		element = buffer->element[buffer->start]; 
		buffer->start++; 
		buffer->count--; 
		if (buffer->start = buffer->size) {
			buffer->start = 0; 
		}

		return element; 
	}
}
 
int32_t popstack(buffer_t *buffer) {
    int index;
    if (empty(buffer)) {
        printf("Buffer underflow\n");
        return 0xFFFFFFFF;
    } else {
        /* LIFO implementation */
        index = buffer->start + buffer->count - 1;
        if (index >= buffer->size) {
           index = buffer->count - buffer->size - 1;
        }      
        buffer->count--;
        return buffer->element[index];
    }
}

int t_flag; /* flag for thread creation */
int exit_flag;
pthread_t tid, tid2;

#define NUM_PRINT_BYTES  16

enum {
    STRING = 1,
    BINARY,
};

enum {
    FALSE,
    TRUE,
};

int32_t adcbuf[4096] = {0, }; 

/*
    spidevlib.c - A user-space program to comunicate using spidev.
                Gustavo Zamboni
*/
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
 
uint8_t buf[10];
uint8_t buf2[10];
 
struct spi_ioc_transfer xfer[2];
 
//////////
// Init SPIdev
//////////

int spi_init(char filename[40])
{
   int file;
    __u8    mode, lsb, bits;
    __u32 speed=16000000;
	
        if ((file = open(filename,O_RDWR)) < 0)
        {
            printf("Failed to open the bus.");
            /* ERROR HANDLING; you can check errno to see what went wrong */
            exit(1);
            }
 
        ///////////////
        // Verifications
        ///////////////
        //possible modes: mode |= SPI_LOOP; mode |= SPI_CPHA; mode |= SPI_CPOL; mode |= SPI_LSB_FIRST; mode |= SPI_CS_HIGH; mode |= SPI_3WIRE; mode |= SPI_NO_CS; mode |= SPI_READY;
        //multiple possibilities using |
         
	mode = SPI_CPHA; 
 
            if (ioctl(file, SPI_IOC_WR_MODE, &mode)<0)   {
                perror("can't set spi mode");
                return;
                }
        
            if (ioctl(file, SPI_IOC_RD_MODE, &mode) < 0)
                {
                perror("SPI rd_mode");
                return;
                }

            if (ioctl(file, SPI_IOC_RD_LSB_FIRST, &lsb) < 0)
                {
                perror("SPI rd_lsb_fist");
                return;
                }
        //sunxi supports only 8 bits
        
            if (ioctl(file, SPI_IOC_WR_BITS_PER_WORD, (__u8[1]){8})<0)   
                {
                perror("can't set bits per word");
                return;
                }
        
            if (ioctl(file, SPI_IOC_RD_BITS_PER_WORD, &bits) < 0) 
                {
                perror("SPI bits_per_word");
                return;
                }
        
            if (ioctl(file, SPI_IOC_WR_MAX_SPEED_HZ, &speed)<0)  
                {
                perror("can't set max speed hz");
                return;
                }
        
            if (ioctl(file, SPI_IOC_RD_MAX_SPEED_HZ, &speed) < 0) 
                {
                perror("SPI max_speed_hz");
                return;
                }
     
 
    printf("%s: spi mode %d, %d bits %sper word, %d Hz max\n",filename, mode, bits, lsb ? "(lsb first) " : "", speed);
 
    //xfer[0].tx_buf = (unsigned char)buf;
    xfer[0].len = 3; /* Length of  command to write*/
    xfer[0].cs_change = 0; /* Keep CS activated */
    xfer[0].delay_usecs = 0, //delay in us
    xfer[0].speed_hz = 16*1000*1000, //speed
    xfer[0].bits_per_word = 8, // bites per word 8
 
    //xfer[1].rx_buf = (unsigned char) buf2;
    xfer[1].len = 4; /* Length of Data to read */
    xfer[1].cs_change = 0; /* Keep CS activated */
    xfer[0].delay_usecs = 0;
    xfer[0].speed_hz = 16*1000*1000; //25000000;
    xfer[0].bits_per_word = 8;
 
    return file;
}
 
 
 
//////////
// Read n bytes from the 2 bytes add1 add2 address
//////////

int32_t spi_dataread(int file)
{
    int status, i;
	int32_t rawdata; 

    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);
    buf[0] = 0x12; 
    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 1; /* Length of  command to write*/
    xfer[1].rx_buf = (unsigned long) buf2;
    xfer[1].len = 3; /* Length of Data to read */

    status = ioctl(file, SPI_IOC_MESSAGE(2), xfer);
    if (status < 0)
        {
        perror("SPI_IOC_MESSAGE");
        return;
        }

    	rawdata = (buf2[0]<<16)|(buf2[1]<<8)|buf2[2]; 

		if ( (rawdata & 0x00800000) > 0 )
		{	
			rawdata |= 0xFF000000;
		}
		else 
			rawdata &= 0x007fffff;  

        //printf("env: 0x%08x : %02x %02x %02x %02x\n", rawdata, buf2[0], buf2[1], buf2[2], buf2[3]);

	return rawdata; 
#if 0 
//    printf("env: %02x %02x %02x\n", buf2[0], buf2[1], buf2[2]);
//    printf("ret: ");

       //for ( i = 0 ; i < 3; i++)
       //         printf("%02x: ", buf2[i]); //, buf2[1], buf2[2], buf2[3]);
       //printf("\t");

    return buf2;
#endif 
}
 
char * spi_read(char add1, char add2, int nbytes,int file)
{
    int status, i;
 
    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);
    buf[0] = add1;
    buf[1] = add2;
    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 2+nbytes; /* Length of  command to write*/
    xfer[1].rx_buf = (unsigned long) buf2;
    xfer[1].len = nbytes; /* Length of Data to read */

    status = ioctl(file, SPI_IOC_MESSAGE(2), xfer); 
    if (status < 0)
        {
        perror("SPI_IOC_MESSAGE");
        return;
        }
    printf("env: %02x %02x %02x\n", buf[0], buf[1], buf[2]);

    printf("ret: "); 

	for ( i = 0 ; i < nbytes; i++)
		printf("%02x ", buf2[i]); //, buf2[1], buf2[2], buf2[3]);
	printf("\n");   
 
    return buf2;
} 
 
//////////
// Write n bytes int the 2 bytes address add1 add2
//////////

void spi_write(int add1,int add2,int nbytes,char value[10],int file)
{
    uint8_t   buf[32], buf2[32];
    int status;
 
    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);

    buf[0] = add1;
    buf[1] = add2;
   
//    if (nbytes>=1) buf[1] = value[0];
//    if (nbytes>=2) buf[2] = value[1];
//    if (nbytes>=3) buf[3] = value[2];
//    if (nbytes>=4) buf[4] = value[3];

    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = nbytes+1; /* Length of  command to write*/
    status = ioctl(file, SPI_IOC_MESSAGE(1), xfer);

    if (status < 0)
    {
        perror("SPI_IOC_MESSAGE");
        return;
    }

//    printf("env: %02x %02x %02x\n", buf[0], buf[1], buf[2]);
//    printf("ret: %02x %02x %02x %02x\n", buf2[0], buf2[1], buf2[2], buf2[3]);
}

int openSPIdev(const char *devicename)
{
	int fd; 
	int ret; 

	fd = open(devicename, O_RDWR); 

        if (fd < 0)
                pabort("can't open device");

        /*
         * spi mode
         */

        ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
        if (ret == -1)
                pabort("can't set spi mode");

        ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
        if (ret == -1)
                pabort("can't get spi mode");

       /*
         * bits per word
         */

        ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
        if (ret == -1)
                pabort("can't set bits per word");

        ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
        if (ret == -1)
                pabort("can't get bits per word");

       /*
         * max speed hz
         */
        ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
        if (ret == -1)
                pabort("can't set max speed hz");

        ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
        if (ret == -1)
                pabort("can't get max speed hz");

        //if (verbose)
        {
                printf("SPI Info \n");
                printf("- spi device : %s\n", devicename);
                printf("- spi mode: %d\n", mode);
                printf("- bits per word: %d\n", bits);
                printf("- max speed: %d Hz (%d KHz)\n", speed, speed/1000);
        }

	return fd; 
}

void spi_srwrite(int fd, char data)
{
    unsigned char   buf[32], buf2[32];
    int status;
 
    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);

    buf[0] = data;
   // buf[1] = add1;
   // buf[2] = add2;

    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 1; // nbytes+3; /* Length of  command to write*/

    status = ioctl(fd, SPI_IOC_MESSAGE(1), xfer);

    if (status < 0)
    {
        perror("SPI_IOC_MESSAGE");
        return;
    }
    //printf("env: %02x %02x %02x\n", buf[0], buf[1], buf[2]);
    //printf("ret: %02x %02x %02x %02x\n", buf2[0], buf2[1], buf2[2], buf2[3]);
}

void transfer_cmd(int fd, uint8_t *tx, uint8_t *rx, size_t len)
{
	int ret;

	struct spi_ioc_transfer tr_cmd = {
	.tx_buf = (unsigned long)tx,
	.rx_buf = (unsigned long)rx,
	.len = len,
	.delay_usecs = delay,
	.speed_hz = speed,
	.bits_per_word = bits,
	};
#if 0
if (mode & SPI_TX_QUAD)
tr_cmd.tx_nbits = 4;
else if (mode & SPI_TX_DUAL)
tr_cmd.tx_nbits = 2;
if (mode & SPI_RX_QUAD)
tr_cmd.rx_nbits = 4;
else if (mode & SPI_RX_DUAL)
tr_cmd.rx_nbits = 2;
if (!(mode & SPI_LOOP)) {
if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
tr_cmd.rx_buf = 0;
else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
tr_cmd.tx_buf = 0;
}
#endif
	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr_cmd);

	if (ret < 1)
	{
		printf("can't send spi messageaaaaaaaaaaaaaa");
	}

	printf("%x:%x:%x:%x\n", (char)rx[0], (char)rx[1], (char)rx[2], (char)rx[3]); 
}

uint8_t send_rx[5]; 
//int file;
 
int readRegister(int fd, char addr1, char addr2)
{
	uint8_t CMD_tx[5]; 

	CMD_tx[0] = addr1; 
	CMD_tx[1] = addr2; 

	transfer_cmd(fd, CMD_tx, send_rx, 2); //sizeof(CMD_STATUS_tx)); 
}

int ctrlSPIdev(char *devname)
{
	char *buffer;
	char buf[10];
	int file; 

	file=spi_init(devname); //dev
	
	buffer=(char *)spi_read(0x20,0x08,10,file); //reading the address 0xE60E
	sleep(1); 

	buf[0] = 0x07; 
//	spi_write(0x07, 0x00, 0, buf, file); // reset
	spi_write(0x08, 0x00, 0, buf, file); // start 
//	spi_write(0x0a, 0x00, 0, buf, file); // stop
	
//	sleep(1); 	
	return file; 
}

int adcStop(int file)
{
	spi_write(0x0a, 0x00, 0, NULL, file); 

	return 0; 
}

int adcStart(int file)
{
	spi_write(0x08, 0x00, 0, NULL, file); 
	return 0; 
}

#define MAXVOLTAGE 3.3f
#define BITS_TO_WRITE 8 
#define BIT_SHIFT 6

//float singleVoltageStep = 5.0f / pow(2, BITS_TO_WRITE); 

float singleVoltageStep = (float)(3.3f / pow(2, 8)); // BITS_TO_WRITE)); 

void setVoltage(float voltage, int file)
{
  // keep within limits
  if( voltage > MAXVOLTAGE ){ voltage = MAXVOLTAGE; }
  else if( voltage < 0 ){ voltage = 0; }

  // calc and set voltage
  unsigned short value = (unsigned short)(voltage/singleVoltageStep);
  value = value << BIT_SHIFT;
  unsigned char upper = value >> 8;
  unsigned char lower = value & 0xff;

  spi_write(upper, lower, 1, buf, file); // start
}

static int ibuf = 0; 
int file; 

void print_bytes(int type, int length, unsigned char *buffer)
{
    int i;
    char temp[NUM_PRINT_BYTES] = {0,};

    for (i = 0; i < length; i++) {
        printf("%02x ", buffer[i]);
        temp[i%NUM_PRINT_BYTES] = buffer[i];
        if (((i + 1) % NUM_PRINT_BYTES) == 0) {
            if (type == STRING) {
                printf("\t%s", temp);
            }
            printf("\n");
            memset(temp, 0, NUM_PRINT_BYTES);
        }
    }
    if (type == STRING) {
        if (i % NUM_PRINT_BYTES != 0)
            printf("\t%s", temp);
    }
}

//Queue rx_int_queue;

static int ridx = 0; 

void *process(void *arg)
{
    int cnt; 
    int i; 

    do {
                cnt = count(&buffer); //Len_queue(&rx_int_queue); //count(&buffer);
                if ( cnt >= 4000 && exit_flag == FALSE)
                {
                        for ( i = 0 ; i < 4000 ; i++, ridx++)
			{
                		adcbuf[ridx] = popqueue(&buffer); //Dequeue(&rx_int_queue);
//				usleep(100); 
			}
		}
                        
		if ( ridx >= 4000 )
		{
			ridx = 0;
			printf("[%d] %x:%x\n", count(&buffer)/*Len_queue(&rx_int_queue)*/, adcbuf[0], adcbuf[3999]);
                }

       usleep(500);
    } while (exit_flag == FALSE);

    pthread_exit(NULL);
    retur NULL;
}

void *serial_rx(void *arg)
{
    int rx_length = 0;
    int value, cur; 
  //  uint8_t* buffer;

    int i; 
    int32_t data; 
	
    t_flag = 1;

    do {
        // Read up to 255 characters from the port if they are there
        if ( exit_flag == FALSE)
        {
	 	while (exit_flag == FALSE)
	 	{
	  		cur = digitalRead(IEPE_DRDY); 

			if (value != cur )
			{
				if ( cur == 0 ) 
				{
					data = spi_dataread(file); 
					push(&buffer, data);  
					//Enqueue(&rx_int_queue, data);
				}

				value = cur; 
			}
	  	}
	}
        
      	usleep(200);
    } while (exit_flag == FALSE);

    pthread_exit(NULL);
    retur NULL;
}

int rx_thread_create(void)
{
    int ret = 0;
    pthread_attr_t attr;

    ret = pthread_attr_init(&attr);
    if (ret != 0) {
        perror("pthread_attr_init failed");
        return -1;
    }

    ret = pthread_create(&tid, &attr, &serial_rx, NULL);
	
    if (ret != 0) {
        perror("pthread_create failed");
        return -1;
    }

    ret = pthread_create(&tid2, &attr, &process, NULL); 

    if ( ret != 0) {
	perror("process pthread_create failed"); 
	return -1;
    }

    ret = pthread_attr_destroy(&attr);
    if (ret != 0) {
        perror("pthread_attr_destroy failed");
        return -1;
    }

    return ret;
}

int tx_loop(void)
{
    unsigned char tx_hex[10] = {0,};
    int count = 0;

    do {
        fscanf(stdin, "%s", tx_hex);
        printf("TX BUFFER : #######\n");

        printf("  hexa string : ");
        print_bytes(STRING, strlen(tx_hex), tx_hex);

        usleep(10000);
    } while (strcmp(tx_hex, "FF") != 0);

    exit_flag = TRUE;
}

//Queue RX_int_queue; 

int main(void)
{
	int fd, fddac; 
	int value, cur, i, ret; 
	//uint8_t* buffer; 
	int32_t data; 
	int cnt = 0; 	
	
	init(&buffer, MAX_BUF_SIZE); 	
//	InitQueue(&rx_int_queue); 
	/* shift reg enable */ 

	digitalPinMode(GPIO_IEPESR_nOE, OUTPUT);
	digitalPinMode(GPIO_IEPESR_nCLR, OUTPUT); 

	digitalPinMode(IEPE_DRDY, INPUT); 

	digitalWrite(GPIO_IEPESR_nCLR, 1); 

	digitalWrite(GPIO_IEPESR_nOE, 1); 
	usleep(100); 
	digitalWrite(GPIO_IEPESR_nOE, 0); 

	//spi_srwrite(fd, 0x00);	//b00010000 RESET LoW, HR mode=0  
	//usleep(1000); 

#if 1

	fd = openSPIdev(DEVIEPESR);
	//spi_srwrite(fd, 0xa8);  // OSR1:OSR0 01 RESET High, HR mode = 1
	//spi_srwrite(fd, 0x20); 
    	spi_srwrite(fd, 0xe8);
	usleep(1000); 

/* DAC5311 Setting Start */

   //     fddac = openSPIdev(DEVCURDAC);
    //    setVoltage(3.0, fddac);
/* DAC5311 Setting End */
	
//	usleep(1000); 
#endif 

	speed = 16*1000*1000;  
	file = openSPIdev(DEVIEPE); 

	adcStart(file); 
	
	value = 1; 

	/* ADC RX */
	rx_thread_create();
		
	while (t_flag != 1) {
		usleep(100);
	}

	printf(" thread ok\n"); 
		
#if 1
#if 0 
    while (1)
    {	
		cnt = count(&buffer); //Len_queue(&rx_int_queue); //count(&buffer); 
		if ( cnt > 4000 )
		{
			for ( i = 0 ; i < 4000 ; i++)
				adcbuf[i] = popqueue(&buffer); //Dequeue(&rx_int_queue);  
			printf("[%d] %x:%x\n", count(&buffer)/*Len_queue(&rx_int_queue)*/, adcbuf[0], adcbuf[3999]);
		}
	}
#endif 	
	ret = tx_loop(); 

	if ( ret < 0 )
		return -1; 

	pthread_join(tid, NULL); 
        pthread_join(tid2, NULL); 	

	adcStop(file); 

#endif 
	close(file); 
	close(fd); 
//	close(fddac); 
}

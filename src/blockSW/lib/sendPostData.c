/*
  Copyright (c) 2009 Dave Gamble
 
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
 
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
 
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include "cJSON.h"

#include <stdio.h> /* printf, sprintf */
#include <stdlib.h> /* exit, atoi, malloc, free */
#include <unistd.h> /* read, write, close */
#include <string.h> /* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> /* struct hostent, gethostbyname */


#define _DEBUG


//char deviceSerial[128] = {0, };
//float sdata[5] = {0.0, };
//int ch = 0;

// Parse text to JSON, then render back to text, and print!
void doit(char *text)
{
	char *out;cJSON *json;
	
	json=cJSON_Parse(text);
	out=cJSON_Print(json);
	cJSON_Delete(json);
	printf("%s\n",out);
	free(out);	
}

// Read a file, parse, render back, etc.
void dofile(char *filename)
{
	FILE *f=fopen(filename,"rb");fseek(f,0,SEEK_END);long len=ftell(f);fseek(f,0,SEEK_SET);
	char *data=malloc(len+1);fread(data,1,len,f);fclose(f);
	doit(data);
	free(data);
}

#define STR_ID	    "id"
#define STR_DBM	    "dbm"
#define STR_MACH1	"machine1"
#define STR_MACH2	"machine2"
#define STR_MACH3	"machine3"

// Used by some code below as an example datatype.
struct record {const char *precision;double lat,lon;const char *address,*city,*state,*zip,*country; };

#if 0 
void makeGWData()
{
 	char* pResult = NULL;
 	cJSON *data;
 	cJSON *sensorArray;
 	cJSON *sensor; 
 	int i; 
 	
 	//char* out;
 	data = cJSON_CreateObject();
	sensorArray = cJSON_CreateArray();

  for (i = 0 ; i < 2 ; i++ )
  {
  
    FILE *rf;
    char str[1024]; 
    
    sprintf(filename,"./%d.dat", node_index);
    
    rf = fopen((const char*)filename, "r"); 
     	  	  	 
   // rf = fopen("./ble1.dat", "r");

    if(rf == NULL)
		{
		  printf("open error\n"); 
		  continue; // return -1;
		}

    fread(str, sizeof(str), 1, rf);
#ifdef _DEBUG
    printf("str:%s\n", str);
#endif  
    sensor=cJSON_Parse(str);
	  
  	cJSON_AddItemToArray(sensorArray, sensor);
  }
 	
 	
 	cJSON_AddStringToObject(data, "d", "G#39");
	cJSON_AddItemToObject(data, "data", sensorArray);
  
  pResult = cJSON_Print(data); 
	cJSON_Delete(data);
	
  printf("%s\n", pResult); 
}
#endif 

//static const char *host= "218.147.182.24"; 
//static char ServiceServerIP[] = "218.147.182.24:8003/api/v1/devices/%s/info";

static const char *host= "125.132.182.152";
static char ServiceServerIP[] = "125.132.182.152:8003/api/v1/devices/%s/info";
static char ServiceServerPort[] = "8003";

int sendPostMessage(char* devname, int ch, char* mesg) 
{
    /* first what are we going to send and where are we going to send it? */
    int portno =        8003;
    //char *host =        "192.168.20.29";
    char *message_fmt = "POST /api/v1/devices/%s/channels/%d/data HTTP/1.0\r\nContent-Type: application/json\r\nContent-Length: %d\r\n\r\n%s";

    struct hostent *server;
    struct sockaddr_in serv_addr;
    int sockfd, bytes, sent, received, total;

    char message[4096],response[4096];

	/* fill in the parameters */
    
#if 0 
    sprintf(message,"%s %s HTTP/1.0\r\n",
    "POST",                  /* method         */
    "/api/v1/insert/data");                    /* path           */
    
    strcat(message,"Content-Type: text/plain");
    strcat(message,"\r\n");
    
    sprintf(message+strlen(message),"Content-Length: %d\r\n",strlen(mesg));
    strcat(message,"\r\n");                                /* blank line     */
    
    strcat(message,mesg);                           /* body           */
#endif 

    sprintf(message, message_fmt, devname, ch, strlen(mesg), mesg);
    
#ifdef _DEBUG 
    printf("Request:\n%s\n",message);
#endif 

    /* create the socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) error("ERROR opening socket");

    /* lookup the ip address */
    server = gethostbyname(host);
    if (server == NULL) error("ERROR, no such host");

    /* fill in the structure */
    memset(&serv_addr,0,sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno);
    memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);

    /* connect the socket */
    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
        error("ERROR connecting");

   /* send the request */
    total = strlen(message);
    sent = 0;
    do {
        bytes = write(sockfd,message+sent,total-sent);
        if (bytes < 0)
            error("ERROR writing message to socket");
        if (bytes == 0)
            break;
        sent+=bytes;
    } while (sent < total);

#if 0 
    /* receive the response */
    memset(response,0,sizeof(response));
    total = sizeof(response)-1;
    received = 0;
   do 
   {
        bytes = read(sockfd,response+received,total-received);
        if (bytes < 0)
            error("ERROR reading response from socket");
   //     if (bytes == 0)
   //          break;
        received+=bytes;
    } while (received < total);

    if (received == total)
        error("ERROR storing complete response from socket");
#endif 

    /* close the socket */
    close(sockfd);

    /* process response */
    printf("Send OK\n"); 

    return 0;
}

static int sensor_cnt = 0; 

char timeprebuf[10][30];   

// Create a bunch of objects as demonstration.
void create_objects()
{
	cJSON *root, *rootsub, *fmt,*img,*thm,*fld;char *out;int i;	// declare a few.

	// Here we construct some JSON standards, from the JSON site.
	
	// Our "Video" datatype:
	root=cJSON_CreateObject();	
	
	cJSON_AddItemToObject(root, "d", cJSON_CreateString("Jack (\"Bee\") Nimble"));
	cJSON_AddItemToObject(root, "data", fmt=cJSON_CreateObject());
	
	cJSON_AddItemToArray(fmt,fld=cJSON_CreateObject());
	cJSON_AddStringToObject(fmt,"type",		"rect");
	cJSON_AddNumberToObject(fmt,"width",		1920);
	cJSON_AddNumberToObject(fmt,"height",		1080);
	cJSON_AddFalseToObject (fmt,"interlace");
	cJSON_AddNumberToObject(fmt,"frame rate",	24);
	
	
	out=cJSON_Print(root);	cJSON_Delete(root);	printf("%s\n",out);	free(out);	// Print to text, Delete the cJSON, print it, release the string.
	

	// Our "days of the week" array:
	const char *strings[7]={"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
	root=cJSON_CreateStringArray(strings,7);

	out=cJSON_Print(root);	cJSON_Delete(root);	printf("%s\n",out);	free(out);

	// Our matrix:
	int numbers[3][3]={{0,-1,0},{1,0,0},{0,0,1}};
	root=cJSON_CreateArray();
	for (i=0;i<3;i++) cJSON_AddItemToArray(root,cJSON_CreateIntArray(numbers[i],3));

//	cJSON_ReplaceItemInArray(root,1,cJSON_CreateString("Replacement"));
	
	out=cJSON_Print(root);	cJSON_Delete(root);	printf("%s\n",out);	free(out);


	// Our "gallery" item:
	int ids[4]={116,943,234,38793};
	root=cJSON_CreateObject();
	cJSON_AddItemToObject(root, "Image", img=cJSON_CreateObject());
	cJSON_AddNumberToObject(img,"Width",800);
	cJSON_AddNumberToObject(img,"Height",600);
	cJSON_AddStringToObject(img,"Title","View from 15th Floor");
	cJSON_AddItemToObject(img, "Thumbnail", thm=cJSON_CreateObject());
	cJSON_AddStringToObject(thm, "Url", "http://www.example.com/image/481989943");
	cJSON_AddNumberToObject(thm,"Height",125);
	cJSON_AddStringToObject(thm,"Width","100");
	cJSON_AddItemToObject(img,"IDs", cJSON_CreateIntArray(ids,4));

	out=cJSON_Print(root);	cJSON_Delete(root);	printf("%s\n",out);	free(out);

	// Our array of "records":
	struct record fields[2]={
		{"zip",37.7668,-1.223959e+2,"","SAN FRANCISCO","CA","94107","US"},
		{"zip",37.371991,-1.22026e+2,"","SUNNYVALE","CA","94085","US"}};

	root=cJSON_CreateArray();
	
	for (i=0;i<2;i++)
	{
		cJSON_AddItemToArray(root,fld=cJSON_CreateObject());
		cJSON_AddStringToObject(fld, "precision", fields[i].precision);
		cJSON_AddNumberToObject(fld, "Latitude", fields[i].lat);
		cJSON_AddNumberToObject(fld, "Longitude", fields[i].lon);
		cJSON_AddStringToObject(fld, "Address", fields[i].address);
		cJSON_AddStringToObject(fld, "City", fields[i].city);
		cJSON_AddStringToObject(fld, "State", fields[i].state);
		cJSON_AddStringToObject(fld, "Zip", fields[i].zip);
		cJSON_AddStringToObject(fld, "Country", fields[i].country);
	}
	
//	cJSON_ReplaceItemInObject(cJSON_GetArrayItem(root,1),"City",cJSON_CreateIntArray(ids,4));
	
	out=cJSON_Print(root);	cJSON_Delete(root);	printf("%s\n",out);	free(out);

}

typedef struct block 
{
	char kindOf[128]; 
	char desc[256]; 
	char blockID[10]; 
	char type[5][10]; 
	int cnt; 
}; 

struct block blockInfo; 

void getBlockInfo(char* filename)
{

#if 1 
  int count=0, i;
  FILE *rf;
  
  char *fstr; // [1024] = {0, }; 
  char *tempstr; 
  
  rf = fopen(filename, "r");
                                                        
  if(rf == NULL)
  {
  	printf(" list file open error\n"); 
	return -1;
  }
	
  fseek(rf, 0, SEEK_END); 

  int length = ftell(rf);
	
	fstr = (char *)malloc(length+1); 
	memset(fstr, length+1, 0); 
	
	if ( fstr == NULL )
	{
	  printf(" malloc error\n"); 
	  return; 
	} 
	
	fseek(rf, 0, SEEK_SET); 
  	fread(fstr, length+1, 1, rf);
  
#ifdef _DEBUG
  printf("length = %d\n", length); 
#endif 

	cJSON *json = cJSON_Parse((const char*)fstr); 
	
	cJSON *type = cJSON_GetObjectItem(json, "type"); 
	cJSON *kindof = cJSON_GetObjectItem(json, "sensorType"); 
	cJSON *desc = cJSON_GetObjectItem(json, "description"); 
	cJSON *bid = cJSON_GetObjectItem(json, "blockID"); 
	
	blockInfo.cnt = cJSON_GetArraySize(type); 
	printf("cnt : %d\n", blockInfo.cnt); 
	
	if (blockInfo.cnt) 
	{
		for ( i = 0 ; i < blockInfo.cnt; i++) 
	  	{	
		  cJSON *cJSONtype = cJSON_GetArrayItem(type, i); 
		  tempstr = cJSON_Print(cJSONtype); 		  
		  memcpy(blockInfo.type[i], &tempstr[1], strlen(tempstr)-2); 
#ifdef _DEBUG	
  		printf(" %s %s\n", cJSON_Print(cJSONtype), blockInfo.type[i]); 
#endif 	
		}
	}

	tempstr = cJSON_Print(kindof); 
  	memcpy(blockInfo.kindOf, &tempstr[1], strlen(tempstr)-2); 
	tempstr = cJSON_Print(desc);  
	memcpy(blockInfo.desc, &tempstr[1], strlen(tempstr)-2); 
	tempstr = cJSON_Print(bid); 	
	memcpy(blockInfo.blockID, &tempstr[1], strlen(tempstr)-2); 
		
#ifdef _DEBUG	
  		printf(" %s : %s, %s\n", blockInfo.kindOf, blockInfo.desc, blockInfo.blockID); 
#endif 	

	free(fstr); 
#endif 

}

void SendResultMessage(int ch, char *serial, float *sdata)//float temp, float hu)
{
        char* pResult = NULL;
        int i;

        cJSON *jsonResult;
        cJSON *jsonArray;

        cJSON *jsonSensorValuesArray;

        cJSON *sensorData[5];

        /* current time */

        struct timeval val;
        struct tm *ptm;

        char timeBuf[128]={0,};
        char usec[7]={0,};
        char usec_use[4]={0,};

        memset(timeBuf, 0, sizeof(timeBuf));
        gettimeofday(&val, NULL);
        ptm = localtime(&val.tv_sec);

        sprintf(usec,"%03ld", val.tv_usec);
        strncpy(usec_use, usec, 3);
        sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s"
                , ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
                , ptm->tm_hour, ptm->tm_min, ptm->tm_sec
                , usec_use);

//        jsonResult = cJSON_CreateObject();
        jsonSensorValuesArray = cJSON_CreateArray();

#if 0 
        for ( i = 0 ; i < blockInfo.cnt ; i++)
        {
                sensorData[i] = cJSON_CreateObject();

                cJSON_AddStringToObject(sensorData[i], "type", blockInfo.type[i]);
                cJSON_AddNumberToObject(sensorData[i], "id", 1);
                cJSON_AddNumberToObject(sensorData[i], "v", sdata[i]);
                cJSON_AddItemToArray(jsonSensorValuesArray, sensorData[i]);
        }

        cJSON_AddStringToObject(jsonResult, "B_ID", blockInfo.blockID);
        cJSON_AddNumberToObject(jsonResult, "channel", ch);
        cJSON_AddStringToObject(jsonResult, "serial", serial);
        cJSON_AddItemToObject(jsonResult, "values", jsonSensorValuesArray);
        cJSON_AddStringToObject(jsonResult, "time", timeBuf);
#else

	printf("blockInfo.cnt : %d\n", blockInfo.cnt); 

        for ( i = 0 ; i < blockInfo.cnt ; i++)
        {
                sensorData[i] = cJSON_CreateObject();

		cJSON_AddStringToObject(sensorData[i], "b_type", blockInfo.blockID);
        	cJSON_AddNumberToObject(sensorData[i], "ch", ch);
        	cJSON_AddStringToObject(sensorData[i], "serial", serial);

                cJSON_AddStringToObject(sensorData[i], "s_type", blockInfo.type[i]);
                cJSON_AddNumberToObject(sensorData[i], "i", 1);
                cJSON_AddNumberToObject(sensorData[i], "v", sdata[i]);
 		cJSON_AddStringToObject(sensorData[i], "t", timeBuf);

                cJSON_AddItemToArray(jsonSensorValuesArray, sensorData[i]);
        }
#endif 
       // pResult = cJSON_Print(jsonResult);
	pResult = cJSON_Print(jsonSensorValuesArray); 
        printf("Result(%d) : %s\n", (ch+1), pResult);
        //cJSON_Delete(jsonResult);
	cJSON_Delete(jsonSensorValuesArray); 

	sendPostMessage(serial, ch, pResult); 
}


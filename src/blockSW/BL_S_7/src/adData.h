#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <asm-generic/mman-common.h>

/*  SNOW GPIO SETTING */
/*
#define GPIO_ADDR 0xC000A000 // base address
#define GPIO_BLOCK 64 // memory block size

#define AD1		43
#define AD2		41
#define AD3		44
#define AD4		42
*/

/*  DRAGON GPIO SETTING */
#define GPIO_ADDR 0xC001A000 // base address
#define GPIO_BLOCK 64 // memory block size

#define AD1             107
#define AD2             106
#define AD3             109
#define AD4             108


volatile unsigned long *gpioAddress;

extern int adDataInit(void);
extern int adDataStart(void);
extern int adDataStop(void);

#include <stdio.h>         // printf()
#include <string.h>        // strlen()
#include <fcntl.h>         // O_WRONLY
#include <unistd.h>        // write(), close()
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <math.h>
#include <termios.h>


#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <time.h>

#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //Used for UART
#include <fcntl.h> //Used for UART

#include <termios.h> //Used for UART
#include <errno.h>
#include <ctype.h>

#include "digital.h"
#include "usergpio.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>

 /****************************************************************
 * Constants
 ****************************************************************/
 
#define SYSFS_GPIO_DIR "/sys/class/gpio"
#define POLL_TIMEOUT 150 // (3 * 1000) /* 3 seconds */
#define MAX_BUF 64

/****************************************************************
 * gpio_export
 ****************************************************************/
int gpio_export(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];
 
	fd = open(SYSFS_GPIO_DIR "/export", O_WRONLY);
	if (fd < 0) {
		perror("gpio/export");
		return fd;
	}
 
	len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	close(fd);
 
	return 0;
}

/****************************************************************
 * gpio_unexport
 ****************************************************************/
int gpio_unexport(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];
 
	fd = open(SYSFS_GPIO_DIR "/unexport", O_WRONLY);
	if (fd < 0) {
		perror("gpio/export");
		return fd;
	}
 
	len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	close(fd);
	return 0;
}

/****************************************************************
 * gpio_set_dir
 ****************************************************************/
int gpio_set_dir(unsigned int gpio, unsigned int out_flag)
{
	int fd, len;
	char buf[MAX_BUF];
 
	len = snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/direction", gpio);
 
	fd = open(buf, O_WRONLY);
	if (fd < 0) {
		perror("gpio/direction");
		return fd;
	}
 
	if (out_flag)
		write(fd, "out", 4);
	else
		write(fd, "in", 3);
 
	close(fd);
	return 0;
}

/****************************************************************
 * gpio_set_value
 ****************************************************************/
int gpio_set_value(unsigned int gpio, unsigned int value)
{
	int fd, len;
	char buf[MAX_BUF];
 
	len = snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
 
	fd = open(buf, O_WRONLY);
	if (fd < 0) {
		perror("gpio/set-value");
		return fd;
	}
 
	if (value)
		write(fd, "1", 2);
	else
		write(fd, "0", 2);
 
	close(fd);
	return 0;
}

/****************************************************************
 * gpio_get_value
 ****************************************************************/
int gpio_get_value(unsigned int gpio, unsigned int *value)
{
	int fd, len;
	char buf[MAX_BUF];
	char ch;

	len = snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
 
	fd = open(buf, O_RDONLY);
	if (fd < 0) {
		perror("gpio/get-value");
		return fd;
	}
 
	read(fd, &ch, 1);

	if (ch != '0') {
		*value = 1;
	} else {
		*value = 0;
	}
 
	close(fd);
	return 0;
}


/****************************************************************
 * gpio_set_edge
 ****************************************************************/

int gpio_set_edge(unsigned int gpio, char *edge)
{
	int fd, len;
	char buf[MAX_BUF];

	len = snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/edge", gpio);
 
	fd = open(buf, O_WRONLY);
	if (fd < 0) {
		perror("gpio/set-edge");
		return fd;
	}
 
	write(fd, edge, strlen(edge) + 1); 
	close(fd);
	return 0;
}

/****************************************************************
 * gpio_fd_open
 ****************************************************************/

int gpio_fd_open(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];

	len = snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
 
	fd = open(buf, O_RDONLY | O_NONBLOCK );
	if (fd < 0) {
		perror("gpio/fd_open");
	}
	return fd;
}

/****************************************************************
 * gpio_fd_close
 ****************************************************************/

int gpio_fd_close(int fd)
{
	return close(fd);
}


static uint32_t mode = 1;
static uint8_t bits = 8;
static uint32_t speed = 50000; // 25*1000*1000; // 50000;
static uint16_t delay = 0;
static int verbose;

int file; 

char tx[10];
char rx[10];

#define NUM_PRINT_BYTES  16

enum {
    STRING = 1,
    BINARY,
};

enum {
    FALSE,
    TRUE,
};

struct spi_ioc_transfer xfer[2];

static void pabort(const char *s)
{
        perror(s);
        abort();
}

void print_bytes(int type, int length, unsigned char *buffer)
{
    int i;
    char temp[NUM_PRINT_BYTES] = {0,};

    for (i = 0; i < length; i++) {
        printf("%02x ", buffer[i]);
        temp[i%NUM_PRINT_BYTES] = buffer[i];
        if (((i + 1) % NUM_PRINT_BYTES) == 0) {
            if (type == STRING) {
                printf("\t%s", temp);
            }
            printf("\n");
            memset(temp, 0, NUM_PRINT_BYTES);
        }
    }
    if (type == STRING) {
        if (i % NUM_PRINT_BYTES != 0)
            printf("\t%s", temp);
    }
}

/*
    spidevlib.c - A user-space program to comunicate using spidev.
                Gustavo Zamboni
*/
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

uint8_t buf[10];
uint8_t buf2[10];

struct spi_ioc_transfer xfer[2];


//////////
// Init SPIdev
//////////

int spi_init(char filename[40])
{
   int file;
    __u8    mode, lsb, bits;
    __u32 speed=16000000;


        if ((file = open(filename,O_RDWR)) < 0)
        {
            printf("Failed to open the bus.");
            /* ERROR HANDLING; you can check errno to see what went wrong */
            exit(1);
            }

        ///////////////
        // Verifications
        ///////////////
        //possible modes: mode |= SPI_LOOP; mode |= SPI_CPHA; mode |= SPI_CPOL; mode |= SPI_LSB_FIRST; mode |= SPI_CS_HIGH; mode |= SPI_3WIRE; mode |= SPI_NO_CS; mode |= SPI_READY;
        //multiple possibilities using |

        mode = SPI_CPHA;

            if (ioctl(file, SPI_IOC_WR_MODE, &mode)<0)   {
                perror("can't set spi mode");
                return;
                }

            if (ioctl(file, SPI_IOC_RD_MODE, &mode) < 0)
                {
                perror("SPI rd_mode");
                return;
                }

            if (ioctl(file, SPI_IOC_RD_LSB_FIRST, &lsb) < 0)
                {
                perror("SPI rd_lsb_fist");
                return;
                }
        //sunxi supports only 8 bits

            if (ioctl(file, SPI_IOC_WR_BITS_PER_WORD, (__u8[1]){8})<0)
                {
                perror("can't set bits per word");
                return;
                }

            if (ioctl(file, SPI_IOC_RD_BITS_PER_WORD, &bits) < 0)
                {
                perror("SPI bits_per_word");
                return;
                }

            if (ioctl(file, SPI_IOC_WR_MAX_SPEED_HZ, &speed)<0)
                {
                perror("can't set max speed hz");
                return;
                }

            if (ioctl(file, SPI_IOC_RD_MAX_SPEED_HZ, &speed) < 0)
                {
                perror("SPI max_speed_hz");
                return;
                }


    printf("%s: spi mode %d, %d bits %sper word, %d Hz max\n",filename, mode, bits, lsb ? "(lsb first) " : "", speed);

    //xfer[0].tx_buf = (unsigned char)buf;
    xfer[0].len = 3; /* Length of  command to write*/
    xfer[0].cs_change = 0; /* Keep CS activated */
    xfer[0].delay_usecs = 0, //delay in us
    xfer[0].speed_hz = 25*1000*1000, //speed
    xfer[0].bits_per_word = 8, // bites per word 8

    //xfer[1].rx_buf = (unsigned char) buf2;
    xfer[1].len = 4; /* Length of Data to read */
    xfer[1].cs_change = 0; /* Keep CS activated */
    xfer[0].delay_usecs = 0;
    xfer[0].speed_hz = 25*1000*1000; //25000000;
    xfer[0].bits_per_word = 8;

    return file;
}



//////////
// Read n bytes from the 2 bytes add1 add2 address
//////////

//char * spi_dataread(int file)
uint32_t spi_dataread(int file)
{
    int status, i;

    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);
    buf[0] = 0x12;
    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 1; /* Length of  command to write*/
    xfer[1].rx_buf = (unsigned long) buf2;
    xfer[1].len = 3; /* Length of Data to read */

    status = ioctl(file, SPI_IOC_MESSAGE(2), xfer);

    if (status < 0)
        {
        perror("SPI_IOC_MESSAGE");
        return;
        }
#if 0
        return (uint32_t)((buffer[0]<<16)|(buffer[1]<<8)|buffer[2]);
#else
//    printf("env: %02x %02x %02x %02x\n", buf2[0], buf2[1], buf2[2], buf2[3]);
//    printf("ret: ");

       //for ( i = 0 ; i < 3; i++)
       //         printf("%02x: ", buf2[i]); //, buf2[1], buf2[2], buf2[3]);
       //printf("\t");

    return buf2;
#endif 
}

char * spi_read(char add1, char add2, int nbytes,int file)
{
    int status, i;

    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);
    buf[0] = add1;
    buf[1] = add2;
    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 2+nbytes; /* Length of  command to write*/
    xfer[1].rx_buf = (unsigned long) buf2;
    xfer[1].len = nbytes; /* Length of Data to read */

    status = ioctl(file, SPI_IOC_MESSAGE(2), xfer);
    if (status < 0)
        {
        perror("SPI_IOC_MESSAGE");
        return;
        }
    printf("env: %02x %02x %02x\n", buf[0], buf[1], buf[2]);

    printf("ret: ");

        for ( i = 0 ; i < nbytes; i++)
                printf("%02x ", buf2[i]); //, buf2[1], buf2[2], buf2[3]);
        printf("\n");

    return buf2;
}

//////////
// Write n bytes int the 2 bytes address add1 add2
//////////

void spi_write(int add1,int add2,int nbytes,char value[10],int file)
{
    uint8_t   buf[32], buf2[32];
    int status;

    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);

    buf[0] = add1;
//    buf[1] = add2;

    if (nbytes>=1) buf[1] = value[0];
    if (nbytes>=2) buf[2] = value[1];
    if (nbytes>=3) buf[3] = value[2];
    if (nbytes>=4) buf[4] = value[3];

    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = nbytes+1; /* Length of  command to write*/
    status = ioctl(file, SPI_IOC_MESSAGE(1), xfer);

    if (status < 0)
    {
        perror("SPI_IOC_MESSAGE");
        return;
    }

    printf("env: %02x %02x %02x\n", buf[0], buf[1], buf[2]);
    printf("ret: %02x %02x %02x %02x\n", buf2[0], buf2[1], buf2[2], buf2[3]);
}

int openSPIdev(const char *devicename)
{
        int fd;
        int ret;

        fd = open(devicename, O_RDWR);

        if (fd < 0)
                pabort("can't open device");

        /*
         * spi mode
         */

        ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
        if (ret == -1)
                pabort("can't set spi mode");

        ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
        if (ret == -1)
                pabort("can't get spi mode");

       /*
         * bits per word
         */

        ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
        if (ret == -1)
                pabort("can't set bits per word");

        ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
        if (ret == -1)
                pabort("can't get bits per word");

       /*
         * max speed hz
         */
        ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
        if (ret == -1)
                pabort("can't set max speed hz");

        ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
        if (ret == -1)
                pabort("can't get max speed hz");

        //if (verbose)
        {
                printf("SPI Info \n");
                printf("- spi device : %s\n", devicename);
                printf("- spi mode: %d\n", mode);
                printf("- bits per word: %d\n", bits);
                printf("- max speed: %d Hz (%d KHz)\n", speed, speed/1000);
        }

        return fd;
}

void spi_srwrite(int fd, char data)
{
    unsigned char   buf[32], buf2[32];
    int status;

    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);

    buf[0] = data;
   // buf[1] = add1;
   // buf[2] = add2;

    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 1; // nbytes+3; /* Length of  command to write*/

    status = ioctl(fd, SPI_IOC_MESSAGE(1), xfer);

    if (status < 0)
    {
        perror("SPI_IOC_MESSAGE");
        return;
    }
    //printf("env: %02x %02x %02x\n", buf[0], buf[1], buf[2]);
    //printf("ret: %02x %02x %02x %02x\n", buf2[0], buf2[1], buf2[2], buf2[3]);
}

void transfer_cmd(int fd, uint8_t *tx, uint8_t *rx, size_t len)
{
        int ret;

        struct spi_ioc_transfer tr_cmd = {
        .tx_buf = (unsigned long)tx,
        .rx_buf = (unsigned long)rx,
        .len = len,
        .delay_usecs = delay,
        .speed_hz = speed,
        .bits_per_word = bits,
        };
#if 0
if (mode & SPI_TX_QUAD)
tr_cmd.tx_nbits = 4;
else if (mode & SPI_TX_DUAL)
tr_cmd.tx_nbits = 2;
if (mode & SPI_RX_QUAD)
tr_cmd.rx_nbits = 4;
else if (mode & SPI_RX_DUAL)
tr_cmd.rx_nbits = 2;
if (!(mode & SPI_LOOP)) {
if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
tr_cmd.rx_buf = 0;
else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
tr_cmd.tx_buf = 0;
}
#endif
        ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr_cmd);

        if (ret < 1)
        {
                printf("can't send spi messageaaaaaaaaaaaaaa");
        }

        printf("%x:%x:%x:%x\n", (char)rx[0], (char)rx[1], (char)rx[2], (char)rx[3]);
}

uint8_t send_rx[5];
//int file;

int readRegister(int fd, char addr1, char addr2)
{
        uint8_t CMD_tx[5];

        CMD_tx[0] = addr1;
        CMD_tx[1] = addr2;

        transfer_cmd(fd, CMD_tx, send_rx, 2); //sizeof(CMD_STATUS_tx));
}

int ctrlSPIdev(char *devname)
{
        char *buffer;
        char buf[10];
        int file;

        file=spi_init(devname); //dev

        buffer=(char *)spi_read(0x20,0x08,10,file); //reading the address 0xE60E
        sleep(1);

        buf[0] = 0x07;
//      spi_write(0x07, 0x00, 0, buf, file); // reset
        spi_write(0x08, 0x00, 0, buf, file); // start
//      spi_write(0x0a, 0x00, 0, buf, file); // stop

//      sleep(1);
        return file;
}

int adcStop(int file)
{
        spi_write(0x0a, 0x00, 0, NULL, file);

        return 0;
}

int adcStart(int file)
{
        spi_write(0x08, 0x00, 0, NULL, file);
        return 0;
}

int t_flag; /* flag for thread creation */
int exit_flag;
int write_flag;

pthread_t tid;

void *serial_rx(void *arg)
{
    int rx_length = 0;
	int value, cur; 
    uint8_t* buffer;
	
    t_flag = 1;

    do {
        // Read up to 255 characters from the port if they are there
        if ( exit_flag == FALSE)
        {
            cur = digitalRead(CUR_DRDY);

            if (value != cur )
            {
                if ( cur == 0 )
                {
                    uint32_t data = 0;
			data =(char *)spi_dataread(file);

//                  data = (uint32_t)((buffer[0]<<16)|(buffer[1]<<8)|buffer[2]);

                  printf("result : %08x-%d\n", data, data);
//  				spi_srwrite(fd, 0x20);
//                    i++;
                }
                value = cur;
            }
        }
      usleep(200);
    } while (exit_flag == FALSE && write_flag == FALSE);

    pthread_exit(NULL);
    return NULL;
}

int rx_thread_create(void)
{
    int ret = 0;
    pthread_attr_t attr;

    ret = pthread_attr_init(&attr);
    if (ret != 0) {
        perror("pthread_attr_init failed");
        return -1;
    }

    ret = pthread_create(&tid, &attr, &serial_rx, NULL);
    if (ret != 0) {
        perror("pthread_create failed");
        return -1;
    }

    ret = pthread_attr_destroy(&attr);
    if (ret != 0) {
        perror("pthread_attr_destroy failed");
        return -1;
    }

    return ret;
}



int tx_loop(void)
{
    unsigned char tx_hex[256] = {0,};
    unsigned char tx_bin[128] = {0,};
    int count = 0;

    do {
        fscanf(stdin, "%s", tx_hex);
        printf("TX BUFFER : #######\n");

        printf("  hexa string : ");
        print_bytes(STRING, strlen(tx_hex), tx_hex);
        printf("  binary : ");
        print_bytes(BINARY, strlen(tx_hex) / 2, tx_bin);

#if 0
        /* Filestrean, bytes to write, number of bytes to write */
        count = write(uart0, &tx_bin[0], strlen(tx_hex) / 2);
        if (count < 0) {
            printf("UART TX error\n");
            close(uart0);
            return -1;
        }
#endif
        usleep(10000);
    } while (strcmp(tx_hex, "FF") != 0);

    exit_flag = TRUE;
}

#if 0 


#else 
	
int main(void)
{
        int fd;
        int value, cur, i;
		int ret = 0; 
        uint8_t* buffer;
		
	unsigned int gpio; 
	struct pollfd fdset[2]; 

	int gpio_fd, rc; 
	int nfds = 2; 
	int len; 
	int timeout; 
		
	gpio = CUR_DRDY; 
		
	gpio_export(gpio);
	gpio_set_dir(gpio, 0);
	gpio_set_edge(gpio, "falling");
	gpio_fd = gpio_fd_open(gpio);
		
	timeout = POLL_TIMEOUT;

        /* shift reg enable */

        digitalPinMode(GPIO_CURSR_nOE, OUTPUT);
        digitalPinMode(GPIO_CURSR_nCLR, OUTPUT);

        //digitalPinMode(ADC_DRDY, INPUT);

        digitalWrite(GPIO_CURSR_nCLR, 1);

        digitalWrite(GPIO_CURSR_nOE, 1);
        usleep(100);
        digitalWrite(GPIO_CURSR_nOE, 0);

        spi_srwrite(fd, 0x00);  //b00010000 RESET LoW, HR mode=0
        usleep(1000);

#if 1
        fd = openSPIdev(DEVCURSR);

//      spi_srwrite(fd, 0xa8);  // OSR1:OSR0 01 RESET High, HR mode = 1
//      spi_srwrite(fd, 0x20);  // 250k
//      spi_srwrite(fd, 0xa0);  // 62.5K
//      spi_srwrite(fd, 0x60);  // 128k
//      spi_srwrite(fd, 0xe0);  // 32k

        spi_srwrite(fd, 0xf8);
        usleep(1000);

#endif
        /* ADC */

#if 1
        speed = 25*1000*1000;

        file = openSPIdev(DEVCUR);

        adcStart(file);
        value = 1;

#if 1
		while (1) {
			
			memset((void*)fdset, 0, sizeof(fdset));
			
			fdset[0].fd = STDIN_FILENO; 
			fdset[0].events = POLLIN; 

			fdset[1].fd = gpio_fd;
			fdset[1].events = POLLPRI;

			rc = poll(fdset, 2, timeout);      

			if (rc < 0) {
				printf("\npoll() failed!\n");
				return -1;
			}
		  
			if (rc == 0) {
				printf(".");
			}
				
			if (fdset[1].revents & POLLPRI) {
				lseek(fdset[1].fd, 0, SEEK_SET);
				len = read(fdset[1].fd, buf, MAX_BUF);
				
				printf("\npoll() GPIO %d:%d interrupt occurred\n", gpio, digitalRead(gpio));
				printf("0x%6x\n", spi_dataread(file)); 
			}
		}
		
#else	
	    /* RX */
		rx_thread_create();
		
		while (t_flag != 1) {
			usleep(10000);
		}
#endif 
		/* TX */
		ret = tx_loop();
		if (ret < 0)
			return -1;

		pthread_join(tid, NULL); // (void**)&res);
	
        adcStop(file);

#endif
	gpio_fd_close(gpio_fd);
        close(file);
        close(fd);
		
		return 0; 
}
#endif 

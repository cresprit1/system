
typedef enum
{
	SENSOR_TYPE_WIFI  = 0,
	SENSOR_TYPE_COMPOUND,
	SENSOR_TYPE_CO2,
	SENSOR_TYPE_VIBRATION,
	SENSOR_TYPE_RELAY_LAMP,
	SENSOR_TYPE_RELAY_PAN,
	SENSOR_TYPE_LCD,
	SENSOR_TYPE_MAX

}SensorType;


typedef struct
{
	int index;
	char swDriverLink[256];
	SensorType sType;
	char blockID[20];
	int mode;
	int power;
	char type[64];
}BlockInfo;



typedef struct
{
	char devAppUrl[256];
	BlockInfo blockInfo[20];
	int blockCount;
	char serviceName[128]; 
}DevCfgRes;

char* getSensorTypeTxt(int _sensorType);

int doProcessDevCfg(char* _jsonData, DevCfgRes* _devCfgRes);

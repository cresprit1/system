#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>

#include "tcpip/tcpip_lib.h"
#include "config.h"
#include "cJSON.h"

static int sock = -1;  

typedef struct _MYIF
{
  struct ifreq ifr_eth0; 
  struct ifreq ifr_wlan0; 
  uint16_t port; 
}; 

struct _MYIF myif; 

extern char gSERVER_NAME[16]; 

/**************************************
/*
/*      Setting Network Server Info    
/*
/**************************************/

int gDATA_PORT = 9999; 
int gALIVE_PORT = 9999; 

char gSERVER_NAME[16] = {0, }; 

void getServerInfo(char* filename)
{
  int count=0, i, k, len;
  char temp[10] = {0, }; 
  
  FILE *rf;
  
  char *fstr; // [1024] = {0, }; 
  char *str; 
  
  rf = fopen(filename, "r");
                                                        
  if(rf == NULL)
	{
	  printf(" list file open error\n"); 
	  return;
	}
	
	fseek(rf, 0, SEEK_END); 
	int length = ftell(rf);
	
	fstr = (char *)malloc(length+1); 
	memset(fstr, length+1, 0); 
	
	if ( fstr == NULL )
	{
	  printf(" malloc error\n"); 
	  return; 
	} 
	
	fseek(rf, 0, SEEK_SET); 
  fread(fstr, length+1, 1, rf);
  
#ifdef _DEBUG
  printf("length = %d\n", length); 
#endif 

	cJSON *json = cJSON_Parse((const char*)fstr); 
	
	cJSON *hostinfo = cJSON_GetObjectItem(json, "hostInfo"); 
	
	cJSON *hostInfo_ip = cJSON_GetObjectItem(hostinfo, "ip"); 	
	cJSON *hostInfo_dport = cJSON_GetObjectItem(hostinfo, "dport"); 
	cJSON *hostInfo_aport = cJSON_GetObjectItem(hostinfo, "aport"); 
		  
#ifdef _DEBUG	
  		printf(" %s : %s : %s\n", cJSON_Print(hostInfo_ip), cJSON_PrintNumber(hostInfo_dport), cJSON_PrintNumber(hostInfo_aport));
#endif 	

		str = cJSON_Print(hostInfo_ip); 
		len = strlen(str); //strlen(cJSON_Print(data_mac)); 
	  memcpy(gSERVER_NAME, &str[1], len-2);
		  
		str = cJSON_Print(hostInfo_dport); 
		len = strlen(str); 
	  memcpy(temp, &str[1], len-2);
	  
	  gDATA_PORT = atoi(temp); 

		str = cJSON_Print(hostInfo_aport); 
		len = strlen(str); 
	  memcpy(temp, &str[1], len-2);
	  
	  gALIVE_PORT = atoi(temp); 
		  
#ifdef _DEBUG	  
	  printf(" %s : %d : %d\n", gSERVER_NAME, gDATA_PORT, gALIVE_PORT);
#endif 

	free(fstr); 
	
  return; 
}

void getMyNetworkInfo(void)
{
   int fd;
   
   fd = socket(AF_INET, SOCK_DGRAM, 0);

    /* I want to get an IPv4 IP address */
    myif.ifr_eth0.ifr_addr.sa_family = AF_INET;
    myif.ifr_wlan0.ifr_addr.sa_family = AF_INET;

    /* I want IP address attached to "eth0" */
    strncpy( myif.ifr_eth0.ifr_name, "eth0", IFNAMSIZ-1);
    ioctl(fd, SIOCGIFADDR, &myif.ifr_eth0);

    /* I want IP address attached to "wlan0" */
    strncpy( myif.ifr_wlan0.ifr_name, "wlan0", IFNAMSIZ-1);
    ioctl(fd, SIOCGIFADDR, &myif.ifr_wlan0);

    close(fd); 
 
   /* display result */
   printf("%x - %s\n", ((struct sockaddr_in *)&myif.ifr_eth0.ifr_addr)->sin_addr, inet_ntoa(((struct sockaddr_in *)&myif.ifr_eth0.ifr_addr)->sin_addr));
   
   /* display result */
   printf("%x - %s\n", ((struct sockaddr_in *)&myif.ifr_wlan0.ifr_addr)->sin_addr, inet_ntoa(((struct sockaddr_in *)&myif.ifr_wlan0.ifr_addr)->sin_addr));
   
  return 0;
}

uint32_t getWLANIP(void)
{
  return (uint32_t)(((struct sockaddr_in *)&myif.ifr_wlan0.ifr_addr)->sin_addr.s_addr); 
}

uint32_t getETHIP(void)
{
  return (uint32_t)(((struct sockaddr_in *)&myif.ifr_eth0.ifr_addr)->sin_addr.s_addr); 
}

int openNetwork(void)
{
    getMyNetworkInfo(); 
}


int sendData_tcpip(int port, unsigned char* data, int len)
{
    int sock;
    //char recv_buf[2048] = {0}; 
    //int recv_len; 

#ifdef CONNECTION_EVENT

	/* printf("CONNECTION_EVENT\n");
  if((sock = TCPIPconnect(gSERVER_NAME, port)) == -1) {
    printf("tcp_connect error: %s\n", strerror(errno));
    return -1;
  } */ 
  
  printf("CONNECTION_EVENT (%d)\n", port);
  
  if((sock = TCPIPconnect_Timeout(gSERVER_NAME, port, 1000)) == -1) {
    printf("tcp_connect error(%d): %s\n", port, strerror(errno));
    return -1;
  }
  
#endif 

  printf("SEND DATA : %s\n", strerror(errno));  

  if((TCPIPsend(sock, data, len, 0)) == -1) {
      printf("SEND ERROR\n"); 
      TCPIPclose(sock); 
      return -1; // exit(1);
  }
  else 
  {
    printf("Send Message.\n"); 
  }
    
  #if 0
  
  if((recv_len = TCPIPrecv(sock, recv_buf, 2048, 0)) == -1) {
    /* TO-DO : ���� �α� ���� */
    TCPIPclose(sock); 
    //exit(1);
  } 
  
  #endif         
      
#ifdef	CONNECTION_EVENT
  TCPIPclose(sock); 
#endif 
    
    return 0;
}

void closeNetwork(void)
{
  /* To-Do */ 
}
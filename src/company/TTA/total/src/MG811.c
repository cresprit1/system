/*******************Demo for MG-811 Gas Sensor Module V1.1*****************************
Author:  Tiequan Shao: tiequan.shao@sandboxelectronics.com
         Peng Wei:     peng.wei@sandboxelectronics.com
         
Lisence: Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0)

Note:    This piece of source code is supposed to be used as a demostration ONLY. More
         sophisticated calibration is required for industrial field application. 
         
                                                    Sandbox Electronics    2012-05-31
************************************************************************************/

#include <stdio.h>
#include <math.h> 

#include "../../lib/userlib.h"

#include "analog.h"
//#include "digital.h"
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#define SHARED_MEMORY_KEY 1005
#define MEMORY_SIZE 200

#define READ_CLIENT_FLAG 0
#define READ_SERVER_FLAG 1
#define PRINT_CLIENT_FLAG 2

float sdata[5]; 
//char deviceSerial[128] = {0, }; 
int ch = 0; 

/************************Hardware Related Macros************************************/
#define         MG_PIN                       (7)     //define which analog input channel you are going to use
#define         BOOL_PIN                     (10)
#define         DC_GAIN                      (8.5) // 3 : 0.6 -> 1.8 , 8.5 0.6 -> 5 (8.5)   //define the DC gain of amplifier

/***********************Software Related Macros************************************/
#define         READ_SAMPLE_INTERVAL         (50)    //define how many samples you are going to take in normal operation
#define         READ_SAMPLE_TIMES            (100)     //define the time interval(in milisecond) between each samples in 
                                                     //normal operation

/**********************Application Related Macros**********************************/
//These two values differ from sensor to sensor. user should derermine this value.
//#define         ZERO_POINT_VOLTAGE           (0.220) //define the output of the sensor in volts when the concentration of CO2 is 400PPM
#define ZERO_POINT_VOLTAGE 	(0.48/8.5) //(0.2l)//(0.260)//(0.39) //(0.324) 
#define         REACTION_VOLTGAE            (0.010) //define the voltage drop of the sensor when move the sensor from air into 1000ppm CO2

/*****************************Globals***********************************************/
float           CO2Curve[3]  =  {2.602,ZERO_POINT_VOLTAGE,(REACTION_VOLTGAE/(2.602-3))};   
                                                     //two points are taken from the curve. 
                                                     //with these two points, a line is formed which is
                                                     //"approximately equivalent" to the original curve.
                                                     //data format:{ x, y, slope}; point1: (lg400, 0.324), point2: (lg4000, 0.280) 
                                                     //slope = ( reaction voltage ) / (log400 –log1000) 

// C function showing how to do time delay

#include <time.h>
 
void delay(int milli_seconds) // number_of_seconds)
{
    // Converting time into milli_seconds
   // int milli_seconds = 1000 * number_of_seconds;
 
    // Stroing start time
    clock_t start_time = clock(); 
 
    // looping till required time is not acheived
    while (clock() < start_time + milli_seconds)
        ;
}
 
void setup()
{
    //digitalPinMode(BOOL_PIN, INPUT);                        //set pin to input
    //digitalWrite(BOOL_PIN, HIGH);                    //turn on pullup resistors

 //   printf("MG-811 Demostration\n");                
}



/*****************************  MGRead *********************************************
Input:   mg_pin - analog channel
Output:  output of SEN-000007
Remarks: This function reads the output of SEN-000007
************************************************************************************/ 
float MGRead(int mg_pin)
{
    int i;
    float v=0;
    int value; 

#if 1 
    for (i=0;i<READ_SAMPLE_TIMES;i++) {

	value = analogRawRead(mg_pin); 

//	printf("%d - %d\n", i, value); 

        v += value; //  * (0.439453125));  // v = in_voltageX_raw * 0.439453125 mV, ARTIK ADC 
;
        delay(READ_SAMPLE_INTERVAL);
    }
#endif 

    v = (v/READ_SAMPLE_TIMES)* (1.8/4096); // (5/1024) ;
    return v;  
}

/*****************************  MQGetPercentage **********************************
Input:   volts   - SEN-000007 output measured in volts
         pcurve  - pointer to the curve of the target gas
Output:  ppm of the target gas
Remarks: By using the slope and a point of the line. The x(logarithmic value of ppm) 
         of the line could be derived if y(MG-811 output) is provided. As it is a 
         logarithmic coordinate, power of 10 is used to convert the result to non-logarithmic 
         value.
************************************************************************************/ 
int  MGGetPercentage(float volts, float *pcurve)
{
   if ((volts/DC_GAIN )>=ZERO_POINT_VOLTAGE) {
      return -1;
   } else { 
      return pow(10, ((volts/DC_GAIN)-pcurve[1])/pcurve[2]+pcurve[0]);
   }
}

int readCo2Data(void)
{
    int percentage;
    float volts;
    int i; 

  {
	   
    volts = MGRead(MG_PIN); 
    
    printf("\n  - SEN-00007: %f V / before_amp : %f V ", volts, volts/DC_GAIN);
    
    percentage = MGGetPercentage(volts,CO2Curve);

   // sdata[0] = percentage;  
    
    if (percentage == -1) {
		printf( "  - CO2: < 400 \n");    
		sdata[0] = 400; // min value (ppm) ZERO_POINT_VOLTAGE; 
		percentage = 400; 
    } else {
        printf("  - CO2: %d ppm\n", percentage); 
    }

	//ch = 1; 
	//SendResultMessage(ch, deviceSerial, sdata); 
        //printf("mg811,%2.1f,%s,", sdata[0], "PPM");
   }
   return percentage; 
}


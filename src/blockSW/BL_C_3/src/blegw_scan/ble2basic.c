
#include "ble2basic.h"

struct hci_request ble_hci_request(uint16_t ocf, int clen, void * status, void * cparam)
{
	struct hci_request rq;
	memset(&rq, 0, sizeof(rq));
	rq.ogf = OGF_LE_CTL;
	rq.ocf = ocf;
	rq.cparam = cparam;
	rq.clen = clen;
	rq.rparam = status;
	rq.rlen = 1;
	return rq;
}

static void eir_parse_name(uint8_t *eir, size_t eir_len,
						char *buf, size_t buf_len)
{
	size_t offset;

	offset = 0;
	while (offset < eir_len) {
		uint8_t field_len = eir[0];
		size_t name_len;

		/* Check for the end of EIR */
		if (field_len == 0)
			break;

		if (offset + field_len > eir_len)
			goto failed;

		switch (eir[1]) {
		case EIR_NAME_SHORT:
		case EIR_NAME_COMPLETE:
			name_len = field_len - 1;
			if (name_len > buf_len)
				goto failed;

			memcpy(buf, &eir[2], name_len);
			return;
		}

		offset += field_len + 1;
		eir += field_len + 1;
	}

failed:
	snprintf(buf, buf_len, "(unknown)");
}

int pushSensorData(int node_index, void* data)
{
	return 1;
}

void process_data(uint8_t *data, size_t data_len, le_advertising_info *info, char rssi, int node_index)
{

}

/* ble device */ 

int ble_Open(void)
{
	int dev; 
	
	// Get HCI device.
	
	dev = hci_open_dev(hci_get_route(NULL));
	
	return dev; 

}

int ble_SetParam(int device)
{
	// Set BLE scan parameters.
	
	int status; 
	int ret; 
	
	le_set_scan_parameters_cp scan_params_cp;
	memset(&scan_params_cp, 0, sizeof(scan_params_cp));
	scan_params_cp.type 			= 0x00; 
	scan_params_cp.interval 		= htobs(0x0010);
	scan_params_cp.window 			= htobs(0x0010);
	scan_params_cp.own_bdaddr_type 	= 0x00; // Public Device Address (default).
	scan_params_cp.filter 			= 0x00; // Accept all.
	
	struct hci_request scan_params_rq = ble_hci_request(OCF_LE_SET_SCAN_PARAMETERS, LE_SET_SCAN_PARAMETERS_CP_SIZE, &status, &scan_params_cp);
	
	ret = hci_send_req(device, &scan_params_rq, 1000);
	
  return ret; 
}

int ble_SetEvent(int device)
{
	// Set BLE events report mask.
	
	int status, ret; 	
	
	le_set_event_mask_cp event_mask_cp;
	memset(&event_mask_cp, 0, sizeof(le_set_event_mask_cp));

	int i = 0;
	for ( i = 0 ; i < 8 ; i++ ) event_mask_cp.mask[i] = 0xFF;

  struct hci_request set_mask_rq = ble_hci_request(OCF_LE_SET_EVENT_MASK, LE_SET_EVENT_MASK_CP_SIZE, &status, &event_mask_cp);
	
	ret = hci_send_req(device, &set_mask_rq, 1000);
	
	if ( ret < 0 ) {
		hci_close_dev(device);
		perror("Failed to set event mask.");
		return 0;
	}

}

int ble_Enablescan(int device)
{
	// Enable scanning.
	
	int status; 
  int ret; 	
  
  le_set_scan_enable_cp scan_cp;
	memset(&scan_cp, 0, sizeof(scan_cp));
	scan_cp.enable 		= 0x01;	// Enable flag.
	scan_cp.filter_dup 	= 0x00; // Filtering disabled.

	struct hci_request enable_adv_rq = ble_hci_request(OCF_LE_SET_SCAN_ENABLE, LE_SET_SCAN_ENABLE_CP_SIZE, &status, &scan_cp);

	ret = hci_send_req(device, &enable_adv_rq, 1000);
	
  return ret; 
}

int ble_SetSocket(int device)
{
  // Get Result 
  
  struct hci_filter nf;
  int ret; 

	hci_filter_clear(&nf);
	hci_filter_set_ptype(HCI_EVENT_PKT, &nf);
	hci_filter_set_event(EVT_LE_META_EVENT, &nf);
	
	ret = setsockopt(device, SOL_HCI, HCI_FILTER, &nf, sizeof(nf));  

  return ret; 
}

void ble_disable(int device) 
{
	// Disable scanning.
	
	int status; 
  int ret; 	
  
  le_set_scan_enable_cp scan_cp;
	memset(&scan_cp, 0, sizeof(scan_cp));
	scan_cp.enable = 0x00;	// Disable flag.

	struct hci_request disable_adv_rq = ble_hci_request(OCF_LE_SET_SCAN_ENABLE, LE_SET_SCAN_ENABLE_CP_SIZE, &status, &scan_cp);

	ret = hci_send_req(device, &disable_adv_rq, 1000);
	
  if ( ret < 0 )
  {
    hci_close_dev(device); 
    perror("Failed to disable scan."); 
    //return 0; 
  }
  
  hci_close_dev(device); 
}

void *ble_thread(void *arg)
{
	int ret, status;
	int device; 

	char name[248] = { 0 };
	struct timeval val;
	struct tm *ptm;
	unsigned long pre_usec=0;

	int i; 
	
	device = ble_Open(); 
	
	if ( device < 0 ) 
	{
		perror("Failed to open HCI device.");
		return 0; 
	}

  ret = ble_SetParam(device); 

	if ( ret < 0 ) {
		hci_close_dev(device);
		perror("Failed to set scan parameters data.");
		return 0;
	}
	
	ret = ble_SetEvent(device); 
	
	if ( ret < 0 ) {
		hci_close_dev(device);
		perror("Failed to set event mask.");
		return 0;
	}
	
	ret = ble_Enablescan(device); 
	
	if ( ret < 0 ) {
		hci_close_dev(device);
		perror("Failed to enable scan.");
		return 0;
	}
	
  ret = ble_SetSocket(device); 
  
  if ( ret < 0 ) {
		hci_close_dev(device);
		perror("Could not set socket options\n");
		return 0;
	}
	
	printf("Scanning....\n");
	
	uint8_t buf[HCI_MAX_EVENT_SIZE];
	evt_le_meta_event * meta_event;
	le_advertising_info * info;

	int len;

	val.tv_usec = 0;
	
	while ( 1 ) 
	{
		pthread_mutex_lock(&receiveMutex);
				
		len = read(device, buf, sizeof(buf));

		if ( len >= HCI_EVENT_HDR_SIZE ) 
		{
			meta_event = (evt_le_meta_event*)(buf+HCI_EVENT_HDR_SIZE+1);
			
			if ( meta_event->subevent == EVT_LE_ADVERTISING_REPORT ) 
			{
				uint8_t reports_count = meta_event->data[0];
				void * offset = meta_event->data + 1;
				
				while ( reports_count-- ) 
				{
					info = (le_advertising_info *)offset;
					char addr[18];
					ba2str(&(info->bdaddr), addr);
					//ret1= hci_read_remote_name(device, &(info->bdaddr), sizeof(name),name, 0);
					memset(name, 0, sizeof(name));
					eir_parse_name(info->data, info->length, name, sizeof(name) - 1);

          {
            char *p = NULL; 
            int i = 0 ; 
            
          	p = &info->data; 
              
       //     if( p[2] == 0x04 && p[4] == 0xff && p[5]== 0x59 & p[6]== 0x00 )   // scan point 
            {
              printf("OK\n"); 
              printf("\n ===== %s ===== \n", addr); 	
              
            	for ( i = 0 ; i < 10 ;  i++) 
            	{
                printf("%x ", p[i]); 
            	}
            	printf("\n");  
            }
          }
				}  // while 
			} // if 
			
			pthread_mutex_unlock(&receiveMutex);				
		}
	} 
	
	ble_disable(device); 

	return 0;
}



	



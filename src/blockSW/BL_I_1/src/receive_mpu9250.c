#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>

#include <time.h>
#include <string.h>

#include "../../lib/userlib.h"

#define BAUDRATE B115200
#define MODEMDEVICE "/dev/ttyAMA3"
#define _POSIX_SOURCE 1         //POSIX compliant source

#define FALSE 0
#define TRUE 1

volatile int STOP=FALSE;

void signal_handler_IO (int status);    //definition of signal handler
int wait_flag=TRUE;                     //TRUE while no signal received
char devicename[80];
long Baud_Rate = 115200;         // default Baud Rate (110 through 38400)
long BAUD;                      // derived baud rate from command line
long DATABITS;
long STOPBITS;
long PARITYON;
long PARITY;
int Data_Bits = 8;              // Number of data bits
int Stop_Bits = 1;              // Number of stop bits
int Parity = 0;                 // Parity as follows:
// 00 = NONE, 01 = Odd, 02 = Even, 03 = Mark, 04 = Space

char buf[1024]; 

float sdata[5]; 
char deviceSerial[128] = {0, }; 
int ch = 0; 

int main(int argc, char *argv[])
{
	int fd; 
	int i; 
	float rms = 0;
	float gyro_y, gyro_z, temp;
	
	if (argc > 1)
		ch = atoi(argv[1]); 
	else 
		ch = 0; 

	if ( argc > 2 )
		memcpy(deviceSerial, argv[2], strlen(argv[2])-1); 

	printf("MPU9250\n"); 
	getBlockInfo("../info.json"); 

	fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY | O_NDELAY); 

	struct termios toptions;

	tcgetattr(fd, &toptions); 

	cfsetispeed(&toptions, B115200); 
	cfsetospeed(&toptions, B115200); 

	toptions.c_cflag &= ~PARENB;
	toptions.c_cflag &= ~CSTOPB;
	toptions.c_cflag &= ~CSIZE;
	toptions.c_cflag |= CS8;
	
	toptions.c_cflag &= ~CRTSCTS;
	
	toptions.c_cflag |= CREAD | CLOCAL;
	
	toptions.c_iflag &= ~(IXON | IXOFF | IXANY); 
	
	toptions.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG); 
	toptions.c_oflag &= ~OPOST;
	
	toptions.c_cc[VMIN]=12;
	toptions.c_cc[VTIME]=0;
	
	tcsetattr(fd, TCSANOW, &toptions); 

	//for ( i = 0 ; i < 1024 ; i++) 
	while(1)
	{	
		usleep(1000);

		int n = read(fd, buf, 1024); 
		char *p = NULL;
		
//		printf("%i bytes got read...\n", n); 

		p = strtok(buf, ",");
		if(p == NULL)
			continue;
		rms = atof(p);

		p = strtok(NULL, ",");
		if(p == NULL)
			continue;
		gyro_y = atof(p);

		p = strtok(NULL, ",");
		if(p == NULL)
			continue;
		gyro_z = atof(p);

		p = strtok(NULL, ",");
		if(p == NULL)
			continue;		
		temp = atof(p);
	
		printf("%1.4f    %f     %f\n", rms, gyro_y, gyro_z); 	
	//	printf("%1.4%f    %f     %f    %f\n", floorf(rms*100)/100, floorf(gyro_y*100)/100, floorf(gyro_z*100)/100, floorf(temp*100)/100); 
		
		sdata[0] = rms; 
		sdata[1] = gyro_y; 
		sdata[2] = gyro_z; 
	
		tcflush(fd, TCIFLUSH); 

		SendResultMessage(ch, deviceSerial, sdata); 
		sleep(1); 
#ifndef USER_LOOP
		break; 
#endif 
	}
	close(fd); 	
		
	return 0; 
}

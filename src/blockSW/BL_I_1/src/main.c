#include <stdio.h>
#include <stdbool.h>
#include <sys/shm.h>

#define HIGH 1
#define LOW 0 
#define INPUT 1
#define OUTPUT 0 

#define SHARED_MEMORY_KEY 2005
#define MESSAGE_QUEUE_KEY 4500

#define MEMORY_SIZE 32

#define READ_CLIENT_FLAG 0
#define READ_SERVER_FLAG 1
#define PRINT_CLIENT_FLAG 2

char *buffer;
char *string;

bool digitalPinMode(int pin, int dir){
  FILE * fd;
  char fName[128];
 
  //Exporting the pin to be used
  if(( fd = fopen("/sys/class/gpio/export", "w")) == NULL) {
    printf("Error: unable to export pin\n");  
    return false;
  }
 
  fprintf(fd, "%d\n", pin);
  fclose(fd);	// Setting direction of the pin
  sprintf(fName, "/sys/class/gpio/gpio%d/direction", pin); 
  if((fd = fopen(fName, "w")) == NULL) { 
    printf("Error: can't open pin direction\n");
    return false;
  }
 
  if(dir == OUTPUT) {
    fprintf(fd, "out\n");
  } else {
    fprintf(fd, "in\n");
  }
 
  fclose(fd);
  return true;
}

 
int digitalRead(int pin) {
  FILE * fd;
  char fName[128];
  char val[2];
 
  //Open pin value file
  sprintf(fName, "/sys/class/gpio/gpio%d/value", pin);
  if((fd = fopen(fName, "r")) == NULL) {
     printf("Error: can't open pin value\n");
     return false;
  }
 
  fgets(val, 2, fd);
  fclose(fd);
 
  return atoi(val);
}

 
bool digitalWrite(int pin, int val) { 
  FILE * fd;
  char fName[128];
 
  // Open pin value file
  sprintf(fName, "/sys/class/gpio/gpio%d/value", pin);
  if((fd = fopen(fName, "w")) == NULL) {
    printf("Error: can't open pin value\n");
    return false;
  }
 
  if(val == HIGH) { 
    fprintf(fd, "1\n");
  } else { 
    fprintf(fd, "0\n");
  }
 
  fclose(fd);
  return true;
}

float sdata[5]; 
char deviceSerial[128] = {0, }; 
int ch = 0; 

#define LAMP_PIN 24

void main(int argc, char *argv[])
{
	int onoff; 
	int shmid; 

  	int is_comm = 0; 

	if ( argc > 1 )
 	{
        	is_comm = atoi(argv[1]);
        	printf(" is_comm : %d\n", is_comm);
  	}

        if (argc > 2)
                ch = atoi(argv[2]);
        else
                ch = 0;

        if ( argc > 3 )
                memcpy(deviceSerial, argv[3], strlen(argv[3]));

/*        if ( argc > 4 )
        {
                onoff = atoi(argv[4]);
                printf("onoff : %d\n", onoff);
        }
*/  
	shmid = shmget((key_t)SHARED_MEMORY_KEY, (size_t)MEMORY_SIZE, 0777|IPC_CREAT); 

	if(shmid == -1)
	{
		perror("shmat failed:"); 
		exit(0); 
	}

	buffer = (char*)shmat(shmid, NULL, 0); 

	if ( buffer == (char*)-1) {
		perror("shmat failed:"); 
		exit(0); 
	}

	string = buffer + 1; 
	buffer[0] = READ_CLIENT_FLAG; 

	getBlockInfo("../info.json"); 

	digitalPinMode(LAMP_PIN, OUTPUT); 

	while(1)
	{
		char dataString[20] = {0, }; 
//		char *pch; 

		if ( buffer[0] == READ_SERVER_FLAG)
		{
			char dataString[20] = {0, }; 
			char *pch; 

			strncpy(dataString, string, strlen(string)); 

			buffer[0] = PRINT_CLIENT_FLAG; 

			printf("BL_I_1 : %s\n", dataString); 

/*			pch = strtok(dataString, ":"); 
			printf("strtok:%s\n", pch);
			while(pch != NULL)
			{
				pch = strtok(NULL, ":"); 
			} 
			printf("strtok:%s\n", pch); 
*/
			onoff = atoi(dataString); 

			printf("onoff : %d\n", onoff); 
	        	
			if ( onoff )
	                	digitalWrite(LAMP_PIN, 0); // ON
        		else
                		digitalWrite(LAMP_PIN, 1); // OFF

        		sdata[0] = (float)onoff;

       	        	SendResultMessage(ch, deviceSerial, sdata);
			printf("BL_I_1 - sendMessage\n"); 
      	        	usleep(500*1000);
		}
		else 
			usleep(500*1000); 
	}

	printf("EXIT BL_I_1\n"); 
	return 0; 
}

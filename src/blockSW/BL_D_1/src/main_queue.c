#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <time.h>
#include <string.h>

#define BAUDRATE B115200
//#define MODEMDEVICE "/dev/ttyUSB0"
#define MODEMDEVICE "/dev/ttyAMA1" 

#define _POSIX_SOURCE 1         //POSIX compliant source

#define FALSE 0
#define TRUE 1

typedef struct {
	long mtype; 
        char mtext[256]; 
} MsgType; 
	
volatile int STOP=FALSE;

void signal_handler_IO (int status);    //definition of signal handler
int wait_flag=TRUE;                     //TRUE while no signal received
char devicename[80];
long Baud_Rate = 115200; // 9600;         // default Baud Rate (110 through 38400)
long BAUD;                      // derived baud rate from command line
long DATABITS;
long STOPBITS;
long PARITYON;
long PARITY;
int Data_Bits = 8;              // Number of data bits
int Stop_Bits = 1;              // Number of stop bits
int Parity = 0;                 // Parity as follows:
// 00 = NONE, 01 = Odd, 02 = Even, 03 = Mark, 04 = Space

char buf[1024]; 

#define SHARED_MEMORY_KEY 1005
#define MESSAGE_QUEUE_KEY 4499 

#define MEMORY_SIZE 200

#define READ_CLIENT_FLAG 0
#define READ_SERVER_FLAG 1 
#define PRINT_CLIENT_FLAG 2 

#if 0 
int main() 
{
	int shmid; 
	char *buffer; 
	char *string; 

	shmid = shmget((key_t)SHARED_MEMORY_KEY, 0, 0); 

	if (shmid == -1)
	{
		perror("shmat failed:"); 
		exit(0); 
	}

	buffer = (char*)shmat(shmid, NULL, 0); 
	if(buffer == (char*)-1) {
		perror("shmat failed:"); 
		exit(0); 
	}

	string = buffer + 1; 

	buffer[0] = READ_CLIENT_FLAG; 

	while(1)
	{
		if(buffer[0] == READ_CLIENT_FLAG)
		{
			printf("message:");  
			fgets(string, 199, stdin); 
			string[strlen(string)-1]="\0"; 

			buffer[0] = READ_SERVER_FLAG; 
		}
		else if (buffer[0] == PRINT_CLIENT_FLAG)
		{
			puts(string); 
			buffer[0] = READ_CLIENT_FLAG; 
		}
		sleep(1); 
	}
}
#endif 

#if 1 
int main(int argc, char *argv[])
{
	int shmid; 
	key_t key = 4499; 
	int que_id; 
	char *buffer; 
	char *string; 
	int string_argc = 0; 
	char *string_argv[10]; 
	char *p2; 

	int fd; 
	int i; 
#if 1
	fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY | O_NDELAY | O_SYNC); 

	struct termios toptions;

	tcgetattr(fd, &toptions); 

	cfsetispeed(&toptions, B9600); 
	cfsetospeed(&toptions, B9600); 

	toptions.c_cflag &= ~PARENB;
	toptions.c_cflag &= ~CSTOPB;
	toptions.c_cflag &= ~CSIZE;
	toptions.c_cflag |= CS8;
	
	toptions.c_cflag &= ~CRTSCTS;
	
	toptions.c_cflag |= CREAD | CLOCAL;
	
	toptions.c_iflag &= ~(IXON | IXOFF | IXANY); 
	
	toptions.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG); 
	toptions.c_oflag &= ~OPOST;
	
	toptions.c_cc[VMIN]=12;
	toptions.c_cc[VTIME]=0;
	
	tcsetattr(fd, TCSANOW, &toptions); 
#endif 

#if 0 // shard memory 
/* shared Memory */ 
	shmid = shmget((key_t)SHARED_MEMORY_KEY, 0, 0); //(size_t)MEMORY_SIZE, 0777|IPC_CREAT); 

	if(shmid == -1)
	{
		perror("shmat failed:"); 
		exit(0); 
	}

	buffer = (char*)shmat(shmid, NULL, 0); 
	if(buffer == (char*)-1) {
		perror("shmat failed:"); 
		exit(0); 
	}

	string = buffer + 1; 
	buffer[0] = READ_CLIENT_FLAG; 
#endif 

#if 1 // message queue
	que_id = msgget(key, IPC_CREAT | 0600); 
#endif 	
	while(1)
	{

		MsgType msg; 
		ssize_t nbytes = 0; 
		int msg_size = 0; 

		msg_size = sizeof(msg) - sizeof(msg.mtype); 

		nbytes = msgrcv(que_id, &msg, msg_size, pid, IPC_NOWAIT); 

		if (nbytes > 0) 
		{
			printf("recv msg from msgque, bytes(%d)\n", nbytes); 

//			procIpcMsg(msg); 


//		if(buffer[0] == READ_SERVER_FLAG)
		{
			char buf[3] = {0xff, 0xff, 0xff}; 
			char cmdbuf[50] = {0, }; 		
			char dataString[20] = {0, }; 
			int len; 

			strncpy(dataString, msg.mtext, msg_size); 	
#if 1 
	printf("%s\n", dataString); 
	
	{
	/* title0 */ 
	int i = 0; 

	string_argc = 0; 

	string_argv[string_argc] = strtok(dataString, ",");

	while (string_argv[string_argc] != NULL)
	{
		printf("%s\n", string_argv[string_argc]); 
		string_argc++; 
		string_argv[string_argc] = strtok(NULL, ","); 
	} 

//	string_argv[string_argc] = 0; 

	for ( i = 0 ; i < string_argc ; i++)
		printf("%d - %s\n", i, string_argv[i]); 
	
	if ( strncmp(string_argv[0], "title", 5)== 0) 
	{
		printf("display title\n"); 
		//len = 18; 
		len = 6 + 7; // title0.txt= 
		len += strlen(dataString); 

		memset(cmdbuf, 0, 50); 	
		sprintf(cmdbuf, "title0.txt=\"%s\"", dataString);  

		write(fd, cmdbuf, len);   
		write(fd, buf, 3);  

//		buffer[0] = PRINT_CLIENT_FLAG; 
	}
	else if ( strncmp(string_argv[0], "htu21d", 6) == 0)
	{
       /* d0 */
                len = 9;
                len += strlen(string_argv[1]);

                memset(cmdbuf, 0, 50);
                sprintf(cmdbuf, "d0.txt=\"%s\"", string_argv[1]);

                write(fd, cmdbuf, len);
                write(fd, buf, 3);

        /* u0 */
                len = 9;
                len += strlen(string_argv[2]);

                memset(cmdbuf, 0, 50);
                sprintf(cmdbuf, "u0.txt=\"%s\"", string_argv[2]);

                write(fd, cmdbuf, len);
                write(fd, buf, 3);

       /* d0 */
                len = 9;
                len += strlen(string_argv[3]);

                memset(cmdbuf, 0, 50);
                sprintf(cmdbuf, "d1.txt=\"%s\"", string_argv[3]);

                write(fd, cmdbuf, len);
                write(fd, buf, 3);

        /* u0 */
                len = 10;
                len += strlen(string_argv[4]);

                memset(cmdbuf, 0, 50);
                sprintf(cmdbuf, "u1.txt=\"%s\"", string_argv[4]);

                write(fd, cmdbuf, len);
                write(fd, buf, 3);

                usleep(100*1000);
	}
	else if ( strncmp(string_argv[0], "mg811", 5) == 0)
	{
	 /* d2 */
                len = 9;
                len += strlen(string_argv[1]);

                memset(cmdbuf, 0, 50);
                sprintf(cmdbuf, "d2.txt=\"%s\"", string_argv[1]);

                write(fd, cmdbuf, len);
                write(fd, buf, 3);

        /* u2 */
                len = 9;
                len += strlen(string_argv[2]);

                memset(cmdbuf, 0, 50);
                sprintf(cmdbuf, "u2.txt=\"%s\"", string_argv[2]);

                write(fd, cmdbuf, len);
                write(fd, buf, 3);
	}
	/* id0 */ 
	else if ( strncmp(argv[1], "id0", 3) == 0 )
	{
		len = 10; 
		len += strlen(argv[2]);
  	
		memset(cmdbuf, 0, 50);
		sprintf(cmdbuf, "id0.txt=\"%s\"", argv[2]); 

		write(fd, cmdbuf, len); 
		write(fd, buf, 3); 
	}
	else if ( strncmp(argv[1], "d0", 2) == 0 )
	{
	/* d0 */ 
		len = 9; 
		len += strlen(argv[2]); 

		memset(cmdbuf, 0, 50); 
		sprintf(cmdbuf, "d0.txt=\"%s\"", argv[2]); 

		write(fd, cmdbuf, len); 
		write(fd, buf, 3); 
	
	/* u0 */ 
		len = 9; 
		len += strlen(argv[3]); 
		
		memset(cmdbuf, 0, 50); 
		sprintf(cmdbuf, "u0.txt=\"%s\"", argv[3]); 

		write(fd, cmdbuf, len); 
		write(fd, buf, 3); 
	}
	else if ( strncmp(argv[1], "d1", 2) == 0 ) 
	{
        	/* d1 */
                len = 9;
                len += strlen(argv[2]);

                memset(cmdbuf, 0, 50);
                sprintf(cmdbuf, "d1.txt=\"%s\"", argv[2]);

                write(fd, cmdbuf, len);
                write(fd, buf, 3);

        /* u1 */
                len = 9;
                len += strlen(argv[3]);

                memset(cmdbuf, 0, 50);
                sprintf(cmdbuf, "u1.txt=\"%s\"", argv[3]);

                write(fd, cmdbuf, len);
                write(fd, buf, 3);
	}
	else if ( strncmp(argv[1], "d2", 2) == 0)
	{

        /* d2 */
                len = 9;
                len += strlen(argv[2]);

                memset(cmdbuf, 0, 50);
                sprintf(cmdbuf, "d2.txt=\"%s\"", argv[2]);

                write(fd, cmdbuf, len);
                write(fd, buf, 3);

        /* u2 */
                len = 9;
                len += strlen(argv[3]);

                memset(cmdbuf, 0, 50);
                sprintf(cmdbuf, "u2.txt=\"%s\"", argv[3]);

                write(fd, cmdbuf, len);
                write(fd, buf, 3);
	}

	procIpcMsg(msg); 
	}
	else
{
		if(errno == ENOMSG) 
		{

		}
		else
		{
			printf("msgrcv() error\n"); 
			return; 	
		}
	}
	usleep(100*1000); 
}
}
#endif 
}
	close(fd); 	
	printf(" EXIT display\n"); 	
	return 0; 
}

#endif 


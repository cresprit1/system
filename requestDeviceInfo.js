var restAPI = require("./restAPI.js"); 
var dn = require("./moduleDN2.js"); 
var bjson = require("./blockInfo.json"); 
var fs = require('fs');

callback = function (res) {

    var chunks = [];
  
    res.on("data", function (chunk) {      
      chunks.push(chunk);
    });
  
    res.on("end", function () {
      
		var body = Buffer.concat(chunks);
		
		var json = JSON.parse(body); 
		
		fs.writeFile('./result.json', body, function(err) {
			if(err) {
				console.log("file write error"); 				
			}						
		}); 		
		 
console.log ("devApplication:"+json.devApplication); 
console.log ("devAppMgntID:"+json.devAppMgntID); 
 
		for ( var i = 0 ; i < bjson.blockList.length ; i++)
		{
			for ( var j = 0 ; j < json.swDriver.length ; j++)
			{
				console.log(bjson.blockList[i].blockID + ":" + json.swDriver[j].blockID); 	
				if ( bjson.blockList[i].blockID == json.swDriver[j].blockID )
				{
					console.log(json.swDriver[j].swDriverLink); 
					dn.InstallBlockSW(json.swDriver[j].swDriverLink, json.swDriver[j].blockID); 
					break; 
				}
			}
	   }

	   dn.InstallServicePackage(json.devApplication, json.installedDevApp); 
	   
    });
}

console.log("++++++++++++++++++++++++++++++"); 
console.log(" Request block & service Info "); 
console.log("++++++++++++++++++++++++++++++"); 

restAPI.post2o("/CRESPRIT_OntologyServer/deviceOntology/devConfiguration", "POST", JSON.stringify(bjson), callback);


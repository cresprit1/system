/* ---------------------------------------------------------
*/

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <math.h>

float fnc_mean(double sum, int num);
float fnc_var(float mean, int num,int data[]);
double fnc_std(float var);

main(int argc, char* argv[])
{
    // unsigned char i,j;

    char s[10];

	char buffer[256]={0,};
	
    int filesize = (int)atof(argv[2]);
    int* da = NULL;
    unsigned int count =0;
    int index;
    short data;
    unsigned char *dp;
    FILE *fp_input;
    FILE *fp_output;
    unsigned int count2=0, i;
    double sum=0;
    double mean = 0, var = 0, stdev = 0;     
    int max=0;
    int min=0;
    
    dp = (unsigned char *)&data;
    struct stat file_info;
    fp_input=fopen(argv[1],"r");

    if( 0 > stat(argv[1],&file_info))
    {
        printf("File Read Error\n");
    }
    
    da =(int*) malloc(sizeof(int) * file_info.st_size/2);

    for(index=0;index<file_info.st_size/2;index++)
      {
          *dp = fgetc(fp_input);
          *(dp+1) = fgetc(fp_input);
          //*(dp+2) = fgetc(fp_input);
          //*(dp+3) = fgetc(fp_input);
          // printf("%d\n",data);
          //da[count++]=data*(-1);
	printf("%d\n", data);
      }
    fclose(fp_input);

    return 0;
}


float fnc_mean(double sum, int num){
    return (float)sum/(float)num;
}

float fnc_var(float mean, int num, int data[]){
    float sumvar=0, var;
    int i;
    for(i=0;i<num;i=i+1){
        sumvar+=(data[i]-mean)*(data[i]-mean);
    }
    var = sumvar/(num);

    return var;
}

double fnc_std(float var){
    return sqrt(var);
}



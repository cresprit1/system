#ifndef _BLE2BASIC_H_
#define _BLE2BASIC_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <curses.h>
#include <unistd.h>
#include <math.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>	
#include <sys/types.h>
#include <termios.h>
#include <fcntl.h>

#include <stdio.h> /* printf, sprintf */
#include <stdlib.h> /* exit, atoi, malloc, free */
#include <unistd.h> /* read, write, close */
#include <string.h> /* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> /* struct hostent, gethostbyname */

#define EIR_NAME_SHORT              0x08  /* shortened local name */
#define EIR_NAME_COMPLETE           0x09  /* complete local name */

#define MACHINE_COUNT			2 		// BLE_node1, BLE_node2 

#define EIR_FLAGS                   0X01
#define EIR_NAME_SHORT              0x08
#define EIR_NAME_COMPLETE           0x09
#define EIR_MANUFACTURE_SPECIFIC    0xFF

typedef struct node 
{
  char macaddr[18]; 
	char topic[128]; 
	char clientId[20]; 
	char akey[128]; 
	time_t pre_t; 
	char pre_index; 
	char pre_type; 
	float rssi; 
}; 

typedef struct node bleNode_t; 

#define LEN_MACADDR 18
#define LEN_CLIENTID 28
#define LEN_BLEDATA 32
#define LEN_TIMEBUF 24 

typedef struct bledata 
{
  char index; 
  char type; 
  char format;                     // ble data format 
  char node_index; 
  char macaddr[LEN_MACADDR]; 
  char clientId[LEN_CLIENTID]; 
  char rdata[LEN_BLEDATA]; 
  char timeBuf[LEN_TIMEBUF]; 

  //float sdata[NUM_AXIS][NUM_PARAMS]; // [NUM_AXIS][NUM_PARAMS]; 
}; 

typedef struct bledata bledata_t; 

pthread_mutex_t receiveMutex;
pthread_mutex_t mqttMutex; 
pthread_mutex_t processMutex; 

pthread_t ble_pthread;
pthread_t queue_pthread; 
pthread_t mqtt_pthread; 

extern int CreateBLENode(char* fname); 
extern int ble_open(void); 
extern int ble_SetParam(int device); 
extern int ble_SetEvent(int device); 
extern int ble_Enablescan(int device); 
extern int ble_SetSocket(int device); 
extern void blethread(int device);
extern void ble_disable(int device); 

extern void *ble_thread(void *arg); 

extern bleNode_t *gBleNode; 
extern int gSensor_cnt; 

#endif 
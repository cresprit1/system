#ifndef __SMART_H__
#define __SMART_H__

#define  LED_IOCTL_MAGIC  '1'

#define LED_INIT  _IO(LED_IOCTL_MAGIC, 0)
#define START _IO(LED_IOCTL_MAGIC, 1)
#define STOP  _IO(LED_IOCTL_MAGIC, 2)
#define GPIOON _IO(LED_IOCTL_MAGIC, 3)
#define GPIOOFF _IO(LED_IOCTL_MAGIC, 4)
#define GETVALUE _IO(LED_IOCTL_MAGIC, 5)  
#define LED_ON_OFF  _IOW(LED_IOCTL_MAGIC, 7, int)

#define LED_IOCTL_MAX  8

#endif // __SMART_H__

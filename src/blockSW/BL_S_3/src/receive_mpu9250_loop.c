#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>

#ifdef DEFINE_MQTT 
#include "MQTTClient.h"
#include "cJSON.h"
#endif 

#include <time.h>
#include <string.h>

#define BAUDRATE B115200
#define MODEMDEVICE "/dev/ttyAMA3"
#define _POSIX_SOURCE 1         //POSIX compliant source

#define FALSE 0
#define TRUE 1

volatile int STOP=FALSE;

void signal_handler_IO (int status);    //definition of signal handler
int wait_flag=TRUE;                     //TRUE while no signal received
char devicename[80];
long Baud_Rate = 115200;         // default Baud Rate (110 through 38400)
long BAUD;                      // derived baud rate from command line
long DATABITS;
long STOPBITS;
long PARITYON;
long PARITY;
int Data_Bits = 8;              // Number of data bits
int Stop_Bits = 1;              // Number of stop bits
int Parity = 0;                 // Parity as follows:
// 00 = NONE, 01 = Odd, 02 = Even, 03 = Mark, 04 = Space

char buf[1024]; 

#ifdef DEFINE_MQTT 
//char buf[1024];
MQTTClient client;
MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;

char* address = "tcp://mqtt.alooh.io:1883";
//char* topic   = "topicA";
//char* topic = "59127957553ec9078f328e87";
char* topic = "5a0e40d0553ec9078f32904e";
char* clientId = "collector2";//when device added, must change the topic ID.
#define QOS         1
#define TIMEOUT     800L
#define STR_DATA	    "data"
#define STR_ID			"id"
#define MACHINE_COUNT			3

int connectMqttServer()
{
	int rc;

	MQTTClient_create(&client, address, clientId, MQTTCLIENT_PERSISTENCE_NONE, NULL);
	conn_opts.keepAliveInterval = 20;
	conn_opts.cleansession = 1;
	//conn_opts.username="9c2d5c314561404cbaed1265feda256b";//uzziel@cresprit.com
	//conn_opts.password="9c2d5c314561404cbaed1265feda256b";
	conn_opts.username="bcd12ab9d78a46d88dce354b8c66787a";//tough45@naver.com/aaaaaa
	conn_opts.password="bcd12ab9d78a46d88dce354b8c66787a";
	
	printf("MQTT Server address:%s\n", address);
	printf("topic:%s\n", topic);	
	if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
	{
		printf("Failed to connect, return code %d\n", rc);
		exit(-1);
	}

}

int publishDataOnMqtt(float rms, float gyro_y, float gyro_z, float temp )
{
	cJSON *jsonResult;
	cJSON *jsonArray;

	cJSON *sensor1;
	cJSON *sensor2;
	cJSON *sensor3;
	cJSON *sensor4;

	cJSON *data;
	cJSON *sensorArray;
	cJSON *dataPointArray1;
	cJSON *dataPointItem1;
	cJSON *dataPointArray2;
	cJSON *dataPointItem2;
	cJSON *dataPointArray3;
	cJSON *dataPointItem3;
	cJSON *dataPointArray4;
	cJSON *dataPointItem4;

 	char* pResult = NULL;
	struct timeval val;
	struct tm *ptm;
	float value = 0;
	
		
	int rc;
	int i=0;
	char timeBuf[128]={0,};
	char usec[7]={0,};
	char usec_use[4]={0,};
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;

	jsonResult =cJSON_CreateObject();
	jsonArray = cJSON_CreateArray();

	data = cJSON_CreateObject();
	sensorArray = cJSON_CreateArray();

	
	sensor1 = cJSON_CreateObject();
	sensor2 = cJSON_CreateObject();
	sensor3 = cJSON_CreateObject();
	sensor4 = cJSON_CreateObject();

	dataPointArray1 = cJSON_CreateArray();
	dataPointItem1 =cJSON_CreateObject();

	dataPointArray2 = cJSON_CreateArray();
	dataPointItem2 =cJSON_CreateObject();

	dataPointArray3 = cJSON_CreateArray();
	dataPointItem3 =cJSON_CreateObject();

	dataPointArray4 = cJSON_CreateArray();
	dataPointItem4 =cJSON_CreateObject();

	cJSON_AddStringToObject(sensor1, STR_ID, "Accelerometer");
	cJSON_AddNumberToObject(dataPointItem1, "v", rms);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem1, "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray1, dataPointItem1);
	cJSON_AddItemToObject(sensor1, "data_points", dataPointArray1);
	cJSON_AddItemToArray(sensorArray, sensor1);

	cJSON_AddStringToObject(sensor2, STR_ID, "Gyroscope_y");
	
	cJSON_AddNumberToObject(dataPointItem2, "v", gyro_y);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);

	cJSON_AddStringToObject(dataPointItem2, "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray2, dataPointItem2);
	cJSON_AddItemToObject(sensor2, "data_points", dataPointArray2);
	cJSON_AddItemToArray(sensorArray, sensor2);


	cJSON_AddStringToObject(sensor3, STR_ID, "Gyroscope_z");
	cJSON_AddNumberToObject(dataPointItem3, "v", gyro_z);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);

	cJSON_AddStringToObject(dataPointItem3, "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray3, dataPointItem3);
	cJSON_AddItemToObject(sensor3, "data_points", dataPointArray3);
	cJSON_AddItemToArray(sensorArray, sensor3);

	cJSON_AddStringToObject(sensor4, STR_ID, "temperature");
	cJSON_AddNumberToObject(dataPointItem4, "v", temp);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);

	cJSON_AddStringToObject(dataPointItem4, "t", timeBuf);
	cJSON_AddItemToArray(dataPointArray4, dataPointItem4);
	cJSON_AddItemToObject(sensor4, "data_points", dataPointArray4);
	cJSON_AddItemToArray(sensorArray, sensor4);


	cJSON_AddItemToObject(data, "sensors", sensorArray);

	cJSON_AddItemToObject(jsonResult, "data", data);	

	pResult = cJSON_Print(jsonResult);
	
//	printf("Result : %s\n", pResult);
	pubmsg.payload = pResult;
	pubmsg.payloadlen = strlen(pResult);
	pubmsg.qos = QOS;
	pubmsg.retained = 0;
	MQTTClient_publishMessage(client, topic, &pubmsg, &token);
/*
	printf("Waiting for up to %d seconds for publication of %s\n"
	  "on topic %s for client with ClientID: %s\n",
	    (int)(TIMEOUT/1000), PAYLOAD, TOPIC, CLIENTID);
*/
	rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
	printf("Message with delivery token %d delivered\n", token);

	//if(rc != MQTTCLIENT_SUCCESS)
	 //{
	 //	printf("Network is disabled\n");
	 //	break;
	 //}
	 /*
	 printf("a\n");
	 	cJSON_Delete(dataPointItem1);
			 printf("b\n");
	cJSON_Delete(dataPointArray1);
		 printf("c\n");
	cJSON_Delete(dataPointItem2);
	printf("d\n");

	cJSON_Delete(dataPointArray2);
	cJSON_Delete(dataPointItem3);

	cJSON_Delete(dataPointArray3);
	cJSON_Delete(dataPointItem4);

	cJSON_Delete(dataPointArray4);

	cJSON_Delete(sensor1);
	cJSON_Delete(sensor2);
	cJSON_Delete(sensor3);
	cJSON_Delete(sensor4);
	cJSON_Delete(sensorArray);	
		cJSON_Delete(data);
			cJSON_Delete(jsonArray);
			*/
	cJSON_Delete(jsonResult);

	return rc;
}

int disConnMqttServer()
{
	MQTTClient_disconnect(client, 10000);
	MQTTClient_destroy(&client);

}
#endif 

int main(int argc, char *argv[])
{
	int fd; 
	int i; 
	float rms = 0;
	float gyro_y, gyro_z, temp;
	
	fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY | O_NDELAY); 

	struct termios toptions;

	tcgetattr(fd, &toptions); 

	cfsetispeed(&toptions, B115200); 
	cfsetospeed(&toptions, B115200); 

	toptions.c_cflag &= ~PARENB;
	toptions.c_cflag &= ~CSTOPB;
	toptions.c_cflag &= ~CSIZE;
	toptions.c_cflag |= CS8;
	
	toptions.c_cflag &= ~CRTSCTS;
	
	toptions.c_cflag |= CREAD | CLOCAL;
	
	toptions.c_iflag &= ~(IXON | IXOFF | IXANY); 
	
	toptions.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG); 
	toptions.c_oflag &= ~OPOST;
	
	toptions.c_cc[VMIN]=12;
	toptions.c_cc[VTIME]=0;
	
	tcsetattr(fd, TCSANOW, &toptions); 

#ifdef DEFINE_MQTT 

	connectMqttServer();
#endif 

	//for ( i = 0 ; i < 1024 ; i++) 
	while(1)
	{	
		usleep(1000);

		int n = read(fd, buf, 1024); 
		char *p = NULL;
		
//		printf("%i bytes got read...\n", n); 

		p = strtok(buf, ",");
		if(p == NULL)
			continue;
		rms = atof(p);

		p = strtok(NULL, ",");
		if(p == NULL)
			continue;
		gyro_y = atof(p);

		p = strtok(NULL, ",");
		if(p == NULL)
			continue;
		gyro_z = atof(p);

		p = strtok(NULL, ",");
		if(p == NULL)
			continue;		
		temp = atof(p);
	
		printf("%f    %f     %f\n", rms, gyro_y, gyro_z); 	
	//	printf("%f    %f     %f    %f\n", floorf(rms*100)/100, floorf(gyro_y*100)/100, floorf(gyro_z*100)/100, floorf(temp*100)/100); 
		
#ifdef DEFINE_MQTT 
		publishDataOnMqtt( floorf(rms*100)/100, floorf(gyro_y*100)/100, floorf(gyro_z*100)/100, floorf(temp*100)/100);
#endif 		
		tcflush(fd, TCIFLUSH); 
	}

#ifdef DEFINE_MQTT	
	disConnMqttServer();
#endif 	
	close(fd); 	
	return 0; 
}




#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>

#include "digital.h"

void init(void)
{
	int i; 

	/* LED Port */ 
	for ( i = LED203_RP00 ; i <= LED208_BP11; i++)
		digitalPinMode(i, OUTPUT); 

	for ( i = SW200_BACK ; i < SW207_6 ; i++)
		digitalPinMode(i, INPUT); 
}

void main(void)

{
  uint8_t err_code; 
  uint8_t data; 
  int num; 

//  digitalPinMode(17, 0); 

/* reset*/ 
//  digitalWrite(17, 1); 
//  sleep(1); 
//  digitalWrite(17, 0); 
//  sleep(1); 
//  digitalWrite(17, 1); 
//  sleep(1); 

  TCA6424AInitDefault((TCA6424ARegs*)&TCA6424A_Reg_map);

  setupI2C(SLAVEADDR); 

  TCA6424AReadInputReg((TCA6424ARegs*)&TCA6424A_Reg_map);
//  TCA6424AInitI2CReg((TCA6424ARegs*)&TCA6424A_Reg_map); 
  sleep(1); 

//  TCA6424AReadInputReg((TCA6424ARegs*)&TCA6424A_Reg_map);
  sleep(1); 

  printf("%x\n", TCA6424A_Reg_map.Input.Port.P0.all);
  printf("%x\n", TCA6424A_Reg_map.Input.Port.P1.all);
  printf("%x]n", TCA6424A_Reg_map.Input.Port.P2.all);
 
  finishI2C(); 
}


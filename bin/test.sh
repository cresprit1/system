#!/bin/sh

#$1 file down url
#$2 fileName
#$3 file location type /system, /service
ARCH_FILE_PATH=$HOME/project/OSLO/system/archFiles
BLOCK_PATH=$HOME/project/OSLO/system/blockSW
SERVICE_PATH=$HOME/project/OSLO/system/service

echo $ARCH_FILE_PATH/$2

if [ ! -f $ARCH_FILE_PATH/$2 ]; then
echo "Downloading the driver file is started~"

wget $1 -P $ARCH_FILE_PATH

if [ $3 = "service" ]; then
tar xvf $ARCH_FILE_PATH/$2 -C $SERVICE_PATH
else
tar xvf $ARCH_FILE_PATH/$2 -C $BLOCK_PATH
fi
else
echo "file exist"
fi



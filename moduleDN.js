var cmd=require('node-cmd');
var restAPI = require("./restAPI2o.js"); 
var url = require('url'); 

var InstallServicePackage = function(urlstring, installedDevApp) {
//	console.log(urlstring); 
//	console.log(installedDevApp); 
	
	console.log("=========== INSTALL " + installedDevApp + " ==========="); 

	var curURL = url.parse(urlstring); 

	console.log(curURL.path); 

	cmd.get(
	`
	cd service
	wget ${urlstring}
	tar xvf .${curURL.path}
	mv .${curURL.path} ../package
	cd ${installedDevApp}
	npm install
	`,
		function(err, data, stderr) {
			if (!err) {
				console.log('INSTALLed Service Package :\n\n',data); 
				
//				serviceID = installedDevApp; 

				var user = {
					"devID" : 'DKOCD0300001', 
					"installedDevApp" : installedDevApp, 
				}

				restAPI.post2o("/deviceOntology/regiDeviceInfo", "POST", JSON.stringify(user), callback2); 				
			}
			else {
				console.log('error', err)
			}
		}
	);
}

var InstallBlockSW = function(urlstring, installedDevApp, flag) {
	console.log(urlstring); 
	console.log(installedDevApp); 

	console.log("=========== INSTALL " + installedDevApp + " ==========="); 
	
	var curURL = url.parse(urlstring); 

	console.log(curURL.path); 

	cmd.get(
	`
	cd blockSW
	wget ${urlstring}
	tar xvf .${curURL.path}
	mv .${curURL.path} ../package
	cd ${installedDevApp}
	npm install
	`,
		function(err, data, stderr) {
			if (!err) {
				console.log('installed Block package :\n\n',data); 
				
				
			}
			else {
				console.log('error', err)
			}
			
			return 1; // flag = 1; 
		}
	);
}

module.exports.InstallServicePackage = InstallServicePackage; 
module.exports.InstallBlockSW = InstallBlockSW; 

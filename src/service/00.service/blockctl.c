#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include "cJSON.h"
#include "digital.h" 
#include "blockctl.h" 

int slotPowerIO[MAX_SLOT] = { BOARD_SD1, BOARD_SD2, BOARD_SD3, BOARD_SD4, BOARD_SD5, BOARD_SD6}; 

int blockIO[MAX_BIDIO] = { BOARD_ID1, BOARD_ID2, BOARD_ID3, BOARD_ID4, BOARD_ID5, BOARD_ID6}; 

int modulePowerIO[MAX_SLOT] = { BOARD_MD1, BOARD_MD2, BOARD_MD3, BOARD_MD4, BOARD_MD5, BOARD_MD6}; 

int setupIO(void)
{
	/* Block ID */ 
//	digitalPinMode(BOARD_ID1, 0);
//	digitalPinMode(BOARD_ID2, 0); 
//	digitalPinMode(BOARD_ID3, 0); 
//	digitalPinMode(BOARD_ID4, 0); 
//	digitalPinMode(BOARD_ID5, 0); 
//	digitalPinMode(BOARD_ID6, 0); 

	/* Slot Power Ctl */ 
	digitalPinMode(BOARD_SD1, 1); 
	digitalPinMode(BOARD_SD2, 1);
	digitalPinMode(BOARD_SD3, 1);
	digitalPinMode(BOARD_SD4, 1);
	digitalPinMode(BOARD_SD5, 1);
	digitalPinMode(BOARD_SD6, 1);

	//powerOn(BOARD_SD1); 
	//powerOn(BOARD_SD2); 
	//powerOn(BOARD_SD3); 
	//powerOn(BOARD_SD4); 
	//powerOn(BOARD_SD5); 
	//powerOn(BOARD_SD6); 

	/* 5V */ 
        digitalPinMode(BOARD_MD1, 1);
        digitalPinMode(BOARD_MD2, 1);
        digitalPinMode(BOARD_MD3, 1);
        digitalPinMode(BOARD_MD4, 1);
        digitalPinMode(BOARD_MD5, 1);
        digitalPinMode(BOARD_MD6, 1);

#if 0 
	/* off only 5v */ 
	powerOn(BOARD_MD1); 
	powerOn(BOARD_MD2); 
	powerOn(BOARD_MD3); 
	powerOn(BOARD_MD4); 
	powerOn(BOARD_MD5); 
	powerOn(BOARD_MD6); 
#else
	/* on only 5v */ 
        powerOff(BOARD_MD1);
        powerOff(BOARD_MD2);
        powerOff(BOARD_MD3);
        powerOff(BOARD_MD4);
        powerOff(BOARD_MD5);
        powerOff(BOARD_MD6);
#endif 
}

void powerOn(int id)
{
	digitalWrite(id, 1); 
	usleep(10000); 
}

void powerOff(int id)
{
	digitalWrite(id, 0); 
}

void powerOnModule(int slotid)
{
	powerOff(modulePowerIO[slotid]); 
	usleep(10000); 
}

void powerOffModule(int slotid)
{
	powerOn(modulePowerIO[slotid]); 
	usleep(10000); 
}

void powerOnBlock(int slotid)
{
      	powerOn(slotPowerIO[slotid]);
	usleep(10000); 

//	printf("slotIO : %d\n", slotPowerIO[slotid]); 

//	sleep(2); 
}

void powerOffBlock(int slotid)
{
	usleep(10000); 	
	powerOff(slotPowerIO[slotid]); 
}

void powerOffBlockAll(void)
{
	int i; 

	for ( i = 0 ; i < MAX_SLOT ; i++)
	{
		powerOffBlock(i);	// 3.3v  
		powerOffModule(i); // 5V 
	}
}


#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include "cJSON.h"
#include "digital.h" 
#include "blockctl.h"
#include "HttpPost.h"
#include "ProcessDevCfgRes.h"
#include "Serial.h"


#define _DEBUG 

#if 0 
#define MAX_SLOT 6
#define MAX_BIDIO 6 
#define MAX_RECORD 10 

#define BOARD_IO 	6 

#define BOARD_ID1 	7
#define BOARD_ID2	8
#define BOARD_ID3	9
#define BOARD_ID4	10
#define BOARD_ID5	11
#define BOARD_ID6	12
#define BOARD_ID7 	13

/* 3.3v */ 
#define BOARD_SD1	(32*3+28) // D28 or E2 
#define BOARD_SD2	(32*3+29) // D29 or E3 
#define BOARD_SD3	(32*3+30) // D30 or E4 
#define BOARD_SD4	(32*3+31) // D31 or C14 
#define BOARD_SD5	(32*4+0) // E0  or C23 
#define BOARD_SD6	(32*4+1) // E1 or B26 

/* 5.0v (module power) */ 
#define BOARD_MD1	(32*4+2)  // E2
#define BOARD_MD2	(32*4+3)  // E3 
#define BOARD_MD3	(32*4+4)  // E4
#define BOARD_MD4	(32*2+14) // C14
#define BOARD_MD5	(32*2+23) // C23
#define BOARD_MD6	(32*1+26) // B26


int slotPowerIO[MAX_SLOT] = { BOARD_SD1, BOARD_SD2, BOARD_SD3, BOARD_SD4, BOARD_SD5, BOARD_SD6}; 

int blockIO[MAX_BIDIO] = { BOARD_ID1, BOARD_ID2, BOARD_ID3, BOARD_ID4, BOARD_ID5, BOARD_ID6}; 
#endif 
//static char OntologyServerIP[] = "218.147.182.24:8080/OntServer/devConfiguration";

static char OntologyServerPort[] = "8080";
static char OntologyServerIP[] = "125.132.182.152:8080/OntServer/devConfiguration";

//static char ServiceServerIP[] = "218.147.182.24:8003/api/v1/devices/%s/info";
static char ServiceServerIP[] = "125.132.182.152:8003/api/v1/devices/%s/info";
static char ServiceServerPort[] = "8003";
//char deviceSerial[128] = {0, }; 

DevCfgRes* _devCfgRes = NULL;

FILE *popen(const char *command, const char *mode);
int pclose(FILE *stream);

void getInterface(char* in, char* out)
{
    FILE *cmd;
    char result[1024];
    char *ptr;

//    cmd = popen("lshw -C network -businfo | grep 'wlan' | grep '1.3'", "r");

    cmd = popen(in, "r");

    if (cmd == NULL) {
        perror("popen");
        exit(EXIT_FAILURE);
    }

    while (fgets(result, sizeof(result), cmd)) {
        printf("%s", result);
    }

    ptr = strstr(result, "wlan");
    if( ptr ) 
    {
//    	ptr[5] = 0;
    	printf("%s\n", ptr);
        memcpy(out, ptr, 5); 
    }

    pclose(cmd);
    return;
}


static const char *filename = "../blockInfo.json";

struct record_blockInfo {

	int index; 
	int ch; 

        char sensorType[20];
        char blockID[10];
	char sType[100]; 
	int exec; 
	int power; 
};

//int inputslot[MAX_SLOT]={0, 255, 2, 1, 255, 255};

int detectBlock[MAX_SLOT] = {-1, }; 

struct record_blockInfo btable[MAX_RECORD]; 

int btable_num = -1; 

void doit(char *text)
{
	int i, j = 0, index; 

	char *out; 
	cJSON *json, *subjson, *item, *value; 

	json = cJSON_Parse(text); 
        subjson = cJSON_GetObjectItem(json, "blockList"); 

	btable_num = cJSON_GetArraySize(subjson); 

	for ( j = 0 ; j < btable_num ; j++) 
	{
		item = cJSON_GetArrayItem(subjson, j); 
		out = cJSON_Print(item); 
#ifdef _DEBUG 
		printf("%s\n", out); 
#endif 
		free(out); 

		value = cJSON_GetObjectItem(item, "index"); 	
		out = cJSON_Print(value); 
		btable[j].index = atoi(out); 
#ifdef _DEBUG
		printf("%s\n", out); 
#endif 
		free(out); 	

		value = cJSON_GetObjectItem(item, "sensorType");  
		out = cJSON_Print(value); 
		memset(btable[j].sensorType, 0, 20); 
		memcpy(btable[j].sensorType, &out[1], strlen(out)-2); 
#ifdef _DEBUG
		printf("%s\n", out); 
#endif 
		free(out); 

		value = cJSON_GetObjectItem(item, "blockID"); 
		out = cJSON_Print(value);
		memset(btable[j].blockID, 0, 10); 
		memcpy(btable[j].blockID, &out[1], strlen(out)-2);  
#ifdef _DEBUG
		printf("%s\n", out); 
#endif 
		free(out); 

		memset(btable[j].sType, 0, 100); 
		value = cJSON_GetObjectItem(item, "type"); 

		if ( value != NULL )
		{
			out = cJSON_Print(value); 
			memcpy(btable[j].sType, &out[i], strlen(out)); 
		}

		btable[j].exec = atoi(cJSON_Print(cJSON_GetObjectItem(item, "exec"))); 
		btable[j].power = atoi(cJSON_Print(cJSON_GetObjectItem(item,"power"))); 
		printf("power : %d\n", btable[j].power); 
	}

#ifdef _DEBUG
	for ( i = 0 ; i < 6 ; i++) 
	{
		if ( btable[i].sType != NULL ) 
			printf("%d, %s, %s, %s\n", btable[i].index, btable[i].sensorType, btable[i].blockID, btable[i].sType); 
	}
#endif 
	
	cJSON_Delete(json); 
}

void doreadfile(char *filename)
{
	FILE *f=fopen(filename,"rb"); 

	fseek(f, 0, SEEK_END); 

	long len=ftell(f); 
	fseek(f, 0, SEEK_SET); 

	char *data = malloc(len+1); 
	fread(data, 1, len, f); 
	fclose(f); 

 	doit(data); 

	free(data); 
}


extern int readBlockID(int slotnum); 

void checkBlockID(void)
{
        int port;
	int i=0; 

	
        for ( i = 0 ; i < MAX_SLOT ; i++ )
        {
		detectBlock[i] = readBlockID(i); 
#ifdef _DEBUG
		printf("detectBlock = %d\n", detectBlock[i]); 
#endif 
        }
}
void GetStdoutFromCommand(char* cmd, char* outstr) {

	FILE *stream; 
//	char buffer[256] = {0, }; 

	stream = popen(cmd, "r"); 

	while(fgets(outstr, sizeof(outstr)-1, stream) != NULL) {
		printf("%s\n", outstr); 
	}	
	pclose(stream); 
}

void main(int argc, char** argv)
{
	int i, j; 
	int slot[MAX_SLOT]; 
	int channel = -1;
	int sensorCount = 0;
	FILE * fPblockInfo; 
	FILE *fServiceInfo;

	char pInterface[128] = {0, }; 
	
	char buf[256] = {0, }; 
	char tempBuf[10] = {0,};
	char* res = NULL;
	char serviceServerAddress[512];
	char* pResult = NULL; 
	
	cJSON *jsonResult; 
	cJSON *jsonBlockListArray; 
	cJSON *block; 
	char* deviceSerial = NULL;

	deviceSerial = malloc(128);
	getSerial(deviceSerial);

	printf("DEVICE SERIAL  %s\n", deviceSerial); 

	fPblockInfo = fopen("../blockInfo.json", "w"); 
	fServiceInfo = fopen("../serviceInfo.json", "w"); 	
        setupIO(); 	
#if 1 

	checkBlockID(); 

	for ( i = 0 ; i < MAX_SLOT ; i++ )
	{
#ifdef _DEBUG		
		printf("%d:%d\n", i, detectBlock[i]); 
#endif 
		for ( j = 0 ; j < btable_num ; j++ )
		{
			slot[i] = -1; 
#ifdef _DEBUG
			printf("%d:%d\n", btable[j].index, detectBlock[i]); 
#endif 	
			if ( detectBlock[i] == btable[j].index )
			{ 		
				slot[i] = j; 
				break; 	
			}
		}
	}
	
	jsonResult = cJSON_CreateObject(); 
	jsonBlockListArray = cJSON_CreateArray(); 
	
	cJSON_AddStringToObject(jsonResult, "osType", "ubuntu_3.4.39"); 


	for ( i = 0 ; i < MAX_SLOT ; i++ )
	{
		if ( slot[i] != -1 && detectBlock[i] != 0) 
		{
			memset(tempBuf, 0, sizeof(buf));
			printf("%d\n", slot[i]); 
			sprintf(tempBuf, "%d", detectBlock[i]);

			block = cJSON_CreateString(tempBuf); 
			cJSON_AddItemToArray(jsonBlockListArray, block);			
		}
	} 

	cJSON_AddItemToObject(jsonResult, "blockList", jsonBlockListArray); 
	pResult = cJSON_Print(jsonResult); 
	cJSON_Delete(jsonResult); 

	printf("Result : %s\n", pResult); 
	res = malloc(51200);
	HttpPost(OntologyServerIP, OntologyServerPort, pResult,res);
	//fprintf(fPblockInfo, "%s\n", res); 
	
	_devCfgRes = malloc(sizeof(DevCfgRes));

	doProcessDevCfg(res, _devCfgRes);



#ifdef _DEBUG 

	for ( i = 0 ; i < MAX_SLOT ; i++) 
		printf("slot[%d] = %d\n", i, slot[i]); 
#endif 	 

	/* send to Service Server */ 
	{
		jsonResult = cJSON_CreateObject(); 
		jsonBlockListArray = cJSON_CreateArray(); 
		for ( i = 0 ; i < _devCfgRes->blockCount  ; i++ )
			if(_devCfgRes->blockInfo[i].mode == 1)
				sensorCount++;


		cJSON_AddNumberToObject(jsonResult, "ch", sensorCount ); 
		cJSON_AddStringToObject(jsonResult, "firmware", "v1.0.0"); 
	//	cJSON_AddStringToObject(jsonResult, "serviceName", _devCfgRes->serviceName); 
		for ( i = 0 ; i < _devCfgRes->blockCount  ; i++ )
		{
#ifdef _DEBUG
			printf("%d\n", slot[i]); 
			printf("index : %d\n", _devCfgRes->blockInfo[i].index); 
#endif 
			if(_devCfgRes->blockInfo[i].mode == 1)
			{
				block = cJSON_CreateObject(); 
				
				for(j = 0;j<MAX_SLOT;j++)
				{
					if(detectBlock[j] == _devCfgRes->blockInfo[i].index)
					{
						channel = j+1;
						break;
					}
				}
				
				cJSON_AddNumberToObject(block, "ch", channel); 
				cJSON_AddStringToObject(block, "b_type", _devCfgRes->blockInfo[i].blockID);
				cJSON_AddStringToObject(block, "s_type", _devCfgRes->blockInfo[i].type); 

				cJSON_AddItemToArray(jsonBlockListArray, block);	
			}

		} 

 		cJSON_AddItemToObject(jsonResult, "blocks", jsonBlockListArray); 
		pResult = cJSON_Print(jsonResult); 
		fprintf(fServiceInfo, "%s\n", pResult); 

		printf("Result : %s\n", pResult); 
		//fprintf(fPblockInfo, "%s\n", pResult); 
		memset(res, 0, sizeof(res));
		memset(serviceServerAddress, 0, sizeof(serviceServerAddress));
		
		sprintf(serviceServerAddress, ServiceServerIP, deviceSerial);
		HttpPost(serviceServerAddress, ServiceServerPort, pResult,res);

		cJSON_Delete(jsonResult); 
	}
	
	/* for Service (Device) */ 
	{

		jsonResult = cJSON_CreateObject(); 
		jsonBlockListArray = cJSON_CreateArray(); 

		cJSON_AddStringToObject(jsonResult, "serial", deviceSerial); 
		cJSON_AddStringToObject(jsonResult, "serviceName", _devCfgRes->serviceName); 
		cJSON_AddNumberToObject(jsonResult, "ch", _devCfgRes->blockCount ); 
		cJSON_AddStringToObject(jsonResult, "firmware", "v1.0.0"); 

		for ( i = 0 ; i < _devCfgRes->blockCount  ; i++ )
		{
#ifdef _DEBUG
			printf("%d\n", slot[i]); 
			printf("index : %d\n", _devCfgRes->blockInfo[i].index); 
#endif 
			//if(_devCfgRes->blockInfo[i].mode == 1)
			{
				block = cJSON_CreateObject(); 
				
				for(j = 0;j<MAX_SLOT;j++)
				{
					if(detectBlock[j] == _devCfgRes->blockInfo[i].index)
					{
						channel = j+1;
						break;
					}
				}
				
				cJSON_AddNumberToObject(block, "ch", channel); 
				cJSON_AddStringToObject(block, "b_type", _devCfgRes->blockInfo[i].blockID);
				cJSON_AddStringToObject(block, "s_type", _devCfgRes->blockInfo[i].type); 
				cJSON_AddNumberToObject(block, "mode", _devCfgRes->blockInfo[i].mode); 
				cJSON_AddNumberToObject(block, "power", _devCfgRes->blockInfo[i].power); 

				cJSON_AddItemToArray(jsonBlockListArray, block);	
			}

		} 

 		cJSON_AddItemToObject(jsonResult, "blocks", jsonBlockListArray); 
		pResult = cJSON_Print(jsonResult); 
		fprintf(fPblockInfo, "%s\n", pResult); 
		printf("Result : %s\n", pResult); 
		
		cJSON_Delete(jsonResult); 
	}
	
	

#endif 
	free(res);
	fclose(fPblockInfo); 
	powerOffBlockAll(); 
}




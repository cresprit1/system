#include <stdio.h>
#include <stdlib.h>

FILE *popen(const char *command, const char *mode);
int pclose(FILE *stream);

int main(void)
{
    FILE *cmd;
    char result[1024];
    char *ptr; 

    cmd = popen("lshw -C network -businfo | grep 'wlan' | grep '1.3'", "r"); 
//    cmd = popen("grep bar /usr/share/dict/words", "r");
    if (cmd == NULL) {
        perror("popen");
        exit(EXIT_FAILURE);
    }
    while (fgets(result, sizeof(result), cmd)) {
        printf("%s", result);
    }

    ptr = strstr(result, "wlan"); 
    ptr[5] = 0; 
    printf("%s\n", ptr); 
    
    pclose(cmd);
    return 0;
}

#include <stdio.h> 
#include <stdbool.h>

#define HIGH 1
#define LOW 0
#define INPUT 1
#define OUTPUT 0

bool digitalPinMode(int pin, int dir); 
int digitalRead(int pin); 
bool digitalWrite(int pin, int val); 


#include <stdio.h>
#include <math.h> 

#include "../../lib/userlib.h"

#include "analog.h"
#include "digital.h"
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#define SHARED_MEMORY_KEY 1005
#define MEMORY_SIZE 200

#define READ_CLIENT_FLAG 0
#define READ_SERVER_FLAG 1
#define PRINT_CLIENT_FLAG 2

/***********************Software Related Macros************************************/
#define         READ_SAMPLE_INTERVAL         (50)    //define how many samples you are going to take in normal operation
#define         READ_SAMPLE_TIMES            (5)     //define the time interval(in milisecond) between each samples in
                                                     //normal operation
// resistance at 25 degrees C
#define THERMISTORNOMINAL 10000      
// temp. for nominal resistance (almost always 25 C)
#define TEMPERATURENOMINAL 25   
// The beta coefficient of the thermistor (usually 3000-4000)
#define BCOEFFICIENT 3984 // 3950    // 3740 or 4570 
// the value of the 'other' resistor
#define SERIESRESISTOR 220

float sdata[5]; 
char deviceSerial[128] = {0, }; 
int ch = 0; 

#include <time.h>
 
void delay(int milli_seconds) // number_of_seconds)
{
    // Converting time into milli_seconds
   // int milli_seconds = 1000 * number_of_seconds;
 
    // Stroing start time
    clock_t start_time = clock(); 
 
    // looping till required time is not acheived
    while (clock() < start_time + milli_seconds)
        ;
}
 
void setup()
{

}

/*****************************  ADCRead *********************************************
************************************************************************************/ 
float a0, a1; 

void ADCRead(void)
{
    int i;
    int value0, value1; 

    for (i=0;i<READ_SAMPLE_TIMES;i++) {

	value0 = analogRawRead(4); 
	value1 = analogRawRead(5); 

        a0 += value0; 
	a1 += value1; 
//	printf("%d , %d\n", value0, value1); 
        delay(READ_SAMPLE_INTERVAL);
    }

    a0 = ((a0/READ_SAMPLE_TIMES)*(3.3/5.0));
    a1 = ((a1/READ_SAMPLE_TIMES)*(3.3/5.0)); 

//    printf("%.3f, %.3f\n", a0, a1); 
}; 

float convert2temp(float a)
{
    float registance; 
    float temperature, temp;
    float steinhart;  

#if 1 
	printf("%f\n", a); 
	temp = (4096.0f / a) - 1.0f; 
 	temp = (float)SERIESRESISTOR * temp;                    
 //            printf("%f\n", temp); 
            steinhart = temp / (float)THERMISTORNOMINAL;     // (R/Ro)
 //	printf("%f\n", steinhart); 

            steinhart = log10f(steinhart);                  // ln(R/Ro)
 // printf("%f\n", steinhart);
            steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
//  printf("%f\n", steinhart);
            steinhart += 1.0 / ((float)TEMPERATURENOMINAL + 273.15); // + (1/To)
//  printf("%f\n", steinhart);

            steinhart = 1.0 / steinhart;                 // Invert
 // printf("%f\n", steinhart);

            steinhart -= 273.15;                         // convert to C
  //printf("%f\n", steinhart);

            temperature = steinhart; 
#endif 
    return temperature;  
}

#if 1 
void main(int argc, char** argv)
{
    float volts;
    int i; 

    memcpy(deviceSerial, "thermistor", strlen("thermistor"));
    printf("deviceSerial : %s\n", deviceSerial); 

    while(1) 
    {
  	ADCRead();  
	printf("adc0 - %.3f, adc1 - %.3f\n", convert2temp(a0), convert2temp(a1));
	a0 = 0.0; 
	a1 = 0.0; 
	sleep(9); 
    }// while(1)
}
#else
void main(int argc, char** argv)
{
	float volts; 
	int i; 

	memcpy(deviceSerial, "thermistor", strlen("thermistor")); 
	printf("deviceSerial : %s\n", deviceSerial); 

	while(1)
	{
		ADCRead(); 
	        printf("adc0 - %.3f, adc1 - %.3f\n", convert2temp(a0), convert2temp(a1));
       		sdata[0] = convert2temp(a0); 
		sdata[1] = convert2temp(a1); 	

		SendResultMessage(1, deviceSerial, sdata); 
        	sleep(9);

		a0 = 0.0; 
		s1 = 0.0; 
	}// while(1)
}
#endif 

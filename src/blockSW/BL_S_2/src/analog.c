#include <stdio.h>

int analogRawRead(int aPinNumber)
{
	FILE *fd; 
	char fName[64]; 
	char val[8]; 

	sprintf(fName, "/sys/bus/iio/devices/iio:device0/in_voltage%d_raw", 
		aPinNumber); 

	if((fd = fopen(fName, "r")) == NULL)
	{
		printf("Error: cannot open analog voltage value\n"); 
		return 0; 
	}

	fgets(val, 8, fd); 
	fclose(fd); 

	return atoi(val); 
}
/* 
void main(void)
{
	// v = in_voltageX_raw * 0.439453125 mV 
	int value; 

	value = analogRawRead(0); 
	printf("%d : %f\n", value, value * 0.439453125);  
}
*/ 

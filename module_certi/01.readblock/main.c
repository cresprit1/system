#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include "cJSON.h"
#include "digital.h" 
#include "blockctl.h"

#define _DEBUG 

#if 0 
#define MAX_SLOT 6
#define MAX_BIDIO 6 
#define MAX_RECORD 10 

#define BOARD_IO 	6 

#define BOARD_ID1 	7
#define BOARD_ID2	8
#define BOARD_ID3	9
#define BOARD_ID4	10
#define BOARD_ID5	11
#define BOARD_ID6	12
#define BOARD_ID7 	13

/* 3.3v */ 
#define BOARD_SD1	(32*3+28) // D28 or E2 
#define BOARD_SD2	(32*3+29) // D29 or E3 
#define BOARD_SD3	(32*3+30) // D30 or E4 
#define BOARD_SD4	(32*3+31) // D31 or C14 
#define BOARD_SD5	(32*4+0) // E0  or C23 
#define BOARD_SD6	(32*4+1) // E1 or B26 

/* 5.0v (module power) */ 
#define BOARD_MD1	(32*4+2)  // E2
#define BOARD_MD2	(32*4+3)  // E3 
#define BOARD_MD3	(32*4+4)  // E4
#define BOARD_MD4	(32*2+14) // C14
#define BOARD_MD5	(32*2+23) // C23
#define BOARD_MD6	(32*1+26) // B26


int slotPowerIO[MAX_SLOT] = { BOARD_SD1, BOARD_SD2, BOARD_SD3, BOARD_SD4, BOARD_SD5, BOARD_SD6}; 

int blockIO[MAX_BIDIO] = { BOARD_ID1, BOARD_ID2, BOARD_ID3, BOARD_ID4, BOARD_ID5, BOARD_ID6}; 
#endif 

static const char *filename = "./blockInfo.json";

struct record_blockInfo {

	int level; 

        char sensorType[20];
        char blockID[10];
	char sType[100]; 
	int power; 
};

//int inputslot[MAX_SLOT]={0, 255, 2, 1, 255, 255};

int detectBlock[MAX_SLOT] = {-1, }; 

struct record_blockInfo btable[MAX_RECORD]; 

int btable_num = -1; 

void doit(char *text)
{
	int i, j = 0, index; 

	char *out; 
	cJSON *json, *subjson, *item, *value; 

	json = cJSON_Parse(text); 
        subjson = cJSON_GetObjectItem(json, "blockList"); 

	btable_num = cJSON_GetArraySize(subjson); 

	for ( j = 0 ; j < btable_num ; j++) 
	{
		item = cJSON_GetArrayItem(subjson, j); 
		out = cJSON_Print(item); 
#ifdef _DEBUG 
		printf("%s\n", out); 
#endif 
		free(out); 

		value = cJSON_GetObjectItem(item, "index"); 	
		out = cJSON_Print(value); 
		btable[j].level = atoi(out); 
#ifdef _DEBUG
		printf("%s\n", out); 
#endif 
		free(out); 	

		value = cJSON_GetObjectItem(item, "sensorType");  
		out = cJSON_Print(value); 
		memset(btable[j].sensorType, 0, 20); 
		memcpy(btable[j].sensorType, &out[1], strlen(out)-2); 
#ifdef _DEBUG
		printf("%s\n", out); 
#endif 
		free(out); 

		value = cJSON_GetObjectItem(item, "blockID"); 
		out = cJSON_Print(value);
		memset(btable[j].blockID, 0, 10); 
		memcpy(btable[j].blockID, &out[1], strlen(out)-2);  
#ifdef _DEBUG
		printf("%s\n", out); 
#endif 
		free(out); 

		memset(btable[j].sType, 0, 100); 
		value = cJSON_GetObjectItem(item, "type"); 

		if ( value != NULL )
		{
			out = cJSON_Print(value); 
			memcpy(btable[j].sType, &out[i], strlen(out)); 
		}

		btable[j].power = atoi(cJSON_Print(cJSON_GetObjectItem(item,"power"))); 
		printf("power : %d\n", btable[j].power); 
	}

#ifdef _DEBUG
	for ( i = 0 ; i < 6 ; i++) 
	{
		if ( btable[i].sType != NULL ) 
			printf("%d, %s, %s, %s\n", btable[i].level, btable[i].sensorType, btable[i].blockID, btable[i].sType); 
	}
#endif 
	
	cJSON_Delete(json); 
}

void doreadfile(char *filename)
{
	FILE *f=fopen(filename,"rb"); 

	fseek(f, 0, SEEK_END); 

	long len=ftell(f); 
	fseek(f, 0, SEEK_SET); 

	char *data = malloc(len+1); 
	fread(data, 1, len, f); 
	fclose(f); 

 	doit(data); 

	free(data); 
}


extern int readBlockID(int slotnum); 

void checkBlockID(void)
{
        int port;
	int i=0; 

	
        for ( i = 0 ; i < MAX_SLOT ; i++ )
        {
		detectBlock[i] = readBlockID(i); 
#ifdef _DEBUG
		printf("detectBlock = %d\n", detectBlock[i]); 
#endif 
        }
}
void GetStdoutFromCommand(char* cmd, char* outstr) {

	FILE *stream; 
//	char buffer[256] = {0, }; 

	stream = popen(cmd, "r"); 

	while(fgets(outstr, sizeof(outstr)-1, stream) != NULL) {
		printf("%s\n", outstr); 
	}	
	pclose(stream); 
}

void main(int argc, char** argv)
{
	int i, j; 
	int slot[MAX_SLOT]; 

	FILE * fPblockInfo; 

	char deviceSerial[128] = {0, }; 

	if ( argc > 1 )
		memcpy(deviceSerial, argv[1], strlen(argv[1])); 

	printf("serial : %s\n", deviceSerial); 

	fPblockInfo = fopen("../blockInfo.json", "w"); 

        setupIO(); 	
#if 1 
	// insert btable to BlockTable Info 
	doreadfile("./BlockTable.json"); 

	checkBlockID(); 

	for ( i = 0 ; i < MAX_SLOT ; i++ )
	{
#ifdef _DEBUG		
		printf("%d:%d\n", i, detectBlock[i]); 
#endif 
		for ( j = 0 ; j < btable_num ; j++ )
		{
			slot[i] = -1; 
#ifdef _DEBUG
			printf("%d:%d\n", btable[j].level, detectBlock[i]); 
#endif 	
			if ( detectBlock[i] == btable[j].level )
			{ 		
				slot[i] = j; 
				break; 	
			}
		}
	}

#ifdef _DEBUG 

	for ( i = 0 ; i < MAX_SLOT ; i++) 
		printf("slot[%d] = %d\n", i, slot[i]); 
#endif 	 


	{
     		char* pResult = NULL; 
	
		cJSON *jsonResult; 
		cJSON *jsonBlockListArray; 
		cJSON *block; 

		jsonResult = cJSON_CreateObject(); 
		jsonBlockListArray = cJSON_CreateArray(); 

		cJSON_AddStringToObject(jsonResult, "osType", "ubuntu_3.4.39"); 
		cJSON_AddStringToObject(jsonResult, "serial", deviceSerial); 

#if 0 
/* s1 block */

    slot[0] = 1;
    slot[1] = -1;
    slot[2] = 4;
    slot[3] = 2;
    slot[4] = -1;
    slot[5] = 0;

#endif 

#if 0 
/* s2 block */ 
		slot[0] = 1; 
		slot[1] = -1; 
		slot[2] = 5; 
		slot[3] = 3; 
		slot[4] = -1; 
		slot[5] = 0;   
#endif


		for ( i = 0 ; i < MAX_SLOT ; i++ )
		{
			if ( slot[i] != -1 ) 
			{
				printf("%d\n", slot[i]); 
	
				block = cJSON_CreateObject(); 
				cJSON_AddNumberToObject(block, "channel", i); 
				cJSON_AddStringToObject(block, "sensorType", btable[slot[i]].sensorType);
                		cJSON_AddStringToObject(block, "blockID", btable[slot[i]].blockID);
				cJSON_AddItemToObject(block, "type", cJSON_Parse(btable[slot[i]].sType)); 

				cJSON_AddItemToArray(jsonBlockListArray, block);			}
		} 

 		cJSON_AddItemToObject(jsonResult, "blockList", jsonBlockListArray); 
		pResult = cJSON_Print(jsonResult); 

		printf("Result : %s\n", pResult); 
		fprintf(fPblockInfo, "%s\n", pResult); 

		cJSON_Delete(jsonResult); 
	}

#endif 
	fclose(fPblockInfo); 

//	powerOffBlockAll(); 
}




#ifndef _BLOCKCTL_H_

#define _BLOCKCTL_H_

#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include "digital.h" 

//#define USEDLIB
//#define _DEBUG 

#define MAX_SLOT 6

#define MAX_BIDIO 6 
#define MAX_RECORD 10 

#define BOARD_IO 	6 

#define BOARD_ID1 	7
#define BOARD_ID2	8
#define BOARD_ID3	9
#define BOARD_ID4	10
#define BOARD_ID5	11
#define BOARD_ID6	12
#define BOARD_ID7 	13

/* 3.3v */ 
#define BOARD_SD1	(32*3+28) // D28 or E2 
#define BOARD_SD2	(32*3+29) // D29 or E3 
#define BOARD_SD3	(32*3+30) // D30 or E4 
#define BOARD_SD4	(32*3+31) // D31 or C14 
#define BOARD_SD5	(32*4+0) // E0  or C23 
#define BOARD_SD6	(32*4+1) // E1 or B26 

/* 5.0v (module power) */ 
#define BOARD_MD1	(32*4+2)  // E2
#define BOARD_MD2	(32*4+3)  // E3 
#define BOARD_MD3	(32*4+4)  // E4
#define BOARD_MD4	(32*2+14) // C14
#define BOARD_MD5	(32*2+23) // C23
#define BOARD_MD6	(32*1+26) // B26


extern int slotPowerIO[MAX_SLOT]; //  = { BOARD_SD1, BOARD_SD2, BOARD_SD3, BOARD_SD4, BOARD_SD5, BOARD_SD6}; 

extern int blockIO[MAX_BIDIO]; //  = { BOARD_ID1, BOARD_ID2, BOARD_ID3, BOARD_ID4, BOARD_ID5, BOARD_ID6}; 

extern void powerOn(int); 
extern void powerOff(int); 
extern int setupIO(void); 

extern void powerOnBlock(int slotid);
extern void powerOffBlock(int slotid);

extern void powerOffModule(void); 

#endif 

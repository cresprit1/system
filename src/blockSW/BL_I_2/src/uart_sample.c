/*
Copyright (c) <2017>, written by jaehoon sim <jayhoon.s@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <time.h>

#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //Used for UART
#include <fcntl.h> //Used for UART

#include <termios.h> //Used for UART
#include <errno.h>
#include <ctype.h>

int uart0 = -1;
int t_flag; /* flag for thread creation */
int exit_flag;
int write_flag; 

pthread_t tid;
extern FILE *stdin;

FILE *outf; 

#define NUM_PRINT_BYTES  16
enum {
    STRING = 1,
    BINARY,
};

enum {
    FALSE,
    TRUE,
};

void fprint_bytes(FILE* fp, int type, int length, unsigned char *buffer)
{
	int i; 
	char temp[NUM_PRINT_BYTES] = {0, }; 
	
	for ( i = 0 ; i < length; i++ ) {
		//printf("%02x ", buffer[i]); 
		fprintf(fp, "%02x ", buffer[i]); 
		temp[i%NUM_PRINT_BYTES] = buffer[i]; 
		if (((i + 1) % NUM_PRINT_BYTES) == 0) {
			if (type == STRING) {
			//	printf("\t %s", temp); 
				fprintf(fp, "\t %s", temp); 
			}
			//printf("\n"); 	
			fprintf(fp, "\n"); 
			memset(temp, 0, NUM_PRINT_BYTES); 
		}
	}
	
	if (type == STRING) {
		if (i % NUM_PRINT_BYTES != 0) {
//			printf("\t%s", temp); 	
			fprintf(fp, "\t%s", temp); 
		}
	}
}

void print_bytes(int type, int length, unsigned char *buffer)
{
    int i;
    char temp[NUM_PRINT_BYTES] = {0,};

    for (i = 0; i < length; i++) {
        printf("%02x ", buffer[i]);
        temp[i%NUM_PRINT_BYTES] = buffer[i];
        if (((i + 1) % NUM_PRINT_BYTES) == 0) {
            if (type == STRING) {
                printf("\t%s", temp);
            }
            printf("\n");
            memset(temp, 0, NUM_PRINT_BYTES);
        }
    }
    if (type == STRING) {
        if (i % NUM_PRINT_BYTES != 0)
            printf("\t%s", temp);
    }
}

char buf[2048][6]; 
static int cnt = 0; 

//#define STEP_8MUX 8
#define STEP_6BYTE 6 

unsigned char rx_bin[256] = {0, }; 

void *serial_rx(void *arg)
{
    int rx_length = 0;

    t_flag = 1;

    do {
        // Read up to 255 characters from the port if they are there
	
	if ( exit_flag == FALSE) 
	{
        
        rx_length = read(uart0, rx_bin, 256);

        if (rx_length > 0) {
            //Bytes received
            rx_bin[rx_length] = '\0';
#if 0
           print_bytes(BINARY, rx_length, rx_bin);
#else 
	   write_flag = TRUE; 
	   fwrite(rx_bin, 1, rx_length, outf); 
	   write_flag = FALSE; 
#endif 
        } else {
            /* something? */
        }
	}
      usleep(200);
    } while (exit_flag == FALSE && write_flag == FALSE);

    pthread_exit(NULL); 
    return NULL;
}

int rx_thread_create(void)
{
    int ret = 0;
    pthread_attr_t attr;

    ret = pthread_attr_init(&attr);
    if (ret != 0) {
        perror("pthread_attr_init failed");
        return -1;
    }

    ret = pthread_create(&tid, &attr, &serial_rx, NULL);
    if (ret != 0) {
        perror("pthread_create failed");
        return -1;
    }

    ret = pthread_attr_destroy(&attr);
    if (ret != 0) {
        perror("pthread_attr_destroy failed");
        return -1;
    }

    return ret;
}

int tx_loop(void)
{
    unsigned char tx_hex[256] = {0,};
    unsigned char tx_bin[128] = {0,};
    int count = 0;

    do {
        fscanf(stdin, "%s", tx_hex);
        printf("TX BUFFER : #######\n");
        
        printf("  hexa string : ");
        print_bytes(STRING, strlen(tx_hex), tx_hex);
        printf("  binary : ");
        print_bytes(BINARY, strlen(tx_hex) / 2, tx_bin);

#if 1 
        /* Filestrean, bytes to write, number of bytes to write */
       // count = write(uart0, &tx_bin[0], strlen(tx_hex) / 2);
	count = write(uart0, "hello! world\n\r", strlen("hello! world\n\r")); 
        if (count < 0) {
            printf("UART TX error\n");
            close(uart0);
            return -1;
        }
#endif 
        usleep(10000);
    } while (strcmp(tx_hex, "FF") != 0);
    
    exit_flag = TRUE;
}

int main(void)
{
    struct termios options;
    
    int ret = 0;
    void *res;

    outf = fopen("./test_uart_sample.data", "wb"); 
    
    printf("Usage : type 'something', then see 'something'\n");
    printf("Usage : type 'FF' to exit\n\n");

    /* Open UART Device */

    uart0 = open("/dev/ttyAMA4", O_RDWR | O_NOCTTY | O_NDELAY);
    if (uart0 == -1) {
        printf("Error - Unable to open UART\n");
        close(uart0);
        return -1;
    }

    /* Set Config UART */
    tcgetattr(uart0, &options);
    options.c_cflag = B4800 | CS8 | CLOCAL | CREAD | IXOFF; //<Set baud rate
    options.c_iflag = IGNPAR | ICRNL;
    options.c_oflag = 0;
    options.c_lflag = 0;
    cfsetispeed(&options, B115200);
    cfsetospeed(&options, B115200);

    tcflush(uart0, TCIFLUSH);
    tcsetattr(uart0, TCSANOW, &options);

    printf(" rx_thread_create \n"); 
    
    /* RX */
    rx_thread_create();
    while (t_flag != 1) {
        usleep(10000);
    }

    /* TX */
    ret = tx_loop();
    if (ret < 0)
        return -1;

    pthread_join(tid, NULL); // (void**)&res);
    close(uart0);
    //fclose(outf); 
    return 0;
}


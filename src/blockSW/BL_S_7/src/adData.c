#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <asm-generic/mman-common.h>
#include <sys/mman.h>
#include "gpio.h"


/*  SNOW GPIO SETTING */
/*
#define GPIO_ADDR 0xC000A000 // base address
#define GPIO_BLOCK 64 // memory block size

#define AD1             43
#define AD2             41
#define AD3             44
#define AD4             42
*/

/*  DRAGON GPIO SETTING */
#define GPIO_ADDR 0xC001A000 // base address
#define GPIO_BLOCK 64 // memory block size

#define AD1             107
#define AD2             106
#define AD3             109
#define AD4             108


extern int adDataInit(void);
extern int adDataStart(void);
extern int adDataStop(void);

volatile unsigned long *gpioAddress;


int adDataInit(void)
{
    	int  m_mfd;
	unsigned char data[1024];
	int index;

	if ((m_mfd = open("/dev/mem", O_RDWR)) < 0)
	{
		printf("GPIO No Mem Device\n");
		return -1;
	}
	gpioAddress = (unsigned long*)mmap(NULL, GPIO_BLOCK, PROT_READ|PROT_WRITE, MAP_SHARED, m_mfd, GPIO_ADDR);
	close(m_mfd);

	if ((gpioAddress == NULL) || (gpioAddress <0))
	{
		printf("GPIO Map Failed\n");
		return -1;
	}
	//printf("GPIO Map OK\n");
	
if(access("/sys/class/gpio/gpio107",0)<0)
	{
		gpio_export(AD1);
		gpio_set_output(AD1, 1);	
	}
	if(access("/sys/class/gpio/gpio106",0)<0)
	{
		gpio_export(AD2);
		gpio_set_output(AD2, 1);	
	}
	if(access("/sys/class/gpio/gpio109",0)<0)
	{
		gpio_export(AD3);
		gpio_set_output(AD3, 1);	
	}
	if(access("/sys/class/gpio/gpio108",0)<0)
	{
		gpio_export(AD4);
		gpio_set_output(AD4, 1);	
	}
	return 0;
}

int adDataStop(void)
{
		*(gpioAddress+16) &= ~(0x3C00);
		return 0;
}

int adDataStart(void)
{
		*(gpioAddress+16) |= (0x3C00);
		return 0;
}

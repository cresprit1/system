var http = require("http");
var fs = require('fs');
var options = {
  "method": "POST",
  "hostname": "192.168.20.16",
  "port": "5000",
  "path": "/api/v1/insert/data",
  "headers": {
    "content-type": "text/plain",
  }
};


setInterval(function(){
  
  var req = http.request(options, function (res) {
    var chunks = [];
  
    res.on("data", function (chunk) {
      chunks.push(chunk);
    });
  
    res.on("end", function () {
      var body = Buffer.concat(chunks);
      console.log(body.toString());
    });
  });
  
  fs.readFile('./data.dat','utf8', function(error, data){
    console.log(JSON.stringify(data));
    req.write(data); 
    req.end();
  });
//req.write("{\n      \"data\": {\n                \"sensors\":      [{\n                                \"id\":   \"Accelerometer\",\n                                \"data_points\":  [{\n                                                \"v\":    4.606105,\n                                                \"t\":    \"2017-10-13T16:17:03.713\",\n                                                \"n\":    \"iot-A\"\n                                        }]\n                        }]\n        }\n}\n");
}, 1000); 

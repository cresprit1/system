/*
1. 보드에서 i2c 드라이버의 Major, minor 번호를 찾습니다.
    cat /proc/devices

2. nodefile 을 만듭니다.
   mknod /dev/i2c-0   c  Major  minor

3. 프로그램을 작성합니다.  
     fd = open( "/dev/i2c-0", O_RDWR );  // 만들어진 노드파일을 엽니다.

4. 접근할 디바이스의 슬레이브주소를 설정합니다.
     ioctl( fd, I2C_SLAVE, ADDR_MY_DEV );

5.  write 할때
     unsigned char buf[32];

     buf[0] =  0x10;   // 접근할 디바이스의 레지스터 주소
     buf[1] =  0xaa;   // 0x10 번지에 쓸 데이타
     buf[2] =  0xbb;   // 0x11 번지에 쓸 데이타

     rtn = write( fd, buf, 3 );    // reg_addr + data + data  버퍼의 유효개수는 3개이다.
     rtn 값으로 성공했는지 확인한다.

6.  read 할때
     unsigned char buf[32];

     buf[0] =  0x10;   // 접근할 디바이스의 레지스터 주소

     rtn = write( fd, buf, 1 );    // 접근할 레지스터의 주소를 설정한다.
     rtn 값으로 성공했는지 확인한다.

     rtn = read( fd, buf, 2 );    //  2개의 데이타를 읽어온다.
     rtn 값으로 성공했는지 확인한다.

*/

#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include "cJSON.h"

#define TEMPERATURE
#define HUMIDITY

static const char *filename = "./data.dat";
#define STR_DATA	    "data"
#define STR_ID			"id"

uint8_t htu21d_address = 0x40; //7 bit address of HTU21D

void delay_ms(int mseconds)
{
	clock_t start_time = clock(); 

	while(clock() < start_time + mseconds)
		; 
}

void HTU21D_Temperature1(int fd, float *r){

    uint8_t trigger_temperature_no_hold_master = 0xE3; //Address to temperature reading with no hold of twi lines
    uint8_t returned_over_I2C[3] = {0,0,0}; //Array to hold data
    uint8_t err_code; 

    returned_over_I2C[0] = trigger_temperature_no_hold_master; //접근할 디바이스의 레지스터 주소
    err_code = write( fd, returned_over_I2C, 1 ); // 접근할 레지스터의 주소를 설정한다.
    //printf("T_W:%d\n", err_code); 

	delay_ms(100); 
    //nrf_delay_ms(50);//Delay 50 ms, which is the maximum measurement time stated in datasheet
    
    err_code = read( fd, returned_over_I2C, 2 );
    //printf("T_R:%x, %x, %d\n", err_code, returned_over_I2C[0], returned_over_I2C[1]);     

	delay_ms(100); 
    // nrf_delay_ms(5);
    uint16_t x = (returned_over_I2C[0]<<8) | returned_over_I2C[1];
    //printf("%d\n", x); 
    float tempRH = x /(float) 65536.0; //2^16 = 65536
    //printf("%f\n", tempRH); 
    float rh = -46.85 + (175.72 * tempRH);
    //printf("%f\n", rh); 
    *r=rh;
}

//void HTU21D_Humidity(int fd, int *r)
void HTU21D_Humidity(int fd, float *r)
{
  uint8_t htu21d_address = 0x40; //7 bit address of HTU21D
  uint8_t trigger_humidity_no_hold_master = 0xE5; //Address to temperature reading with no hold of twi lines
  uint8_t returned_over_I2C[3] = {0,0,0}; //Array to hold data
  uint8_t err_code; 

  returned_over_I2C[0] = trigger_humidity_no_hold_master; //접근할 디바이스의 레지스터 주소
  
  err_code = write( fd, returned_over_I2C, 1 );// 접근할 레지스터의 주소를 설정한다.

  //printf("H_W:%d\n", err_code); 

  delay_ms(100); 
  //nrf_delay_ms(5);
  
  err_code = read( fd, returned_over_I2C, 2 );
  
//  printf("H_R:%x, %x, %d\n", err_code, returned_over_I2C[0], returned_over_I2C[1]);     
  delay_ms(100);
  
  //Calculate humidity
  uint16_t rawHumidity = ((unsigned int) returned_over_I2C[0] << 8) | (unsigned int) returned_over_I2C[1];

	rawHumidity &= 0xFFFC; 

  float tempRH = rawHumidity / (float)65536.0; //2^16 = 65536
  float rh = -6 + (125 * tempRH); //From page 14 in datasheet
  //*r = (int)(rh); // this could be change to (int)(rh*10), then one decimal of the humidity data will be kept (but 34.5  = 345 ) 
  *r=rh; 
    
}
#if 0 
int SendResultMessage(float temp, float hu)
{
	FILE* fp; 

	cJSON *jsonResult;
	cJSON *jsonArray;

	cJSON *sensor1;
	cJSON *sensor2;
	cJSON *sensor3;

	cJSON *data;
	cJSON *sensorArray;
	cJSON *dataPointArray1;
	cJSON *dataPointItem1;
	cJSON *dataPointArray2;
	cJSON *dataPointItem2;
	cJSON *dataPointArray3;
	cJSON *dataPointItem3;

 	char* pResult = NULL;
	struct timeval val;
	struct tm *ptm;
	float value = 0;
	
	int rc;
	int i=0;
	char timeBuf[128]={0,};
	char usec[7]={0,};
	char usec_use[4]={0,};
	
  fp = fopen(filename, "w"); 

	jsonResult =cJSON_CreateObject();
	jsonArray = cJSON_CreateArray();

	data = cJSON_CreateObject();
	sensorArray = cJSON_CreateArray();

	
	sensor1 = cJSON_CreateObject();
	sensor2 = cJSON_CreateObject();
	sensor3 = cJSON_CreateObject();

	dataPointArray1 = cJSON_CreateArray();
	dataPointItem1 =cJSON_CreateObject();

	dataPointArray2 = cJSON_CreateArray();
	dataPointItem2 =cJSON_CreateObject();

	dataPointArray3 = cJSON_CreateArray();
	dataPointItem3 =cJSON_CreateObject();

#ifdef TEMPERATURE 
/** Temperature **/

	cJSON_AddStringToObject(sensor1, STR_ID, "Temperature");
	cJSON_AddNumberToObject(dataPointItem1, "v", temp);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);
	

	cJSON_AddStringToObject(dataPointItem1, "t", timeBuf);
	cJSON_AddStringToObject(dataPointItem1, "n", "iot-T");
	cJSON_AddItemToArray(dataPointArray1, dataPointItem1);
	cJSON_AddItemToObject(sensor1, "data_points", dataPointArray1);
	cJSON_AddItemToArray(sensorArray, sensor1);
#endif 

#ifdef HUMIDITY 
/** HTU21D_Humidity **/

	cJSON_AddStringToObject(sensor2, STR_ID, "Humidity");
	
	cJSON_AddNumberToObject(dataPointItem2, "v", hu);

	memset(timeBuf, 0, sizeof(timeBuf));
	gettimeofday(&val, NULL);
	ptm = localtime(&val.tv_sec);

	sprintf(usec,"%03ld", val.tv_usec);
	strncpy(usec_use, usec, 3);	
	sprintf(timeBuf, "%04d-%02d-%02dT%02d:%02d:%02d.%s"
		, ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
		, ptm->tm_hour, ptm->tm_min, ptm->tm_sec
		, usec_use);

	cJSON_AddStringToObject(dataPointItem2, "t", timeBuf);
	cJSON_AddStringToObject(dataPointItem2, "n", "iot-H");
	cJSON_AddItemToArray(dataPointArray2, dataPointItem2);
	cJSON_AddItemToObject(sensor2, "data_points", dataPointArray2);
	cJSON_AddItemToArray(sensorArray, sensor2);

#endif 

	cJSON_AddItemToObject(data, "sensors", sensorArray);
	cJSON_AddItemToObject(jsonResult, "data", data);	 
	
	pResult = cJSON_Print(jsonResult);
	
	printf("Result : %s\n", pResult);
  fprintf(fp, "%s\n", pResult); 
  
	cJSON_Delete(jsonResult);
	
  fclose(fp); 

	return rc;
}
#endif 


void readHTU21D(void)
{
  int fd; 
  float s;
  float hu;
  int result; 

  struct timeval val; 
  struct tm *ptm; 

  uint8_t returned_over_I2C[3] = {0,0,0}; //Array to hold data
  uint8_t err_code; 

  fd = open("/dev/i2c-0", O_RDWR); 
 
  if ( fd == -1 )
  {
	printf(" Device Open Error\n"); 
 	exit(0); 
  } 

  ioctl(fd, I2C_SLAVE, htu21d_address); 
  
  returned_over_I2C[0] = 0xFE; 
  err_code = write( fd, returned_over_I2C, 1 );
  usleep(500); 
  
//  while(1)
  {
    HTU21D_Temperature1(fd, &s); 

    usleep(100); 
    HTU21D_Humidity(fd, &hu); 
    usleep(100);  

	if ( hu > 50.0 )
		result = 1; 
	else 
		result = 0; 

/* 	gettimeofday(&val, NULL); 

	ptm = localtime(&val.tv_sec); 

	printf("%04d-%02d-%02dT%02d:%02d:%02d.%03ld\n"
        , ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday
        , ptm->tm_hour, ptm->tm_min, ptm->tm_sec
        , val.tv_usec);  
   
    	printf("TEMP = %f, HU = %f\n", s, hu); 
*/
	printf("  - humidity : %2.1f, temparature : %2.1f\n", hu, s); 
  
    	//SendResultMessage(s, hu); 
    	//	usleep(1000*1000); 
//	sleep(5); 
   }

	close(fd); 
}




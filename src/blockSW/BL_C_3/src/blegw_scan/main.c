#include <stdio.h> /* printf, sprintf */
#include <stdlib.h> /* exit, atoi, malloc, free */
#include <unistd.h> /* read, write, close */
#include <string.h> /* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> /* struct hostent, gethostbyname */

#include "ble2basic.h" 

int main(int argc, char *argv[])
{
	pthread_mutex_init(&receiveMutex, NULL);

  pthread_create(&ble_pthread, NULL,  ble_thread, 0);

  pthread_join(ble_pthread, NULL); 

}  


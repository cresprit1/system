#include <stdio.h> 
#include <stdbool.h>

#define HIGH 1
#define LOW 0
#define INPUT 1
#define OUTPUT 0

#define LED203_RP00 168
#define LED202_RP01 169
#define LED201_RP02 170
#define LED200_RP03 171 

#define LED207_GP04 172
#define LED206_GP05 173
#define LED205_GP06 174
#define LED204_GP07 175

#define LED211_BP08 176
#define LED210_BP09 177
#define LED209_BP10 178
#define LED208_BP11 179

#define SW200_BACK	180
#define SW201_HOME	181
#define SW202_UP	182
#define SW203_LEFT	183
#define SW204_RIGHT	184
#define SW205_DOWN	185


#define SW200_BACK_MASK 0x01
#define SW201_HOME_MASK 0x02
#define SW202_UP_MASK 0x04
#define SW203_LEFT_MASK 0x08
#define SW204_RIGHT_MASK 0x10
#define SW205_DOWN_MASK 0x20

#define SW206_1	186
#define SW206_2 187
#define SW206_3 188
#define SW206_4 189
#define SW206_5 190
#define SW206_6 191

//#define R20

bool digitalPinMode(int pin, int dir); 
int digitalRead(int pin); 
bool digitalWrite(int pin, int val); 


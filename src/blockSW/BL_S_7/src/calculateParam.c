#include "calculateParam.h"











/**********************************************************************************************
*						High Fequency Detection
*						High Frequency
*						Sum From F1(Hz) to F2(Hz) and then
*						Pow2
***********************************************************************************************/
double getParam_ewr(float _f1, float _f2,int _sampleCount ,fftw_complex *_out, float* _fftBuf)
{
	int i;
	double ewr=0;
	float frequency = 0.0;
	float value = 0.0;
	
	frequency = (float)51200/_sampleCount;

	for(i = 1; i<= _sampleCount; i++)
	//for(i = _sampleCount; i != 0; i--)
	{
		value = (double)(sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))* sqrt(pow(_out[i-1][0],2) + pow(_out[i-1][1],2))/(2*3.14*i*frequency));
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			ewr += value;
		}
		_fftBuf[i] = value;
	}
	return ewr;
}





/**********************************************************************************************
*						Synchronous Peak
*						14x Peak
*						
***********************************************************************************************/

SPKData getParam_spk14H(float _tacho, int _sampleCount, fftw_complex *_out, int _index)
{
	int i;
	double spk;
	float frequency = 0.0;
	SPKData spkData;
	
	
	frequency = (float)51200/_sampleCount;
#ifdef FEATURE_PARAM_DEBUG	
	printf("tacho : %f\n", _tacho);
#endif
	i = (int)_tacho*14;
#ifdef FEATURE_PARAM_DEBUG	
	printf("i======> %d  fft:%lf  2*3.14*i====> %lf\n", i,(double)sqrt(pow(_out[i][0],2) + pow(_out[i][1],2)),  (double)(2*3.14*i));
#endif
	spkData.y_value = (double)sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))/(2*3.14*i);
#ifdef FEATURE_PARAM_DEBUG	
	printf("SPK14H item value : %lf\n", (double)(sqrt(pow(_out[i][0],2) + pow(_out[i][1],2))/(2*3.14*i*frequency)));
#endif



	
	spkData.x_value = i*(_index+1);
	
	return spkData;
}





/**********************************************************************************************
*						Synchronous Peak
*						28x Peak
*					
***********************************************************************************************/

SPKData getParam_spk28H(float _tacho, int _sampleCount, fftw_complex *_out, int _index)
{
	int i;
	double spk;
	float frequency = 0.0;
	SPKData spkData;
	
	frequency = (float)51200/_sampleCount;
	
	i = _tacho*28/frequency;
	spkData.y_value = sqrt(pow(_out[i][0],2) + pow(_out[i][1],2)/(2*3.14*i*frequency));
#ifdef FEATURE_PARAM_DEBUG	
	printf("SPK28H item value : %lf\n", sqrt(pow(_out[i][0],2) + pow(_out[i][1],2)/(2*3.14*i*frequency)));	
#endif
	spkData.x_value = i*(_index+1);
	
	return spkData;
}


double getParam_spk(float _tacho, int _sampleCount, fftw_complex *out, float* _fftBuf)
{
	int i;
	double spk;
	float frequency = 0.0;
	
	frequency = (float)51200/_sampleCount;
	i = _tacho/frequency;
	spk = sqrt(pow(out[i][0],2) + pow(out[i][1],2))/(2*3.14*i*frequency);

	return spk;
}



#if 0
/***********************************************************************
High Fequency Detection
High Frequency
Sum From 5000Hz to 20000Hz  
Pow2
************************************************************************/
double fnt_ewr_total_hfd(float _f1, float _f2, fftw_complex *out)
{
	int i;
	double ewr=0;
	total_energy = 0;
	
//	for(i = 1; i<= N/2; i++)
	for(i = N/2; i != 0; i--)
	{
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}

		if(i*frequency>= 5 && i*frequency <= 2000)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			total_energy +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
		}

		if(i*frequency>= 5000 && i*frequency <= 20000)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			hfd +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
		}
		i--;
		
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}

		if(i*frequency>= 5 && i*frequency <= 2000)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			total_energy +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
		}

		if(i*frequency>= 5000 && i*frequency <= 20000)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			hfd +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
		}		
		i--;
				if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}

		if(i*frequency>= 5 && i*frequency <= 2000)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			total_energy +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
		}

		if(i*frequency>= 5000 && i*frequency <= 20000)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			hfd +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
		}
		i--;
				if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}

		if(i*frequency>= 5 && i*frequency <= 2000)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			total_energy +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
		}

		if(i*frequency>= 5000 && i*frequency <= 20000)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			hfd +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
		}
	}

	return ewr;
}



/***********************************************************************
Energy in Frequency Range
Rubs
Pow from Frequency F1 to Frequency to F2
Sum
************************************************************************/
double fnt_ewr_rubs(float _f1, float _f2, fftw_complex *out)
{
	int i;
	double ewr=0;
	
	for(i = N/2; i != 0; i--)

	{
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}
		i--;
		
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}
		i--;
		
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}
		i--;
		
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}
		
		if(i*frequency > _f2)
			break;

	}

	return ewr;
}

double fnt_ewr_unbalance(float _f1, float _f2, fftw_complex *out)
{
	int i;
	double ewr=0;
	
	for(i = N/2; i != 0; i--)

	{
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}

		i--;		
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}
		i--;		

	if(i*frequency>= _f1 && i*frequency <= _f2)
	{
		//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
		ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
		
	}
	i--;		

	if(i*frequency>= _f1 && i*frequency <= _f2)
	{
		//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
		ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
		
	}
	if(i*frequency > _f2)
		break;

	}

	return ewr;
}


double fnt_ewr_line_freq(float _f1, float _f2, fftw_complex *out)
{
	int i;
	double ewr=0;
	
	for(i = N/2; i != 0; i--)

	{
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}
		
		if(i*frequency > _f2)
			break;
		
	}

	return ewr;
}

double fnt_ewr_2xline_freq(float _f1, float _f2, fftw_complex *out)
{
	int i;
	double ewr=0;
	
	for(i = N/2; i != 0; i--)

	{
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}

		if(i*frequency > _f2)
			break;
		
	}

	return ewr;
}


double fnt_ewr_measuring_freq(float _f1, float _f2, fftw_complex *out)
{
	int i;
	double ewr=0;
	
	for(i = N/2; i != 0; i--)

	{
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}
		if(i*frequency > _f2)
			break;
	}

	return ewr;
}

double fnt_ewr_rotorBarPass_freq(float _f1, float _f2, fftw_complex *out)
{
	int i;
	double ewr=0;
	
	for(i = N/2; i != 0; i--)
	{
		if(i*frequency>= _f1 && i*frequency <= _f2)
		{
			//printf("%lf \n", i, sqrt(pow(out[i][0],2) + pow(out[i][1],2))* sqrt(pow(out[i][0],2) + pow(out[i][1],2)));
			ewr +=(sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))* sqrt(pow(out[i-1][0],2) + pow(out[i-1][1],2))/(2*3.14*i*frequency));
			
		}
		
		if(i*frequency > _f2)
			break;
	}

	return ewr;
}


double fnt_spk(float _tacho, fftw_complex *out)
{
	int i;
	double spk;
	
	i = _tacho/frequency;
	spk = sqrt(pow(out[i][0],2) + pow(out[i][1],2))/(2*3.14*i*frequency);

	return spk;
}

double fnt_spk2x(float _tacho, fftw_complex *out)
{
	int i;
	double spk;
	
	i = _tacho*2/frequency;
	spk = sqrt(pow(out[i][0],2) + pow(out[i][1],2)/(2*3.14*i*frequency));

	return spk;
}

double fnt_sph(float _tacho, fftw_complex *out)
{
	double sph;
	int i;

	i = _tacho/frequency;
	if(out[i][0] > 0 && out[i][1] >= 0)
		sph = atan(out[i][1]/out[i][0]);
	else if(out[i][0] > 0 && out[i][1] < 0)
		sph = atan(out[i][1]/out[i][0])+(3.14*2);		
	else if(out[i][0] < 0)
		sph = atan(out[i][1]/out[i][0])+3.14;	
	else if(out[i][0] == 0 && out[i][1] > 0)
		sph = 3.14/2;
	else if(out[i][0] == 0 && out[i][1] < 0)
		sph = 3*3.14/2;
	else
		sph = -1;
	return sph;
}

double fnt_sph2x(float _tacho, fftw_complex *out)
{
	double sph;
	int i;

	i = _tacho*2/frequency;
	if(out[i][0] > 0 && out[i][1] >= 0)
		sph = atan(out[i][1]/out[i][0]);
	else if(out[i][0] > 0 && out[i][1] < 0)
		sph = atan(out[i][1]/out[i][0])+(3.14*2);		
	else if(out[i][0] < 0)
		sph = atan(out[i][1]/out[i][0])+3.14;	
	else if(out[i][0] == 0 && out[i][1] > 0)
		sph = 3.14/2;
	else if(out[i][0] == 0 && out[i][1] < 0)
		sph = 3*3.14/2;
	else
		sph = -1;
	return sph;
}


double fnt_rsh_multi(float _inputFrequency, fftw_complex *out)
{
	int i;

	i = _inputFrequency/frequency;
	rsh1x= sqrt(pow(out[i][0],2) + pow(out[i][1],2))/(2*3.14*i*frequency);
	
	i = _inputFrequency*2/frequency;
	rsh2x= sqrt(pow(out[i][0],2) + pow(out[i][1],2))/(2*3.14*i*frequency);

	i = _inputFrequency*3/frequency;
	rsh3x= sqrt(pow(out[i][0],2) + pow(out[i][1],2))/(2*3.14*i*frequency);	
	
}

double fnt_ser(float _f1, float _f2, float _tacho, fftw_complex *out)
{
	int f1_index;
	int f2_index;
	int tacho_index;
	int i = 0;
	
	double ser=0;

	f1_index = _f1/frequency;
	f2_index = _f2/frequency;
	tacho_index = _tacho/frequency;
	
	for(i = f1_index; i<= f2_index; i++)
	{
		if(i%tacho_index == 0)
		{
			ser +=pow(sqrt(pow(out[i][0],2) + pow(out[i][1],2))/(2*3.14*i*frequency), 2);
		}
	}

	return ser;
}

double fnt_ser_sync(float _f1, float _f2, float _tacho, fftw_complex *out)
{
	int f1_index;
	int f2_index;
	int tacho_index;
	int i = 0;
	
	double ser=0;

	f1_index = _f1/frequency;
	f2_index = _f2/frequency;
	tacho_index = _tacho/frequency;
	
	for(i = f1_index; i<= f2_index; i++)
	{
		if(i%tacho_index == 0)
		{
			ser +=pow(sqrt(pow(out[i][0],2) + pow(out[i][1],2))/(2*3.14*i*frequency), 2);
		}
	}

	return ser;
}

double fnt_nser(float _f1, float _f2, float _tacho, fftw_complex *out)
{
	int f1_index;
	int f2_index;
	int tacho_index;
	int i = 0;
	
	double ser=0;

	f1_index = _f1/frequency;
	f2_index = _f2/frequency;
	tacho_index = _tacho/frequency;
	
	for(i = f1_index; i<= f2_index; i++)
	{
		if(i%tacho_index != 0)
		{
			ser +=pow(sqrt(pow(out[i][0],2) + pow(out[i][1],2))/(2*3.14*i*frequency), 2);
		}
	}

	return ser;
}


double fnt_rsh(float _bps, int _scale, fftw_complex *out)
{
	int bps_index;
	int i = 0;
	
	double rsh=0;

	bps_index = _bps/frequency;
	
	for(i = bps_index; i<= bps_index*_scale; i++)
	{
		if(i%bps_index == 0)
		{
			rsh +=sqrt(pow(out[i][0],2) + pow(out[i][1],2))/(2*3.14*i*frequency);
		}
	}

	return rsh;
}



double fnc_mean(double sum, int num){
    return (double)(sum/num);
}

float fnc_var(double mean, int num, int data[]){
    float sumvar=0, var;
    int i;
    for(i=num;i>0;i--){
        sumvar+=(data[i-1]-mean)*(data[i-1]-mean);
    }
    var = sumvar/(num);

    return var;
}

double fnc_std(double var){
    return sqrt(var);
}


double fnc_kurt(double stdev, int num, int data[])
{
	int i;
	double kurt=0;
	double sum4sqrt= 0;
	
	for(i=num; i>0; i--)
	{
		sum4sqrt+=pow(data[i-1],4);
	}
	
	kurt= (sum4sqrt/num)/(pow(stdev, 4));
	
	//printf("(sum4sqrt/num): %lf,   pow(stdev, 4): %lf,   kurt: %lf\n",(sum4sqrt/num), pow(stdev, 4), kurt);
	return kurt;
}

double fnc_skew(double stdev, int num, int data[])
{
	int i;
	double skew=0;
	double sum3sqrt= 0;
	
	for(i=num; i>0; i--)
	{
		sum3sqrt+=pow(data[i-1], 3);
	}
	
	skew= (sum3sqrt/num)/pow(stdev,3);
	
	//printf("(sum3sqrt/num): %lf,    pow(stdev, 3): %lf,   skew: %lf\n",(sum3sqrt/num), pow(stdev, 3), skew);
	return skew;
}

double fnc_cf(int num, int data[]){
    double pow2sum=0, cf;
    int i;
    for(i=num;i>0;i--){
        pow2sum += pow(data[i-1], 2);
    }
    cf = sqrt(pow2sum/(num));

    return cf;
}


#endif




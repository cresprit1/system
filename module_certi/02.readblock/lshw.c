#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <string.h>
#include <stdlib.h>

char *strjoin(char *s1, char *s2, int n)
{
    int i = strlen(s2);
    int j = 0;
    if ((s2 = realloc(s2, (i + n + 1))) == NULL)
            perror(0);
    while (j < n && s1[j])
    {
            s2[i] = s1[j];
            i++;
            j++;
    }
    s2[i] = 0;
    return (s2);
}

int main()
{
    pid_t father;
    char buf[500] = {0};
    char *str;
    char *argv[13] = {"/usr/bin/lshw", "-class", "network", "-businfo", 
			"|", "grep ", "-i", "wlan","|", 
			"grep", "-i", "1.3"}; 

//    char *argv[5] = {"/usr/bin/lshw", "-short", "-c", "memory"};
    int fd[2];
    int ret;

    if (pipe(fd) == -1)
    {
            perror(NULL);
            return -1;
    }
    father = fork();
    if (father == 0)
    {
            close(fd[1]);
            while ((ret = read(fd[0], buf, 500)))
            {
                    str = strjoin(buf, str, ret);
            }
            close(fd[0]);
    }
    else
    {
            close(fd[0]);
            execv(argv[0], argv);
            close(fd[1]);
            wait(0);
    }
    wait(0);
    printf("%s", str);
    return 0;
}

#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include "cJSON.h"
#include "digital.h" 
#include "blockctl.h"

#define _DEBUG 

static const char *filename = "./blockInfo.json";

#define SHARED_MEMORY_KEY 1005
#define MEMORY_SIZE 200

#define READ_CLIENT_FLAG 0
#define READ_SERVER_FLAG 1
#define PRINT_CLIENT_FLAG 2

struct record_blockInfo {

	char index[10]; 
	int ch; 
        char sensorType[20];
        char blockID[10];
	int exec; 
	int power; 
	char interface[20]; 
};

char deviceSerial[128] = {0, }; 

char serviceName[128] = {0, }; 

int detectBlock[MAX_SLOT] = {-1, }; 

struct record_blockInfo btable[MAX_RECORD]; 

int btable_num = -1; 
int is_comm = 0; 

int doit(char *text)
{
	int i, j = 0, index; 

	char *out; 
	cJSON *json, *subjson, *item, *value; 

	json = cJSON_Parse(text); 

	value = cJSON_GetObjectItem(json, "serial"); 
	out = cJSON_Print(value); 
        memset(deviceSerial, 0, 128);
        memcpy(deviceSerial, &out[1], strlen(out)-2);

	free(out); 

        value = cJSON_GetObjectItem(json, "serviceName");
        out = cJSON_Print(value);
        memset(serviceName, 0, 128);
        memcpy(serviceName, &out[1], strlen(out)-5);

        free(out);

        subjson = cJSON_GetObjectItem(json, "blocks"); 

	btable_num = cJSON_GetArraySize(subjson); 

	for ( j = 0 ; j < btable_num ; j++) 
	{
		item = cJSON_GetArrayItem(subjson, j); 
		out = cJSON_Print(item); 
#ifdef _DEBUG 
		printf("%s\n", out); 
#endif 
		free(out); 

		value = cJSON_GetObjectItem(item, "b_type"); 
		out = cJSON_Print(value); 
		memcpy(btable[j].index, &out[1], strlen(out)-2);

#ifdef _DEBUG
		printf("b_type = %s\n", out); 
#endif 	
		//btable[j].index = atoi(out); 

		if( strncmp(btable[j].index, "BL_C_1", 6) == 0 ) // wifi block
			is_comm = 1;
		else if ( strncmp(btable[j].index, "BL_D_1", 6) == 0)
			is_comm = 3; 
#ifdef _DEBUG
		printf("index = %s\n", out); 
#endif 
		free(out); 

		value = cJSON_GetObjectItem(item, "ch"); 	
		out = cJSON_Print(value); 
		btable[j].ch = atoi(out); 
#ifdef _DEBUG
		printf("channel = %s\n", out); 
#endif 
		free(out); 

		value = cJSON_GetObjectItem(item, "mode"); 

		if ( value ) 
		{
			out = cJSON_Print(value); 
			btable[j].exec = atoi(out); 

			free(out); 
		}	

		value = cJSON_GetObjectItem(item, "power"); 
		if (value)
		{ 
			out = cJSON_Print(value); 
			btable[j].power = atoi(out); 
		
			free(out); 
		}
#if 0 
		value = cJSON_GetObjectItem(item, "interface");
		if (value) 
		{
                	out = cJSON_Print(value);
                	memset(btable[j].interface, 0, 20);
                	memcpy(btable[j].interface, &out[1], strlen(out)-2);
#ifdef _DEBUG
			printf("interface = %s\n", out);
#endif 
			free(out);  
		}
#endif 
		value = cJSON_GetObjectItem(item, "sensorType");  
		if (value) 
		{
			out = cJSON_Print(value); 
			memset(btable[j].sensorType, 0, 20); 
			memcpy(btable[j].sensorType, &out[1], strlen(out)-2); 
#ifdef _DEBUG
			printf("sensorType = %s\n", out); 
#endif 
			free(out); 
		}

		value = cJSON_GetObjectItem(item, "b_type"); 
		if (value)
		{
			out = cJSON_Print(value);
			memset(btable[j].blockID, 0, 10); 
			memcpy(btable[j].blockID, &out[1], strlen(out)-2);  
#ifdef _DEBUG
			printf("blockID = %s\n", out); 
#endif 
			free(out); 
		}
	}

#ifdef _DEBUG
	for ( i = 0 ; i < 6 ; i++) 
	{
		printf("%s, %d %s, %s\n", btable[i].index, btable[i].ch, btable[i].sensorType, btable[i].blockID); 
	}
#endif 
	
	cJSON_Delete(json); 
	return btable_num; 
}

int doreadfile(char *filename)
{
	int ret; 

	FILE *f=fopen(filename,"rb"); 
 
	fseek(f, 0, SEEK_END); 

	long len=ftell(f); 
	fseek(f, 0, SEEK_SET); 

	char *data = malloc(len+1); 
	fread(data, 1, len, f); 
	fclose(f); 
 
 	ret = doit(data); 

	free(data); 
 
	return ret; 
}

#if 0 
void checkBlockID(void)
{
        int port;
	int i=0; 

	
        for ( i = 0 ; i < MAX_SLOT ; i++ )
        {
		detectBlock[i] = readBlockID(i); 
#ifdef _DEBUG
		printf("detectBlock = %d\n", detectBlock[i]); 
#endif 
        }
}
#endif 

void execute(char **argv)
{
	pid_t pid; 
	int status; 

	if((pid = fork()) <0) { /* fork a child process */
		printf("*** ERROR : forking child process failed\n"); 
		exit(1); 
	}
	else if (pid == 0) {
		if (execvp(*argv, argv) < 0) { /* execute the command */
			printf("*** ERROR : exec failed\n"); 
			exit(1); 
		}
	}
	else {				      /* for the parent: */ 
		while( wait(&status) != pid)  /* wait for completion */ 
			; 
	}
}

int createshm(int key)
{
	int shmid; 
	char *buffer; 
	char *string; 

	shmid = shmget((key_t)(SHARED_MEMORY_KEY+1), (size_t)MEMORY_SIZE, 0777 | IPC_CREAT); 

	if ( shmid == -1 )
	{
		perror("shmat failed:"); 
		exit(0); 
	}

	buffer = (char*) shmat(shmid, NULL, 0); 
	if (buffer == (char*)-1) {
		perror("shmat failed:"); 
		exit(0); 
	}

	string = buffer + 1; 

	buffer[0] = READ_CLIENT_FLAG; 

	return shmid;	
}

int gSHMid; 

void main(void)
{
	int i, j, bcnt, index = -1; 
	char buf[256] = { 0, }; 
 
	setupIO(); 
 
	bcnt = doreadfile("./blockInfo.json"); 

#ifdef DATA_LOOP
 	while(1)
#endif 

{
	if (is_comm == 3)
        {
		for ( i = 0 ; i < bcnt ; i++ )
		{
			printf("%d:%s\n", i, btable[i].index); 

  			if ( strncmp(btable[i].index, "BL_D_1", 6) == 0)
			{
			
		         sprintf(buf, "./blockSW/%s/app.sh %d %d %s /blockSW/%s %s",
                                btable[i].blockID,
                                (bcnt-1), // display shared memory num (bcnt-1) is_comm,
                                btable[i].ch,
                                deviceSerial,
                                btable[i].blockID,
				serviceName);

			printf("%s\n", buf); 

        system(buf);
        usleep(100*1000);

				break; 
			}
		}
        }

	for( i = 0 ; i < bcnt ; i++ )
	{
		gSHMid = createshm(i); 
		index = btable[i].ch; 

		if ( index == -1 ) 
			continue; 

		if (!btable[i].exec)
			continue; 

		printf("\n == CH : %d (%s-%s)(%d)\n", btable[i].ch, btable[i].blockID, btable[i].sensorType, index); 

#ifdef MODULEPOWERSINGLE
                powerOnModule(index);
//                powerOnBlock(index);
#endif 
#ifdef _DEBUG
	 	printf("%s, %s, %s\n", btable[i].index, btable[i].sensorType, btable[i].blockID);
#endif 
		memset(buf, 0, 256); 

		if ( btable[i].exec == 1 ) 
		sprintf(buf, "./blockSW/%s/app.sh %d %d %s /blockSW/%s", 
				btable[i].blockID, 
				is_comm, 
				btable[i].ch, 
				deviceSerial, 
				btable[i].blockID);  
		else if ( btable[i].exec == 2 )
		{
			/* relay */ 
			int status = 1; 

		sprintf(buf, "./blockSW/%s/app.sh %d %d %s /blockSW/%s %d", 
				btable[i].blockID, 
				is_comm, 
				btable[i].ch, 
				deviceSerial, 
				btable[i].blockID, 
				status); 
		}
		else 
		{
#if 0 
			char msgbuf[1024] = {0, }; 

			sprintf(buf, "./blockSW/%s/app.sh %d %d %s /blockSW/%s %s",				btable[i].blockID, 
				is_comm, 
				btable[i].ch, 
				deviceSerial, 
				btable[i].blockID, 
				msgbuf);  
#endif 
		}

		printf("%s\n", buf); 
		j = system(buf);  	

#ifdef MODULEPOWERSINGLE
		if ( !btable[i].power ) 
		{
			printf("power %d\n", btable[i].power); 
                	powerOffModule(index);
	 //               powerOffBlock(index);
		}
#endif 
	}
}
//	while(1); 
}


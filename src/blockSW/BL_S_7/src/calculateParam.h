#include <fftw3.h>
#include <math.h>





extern double getParam_velocity_overall(int _sampleCount ,fftw_complex *_out);
extern double getParam_acceleration_overall(int _sampleCount ,fftw_complex *_out);
extern double getParam_acceleration_peak(int _sampleCount ,fftw_complex *_out);
extern double getParam_crest_factor(int _sampleCount ,fftw_complex *_out);
extern double getParam_1x(int _tacho, int _sampleCount, fftw_complex *_out);
extern double getParam_2x(int _tacho, int _sampleCount, fftw_complex *_out);
extern double getParam_120Hz(int _sampleCount, fftw_complex *_out);
extern double getParam_From3xTo5x(int _tacho, int _sampleCount, fftw_complex *_out);
extern double getParam_1xVPF(int _tacho, int _vain, int _sampleCount, fftw_complex *_out);
extern double getParam_2xVPF(int _tacho, int _vain, int _sampleCount, fftw_complex *_out);
extern double getParam_partial_overall(int _f1, int _f2, int _sampleCount, fftw_complex *out);
extern double getParam_ewr(float _f1, float _f2,int _sampleCount ,fftw_complex *_out, float* _fftBuf);
extern SPKData getParam_spk14H(float _tacho, int _sampleCount, fftw_complex *_out, int _index);
extern SPKData getParam_spk28H(float _tacho, int _sampleCount, fftw_complex *_out, int _index);
extern double getParam_spk(float _tacho, int _sampleCount, fftw_complex *out, float* _fftBuf);


double fnt_ewr_total_hfd(float _f1, float _f2, fftw_complex *out);

double fnt_ewr_rubs(float _f1, float _f2, fftw_complex *out);

double fnt_ewr_unbalance(float _f1, float _f2, fftw_complex *out);

double fnt_ewr_line_freq(float _f1, float _f2, fftw_complex *out);

double fnt_ewr_2xline_freq(float _f1, float _f2, fftw_complex *out);

double fnt_ewr_measuring_freq(float _f1, float _f2, fftw_complex *out);

double fnt_ewr_rotorBarPass_freq(float _f1, float _f2, fftw_complex *out);

double fnt_spk(float _tacho, fftw_complex *out);

double fnt_spk2x(float _tacho, fftw_complex *out);

double fnt_sph(float _tacho, fftw_complex *out);

double fnt_sph2x(float _tacho, fftw_complex *out);

double fnt_rsh_multi(float _inputFrequency, fftw_complex *out);

double fnt_ser(float _f1, float _f2, float _tacho, fftw_complex *out);

double fnt_ser_sync(float _f1, float _f2, float _tacho, fftw_complex *out);

double fnt_nser(float _f1, float _f2, float _tacho, fftw_complex *out);

double fnt_rsh(float _bps, int _scale, fftw_complex *out);

double fnc_mean(double sum, int num);

float fnc_var(double mean, int num, int data[]);

double fnc_std(double var);

double fnc_kurt(double stdev, int num, int data[]);

double fnc_skew(double stdev, int num, int data[]);

double fnc_cf(int num, int data[]);



